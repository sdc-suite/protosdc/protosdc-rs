pub mod xml {
    pub mod messages {
        pub mod addressing;
        pub mod common;
        pub mod discovery;
        pub(crate) mod macros;
        pub mod soap {
            pub mod envelope;
            pub mod fault;
        }
        pub mod ws_metadata_exchange {
            pub mod get_metadata;
            pub mod metadata;
        }
        pub mod dpws {
            pub mod common;
            pub mod host;
            pub mod relationship;
            pub mod this;
        }
        pub mod ws_eventing {
            pub mod common;
            pub mod renew;
            pub mod subscribe;
            pub mod subscribe_response;
            pub mod subscription_end;
        }
        pub mod biceps_ext;
        pub mod duration;
    }
    pub mod common;
}

pub mod discovery {
    pub mod actions;
    pub mod common;
    pub mod constants;
    pub(crate) mod util;
    pub mod consumer {
        pub mod discovery_consumer;

        pub mod discovery_proxy_consumer;
    }
    pub mod provider {
        pub mod discovery_provider;

        pub mod discovery_proxy_provider;
    }
}

pub mod http {
    pub mod server {
        pub mod acceptor;
        pub mod axum;
        pub mod axum_server {
            pub mod handle;
            pub mod notify_once;
            pub mod server;
        }
        pub mod common;
    }

    pub mod client {
        pub mod common;
        pub mod reqwest;
    }
}

pub mod soap {
    pub mod common;
    pub mod eventing;
    pub mod soap_client;
    pub mod soap_server;
    pub mod dpws {
        pub mod client {
            pub mod consumer;
            pub mod event_sink;
            pub mod hosted_service;
            pub mod hosting_service;
            pub mod notification_sink;
        }
        pub mod device {
            pub mod dpws_device;
            pub mod hosted_service;
            pub mod hosting_service;
        }
        pub mod subscription_manager;
        pub mod ws_eventing {
            pub mod sink;
            pub mod source;
        }
    }
}

pub mod consumer {
    pub mod sco;
    pub mod sdc_consumer;
    pub mod sdc_remote_device_connector;
    pub mod services;
    pub mod watchdog;
}

pub mod provider {
    pub mod high_priority_services;
    pub mod low_priority_services;
    pub mod sdc_provider;
    pub mod sdc_provider_plugin;
}

pub mod common {
    pub mod extension_handling;
    pub mod local_address_resolver;
}
