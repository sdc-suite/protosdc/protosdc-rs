use crate::soap::common::SoapError;
use crate::soap::dpws::client::hosted_service::HostedService;
use crate::soap::dpws::client::hosting_service::HostingService;
use crate::soap::soap_client::SoapClient;
use crate::xml::common::SoapHeaderBuilder;
use crate::xml::messages::biceps_ext::{
    BICEPS_ACTION_GET_CONTEXT_STATES, BICEPS_ACTION_GET_LOCALIZED_TEXT, BICEPS_ACTION_GET_MDIB,
    BICEPS_ACTION_GET_MD_DESCRIPTION, BICEPS_ACTION_GET_MD_STATE,
    BICEPS_ACTION_GET_SUPPORTED_LANGUAGES, BICEPS_PORT_TYPE_CONTEXT, BICEPS_PORT_TYPE_GET,
    BICEPS_PORT_TYPE_LOCALIZATION,
};
use crate::xml::messages::common::ConstQualifiedName;
use protosdc::provider::localization_storage::TextWidth;
use protosdc_biceps::biceps::{
    AbstractGet, GetContextStates, GetContextStatesResponse, GetLocalizedText,
    GetLocalizedTextResponse, GetMdDescription, GetMdDescriptionResponse, GetMdState,
    GetMdStateResponse, GetMdib, GetMdibResponse, GetSupportedLanguages,
    GetSupportedLanguagesResponse, HandleRef, LocalizedTextRef, ReferencedVersion, VersionCounter,
};
use protosdc_biceps::types::ProtoLanguage;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum SoapServiceError {
    #[error("The required service was not found")]
    ServiceUnavailable,
    #[error(transparent)]
    SoapError(#[from] SoapError),
}

/// This function is used to find a hosted service for a given type.
/// It uses hosting service to iterate over its hosted services, looking for a service
/// that matches the provided `qualified_name`.
///
/// # Arguments
///
/// * `qualified_name` - A `ConstQualifiedName<'static>` representing the type for which we are searching the hosted service.
/// * `hosting_service` - A reference to hosting service, which provides a multitude of hosted services to find from.
///
/// In generics, it uses four different traits:
///
/// * `H` : Represents a hosting service.
/// * `HOSTED` : Represents a hosted service.
/// * `S` : Represents a soap client.
/// * `HS` : Represents a soap client that extends `Send`, `Sync`, and `Clone` traits.
///
/// # Returns
///
/// This function returns `Option<(String, &'a HOSTED)>`. It will return `None` if no matching service type is found,
/// otherwise it will return `Some` tuple with `service_id` as `String` and a reference to that hosted service.
pub async fn find_hosted_service_for_type<'a, H, HOSTED, S, HS>(
    qualified_name: ConstQualifiedName<'static>,
    hosting_service: &'a H,
) -> Option<(String, &'a HOSTED)>
where
    H: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS> + 'a,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    hosting_service
        .hosted_services()
        .iter()
        .find(|(_service_id, service)| {
            service
                .service_type()
                .types
                .list
                .list
                .iter()
                .any(|it| it == &qualified_name)
        })
        .map(|it| (it.0.clone(), it.1))
}

/// Executes a GetMdib request as specified in IEEE 11073-10207-2017.
///
/// # Arguments
///
/// * `hosting_service`: hosting service against which to execute the request
pub async fn get_mdib<H, HOSTED, S, HS>(
    hosting_service: &H,
) -> Result<GetMdibResponse, SoapServiceError>
where
    H: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    let (_, get_hosted) = find_hosted_service_for_type(BICEPS_PORT_TYPE_GET, hosting_service)
        .await
        .ok_or(SoapServiceError::ServiceUnavailable)?;

    // retrieve mdib
    let (_soap_msg, response): (_, GetMdibResponse) = get_hosted
        .request_response(
            SoapHeaderBuilder::new().action(BICEPS_ACTION_GET_MDIB),
            GetMdib {
                abstract_get: AbstractGet {
                    extension_element: None,
                },
            },
        )
        .await?;
    Ok(response)
}

/// Executes a GetMdDescription request as specified in IEEE 11073-10207-2017.
///
/// # Arguments
///
/// * `hosting_service`: hosting service against which to execute the request
/// * `handles`: the list of handles to include as filter criteria
pub async fn get_md_description<H, HOSTED, S, HS, HANDLES: AsRef<str>>(
    hosting_service: &H,
    handles: &[HANDLES],
) -> Result<GetMdDescriptionResponse, SoapServiceError>
where
    H: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    let (_, get_hosted) = find_hosted_service_for_type(BICEPS_PORT_TYPE_GET, hosting_service)
        .await
        .ok_or(SoapServiceError::ServiceUnavailable)?;

    let (_soap_msg, response): (_, GetMdDescriptionResponse) = get_hosted
        .request_response(
            SoapHeaderBuilder::new().action(BICEPS_ACTION_GET_MD_DESCRIPTION),
            GetMdDescription {
                abstract_get: AbstractGet {
                    extension_element: None,
                },
                handle_ref: handles
                    .iter()
                    .map(|it| HandleRef {
                        string: it.as_ref().to_string(),
                    })
                    .collect(),
            },
        )
        .await?;
    Ok(response)
}

/// Executes a GetMdState request as specified in IEEE 11073-10207-2017.
///
/// # Arguments
///
/// * `hosting_service`: hosting service against which to execute the request
/// * `handles`: the list of handles to include as filter criteria
pub async fn get_md_state<H, HOSTED, S, HS, HANDLES: AsRef<str>>(
    hosting_service: &H,
    handles: &[HANDLES],
) -> Result<GetMdStateResponse, SoapServiceError>
where
    H: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    let (_, get_hosted) = find_hosted_service_for_type(BICEPS_PORT_TYPE_GET, hosting_service)
        .await
        .ok_or(SoapServiceError::ServiceUnavailable)?;

    let (_soap_msg, response): (_, GetMdStateResponse) = get_hosted
        .request_response(
            SoapHeaderBuilder::new().action(BICEPS_ACTION_GET_MD_STATE),
            GetMdState {
                abstract_get: AbstractGet {
                    extension_element: None,
                },
                handle_ref: handles
                    .iter()
                    .map(|it| HandleRef {
                        string: it.as_ref().to_string(),
                    })
                    .collect(),
            },
        )
        .await?;
    Ok(response)
}

/// Executes a GetContextStates request as specified in IEEE 11073-10207-2017.
///
/// # Arguments
///
/// * `hosting_service`: hosting service against which to execute the request
/// * `handles`: the list of handles to include as filter criteria
pub async fn get_context_states<H, HOSTED, S, HS, HANDLES: AsRef<str>>(
    hosting_service: &H,
    handles: &[HANDLES],
) -> Result<GetContextStatesResponse, SoapServiceError>
where
    H: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    let (_, context_hosted) =
        find_hosted_service_for_type(BICEPS_PORT_TYPE_CONTEXT, hosting_service)
            .await
            .ok_or(SoapServiceError::ServiceUnavailable)?;

    let (_soap_msg, response): (_, GetContextStatesResponse) = context_hosted
        .request_response(
            SoapHeaderBuilder::new().action(BICEPS_ACTION_GET_CONTEXT_STATES),
            GetContextStates {
                abstract_get: AbstractGet {
                    extension_element: None,
                },
                handle_ref: handles
                    .iter()
                    .map(|it| HandleRef {
                        string: it.as_ref().to_string(),
                    })
                    .collect(),
            },
        )
        .await?;
    Ok(response)
}

/// Executes a GetSupportedLanguages request as specified in IEEE 11073-10207-2017.
///
/// # Arguments
///
/// * `hosting_service`: hosting service against which to execute the request
pub async fn get_supported_languages<H, HOSTED, S, HS>(
    hosting_service: &H,
) -> Result<GetSupportedLanguagesResponse, SoapServiceError>
where
    H: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    let (_, context_hosted) =
        find_hosted_service_for_type(BICEPS_PORT_TYPE_LOCALIZATION, hosting_service)
            .await
            .ok_or(SoapServiceError::ServiceUnavailable)?;

    let (_soap_msg, response): (_, GetSupportedLanguagesResponse) = context_hosted
        .request_response(
            SoapHeaderBuilder::new().action(BICEPS_ACTION_GET_SUPPORTED_LANGUAGES),
            GetSupportedLanguages {
                abstract_get: AbstractGet {
                    extension_element: None,
                },
            },
        )
        .await?;
    Ok(response)
}

/// Executes a GetLocalizedText request as specified in IEEE 11073-10207-2017.
///
/// # Arguments
///
/// * `hosting_service`: hosting service against which to execute the request
/// * `get_localized_text`: the body of the request specifying the filter criteria
pub async fn get_localized_text_msg<H, HOSTED, S, HS>(
    hosting_service: &H,
    get_localized_text: GetLocalizedText,
) -> Result<GetLocalizedTextResponse, SoapServiceError>
where
    H: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    let (_, context_hosted) =
        find_hosted_service_for_type(BICEPS_PORT_TYPE_LOCALIZATION, hosting_service)
            .await
            .ok_or(SoapServiceError::ServiceUnavailable)?;

    let (_soap_msg, response): (_, GetLocalizedTextResponse) = context_hosted
        .request_response(
            SoapHeaderBuilder::new().action(BICEPS_ACTION_GET_LOCALIZED_TEXT),
            get_localized_text,
        )
        .await?;
    Ok(response)
}

/// Executes a GetLocalizedText request as specified in IEEE 11073-10207-2017.
///
/// # Arguments
///
/// * `hosting_service`: hosting service against which to execute the request
/// * `text_ref`: the list of references to include as filter criteria
/// * `version`: the optional version to filter by
/// * `lang`: the list of languages to filter by
/// * `text_width`: the optional text width to filter by
/// * `number_of_lines`: the optional number of lines to filter by
pub async fn get_localized_text<H, HOSTED, S, HS, TEXT: Into<String>, LANG: Into<String>>(
    hosting_service: &H,
    text_ref: Vec<TEXT>,
    version: Option<u64>,
    lang: Vec<LANG>,
    text_width: Option<TextWidth>,
    number_of_lines: Option<i64>,
) -> Result<GetLocalizedTextResponse, SoapServiceError>
where
    H: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    get_localized_text_msg(
        hosting_service,
        GetLocalizedText {
            abstract_get: AbstractGet {
                extension_element: None,
            },
            r#ref: text_ref
                .into_iter()
                .map(|txt| LocalizedTextRef { string: txt.into() })
                .collect(),
            version: version.map(|it| ReferencedVersion {
                version_counter: VersionCounter { unsigned_long: it },
            }),
            lang: lang
                .into_iter()
                .map(|it| ProtoLanguage {
                    language: it.into(),
                })
                .collect(),
            text_width: match text_width {
                None => Vec::with_capacity(0),
                Some(width) => vec![width.into()],
            },
            number_of_lines: match number_of_lines {
                None => Vec::with_capacity(0),
                Some(l) => vec![l],
            },
        },
    )
    .await
}
