use std::collections::HashMap;
use std::sync::Arc;
use std::time::Duration;

use itertools::Itertools;
use lazy_static::lazy_static;
use log::{error, info, warn};
use protosdc_xml::ExpandedName;
use thiserror::Error;
use tokio::sync::Mutex;

use crate::common::extension_handling::ExtensionHandlerFactory;
use biceps::common::mdib_description_modification::MdibDescriptionModification;
use biceps::common::mdib_description_modifications::{
    MdibDescriptionModifications, ModificationError,
};
use biceps::common::mdib_version::MdibVersion;
use biceps::common::storage::mdib_storage::WriteError;
use biceps::consumer::access::remote_mdib_access::{
    InternalRemoteMdibAccess, RemoteMdibAccessImpl,
};
use biceps::utilities::modifications::{modifications_from_mdib, MdibError};
use common::{Service, ServiceError};
use protosdc::consumer::remote_device::DefaultRemoteMdibAccessImpl;
use protosdc::consumer::report_processor::{ReportProcessor, ReportProcessorImpl};
use protosdc::consumer::report_writer::ReportWriterError;
use protosdc::consumer::sco::sco_controller::{ScoController, SetServiceAccess, SetServiceHandler};
use protosdc::consumer::sco::sco_transaction::{ScoTransaction, ScoTransactionImpl};

use crate::consumer::sco::DpwsSetServiceHandler;
use crate::consumer::sdc_consumer::{BicepsNotificationSink, ConsumerConfig, SdcRemoteDevice};
use crate::consumer::services::{find_hosted_service_for_type, get_mdib, SoapServiceError};
use crate::consumer::watchdog::RemoteDeviceWatchdog;
use crate::soap::common::SoapError;
use crate::soap::dpws::client::event_sink::{EventSinkError, SubscribeResult};
use crate::soap::dpws::client::hosted_service::HostedService;
use crate::soap::dpws::client::hosting_service::HostingService;
use crate::soap::dpws::ws_eventing::sink::WsEventingSink;
use crate::soap::soap_client::SoapClient;
use crate::xml::messages::biceps_ext::{
    BICEPS_ACTION_DESCRIPTION_MODIFICATION_REPORT, BICEPS_ACTION_EPISODIC_ALERT_REPORT,
    BICEPS_ACTION_EPISODIC_COMPONENT_REPORT, BICEPS_ACTION_EPISODIC_CONTEXT_REPORT,
    BICEPS_ACTION_EPISODIC_METRIC_REPORT, BICEPS_ACTION_EPISODIC_OPERATIONAL_STATE_REPORT,
    BICEPS_ACTION_OPERATION_INVOKED_REPORT, BICEPS_ACTION_SYSTEM_ERROR_REPORT,
    BICEPS_ACTION_WAVEFORM_STREAM, BICEPS_PORT_TYPE_CONTEXT, BICEPS_PORT_TYPE_DESCRIPTION_EVENT,
    BICEPS_PORT_TYPE_SET, BICEPS_PORT_TYPE_STATE_EVENT, BICEPS_PORT_TYPE_WAVEFORM,
};
use crate::xml::messages::common::{ConstQualifiedName, DPWS_ACTION_DIALECT};
use crate::xml::messages::ws_eventing::common::{Filter, FilterContent};

pub const DEFAULT_DURATION: Duration = Duration::from_secs(30);

pub const EPISODIC_REPORTS: [&str; 8] = [
    BICEPS_ACTION_EPISODIC_ALERT_REPORT,
    BICEPS_ACTION_EPISODIC_CONTEXT_REPORT,
    BICEPS_ACTION_EPISODIC_COMPONENT_REPORT,
    BICEPS_ACTION_EPISODIC_METRIC_REPORT,
    BICEPS_ACTION_EPISODIC_OPERATIONAL_STATE_REPORT,
    BICEPS_ACTION_DESCRIPTION_MODIFICATION_REPORT,
    BICEPS_ACTION_OPERATION_INVOKED_REPORT,
    BICEPS_ACTION_SYSTEM_ERROR_REPORT,
];

lazy_static! {
    pub static ref ALL_EPISODIC_AND_WAVEFORM_REPORTS: Vec<&'static str> = {
        let mut reps = EPISODIC_REPORTS.to_vec();
        reps.push(BICEPS_ACTION_WAVEFORM_STREAM);
        reps
    };
}

#[derive(Debug, Error)]
pub enum ConnectConfigurationError {
    #[error("Could not resolve the port type for: {0}")]
    UnresolvedActions(String),
}

pub struct ConnectConfiguration {
    mandatory_actions: Vec<String>,
    optional_actions: Vec<String>,
    required_port_types: Vec<ConstQualifiedName<'static>>,
    // TODO: Implement
    _optional_port_types: Vec<ConstQualifiedName<'static>>,
}

impl ConnectConfiguration {
    pub fn new(
        mandatory_actions: Vec<String>,
        optional_actions: Vec<String>,
    ) -> Result<Self, ConnectConfigurationError> {
        let (resolved_mandatory, unresolved_mandatory) = port_types_for_actions(&mandatory_actions);
        if !unresolved_mandatory.is_empty() {
            Err(ConnectConfigurationError::UnresolvedActions(
                unresolved_mandatory
                    .iter()
                    .map(|it| it.to_string())
                    .join(" "),
            ))?
        }
        let (resolved_optional, unresolved_optional) = port_types_for_actions(&optional_actions);
        if !unresolved_optional.is_empty() {
            warn!(
                "Could not resolve port types for optional actions: {:?}",
                unresolved_optional
            );
        }
        Ok(Self {
            mandatory_actions,
            optional_actions,
            required_port_types: resolved_mandatory,
            _optional_port_types: resolved_optional,
        })
    }
}

fn port_types_for_actions(actions: &[String]) -> (Vec<ConstQualifiedName<'static>>, Vec<&String>) {
    let (resolved_port_types, unresolved_actions) = actions.iter().map(port_type_for_action).fold(
        (vec![], vec![]),
        |(mut ok, mut err), data| {
            match data {
                Ok(it) => ok.push(it),
                Err(it) => err.push(it),
            };
            (ok, err)
        },
    );

    (resolved_port_types, unresolved_actions)
}

fn port_type_for_action(action: &String) -> Result<ConstQualifiedName<'static>, &String> {
    match action.as_str() {
        BICEPS_ACTION_EPISODIC_ALERT_REPORT
        | BICEPS_ACTION_EPISODIC_COMPONENT_REPORT
        | BICEPS_ACTION_EPISODIC_METRIC_REPORT
        | BICEPS_ACTION_EPISODIC_OPERATIONAL_STATE_REPORT
        | BICEPS_ACTION_SYSTEM_ERROR_REPORT => Ok(BICEPS_PORT_TYPE_STATE_EVENT),
        BICEPS_ACTION_EPISODIC_CONTEXT_REPORT => Ok(BICEPS_PORT_TYPE_CONTEXT),
        BICEPS_ACTION_DESCRIPTION_MODIFICATION_REPORT => Ok(BICEPS_PORT_TYPE_DESCRIPTION_EVENT),
        BICEPS_ACTION_OPERATION_INVOKED_REPORT => Ok(BICEPS_PORT_TYPE_SET),
        BICEPS_ACTION_WAVEFORM_STREAM => Ok(BICEPS_PORT_TYPE_WAVEFORM),
        _ => Err(action),
    }
}

#[derive(Debug, Error)]
pub enum SdcRemoteDeviceConnectorError {
    #[error("Required services were missing: {services:?}")]
    RequiredServicesMissing { services: Vec<ExpandedName> },
    #[error("Could not find a service for port type {0}")]
    ServiceForPortTypeMissing(ExpandedName),

    #[error(transparent)]
    SoapServiceError(#[from] SoapServiceError),

    #[error("Could not find a service for service id {0}")]
    ServiceMissing(String),
    #[error(transparent)]
    EventSinkError(#[from] EventSinkError),
    #[error(transparent)]
    ModificationError(#[from] ModificationError),
    #[error(transparent)]
    WriteError(#[from] WriteError),
    #[error(transparent)]
    ReportWriterError(#[from] ReportWriterError),
    #[error(transparent)]
    MdibError(#[from] MdibError),
    #[error(transparent)]
    SoapError(#[from] SoapError),
    #[error(transparent)]
    ServiceError(#[from] ServiceError),
}

pub async fn connect<H, HOSTED, S, HS, SSA, ST, EHF>(
    proxy: H,
    connect_configuration: ConnectConfiguration,
    consumer_config: ConsumerConfig<EHF>,
) -> Result<
    SdcRemoteDevice<
        DefaultRemoteMdibAccessImpl,
        H,
        HOSTED,
        S,
        HS,
        ScoController<DpwsSetServiceHandler<HS>>,
        ScoTransactionImpl,
        EHF,
    >,
    SdcRemoteDeviceConnectorError,
>
where
    H: HostingService<HOSTED, S, HS> + Send + Sync + 'static,
    HOSTED: HostedService<HS> + Send + Sync,
    S: SoapClient + Send + Sync,
    HS: SoapClient + Send + Sync + Clone + 'static,
    SSA: SetServiceAccess<ST>,
    ST: ScoTransaction,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    // TODO: check if proxy is already connected
    check_required_services(&proxy, &connect_configuration)?;

    info!("Starting connect");

    let report_processor = Arc::new(Mutex::new(ReportProcessorImpl::default()));

    let set_service_access = match find_hosted_service_for_type(BICEPS_PORT_TYPE_SET, &proxy).await
    {
        Some((_service_id, set_service)) => {
            let set_soap_client = set_service.soap_client().clone();

            let context_soap_client =
                find_hosted_service_for_type(BICEPS_PORT_TYPE_CONTEXT, &proxy)
                    .await
                    .map(|it| it.1.soap_client().clone());

            let x = DpwsSetServiceHandler::new(set_soap_client, context_soap_client);

            Some(ScoController::new(x, Duration::from_secs(5)))
        }
        None => None,
    };

    let subscribe_result = subscribe_services(
        &proxy,
        &connect_configuration,
        report_processor.clone(),
        set_service_access.clone(),
        &consumer_config,
    )
    .await?;

    let remote_mdib = create_remote_mdib_access_impl(&proxy).await?;

    // apply buffered reports on mdib
    report_processor
        .lock()
        .await
        .start_applying_reports(remote_mdib.clone())
        .await?;

    // start watchdog
    let proxy_arc = Arc::new(Mutex::new(proxy));
    let mut watchdog = RemoteDeviceWatchdog::new(
        proxy_arc.clone(),
        subscribe_result,
        consumer_config.watchdog_period,
        consumer_config.requested_expires,
    );
    watchdog.start().await?;

    info!("Connected");

    Ok(SdcRemoteDevice::new(
        remote_mdib,
        proxy_arc,
        watchdog,
        set_service_access,
        consumer_config.extension_handler_factory.clone(),
    ))
}

async fn create_remote_mdib_access_impl<H, HOSTED, S, HS>(
    hosting_service: &H,
) -> Result<DefaultRemoteMdibAccessImpl, SdcRemoteDeviceConnectorError>
where
    H: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    // get mdib
    let get_mdib_response = get_mdib(hosting_service).await?;

    let mdib_version: MdibVersion =
        MdibVersion::from(get_mdib_response.mdib.mdib_version_group_attr.clone());
    let modifications: Vec<MdibDescriptionModification> =
        modifications_from_mdib(get_mdib_response.mdib).await?;

    let mut description_modifications = MdibDescriptionModifications::default();
    description_modifications.add_all(modifications)?;

    // create remote mdib
    let mut remote_mdib = RemoteMdibAccessImpl::new(mdib_version.clone());
    remote_mdib
        .write_description(mdib_version, description_modifications)
        .await?;

    Ok(remote_mdib)
}

fn find_service_id_for_qname<'b>(
    hosted_services: &'b HashMap<&'b String, &Vec<ExpandedName>>,
    qname: &ConstQualifiedName,
) -> Option<&'b &'b String> {
    hosted_services
        .iter()
        .find(|(_service_id, types)| types.iter().any(|t| t == qname))
        .map(|it| it.0)
}

async fn subscribe_services<H, HOSTED, S, HS, RP, SCH, EHF>(
    hosting_service: &H,
    connect_configuration: &ConnectConfiguration,
    report_processor: Arc<Mutex<RP>>,
    sco_controller: Option<ScoController<SCH>>,
    consumer_config: &ConsumerConfig<EHF>,
) -> Result<HashMap<String, SubscribeResult>, SdcRemoteDeviceConnectorError>
where
    H: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
    RP: ReportProcessor + Send + 'static,
    SCH: SetServiceHandler + Clone + Send + Sync + 'static,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    // determine necessary subscriptions
    let hosted_services: HashMap<&String, &Vec<ExpandedName>> = hosting_service
        .hosted_services()
        .iter()
        .map(|(service_id, hosted)| (service_id, &hosted.service_type().types.list.list))
        .collect();
    let mandatory_subs_vec = connect_configuration
        .mandatory_actions
        .iter()
        .map(|action| {
            (
                action,
                port_type_for_action(action).expect("Already checked, can't not work"),
            )
        })
        .map(|(action, qname)| {
            let service_id = find_service_id_for_qname(&hosted_services, &qname);

            match service_id {
                None => Err(SdcRemoteDeviceConnectorError::RequiredServicesMissing {
                    services: vec![qname.into()],
                }),
                Some(service_id) => Ok((service_id.to_string(), action)),
            }
        })
        .collect::<Result<Vec<_>, SdcRemoteDeviceConnectorError>>()?;
    let optional_subs_vec = connect_configuration
        .optional_actions
        .iter()
        .map(|action| {
            (
                action,
                port_type_for_action(action).expect("Already checked, can't not work"),
            )
        })
        .filter_map(|(action, qname)| {
            let service_id = find_service_id_for_qname(&hosted_services, &qname);

            service_id
                .as_ref()
                .map(|service_id| (service_id.to_string(), action))
        })
        .collect::<Vec<_>>();

    let subs = mandatory_subs_vec
        .into_iter()
        .chain(optional_subs_vec.into_iter())
        .into_group_map();

    let mut result_map = HashMap::new();

    for (service_id, actions) in subs {
        let hosted = hosting_service.hosted_services().get(&service_id).ok_or(
            SdcRemoteDeviceConnectorError::ServiceMissing(service_id.to_string()),
        )?;

        let desired_actions: String = actions.iter().join(" ");

        let filter = Filter {
            attributes: Default::default(),
            dialect: Some(DPWS_ACTION_DIALECT.to_string()),
            filters: Some(FilterContent::Text(desired_actions)),
        };

        let sink = BicepsNotificationSink::new(report_processor.clone(), sco_controller.clone());

        let subscribe_result = hosted
            .subscribe(
                Some(filter),
                consumer_config.requested_expires,
                WsEventingSink::new(sink, consumer_config.extension_handler_factory.clone()),
            )
            .await?;

        result_map.insert(service_id, subscribe_result);
    }

    Ok(result_map)
}

fn check_required_services<H, HOSTED, S, HS>(
    hosting_service: &H,
    connect_configuration: &ConnectConfiguration,
) -> Result<(), SdcRemoteDeviceConnectorError>
where
    H: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    let mut missing_port_types = Vec::with_capacity(0); // should not happen often

    for required_port_type in &connect_configuration.required_port_types {
        let missing = !hosting_service
            .hosted_services()
            .iter()
            .map(|(_, it)| it.service_type())
            .any(|it| {
                it.types
                    .list
                    .list
                    .iter()
                    .any(|it2| required_port_type == it2)
            });
        if missing {
            missing_port_types.push(required_port_type)
        }
    }

    if missing_port_types.is_empty() {
        return Ok(());
    }
    Err(SdcRemoteDeviceConnectorError::RequiredServicesMissing {
        services: missing_port_types
            .into_iter()
            .map(|it| ExpandedName::from(it.clone()))
            .collect(),
    })
}
