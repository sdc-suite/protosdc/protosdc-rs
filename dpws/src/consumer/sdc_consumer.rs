use std::marker::PhantomData;
use std::sync::Arc;
use std::time::Duration;

use async_trait::async_trait;
use log::error;
use tokio::sync::Mutex;

use crate::common::extension_handling::{
    ExtensionHandlerFactory, ReplacementExtensionHandlerFactory,
};
use biceps::consumer::access::remote_mdib_access::RemoteMdibAccess;
use common::ServiceError;
use protosdc::common::reports::EpisodicReport;
use protosdc::consumer::report_processor::ReportProcessor;
use protosdc::consumer::sco::sco_controller::{ScoController, SetServiceAccess, SetServiceHandler};
use protosdc::consumer::sco::sco_transaction::ScoTransaction;

use crate::consumer::watchdog::RemoteDeviceWatchdog;
use crate::soap::common::SoapError;
use crate::soap::dpws::client::hosted_service::HostedService;
use crate::soap::dpws::client::hosting_service::HostingService;
use crate::soap::dpws::client::notification_sink::{NotificationSinkError, NotificationSinkTyped};
use crate::soap::eventing::{BicepsReport, DpwsEpisodicReport};
use crate::soap::soap_client::SoapClient;
use crate::xml::messages::soap::fault::{Fault, FaultCode, FaultCodeEnum, FaultReason};

pub struct ConsumerConfig<EHF>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    pub(crate) watchdog_period: Duration,
    pub(crate) requested_expires: Duration,
    pub(crate) extension_handler_factory: EHF,
}

#[derive(Debug, Default)]
pub struct ConsumerConfigBuilder {
    watchdog_period: Option<Duration>,
    requested_expires: Option<Duration>,
}

#[derive(Debug, Default)]
pub struct ConsumerConfigBuilderTyped<EHF>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    watchdog_period: Option<Duration>,
    requested_expires: Option<Duration>,
    extension_handler_factory: EHF,
}

impl ConsumerConfigBuilder {
    pub fn watchdog_period(&mut self, watchdog_period: Duration) {
        self.watchdog_period = Some(watchdog_period)
    }
    pub fn requested_expires(&mut self, requested_expires: Duration) {
        self.requested_expires = Some(requested_expires)
    }

    pub fn default_extension_handler(
        self,
    ) -> ConsumerConfigBuilderTyped<ReplacementExtensionHandlerFactory> {
        self.extension_handler_factory(ReplacementExtensionHandlerFactory)
    }
    pub fn extension_handler_factory<EHF>(
        self,
        extension_handler_factory: EHF,
    ) -> ConsumerConfigBuilderTyped<EHF>
    where
        EHF: ExtensionHandlerFactory + Send + Sync,
    {
        ConsumerConfigBuilderTyped {
            watchdog_period: self.watchdog_period,
            requested_expires: self.requested_expires,
            extension_handler_factory,
        }
    }
}

impl<EHF> ConsumerConfigBuilderTyped<EHF>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    pub fn watchdog_period(&mut self, watchdog_period: Duration) {
        self.watchdog_period = Some(watchdog_period)
    }
    pub fn requested_expires(&mut self, requested_expires: Duration) {
        self.requested_expires = Some(requested_expires)
    }
    pub fn build(self) -> ConsumerConfig<EHF> {
        let watchdog_period = self.watchdog_period.unwrap_or(Duration::from_secs(5));
        ConsumerConfig {
            watchdog_period,
            requested_expires: self.requested_expires.unwrap_or(watchdog_period * 3),
            extension_handler_factory: self.extension_handler_factory,
        }
    }
}

pub struct SdcRemoteDevice<U, HOSTING, HOSTED, S, HS, SSA, ST, EHF>
where
    U: RemoteMdibAccess,
    HOSTING: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
    SSA: SetServiceAccess<ST>,
    ST: ScoTransaction,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    pub remote_mdib: U,
    pub hosting_service: Arc<Mutex<HOSTING>>,
    pub set_service: Option<SSA>,
    pub watchdog: RemoteDeviceWatchdog<HOSTING, HOSTED, S, HS>,
    _extension_handler_factory: EHF,
    phantom_hosted: PhantomData<HOSTED>,
    phantom_s: PhantomData<S>,
    phantom_hs: PhantomData<HS>,
    phantom_st: PhantomData<ST>,
}

impl<U: RemoteMdibAccess, HOSTING: HostingService<HOSTED, S, HS>, HOSTED, S, HS, SSA, ST, EHF>
    SdcRemoteDevice<U, HOSTING, HOSTED, S, HS, SSA, ST, EHF>
where
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
    SSA: SetServiceAccess<ST>,
    ST: ScoTransaction,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    pub fn new(
        remote_mdib: U,
        hosting: Arc<Mutex<HOSTING>>,
        watchdog: RemoteDeviceWatchdog<HOSTING, HOSTED, S, HS>,
        set_service_access: Option<SSA>,
        extension_handler_factory: EHF,
    ) -> Self {
        Self {
            remote_mdib,
            watchdog,
            set_service: set_service_access,
            hosting_service: hosting,
            phantom_hosted: Default::default(),
            phantom_s: Default::default(),
            phantom_hs: Default::default(),
            phantom_st: Default::default(),
            _extension_handler_factory: extension_handler_factory,
        }
    }

    pub async fn disconnect(&self) -> Result<(), ServiceError> {
        let mut hosting = self.hosting_service.lock().await;
        hosting.disconnect().await;
        Ok(())
    }
}

pub struct BicepsNotificationSink<R, SCH>
where
    R: ReportProcessor,
    SCH: SetServiceHandler + Clone + Send + Sync,
{
    report_processor: Arc<Mutex<R>>,
    sco_controller: Option<ScoController<SCH>>,
}

impl<R, SCH> BicepsNotificationSink<R, SCH>
where
    R: ReportProcessor,
    SCH: SetServiceHandler + Clone + Send + Sync,
{
    pub fn new(processor: Arc<Mutex<R>>, sco_controller: Option<ScoController<SCH>>) -> Self {
        Self {
            report_processor: processor,
            sco_controller,
        }
    }
}

#[async_trait]
impl<R, SCH> NotificationSinkTyped<BicepsReport> for BicepsNotificationSink<R, SCH>
where
    R: ReportProcessor + Send,
    SCH: SetServiceHandler + Clone + Send + Sync,
{
    async fn receive_notification(
        &self,
        notification: BicepsReport,
    ) -> Result<(), NotificationSinkError> {
        match notification {
            BicepsReport::Episodic(ep) => {
                let rep: EpisodicReport = match ep {
                    DpwsEpisodicReport::Alert(x) => EpisodicReport::Alert(x),
                    DpwsEpisodicReport::Component(x) => EpisodicReport::Component(x),
                    DpwsEpisodicReport::Context(x) => EpisodicReport::Context(x),
                    DpwsEpisodicReport::Metric(x) => EpisodicReport::Metric(x),
                    DpwsEpisodicReport::Operation(x) => EpisodicReport::Operation(x),
                    DpwsEpisodicReport::Waveform(x) => EpisodicReport::Waveform(x),
                    DpwsEpisodicReport::Description(x) => EpisodicReport::Description(x),
                    DpwsEpisodicReport::OperationInvokedReport(oir) => {
                        return if let Some(sco_controller) = &self.sco_controller {
                            sco_controller
                                .process_operation_invoked_report(*oir)
                                .await
                                .map_err(|err| {
                                    NotificationSinkError::SoapError(SoapError::SoapFault(
                                        Fault::builder()
                                            .code(
                                                FaultCode::builder()
                                                    .value(FaultCodeEnum::Sender)
                                                    .build(),
                                            )
                                            .reason(FaultReason::simple_text(
                                                "en",
                                                format!("Error processing operation: {:?}", err),
                                            ))
                                            .build(),
                                    ))
                                })
                        } else {
                            Ok(())
                        };
                    }
                    DpwsEpisodicReport::SystemErrorReport(_) => {
                        error!("Received system error report, no handling available");
                        Err(SoapError::SoapFault(
                            Fault::builder()
                                .code(FaultCode::builder().value(FaultCodeEnum::Sender).build())
                                .reason(FaultReason::simple_text(
                                    "en",
                                    "Cannot handle system error reports",
                                ))
                                .build(),
                        ))?
                    }
                };

                self.report_processor
                    .lock()
                    .await
                    .process_episodic_report(rep)
                    .await
                    .map_err(|err| {
                        SoapError::SoapFault(
                            Fault::builder()
                                .code(FaultCode::builder().value(FaultCodeEnum::Receiver).build())
                                .reason(FaultReason::simple_text(
                                    "en",
                                    format!("Could not process incoming report: {:?}", err),
                                ))
                                .build(),
                        )
                    })
            }
            BicepsReport::Periodic(_) => Err(SoapError::SoapFault(
                Fault::builder()
                    .code(FaultCode::builder().value(FaultCodeEnum::Sender).build())
                    .reason(FaultReason::simple_text(
                        "en",
                        "Cannot handle periodic reports",
                    ))
                    .build(),
            )),
        }?;

        Ok(())
    }
}
