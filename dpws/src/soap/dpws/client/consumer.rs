use crate::common::extension_handling::ExtensionHandlerFactory;
use crate::common::local_address_resolver::{try_resolve_local_address, ResolverError};
use crate::discovery::consumer::discovery_consumer::{DpwsDiscoveryConsumer, DpwsDiscoveryError};
use crate::http::client::reqwest::ReqwestClientAccess;
use crate::http::server::axum::{AxumRegistryAccess, AxumServerAccess};
use crate::http::server::common::RegistryError;
use crate::soap::common::{SoapError, WSA_ACTION_GET_METADATA_REQUEST_STR, WST_ACTION_GET};
use crate::soap::dpws::client::event_sink::EventSinkImpl;
use crate::soap::dpws::client::hosted_service::{HostedService, HostedServiceImpl};
use crate::soap::dpws::client::hosting_service::{HostingService, HostingServiceImpl};
use crate::soap::dpws::subscription_manager::SinkSubscriptionManagerImpl;
use crate::soap::soap_client::{SoapClient, SoapClientCloneable, SoapClientImpl};
use crate::xml::common::SoapHeaderBuilder;
use crate::xml::messages::addressing::WsaEndpointReference;
use crate::xml::messages::common::HttpUri;
use crate::xml::messages::discovery::ResolveMatches;
use crate::xml::messages::dpws::host::{Host, Hosted};
use crate::xml::messages::dpws::relationship::Relationship;
use crate::xml::messages::dpws::this::{ThisDevice, ThisModel};
use crate::xml::messages::soap::envelope::{SoapEmptyBody, SoapMessage};
use crate::xml::messages::ws_metadata_exchange::get_metadata::GetMetadata;
use crate::xml::messages::ws_metadata_exchange::metadata::{Metadata, MetadataPayload};
use async_trait::async_trait;
use log::{debug, error};
use protosdc_xml::ExpandedName;
use std::collections::HashMap;
use std::net::SocketAddr;
use std::time::Duration;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum ConsumerError {
    #[error("ResolveMatches did not contain a ResolveMatch")]
    ResolveWithoutMatch,
    #[error("ResolveMatch did not contain any XAddrs")]
    MatchWithoutXAddrs,
    #[error("Did not receive any transfer get responses")]
    NoTransferGetResponse,
    #[error("Response did not contain any metadata sections")]
    EmptyMetadataSections,
    #[error("Response did not contain a relationship")]
    NoRelationship,
    #[error("No valid GetMetadataResponse was received")]
    NoValidGetMetadataResponse,
    #[error(transparent)]
    RegistryError(#[from] RegistryError),
    #[error(transparent)]
    DpwsDiscoveryError(#[from] DpwsDiscoveryError),
    #[error(transparent)]
    ResolverError(#[from] ResolverError),
}

#[async_trait]
pub trait Consumer<HOSTING, HOSTED, S, HS>
where
    HOSTING: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    /// Establishes a connection to the specified EPR address.
    ///
    /// Resolves the provided address and connects to it.
    ///
    /// # Arguments
    ///
    /// * `epr_address`: The address of the EPR (Endpoint Reference).
    /// * `timeout`: An optional duration specifying the maximum duration for the connection,
    /// uses a default timeout otherwise
    async fn connect(
        &self,
        epr_address: &str,
        timeout: Option<Duration>,
    ) -> Result<HOSTING, ConsumerError>;
}

pub struct ConsumerImpl<EHF, DISCO>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
    DISCO: DpwsDiscoveryConsumer + Send + Sync + 'static,
{
    discovery_client: DISCO,
    http_client: ReqwestClientAccess,
    registry_access: AxumRegistryAccess,
    extension_handler_factory: EHF,
}

pub type ReqwestSoapClient<EHF> = SoapClientImpl<ReqwestClientAccess, EHF>;
pub type ConsumerEventSink<EHF> = EventSinkImpl<
    ReqwestSoapClient<EHF>,
    AxumRegistryAccess,
    AxumServerAccess,
    SinkSubscriptionManagerImpl,
>;
pub type ConsumerHostedService<EHF> =
    HostedServiceImpl<ReqwestSoapClient<EHF>, ConsumerEventSink<EHF>, EHF>;
pub type ConsumerHostingService<EHF> =
    HostingServiceImpl<ReqwestSoapClient<EHF>, ConsumerHostedService<EHF>, ReqwestSoapClient<EHF>>;

pub struct RelationshipData<EHF>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    epr: WsaEndpointReference,
    types: Vec<ExpandedName>,
    hosted: HashMap<String, ConsumerHostedService<EHF>>,
}

async fn extract_relationship<EHF>(
    mut relationship: Relationship,
    other_soap_client: &ReqwestSoapClient<EHF>,
    registry: &AxumRegistryAccess,
) -> Result<RelationshipData<EHF>, ConsumerError>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    // make sure there is only one host
    if relationship.host.len() != 1 {
        Err(ConsumerError::NoRelationship)?
    }
    let host = relationship.host.pop().expect("must be present");

    let mut hosted_services = HashMap::new();
    for hosted in relationship.hosted {
        let extracted = extract_hosted_service(&host, hosted, other_soap_client, registry).await?;
        hosted_services.insert(extracted.service_type().service_id.clone(), extracted);
    }

    let epr = host.endpoint_reference;
    let types = host.types.map(|it| it.list.list).unwrap_or_default();

    Ok(RelationshipData {
        epr,
        types,
        hosted: hosted_services,
    })
}

async fn extract_hosted_service<EHF>(
    _host: &Host,
    hosted: Hosted,
    other_soap_client: &ReqwestSoapClient<EHF>,
    registry: &AxumRegistryAccess,
) -> Result<ConsumerHostedService<EHF>, ConsumerError>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    let mut epr_opt = None;
    let mut soap_client_opt = None;
    let mut metadata_response_opt = None;

    // determine active epr
    for epr in &(hosted.endpoint_reference) {
        let active_soap_client = other_soap_client.new_epr(epr.clone());

        let (_metadata_msg, get_metadata_response) =
            match send_get_metadata(&active_soap_client).await {
                Ok(x) => x,
                Err(err) => {
                    error!("Could not retrieve transfer get: {:?}", err);
                    continue;
                }
            };

        epr_opt = Some(epr);
        soap_client_opt = Some(active_soap_client);
        metadata_response_opt = Some(get_metadata_response);
        break;
    }

    let epr = epr_opt.ok_or(ConsumerError::NoValidGetMetadataResponse)?;
    let soap_client = soap_client_opt.ok_or(ConsumerError::NoValidGetMetadataResponse)?;
    let _metadata_response =
        metadata_response_opt.ok_or(ConsumerError::NoValidGetMetadataResponse)?;

    // create event sink for this address
    let server_host_address = try_resolve_local_address(&epr.address.value).await?;

    debug!("Resolved {:?} for event sink", server_host_address);

    let event_sink = ConsumerEventSink::new(
        soap_client.clone(),
        SocketAddr::new(server_host_address, 0),
        registry.clone(),
    );

    Ok(ConsumerHostedService::new(
        soap_client,
        epr.clone(),
        hosted,
        event_sink,
        other_soap_client.extension_handler_factory.clone(),
    ))
}

async fn send_transfer_get<S: SoapClient>(
    client: &S,
) -> Result<(SoapMessage, Metadata), SoapError> {
    let (message, response): (_, Metadata) = client
        .request_response(
            SoapHeaderBuilder::new().action(WST_ACTION_GET),
            SoapEmptyBody {},
        )
        .await?;

    Ok((message, response))
}

async fn send_get_metadata<S: SoapClient>(
    client: &S,
) -> Result<(SoapMessage, Metadata), SoapError> {
    let (message, response): (_, Metadata) = client
        .request_response(
            SoapHeaderBuilder::new().action(WSA_ACTION_GET_METADATA_REQUEST_STR),
            GetMetadata {
                attributes: Default::default(),
                dialect: None,
                identifier: None,
            },
        )
        .await?;

    Ok((message, response))
}

async fn extract_hosting_service_impl<EHF>(
    metadata: Metadata,
    other_soap_client: &ReqwestSoapClient<EHF>,
    registry: &AxumRegistryAccess,
    active_x_addr: HttpUri,
) -> Result<ConsumerHostingService<EHF>, ConsumerError>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    let mut this_device_opt: Option<ThisDevice> = None;
    let mut this_model_opt: Option<ThisModel> = None;
    let mut relationship_opt: Option<Relationship> = None;

    for section in metadata.metadata_sections {
        match section.payload {
            MetadataPayload::MetadataReference(_) => {}
            MetadataPayload::Location(_) => {}
            MetadataPayload::Relationship(relationship) => {
                relationship_opt = Some(relationship);
            }
            MetadataPayload::ThisDevice(device) => {
                this_device_opt = Some(device);
            }
            MetadataPayload::ThisModel(model) => {
                this_model_opt = Some(model);
            }
            MetadataPayload::Other(_) => {}
            MetadataPayload::InlineWsdl(_) => {}
        }
    }

    let relationship = relationship_opt.ok_or(ConsumerError::NoRelationship)?;
    let data = extract_relationship(relationship, other_soap_client, registry).await?;

    let hosting_service = ConsumerHostingService {
        phantom: Default::default(),
        soap_client: other_soap_client.clone(),
        active_x_addr: active_x_addr.uri,
        epr: data.epr,
        types: data.types,
        this_device: this_device_opt,
        this_model: this_model_opt,
        hosted: data.hosted,
    };

    Ok(hosting_service)
}

impl<EHF, DISCO> ConsumerImpl<EHF, DISCO>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
    DISCO: DpwsDiscoveryConsumer + Send + Sync + 'static,
{
    pub fn new(
        discovery_client: DISCO,
        http_client: ReqwestClientAccess,
        registry: AxumRegistryAccess,
        extension_handler_factory: EHF,
    ) -> Self {
        Self {
            discovery_client,
            http_client,
            registry_access: registry,
            extension_handler_factory,
        }
    }

    async fn connect_resolve(
        &self,
        epr: &ResolveMatches,
    ) -> Result<ConsumerHostingService<EHF>, ConsumerError>
    where
        EHF: ExtensionHandlerFactory + Send + Sync + 'static,
    {
        let resolve_match = &epr
            .resolve_match
            .as_ref()
            .ok_or(ConsumerError::ResolveWithoutMatch)?;

        let x_addrs = resolve_match
            .x_addrs
            .as_ref()
            .ok_or(ConsumerError::MatchWithoutXAddrs)?;

        let mut soap_client_opt = None;
        let mut active_x_addr_opt: Option<HttpUri> = None;
        let mut metadata_opt = None;

        // look for any uri that works
        for x_addr in &x_addrs.list {
            let local_active_x_addr = x_addr;
            let local_soap_client = SoapClientImpl::new_string(
                self.http_client.clone(),
                x_addr.to_string(),
                self.extension_handler_factory.clone(),
            );

            let (_transfer_get_msg, transfer_get_metadata) =
                match send_transfer_get(&local_soap_client).await {
                    Ok(x) => x,
                    Err(err) => {
                        error!("Could not retrieve transfer get: {:?}", err);
                        continue;
                    }
                };

            active_x_addr_opt = Some(local_active_x_addr.clone());
            soap_client_opt = Some(local_soap_client);
            metadata_opt = Some(transfer_get_metadata);
            break;
        }

        let soap_client = soap_client_opt.ok_or(ConsumerError::NoTransferGetResponse)?;
        let active_x_addr = active_x_addr_opt.ok_or(ConsumerError::NoTransferGetResponse)?;
        let metadata = metadata_opt.ok_or(ConsumerError::NoTransferGetResponse)?;

        if metadata.metadata_sections.is_empty() {
            return Err(ConsumerError::EmptyMetadataSections);
        }

        let hosting_service = extract_hosting_service_impl(
            metadata,
            &soap_client,
            &self.registry_access,
            active_x_addr,
        )
        .await?;

        Ok(hosting_service)
    }
}

#[async_trait]
impl<EHF, DISCO>
    Consumer<
        ConsumerHostingService<EHF>,
        ConsumerHostedService<EHF>,
        ReqwestSoapClient<EHF>,
        ReqwestSoapClient<EHF>,
    > for ConsumerImpl<EHF, DISCO>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
    DISCO: DpwsDiscoveryConsumer + Send + Sync + 'static,
{
    async fn connect(
        &self,
        epr_address: &str,
        timeout: Option<Duration>,
    ) -> Result<ConsumerHostingService<EHF>, ConsumerError> {
        // resolve the epr
        let resolved = self.discovery_client.resolve(epr_address, timeout).await?;

        self.connect_resolve(&resolved).await
    }
}
