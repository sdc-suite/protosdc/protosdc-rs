use crate::soap::common::SoapError;
use crate::soap::dpws::client::hosted_service::HostedService;
use crate::soap::soap_client::SoapClient;
use crate::xml::common::{Others, SoapHeaderBuilder};
use crate::xml::messages::addressing::WsaEndpointReference;
use crate::xml::messages::common::SoapElement;
use crate::xml::messages::dpws::this::{ThisDevice, ThisModel};
use crate::xml::messages::soap::envelope::SoapMessage;
use async_trait::async_trait;
use bytes::Bytes;
use http::Uri;
use log::error;
use protosdc_xml::{ComplexXmlTypeWrite, ExpandedName};
use std::collections::HashMap;
use std::marker::PhantomData;

#[async_trait]
pub trait HostingService<HOSTED, S, HS>: SoapClient
where
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    fn endpoint_reference_address(&self) -> &str;

    fn types(&self) -> &[ExpandedName];

    fn this_model(&self) -> Option<&ThisModel>;

    fn this_device(&self) -> Option<&ThisDevice>;

    fn hosted_services(&self) -> &HashMap<String, HOSTED>;

    fn active_x_addr(&self) -> &Uri;

    fn soap_client(&self) -> &S;

    async fn disconnect(&mut self) -> ();
}

pub struct HostingServiceImpl<S, HOSTED, HS>
where
    S: SoapClient + Send + Sync + Clone,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    pub(crate) phantom: PhantomData<HS>,
    pub(crate) soap_client: S,
    pub(crate) active_x_addr: Uri,
    pub(crate) epr: WsaEndpointReference,
    pub(crate) types: Vec<ExpandedName>,
    pub(crate) this_device: Option<ThisDevice>,
    pub(crate) this_model: Option<ThisModel>,
    pub(crate) hosted: HashMap<String, HOSTED>,
}

#[async_trait]
impl<S, HOSTED, HS> SoapClient for HostingServiceImpl<S, HOSTED, HS>
where
    S: SoapClient + Send + Sync + Clone,
    HOSTED: HostedService<HS> + Send + Sync,
    HS: SoapClient + Send + Sync + Clone,
{
    async fn request_response<REQ, RESP>(
        &self,
        header_builder: SoapHeaderBuilder<Others>,
        body: REQ,
    ) -> Result<(SoapMessage, RESP), SoapError>
    where
        REQ: ComplexXmlTypeWrite + Send + Sync,
        RESP: SoapElement + Send + Sync,
    {
        self.soap_client
            .request_response(header_builder, body)
            .await
    }

    async fn request_response_opt_body<REQ, RESP>(
        &self,
        header_builder: SoapHeaderBuilder<Others>,
        body: REQ,
    ) -> Result<(SoapMessage, Option<RESP>), SoapError>
    where
        REQ: ComplexXmlTypeWrite + Send + Sync,
        RESP: SoapElement + Send + Sync,
    {
        self.soap_client
            .request_response_opt_body(header_builder, body)
            .await
    }

    async fn notify_serialized(&self, _body: Bytes) -> Result<(), SoapError> {
        // no notifications for hosting service
        error!("No notifications for hosting service, do not use");
        Err(SoapError::TransportError)
    }

    async fn request_response_opt_body_serialized<RESP>(
        &self,
        body: Bytes,
    ) -> Result<(SoapMessage, Option<RESP>), SoapError>
    where
        RESP: SoapElement + Send + Sync,
    {
        self.soap_client
            .request_response_opt_body_serialized(body)
            .await
    }

    fn target_url(&self) -> &str {
        self.soap_client.target_url()
    }
}

#[async_trait]
impl<S, HOSTED, HS> HostingService<HOSTED, S, HS> for HostingServiceImpl<S, HOSTED, HS>
where
    S: SoapClient + Send + Sync + Clone,
    HOSTED: HostedService<HS> + Send + Sync,
    HS: SoapClient + Send + Sync + Clone,
{
    fn endpoint_reference_address(&self) -> &str {
        self.epr.address.value.as_str()
    }

    fn types(&self) -> &[ExpandedName] {
        &self.types
    }

    fn this_model(&self) -> Option<&ThisModel> {
        self.this_model.as_ref()
    }

    fn this_device(&self) -> Option<&ThisDevice> {
        self.this_device.as_ref()
    }

    fn hosted_services(&self) -> &HashMap<String, HOSTED> {
        &self.hosted
    }

    fn active_x_addr(&self) -> &Uri {
        &self.active_x_addr
    }

    fn soap_client(&self) -> &S {
        &self.soap_client
    }

    async fn disconnect(&mut self) -> () {
        for (_name, hosted) in self.hosted.drain() {
            hosted.disconnect().await;
        }
    }
}
