use std::collections::HashMap;
use std::marker::PhantomData;
use std::net::SocketAddr;
use std::sync::Arc;
use std::time::Duration;

use async_trait::async_trait;
use const_format::concatcp;
use log::{error, info};
use thiserror::Error;
use tokio::select;
use tokio::sync::Mutex;
use tokio::task::JoinHandle;
use tokio_util::sync::CancellationToken;
use uuid::Uuid;

use crate::http::server::common::{Server, ServerRegistry};
use crate::soap::common::{
    SoapError, WSE_ACTION_GET_STATUS_STR, WSE_ACTION_RENEW_STR, WSE_ACTION_SUBSCRIBE_STR,
    WSE_ACTION_UNSUBSCRIBE_STR,
};
use crate::soap::dpws::client::notification_sink::{NotificationSink, NotificationSinkError};
use crate::soap::dpws::subscription_manager::SinkSubscriptionManager;
use crate::soap::soap_client::SoapClientCloneable;
use crate::xml::common::SoapHeaderBuilder;
use crate::xml::messages::addressing::{WsaAttributedURIType, WsaEndpointReference};
use crate::xml::messages::common::WSE_DELIVERY_MODE_PUSH;
use crate::xml::messages::soap::envelope::SoapMessage;
use crate::xml::messages::ws_eventing::common::Filter;
use crate::xml::messages::ws_eventing::renew::{
    GetStatus, GetStatusResponse, Renew, RenewResponse, Unsubscribe, UnsubscribeResponse,
};
use crate::xml::messages::ws_eventing::subscribe::{Delivery, NotifyTo, Subscribe};
use crate::xml::messages::ws_eventing::subscribe_response::SubscribeResponse;

const EVENT_SINK_CONTEXT_PREFIX: &str = "event_sink_rs/";
const EVENT_SINK_NOTIFY_TO_CONTEXT_PREFIX: &str =
    concatcp!(EVENT_SINK_CONTEXT_PREFIX, "notify_to/");

#[derive(Debug, Error)]
pub enum EventSinkError {
    #[error(transparent)]
    SoapError(#[from] SoapError),
    #[error("Subscription with id {0} was not found")]
    SubscriptionNotFound(String),
    #[error("Expires element is missing")]
    ExpiresMissing,
}

pub struct SubscribeResult {
    pub subscription_id: String,
    pub expires: Duration,
}

#[async_trait]
pub trait EventSink {
    async fn subscribe<NS>(
        &self,
        filter: Option<Filter>,
        duration: Duration,
        sink: NS,
    ) -> Result<SubscribeResult, EventSinkError>
    where
        NS: NotificationSink + Sync + Send + 'static;

    async fn renew(
        &self,
        subscription_id: &str,
        duration: Duration,
    ) -> Result<Duration, EventSinkError>;

    async fn get_status(&self, subscription_id: &str) -> Result<Duration, EventSinkError>;

    async fn unsubscribe(&self, subscription_id: &str) -> Result<(), EventSinkError>;

    async fn unsubscribe_all(&self) -> Result<(), EventSinkError>;
}

#[derive(Debug, Default)]
pub struct SubscriptionData<M, C>
where
    C: SoapClientCloneable,
    M: SinkSubscriptionManager,
{
    subscription_managers: HashMap<String, M>,
    subscription_clients: HashMap<String, C>,
    cancellation_tokens: HashMap<String, CancellationToken>,
}

pub struct EventSinkImpl<C, R, S, M>
where
    C: SoapClientCloneable,
    R: ServerRegistry<S>,
    S: Server,
    M: SinkSubscriptionManager,
{
    phantom: PhantomData<S>, // hold this for a moment
    soap_client: C,
    host_address: SocketAddr,
    http_server_registry: R,
    _processing_task: Option<JoinHandle<()>>,
    subscription_data: Arc<Mutex<SubscriptionData<M, C>>>,
}

async fn unregister_subscription<M, C>(
    subscription_data: &Arc<Mutex<SubscriptionData<M, C>>>,
    subscription_id: &str,
) -> Result<(M, C, CancellationToken), EventSinkError>
where
    M: SinkSubscriptionManager,
    C: SoapClientCloneable,
{
    let (subscription_manager, subscription_client, cancellation_token) = {
        let mut data = subscription_data.lock().await;
        let subscription_manager = data.subscription_managers.remove(subscription_id).ok_or(
            EventSinkError::SubscriptionNotFound(subscription_id.to_string()),
        )?;
        let subscription_client = data.subscription_clients.remove(subscription_id).ok_or(
            EventSinkError::SubscriptionNotFound(subscription_id.to_string()),
        )?;
        let cancellation_token = data.cancellation_tokens.remove(subscription_id).ok_or(
            EventSinkError::SubscriptionNotFound(subscription_id.to_string()),
        )?;
        (
            subscription_manager,
            subscription_client,
            cancellation_token,
        )
    };
    info!("Unregistered subscription {}", subscription_id);
    Ok((
        subscription_manager,
        subscription_client,
        cancellation_token,
    ))
}

impl<C, R, S, M> EventSinkImpl<C, R, S, M>
where
    C: SoapClientCloneable,
    R: ServerRegistry<S>,
    S: Server,
    M: SinkSubscriptionManager,
{
    pub fn new(soap_client: C, host_address: SocketAddr, registry: R) -> Self {
        Self {
            phantom: Default::default(),
            soap_client,
            host_address,
            http_server_registry: registry,
            _processing_task: None,
            subscription_data: Arc::new(Mutex::new(SubscriptionData {
                subscription_managers: HashMap::new(),
                subscription_clients: HashMap::new(),
                cancellation_tokens: HashMap::new(),
            })),
        }
    }
}

fn create_epr(to: String) -> WsaEndpointReference {
    WsaEndpointReference {
        attributes: Default::default(),
        address: WsaAttributedURIType {
            attributes: Default::default(),
            value: to,
        },
        reference_parameters: None,
        metadata: None,
        children: vec![],
    }
}

#[async_trait]
impl<C, R, S, M> EventSink for EventSinkImpl<C, R, S, M>
where
    C: SoapClientCloneable + Send + Sync + 'static,
    R: ServerRegistry<S> + Send + Sync,
    S: Server + Send + Sync + Clone + 'static,
    M: SinkSubscriptionManager + Send + Sync + 'static,
{
    async fn subscribe<NS>(
        &self,
        filter: Option<Filter>,
        duration: Duration,
        sink: NS,
    ) -> Result<SubscribeResult, EventSinkError>
    where
        NS: NotificationSink + Sync + Send + 'static,
    {
        let subscription_id = Uuid::new_v4().to_string();
        let notify_to = format!("{}{}", EVENT_SINK_NOTIFY_TO_CONTEXT_PREFIX, subscription_id);

        // create end-to address
        let server = self
            .http_server_registry
            .init_server(self.host_address)
            .await
            .map_err(SoapError::RegistryError)?;

        // create notify-to address
        let mut notify_to_registration_result = server
            .register_context_path(&notify_to)
            .await
            .map_err(|_| SoapError::TransportError)?;
        // process message incoming to notify_to
        let s = server.clone();
        let subscription_data = self.subscription_data.clone();
        let subscription_id_task = subscription_id.clone();
        let cancellation_token = CancellationToken::new();
        let cloned_token = cancellation_token.clone();
        // TODO: Store somewhere
        let _notify_to_task = tokio::spawn(async move {
            loop {
                select! {
                    _ = cloned_token.cancelled() => {
                        break
                    }
                    r = notify_to_registration_result.channel.recv() => {
                        if let Some(request) = r {
                            match sink.receive_notification(request).await {
                                Ok(_) => {} // successfully processed
                                Err(NotificationSinkError::SubscriptionEnd { status }) => {
                                    info!(
                                        "Subscription {} is ending: {}",
                                        subscription_id_task, status
                                    );
                                    break;
                                }
                                Err(err) => {
                                    error!("{:?}", err);
                                    break;
                                }
                            };
                        } else {
                            break;
                        }
                    }
                }
            }
            // unregister subscription
            s.unregister_context_path(&notify_to).await.ok(); // failure doesn't matter, no processing
            unregister_subscription(&subscription_data, subscription_id_task.as_str())
                .await
                .ok(); // failure doesn't matter
        });

        let notify_to_epr = create_epr(notify_to_registration_result.address.address.to_string());
        let end_to_epr = create_epr(notify_to_registration_result.address.address.to_string());

        // subscribe for more awesome addresses
        let subscribe = Subscribe {
            attributes: Default::default(),
            end_to: Some(end_to_epr.clone()),
            delivery: Delivery {
                attributes: Default::default(),
                mode: Some(WSE_DELIVERY_MODE_PUSH.to_string()),
                notify_to: vec![NotifyTo {
                    endpoint_reference: notify_to_epr.clone(),
                }],
                children: vec![],
            },
            expires: Some(duration.into()),
            filter: filter.clone(),
            children: vec![],
        };

        let (_response_soap_message, subscribe_response): (SoapMessage, SubscribeResponse) = self
            .soap_client
            .request_response(
                SoapHeaderBuilder::new().action(WSE_ACTION_SUBSCRIBE_STR),
                subscribe,
            )
            .await?;

        let subscription_manager_soap_client = self
            .soap_client
            .new_epr(subscribe_response.subscription_manager.clone());

        let expires: Duration = subscribe_response.expires.into();

        let subscription_manager = M::new(
            subscribe_response.subscription_manager,
            expires,
            notify_to_epr,
            Some(end_to_epr),
            filter,
            subscription_id,
        );

        let subscription_id = subscription_manager.subscription_id().await.to_string();
        {
            let mut subs = self.subscription_data.lock().await;
            subs.subscription_managers
                .insert(subscription_id.clone(), subscription_manager);
            subs.subscription_clients
                .insert(subscription_id.clone(), subscription_manager_soap_client);
            subs.cancellation_tokens
                .insert(subscription_id.clone(), cancellation_token);
        }

        Ok(SubscribeResult {
            subscription_id,
            expires,
        })
    }

    async fn renew(
        &self,
        subscription_id: &str,
        duration: Duration,
    ) -> Result<Duration, EventSinkError> {
        // TODO: keep the lock only as long as needed
        let data = self.subscription_data.lock().await;
        let subscription_manager = data.subscription_managers.get(subscription_id).ok_or(
            EventSinkError::SubscriptionNotFound(subscription_id.to_string()),
        )?;
        let subscription_client = data.subscription_clients.get(subscription_id).ok_or(
            EventSinkError::SubscriptionNotFound(subscription_id.to_string()),
        )?;

        let renew = Renew {
            attributes: Default::default(),
            expires: Some(duration.into()),
            children: vec![],
        };

        let (_response_soap_message, renew_response): (SoapMessage, RenewResponse) =
            subscription_client
                .request_response(SoapHeaderBuilder::new().action(WSE_ACTION_RENEW_STR), renew)
                .await?;

        let new_expires: Duration = renew_response
            .renew_response
            .expires
            .ok_or(EventSinkError::ExpiresMissing)?
            .into();

        subscription_manager.process_renew(new_expires).await;
        Ok(new_expires)
    }

    async fn get_status(&self, subscription_id: &str) -> Result<Duration, EventSinkError> {
        // TODO: keep the lock only as long as needed
        let data = self.subscription_data.lock().await;
        let _subscription_manager = data.subscription_managers.get(subscription_id).ok_or(
            EventSinkError::SubscriptionNotFound(subscription_id.to_string()),
        )?;
        let subscription_client = data.subscription_clients.get(subscription_id).ok_or(
            EventSinkError::SubscriptionNotFound(subscription_id.to_string()),
        )?;

        let get_status = GetStatus {
            attributes: Default::default(),
            children: vec![],
        };

        let (_response_soap_message, get_status_response): (SoapMessage, GetStatusResponse) =
            subscription_client
                .request_response(
                    SoapHeaderBuilder::new().action(WSE_ACTION_GET_STATUS_STR),
                    get_status,
                )
                .await?;

        let expires: Duration = get_status_response
            .get_status_response
            .expires
            .ok_or(EventSinkError::ExpiresMissing)?
            .into();

        Ok(expires)
    }

    async fn unsubscribe(&self, subscription_id: &str) -> Result<(), EventSinkError> {
        let (_subscription_manager, subscription_client, t) =
            unregister_subscription(&self.subscription_data, subscription_id).await?;

        let get_status = Unsubscribe {
            get_status: GetStatus {
                attributes: Default::default(),
                children: vec![],
            },
        };

        let (_response_soap_message, _unsubscribe_response_opt): (
            SoapMessage,
            Option<UnsubscribeResponse>,
        ) = subscription_client
            .request_response_opt_body(
                SoapHeaderBuilder::new().action(WSE_ACTION_UNSUBSCRIBE_STR),
                get_status,
            )
            .await?;

        // cancelling the task automatically unregisters contexts
        t.cancel();

        Ok(())
    }

    async fn unsubscribe_all(&self) -> Result<(), EventSinkError> {
        let subscription_ids: Vec<String> = {
            self.subscription_data
                .lock()
                .await
                .subscription_managers
                .keys()
                .cloned()
                .collect()
        };

        for subscription_id in subscription_ids {
            match self.unsubscribe(&subscription_id).await {
                Ok(_) => {}
                Err(err) => error!("Error unsubscribing {}: {:?}", subscription_id, err),
            }
        }

        Ok(())
    }
}
