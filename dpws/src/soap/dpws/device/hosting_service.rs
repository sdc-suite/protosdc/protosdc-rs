use crate::common::extension_handling::ExtensionHandlerFactory;
use crate::discovery::actions::PROBE_MATCHES;
use crate::discovery::provider::discovery_provider::DpwsDiscoveryProcessor;
use crate::http::server::common::{
    ContextMessageResponse, RegistryError, Server, ServerError, ServerRegistry,
};
use crate::soap::common::{
    create_soap_http_message, validate_content_type, AcceptableContentType, SoapError,
    WST_ACTION_GET, WST_ACTION_GET_RESPONSE,
};
use crate::soap::dpws::device::dpws_device::DeviceSettings;
use crate::soap::dpws::device::hosted_service::HostedService;
use crate::xml::common::SoapHeaderBuilder;
use crate::xml::messages::addressing::WsaEndpointReference;
use crate::xml::messages::common::{
    DPWS_RELATIONSHIP_DIALECT, DPWS_THIS_DEVICE_DIALECT, DPWS_THIS_MODEL_DIALECT, DPWS_TYPE_HOST,
    PROBE_TAG, PROBE_TAG_REF, SOAP_BODY_TAG_REF, SOAP_ENVELOPE_TAG, SOAP_ENVELOPE_TAG_REF,
};
use crate::xml::messages::discovery::{Probe, ProbeMatches};
use crate::xml::messages::dpws::common::Types;
use crate::xml::messages::dpws::host::{Host, Hosted};
use crate::xml::messages::dpws::relationship::Relationship;
use crate::xml::messages::dpws::this::{ThisDevice, ThisModel};
use crate::xml::messages::soap::envelope::SoapMessage;
use crate::xml::messages::soap::fault::Fault;
use crate::xml::messages::ws_metadata_exchange::metadata::{
    Metadata, MetadataPayload, MetadataSection,
};
use async_trait::async_trait;
use bytes::Buf;
use http::{StatusCode, Uri};
use http_body_util::BodyExt;
use log::{error, warn};
use protosdc_xml::{
    find_start_element_reader, ExpandedName, GenericXmlReaderComplexTypeRead, ParserError,
    XmlReader, XmlWriter,
};
use quick_xml::events::Event;
use std::collections::HashSet;
use std::fmt::Debug;
use std::sync::Arc;
use thiserror::Error;
use tokio::sync::{oneshot, RwLock};
use tokio::task::JoinHandle;

#[derive(Debug, Error)]
pub enum HostingServiceError {
    #[error(transparent)]
    RegistryError(#[from] RegistryError),
    #[error(transparent)]
    ServerError(#[from] ServerError),
}

#[async_trait]
pub trait HostingService {
    async fn endpoint_reference(&self) -> WsaEndpointReference;

    async fn x_addrs(&self) -> Vec<Uri>;

    async fn types(&self) -> Vec<ExpandedName>;

    async fn this_model(&self) -> ThisModel;

    async fn this_device(&self) -> ThisDevice;

    async fn hosted_services(&self) -> Arc<Vec<Box<dyn HostedService + Send + Sync + 'static>>>;
}

struct HostingServiceImplInner {
    hosted: Arc<Vec<Box<dyn HostedService + Sync + Send + 'static>>>,
    this_model: ThisModel,
    this_device: ThisDevice,
    types: HashSet<ExpandedName>,
    endpoint_reference: WsaEndpointReference,
    x_addr: Uri,
}

pub struct HostingServiceImpl {
    _processing_task: JoinHandle<()>,
    inner: Arc<RwLock<HostingServiceImplInner>>,
}

fn oneshot_send<T: Debug, I: Into<T>>(sender: oneshot::Sender<T>, value: I) {
    if let Err(err) = sender.send(value.into()) {
        error!("Could not forward response: {:?}", err)
    }
}

impl HostingServiceImpl {
    pub async fn new<R, S, D, E>(
        device_settings: &DeviceSettings<E>,
        hosted: Vec<Box<dyn HostedService + Sync + Send>>,
        registry: R,
        discovery_processor: D,
    ) -> Result<Self, HostingServiceError>
    where
        R: ServerRegistry<S>,
        S: Server,
        D: DpwsDiscoveryProcessor + Send + Sync + 'static,
        E: ExtensionHandlerFactory + Send + Sync + 'static,
    {
        // register endpoint
        let server = registry
            .init_server(device_settings.network_interface)
            .await?;
        let context_path = format!("{}/hosting", device_settings.server_prefix);

        let mut registration_result = server.register_context_path(&context_path).await?;

        let x_addr = registration_result.address.address;

        let inner = Arc::new(RwLock::new(HostingServiceImplInner {
            hosted: Arc::new(hosted),
            this_model: device_settings.this_model.clone(),
            this_device: device_settings.this_device.clone(),
            types: device_settings.types.clone(),
            endpoint_reference: device_settings.endpoint_reference.clone(),
            x_addr,
        }));

        let task_inner = inner.clone();
        let task_ehf = device_settings.extension_handler_factory.clone();
        let task = tokio::spawn(async move {
            loop {
                if let Some(context_message) = registration_result.channel.recv().await {
                    let (parts, body) = context_message.request.into_parts();

                    // validate content type
                    let _content_type: AcceptableContentType =
                        match validate_content_type(&parts.headers) {
                            Ok(it) => it,
                            Err(err) => {
                                error!("Could not parse content-type: {err:?}");
                                continue;
                            }
                        };

                    // start by parsing soap envelope and header
                    let body_vec = match body
                        .collect()
                        .await
                        .map_err(|_| SoapError::ResponseBodyMissing)
                    {
                        Ok(it) => it,
                        Err(err) => {
                            error!("Could not load body: {err:?}");
                            continue;
                        }
                    }
                    .to_bytes();
                    let mut buf = Vec::with_capacity(body_vec.len());
                    let mut reader = XmlReader::create_custom(body_vec.reader(), task_ehf.create());

                    let soap_message: SoapMessage = match find_start_element_reader!(
                        SoapMessage,
                        SOAP_ENVELOPE_TAG,
                        reader,
                        buf
                    ) {
                        Ok(it) => it,
                        Err(err) => {
                            error!("Could not parse soap message: {err:?}");
                            continue;
                        }
                    };

                    let action_string = soap_message.header.action.map(|it| it.value);

                    let action = action_string.as_deref();

                    // check next event and dispatch to parser
                    loop {
                        match reader.read_next(&mut buf) {
                            Ok((ref ns, Event::Start(ref e))) => {
                                match (ns, e.local_name().into_inner()) {
                                    PROBE_TAG_REF => {
                                        let probe =
                                            Probe::from_xml_complex(PROBE_TAG, e, &mut reader);

                                        let probe_matches = match probe {
                                            Ok(it) => discovery_processor.process_probe(&it).await,
                                            Err(err) => {
                                                let soap_error: SoapError = err.into();
                                                let fault: Fault = soap_error.into();
                                                oneshot_send(
                                                    context_message.response_sender,
                                                    fault,
                                                );
                                                // context_message.response_sender.send(fault.into());
                                                break;
                                            }
                                        };

                                        let pm = match probe_matches {
                                            Some((_, matches)) => matches,
                                            None => ProbeMatches {
                                                attributes: Default::default(),
                                                probe_match: vec![],
                                                children: vec![],
                                            },
                                        };

                                        let header_builder = SoapHeaderBuilder::new()
                                            .action(PROBE_MATCHES)
                                            .relates_to_optional(
                                                soap_message.header.message_id.map(|it| it.value),
                                            );

                                        let msg = SoapMessage {
                                            header: header_builder.build(),
                                        };

                                        let mut writer = XmlWriter::new(vec![]);

                                        let body = msg
                                            .to_xml_complex(&mut writer, pm)
                                            .map_err(SoapError::from);
                                        match body {
                                            Ok(_) => {
                                                oneshot_send(
                                                    context_message.response_sender,
                                                    ContextMessageResponse {
                                                        status_code: StatusCode::OK,
                                                        payload: create_soap_http_message(
                                                            writer.into_inner().into(),
                                                        ),
                                                    },
                                                );
                                            }
                                            Err(err) => {
                                                let fault: Fault = err.into();
                                                oneshot_send(
                                                    context_message.response_sender,
                                                    fault,
                                                );
                                            }
                                        }
                                        break;
                                    }
                                    _ => {
                                        let err = ParserError::UnexpectedParserEvent {
                                            event: format!("End: {:?}", e.local_name()),
                                        };
                                        error!("{:?}", err);
                                        break;
                                    }
                                }
                            }
                            Ok((ref ns, Event::End(ref e))) => {
                                match (action, (ns, e.local_name().into_inner())) {
                                    (Some(WST_ACTION_GET), SOAP_ENVELOPE_TAG_REF) => {
                                        warn!("Prepare WS Transfer Get Response Envelope Tag!");
                                        break;
                                    }
                                    (Some(WST_ACTION_GET), SOAP_BODY_TAG_REF) => {
                                        let resp = {
                                            let locked = task_inner.read().await;
                                            process_get(&*locked).await
                                        };

                                        let header_builder = SoapHeaderBuilder::new()
                                            .action(WST_ACTION_GET_RESPONSE)
                                            .relates_to_optional(
                                                soap_message.header.message_id.map(|it| it.value),
                                            );

                                        let msg = SoapMessage {
                                            header: header_builder.build(),
                                        };

                                        let mut writer = XmlWriter::new(vec![]);
                                        let body = msg
                                            .to_xml_complex(&mut writer, resp)
                                            .map_err(SoapError::from);

                                        match body {
                                            Ok(_) => {
                                                oneshot_send(
                                                    context_message.response_sender,
                                                    ContextMessageResponse {
                                                        status_code: StatusCode::OK,
                                                        payload: create_soap_http_message(
                                                            writer.into_inner().into(),
                                                        ),
                                                    },
                                                );
                                            }
                                            Err(err) => {
                                                let fault: Fault = err.into();
                                                oneshot_send(
                                                    context_message.response_sender,
                                                    fault,
                                                );
                                            }
                                        }
                                        break;
                                    }
                                    _ => {
                                        let err = ParserError::UnexpectedParserEvent {
                                            event: format!("End: {:?}", e.local_name()),
                                        };
                                        error!("{:?}", err);
                                        break;
                                    }
                                }
                            }
                            Ok((ref _ns, Event::DocType(ref _e))) => {}
                            Ok((ref _ns, Event::Decl(ref _e))) => {}
                            Ok((ref _ns, Event::Comment(ref _e))) => {}
                            other => {
                                let err = ParserError::UnexpectedParserEvent {
                                    event: format!("{:?}", other),
                                };
                                error!("{:?}", err);
                                break;
                            }
                        }
                    }
                }
            }
        });

        Ok(Self {
            inner,
            _processing_task: task,
        })
    }
}

async fn process_get<HOSTING>(hosting: &HOSTING) -> Metadata
where
    HOSTING: HostingService,
{
    let mut metadata = Metadata {
        attributes: Default::default(),
        metadata_sections: vec![],
        children: vec![],
    };

    metadata.metadata_sections.push(MetadataSection {
        attributes: Default::default(),
        payload: MetadataPayload::ThisDevice(hosting.this_device().await),
        dialect: DPWS_THIS_DEVICE_DIALECT.to_string(),
        identifier: None,
    });

    metadata.metadata_sections.push(MetadataSection {
        attributes: Default::default(),
        payload: MetadataPayload::ThisModel(hosting.this_model().await),
        dialect: DPWS_THIS_MODEL_DIALECT.to_string(),
        identifier: None,
    });

    metadata.metadata_sections.push(MetadataSection {
        attributes: Default::default(),
        payload: MetadataPayload::Relationship(create_relationship(hosting).await),
        dialect: DPWS_RELATIONSHIP_DIALECT.to_string(),
        identifier: None,
    });

    metadata
}

async fn create_relationship<HOSTING>(hosting: &HOSTING) -> Relationship
where
    HOSTING: HostingService,
{
    let host = Host {
        attributes: Default::default(),
        endpoint_reference: hosting.endpoint_reference().await,
        types: Some(Types {
            list: (hosting.types().await).as_slice().into(),
        }),
        children: vec![],
    };

    let hosted = hosting
        .hosted_services()
        .await
        .iter()
        .map(create_hosted)
        .collect();

    Relationship {
        attributes: Default::default(),
        r#type: DPWS_TYPE_HOST.to_string(),
        host: vec![host],
        hosted,
        children: vec![],
    }
}

pub(crate) fn create_hosted<HOSTED>(hosted: &HOSTED) -> Hosted
where
    HOSTED: HostedService,
{
    Hosted {
        attributes: Default::default(),
        endpoint_reference: vec![hosted.endpoint_reference()],
        types: Types {
            list: hosted.types().as_slice().into(),
        },
        service_id: hosted.service_id().to_string(),
        children: vec![],
    }
}

#[async_trait]
impl HostingService for HostingServiceImplInner {
    async fn endpoint_reference(&self) -> WsaEndpointReference {
        self.endpoint_reference.clone()
    }

    async fn x_addrs(&self) -> Vec<Uri> {
        vec![self.x_addr.clone()]
    }

    async fn types(&self) -> Vec<ExpandedName> {
        self.types.iter().cloned().collect()
    }

    async fn this_model(&self) -> ThisModel {
        self.this_model.clone()
    }

    async fn this_device(&self) -> ThisDevice {
        self.this_device.clone()
    }

    async fn hosted_services(&self) -> Arc<Vec<Box<dyn HostedService + Send + Sync + 'static>>> {
        self.hosted.clone()
    }
}

#[async_trait]
impl HostingService for HostingServiceImpl {
    async fn endpoint_reference(&self) -> WsaEndpointReference {
        self.inner.read().await.endpoint_reference().await
    }

    async fn x_addrs(&self) -> Vec<Uri> {
        self.inner.read().await.x_addrs().await
    }

    async fn types(&self) -> Vec<ExpandedName> {
        self.inner.read().await.types().await
    }

    async fn this_model(&self) -> ThisModel {
        self.inner.read().await.this_model().await
    }

    async fn this_device(&self) -> ThisDevice {
        self.inner.read().await.this_device().await
    }

    async fn hosted_services(&self) -> Arc<Vec<Box<dyn HostedService + Send + Sync + 'static>>> {
        self.inner.read().await.hosted_services().await
    }
}
