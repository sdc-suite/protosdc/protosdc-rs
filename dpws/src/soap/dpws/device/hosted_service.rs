use crate::common::extension_handling::ExtensionHandlerFactory;
use crate::http::server::common::{ContextMessage, ContextMessageResponse};
use crate::soap::common::{
    create_soap_http_message, AcceptableContentType, SoapError,
    WSA_ACTION_GET_METADATA_RESPONSE_STR,
};
use crate::soap::eventing::ComplexXmlTypeReadInner;
use crate::soap::soap_server::{RequestResponseHandlerTyped, TransportInfo};
use crate::xml::common::SoapHeaderBuilder;
use crate::xml::messages::addressing::WsaEndpointReference;
use crate::xml::messages::common::{
    HttpUri, SoapEventMessage, DPWS_RELATIONSHIP_DIALECT, DPWS_TYPE_HOST, MEX_GET_METADATA_TAG,
    MEX_GET_METADATA_TAG_REF, SOAP_ENVELOPE_TAG, SOAP_FAULT_TAG, SOAP_FAULT_TAG_REF, WSDL_DIALECT,
    WSE_SUBSCRIBE_TAG, WSE_SUBSCRIBE_TAG_REF,
};
use crate::xml::messages::dpws::common::Types;
use crate::xml::messages::dpws::host::{Host, Hosted};
use crate::xml::messages::dpws::relationship::Relationship;
use crate::xml::messages::soap::envelope::SoapMessage;
use crate::xml::messages::soap::fault::Fault;
use crate::xml::messages::ws_eventing::subscribe::Subscribe;
use crate::xml::messages::ws_metadata_exchange::get_metadata::GetMetadata;
use crate::xml::messages::ws_metadata_exchange::metadata::{
    Location, Metadata, MetadataPayload, MetadataSection,
};
use async_trait::async_trait;
use bytes::Buf;
use common::oneshot_log_error;
use http::{header, StatusCode};
use http_body_util::BodyExt;
use protosdc_xml::{
    find_start_element_reader, ExpandedName, GenericXmlReaderComplexTypeRead, ParserError,
    XmlReader, XmlWriter,
};
use quick_xml::events::Event;

#[async_trait]
pub trait HostedService {
    fn service_id(&self) -> &'static str;

    fn endpoint_reference(&self) -> WsaEndpointReference;

    fn types(&self) -> Vec<ExpandedName>;

    fn wsdl(&self) -> &'static [u8];
}

impl<T: HostedService + ?Sized> HostedService for Box<T> {
    fn service_id(&self) -> &'static str {
        (**self).service_id()
    }

    fn endpoint_reference(&self) -> WsaEndpointReference {
        (**self).endpoint_reference()
    }

    fn types(&self) -> Vec<ExpandedName> {
        (**self).types()
    }

    fn wsdl(&self) -> &'static [u8] {
        (**self).wsdl()
    }
}

#[async_trait]
pub trait HostedServiceInfo {
    fn service_id(&self) -> &'static str;

    fn host_endpoint_reference(&self) -> WsaEndpointReference;

    fn host_types(&self) -> Vec<ExpandedName>;

    fn endpoint_reference(&self) -> WsaEndpointReference;

    fn types(&self) -> Vec<ExpandedName>;

    fn wsdl(&self) -> &'static [u8];

    fn wsdl_location(&self) -> HttpUri;
}

pub(crate) async fn hosted_service_handle<H, M, W, I, E>(
    service_handler: &H,
    ws_eventing_handler: &W,
    context_message: ContextMessage,
    metadata: &I,
    extension_handler_factory: E,
) -> Result<(), SoapError>
where
    H: RequestResponseHandlerTyped<M>,
    W: RequestResponseHandlerTyped<Subscribe>,
    M: SoapEventMessage + ComplexXmlTypeReadInner,
    I: HostedServiceInfo + 'static,
    E: ExtensionHandlerFactory + Send + Sync,
{
    let _scheme = context_message
        .request
        .uri()
        .scheme_str()
        .map(|it| it.to_string());
    let (parts, body) = context_message.request.into_parts();

    let transport_info = TransportInfo {
        // TODO: figure out why this is not present
        //   Note: it is not present because this information is unavailable on http server level,
        //         if necessary, retrieve it from the connection
        // scheme: scheme
        //     .ok_or(SoapError::SoapFault(simple_sender_fault(
        //         format!("No scheme in request with uri {}", parts.uri),
        //     )))?,
        local: context_message.receiver_addr,
        remote: context_message.sender_addr,
        peer_certificates: context_message.peer_certificates,
    };

    // validate content type
    let _content_type: AcceptableContentType = parts
        .headers
        .get(header::CONTENT_TYPE)
        .ok_or(SoapError::InvalidContentType)?
        .try_into()?;

    // start by parsing soap envelope and header
    let body_vec = body
        .collect()
        .await
        .map_err(|_| SoapError::ResponseBodyMissing)?
        .to_bytes();

    let mut buf = Vec::with_capacity(body_vec.len());
    let mut reader =
        XmlReader::create_custom(body_vec.reader(), extension_handler_factory.create());

    let soap_message: SoapMessage =
        find_start_element_reader!(SoapMessage, SOAP_ENVELOPE_TAG, reader, buf)?;

    // dispatch based on body
    loop {
        match reader.read_next(&mut buf) {
            Ok((ref ns, Event::Start(ref e))) => {
                let m_ns = M::ns_tag();
                break match (ns, e.local_name().into_inner()) {
                    SOAP_FAULT_TAG_REF => {
                        match Fault::from_xml_complex(SOAP_FAULT_TAG, e, &mut reader) {
                            Ok(fault) => Err(SoapError::SoapFault(fault)),
                            Err(err) => Err(SoapError::ParserError(err)),
                        }?;
                    }
                    // handle get metadata
                    MEX_GET_METADATA_TAG_REF => {
                        let _get_metadata =
                            GetMetadata::from_xml_complex(MEX_GET_METADATA_TAG, e, &mut reader)?;

                        let resp = process_get_metadata(metadata).await;

                        let header_builder = SoapHeaderBuilder::new()
                            .action(WSA_ACTION_GET_METADATA_RESPONSE_STR)
                            .relates_to_optional(soap_message.header.message_id.map(|it| it.value));

                        let msg = SoapMessage {
                            header: header_builder.build(),
                        };

                        let mut writer = XmlWriter::new(vec![]);
                        let body = msg
                            .to_xml_complex(&mut writer, resp)
                            .map_err(SoapError::from);

                        match body {
                            Ok(_) => {
                                oneshot_log_error(
                                    context_message.response_sender,
                                    ContextMessageResponse {
                                        status_code: StatusCode::OK,
                                        payload: create_soap_http_message(
                                            writer.into_inner().into(),
                                        ),
                                    },
                                );
                            }
                            Err(err) => {
                                let fault: Fault = err.into();
                                oneshot_log_error(context_message.response_sender, fault);
                            }
                        }
                        break;
                    }
                    // handle WS-Eventing Tags
                    // Dispatch to Subscription
                    WSE_SUBSCRIBE_TAG_REF => {
                        let subscribe =
                            Subscribe::from_xml_complex(WSE_SUBSCRIBE_TAG, e, &mut reader)?;
                        let response = ws_eventing_handler
                            .request_response(transport_info, soap_message.header, subscribe)
                            .await;

                        match response {
                            Ok(resp) => {
                                oneshot_log_error(
                                    context_message.response_sender,
                                    ContextMessageResponse {
                                        status_code: StatusCode::OK,
                                        payload: resp,
                                    },
                                );
                            }
                            Err(err) => {
                                let fault: Fault = err.into();
                                oneshot_log_error(context_message.response_sender, fault);
                            }
                        }
                    }
                    (x, _) if x == &m_ns => {
                        let msg =
                            <M as ComplexXmlTypeReadInner>::from_xml_complex(m_ns, e, &mut reader)?;

                        let response = service_handler
                            .request_response(transport_info, soap_message.header, msg)
                            .await?;

                        oneshot_log_error(
                            context_message.response_sender,
                            ContextMessageResponse {
                                status_code: StatusCode::OK,
                                payload: response,
                            },
                        );
                    }
                    (_, _) => Err(ParserError::UnexpectedParserStartState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: "EventSinkHandler".to_string(),
                    })?,
                };
            }
            Ok((ref _ns, Event::DocType(ref _e))) => {}
            Ok((ref _ns, Event::Decl(ref _e))) => {}
            Ok((ref _ns, Event::Comment(ref _e))) => {}
            other => Err(SoapError::ParserError(ParserError::UnexpectedParserEvent {
                event: format!("{:?}", other),
            }))?,
        }
    }

    Ok(())
}

async fn process_get_metadata<I: HostedServiceInfo + 'static>(info: &I) -> Metadata {
    let mut metadata = Metadata {
        attributes: Default::default(),
        metadata_sections: vec![],
        children: vec![],
    };

    metadata.metadata_sections.push(MetadataSection {
        attributes: Default::default(),
        payload: MetadataPayload::Relationship(create_relationship(info).await),
        dialect: DPWS_RELATIONSHIP_DIALECT.to_string(),
        identifier: None,
    });

    metadata.metadata_sections.push(MetadataSection {
        attributes: Default::default(),
        payload: MetadataPayload::Location(Location {
            location: Some(info.wsdl_location().to_string()),
        }),
        dialect: WSDL_DIALECT.to_string(),
        identifier: None,
    });

    metadata
}

async fn create_relationship<I: HostedServiceInfo + 'static>(info: &I) -> Relationship {
    let host = Host {
        attributes: Default::default(),
        endpoint_reference: info.host_endpoint_reference(),
        types: Some(Types {
            list: info.host_types().as_slice().into(),
        }),
        children: vec![],
    };

    let hosted = Hosted {
        attributes: Default::default(),
        endpoint_reference: vec![info.endpoint_reference()],
        types: Types {
            list: info.types().as_slice().into(),
        },
        service_id: info.service_id().to_string(),
        children: vec![],
    };

    Relationship {
        attributes: Default::default(),
        r#type: DPWS_TYPE_HOST.to_string(),
        host: vec![host],
        hosted: vec![hosted],
        children: vec![],
    }
}
