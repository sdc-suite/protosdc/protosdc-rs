use crate::common::extension_handling::{
    ExtensionHandlerFactory, ReplacementExtensionHandlerFactory,
};
use crate::xml::messages::addressing::{WsaAttributedURIType, WsaEndpointReference};
use crate::xml::messages::common::{DPWS_DEVICE_TYPE, MDPWS_MEDICAL_DEVICE_TYPE};
use crate::xml::messages::dpws::common::LocalizedStringType;
use crate::xml::messages::dpws::this::{ThisDevice, ThisModel};
use common::crypto::CryptoConfig;
use protosdc_xml::ExpandedName;
use std::collections::HashSet;
use std::net::SocketAddr;
use typed_builder::TypedBuilder;
use uuid::Uuid;

#[derive(TypedBuilder)]
pub struct DeviceSettings<EHF: ExtensionHandlerFactory + Send + Sync + 'static> {
    #[builder(default =
    WsaEndpointReference::builder()
        .address(WsaAttributedURIType::builder().value(format!("urn:uuid:{}", Uuid::new_v4())).build())
        .build(),
    )]
    pub endpoint_reference: WsaEndpointReference,
    pub network_interface: SocketAddr,
    #[builder(default =
    ThisDevice::builder()
        .friendly_name(vec![LocalizedStringType::builder()
            .lang("en")
            .value("protosdc-rs dpws friendly placeholder device")
            .build()
        ])
        .build(),
    )]
    pub this_device: ThisDevice,
    #[builder(default =
    ThisModel::builder()
        .manufacturer(vec![LocalizedStringType::builder()
            .lang("en")
            .value("protosdc-rs dpws placeholder manufacturer")
            .build(),
        ])
        .model_name(vec![LocalizedStringType::builder()
            .lang("en")
            .value("protosdc-rs dpws placeholder model")
            .build()
        ])
    .build(),
    )]
    pub this_model: ThisModel,
    #[builder(setter(into), default =
    std::collections::HashSet::from_iter(vec![MDPWS_MEDICAL_DEVICE_TYPE.into(), DPWS_DEVICE_TYPE.into()])
    )]
    pub types: HashSet<ExpandedName>,
    pub crypto: Option<CryptoConfig>,
    #[builder(default = Uuid::new_v4().to_string(), setter(into))]
    pub server_prefix: String,
    pub extension_handler_factory: EHF,
    #[builder(default, setter(into))]
    pub discovery_proxy_address: Option<String>,
}

/// Add default extension handler factory to typed builder.
impl<
        __EndpointReference,
        __NetworkInterface,
        __ThisDevice,
        __ThisModel,
        __Types,
        __Crypto,
        __ServerPrefix,
        __DiscoveryProxyAddress,
    >
    DeviceSettingsBuilder<
        ReplacementExtensionHandlerFactory,
        (
            __EndpointReference,
            __NetworkInterface,
            __ThisDevice,
            __ThisModel,
            __Types,
            __Crypto,
            __ServerPrefix,
            (),
            __DiscoveryProxyAddress,
        ),
    >
{
    #[allow(clippy::type_complexity)] // complexity comes from typed builder
    pub fn extension_handler_factory_default(
        self,
    ) -> DeviceSettingsBuilder<
        ReplacementExtensionHandlerFactory,
        (
            __EndpointReference,
            __NetworkInterface,
            __ThisDevice,
            __ThisModel,
            __Types,
            __Crypto,
            __ServerPrefix,
            (ReplacementExtensionHandlerFactory,),
            __DiscoveryProxyAddress,
        ),
    > {
        self.extension_handler_factory(ReplacementExtensionHandlerFactory)
    }
}
