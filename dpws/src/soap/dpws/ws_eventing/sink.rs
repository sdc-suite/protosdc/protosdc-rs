use crate::common::extension_handling::ExtensionHandlerFactory;
use crate::http::server::common::{ContextMessage, ContextMessageResponse};
use crate::soap::common::{AcceptableContentType, SoapError, MEDIA_TYPE_SOAP_HEADER};
use crate::soap::dpws::client::notification_sink::{
    NotificationSink, NotificationSinkError, NotificationSinkTyped,
};
use crate::soap::eventing::ComplexXmlTypeReadInner;
use crate::xml::messages::common::{
    SoapEventMessage, SOAP_ENVELOPE_TAG, SOAP_FAULT_TAG, SOAP_FAULT_TAG_REF,
    WSE_SUBSCRIPTION_END_TAG, WSE_SUBSCRIPTION_END_TAG_REF,
};
use crate::xml::messages::soap::envelope::SoapMessage;
use crate::xml::messages::soap::fault::Fault;
use crate::xml::messages::ws_eventing::subscription_end::SubscriptionEnd;
use async_trait::async_trait;
use bytes::Bytes;
use common::oneshot_log_error;
use http::{header, Response, StatusCode};
use http_body_util::BodyExt;
use protosdc_xml::{
    find_start_element_reader, GenericXmlReaderComplexTypeRead, ParserError, XmlReader,
};
use quick_xml::events::Event;
use std::marker::PhantomData;

pub struct WsEventingSink<H, M, EHF>
where
    H: NotificationSinkTyped<M>,
    M: SoapEventMessage + ComplexXmlTypeReadInner,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    pub(crate) phantom: PhantomData<M>,
    pub(crate) event_handler: H,
    pub(crate) extension_handler_factory: EHF,
}

#[async_trait]
pub trait EventHandler<M>
where
    M: ComplexXmlTypeReadInner,
{
    async fn process_message(message: M) -> Result<(), SoapError>;
}

async fn handle<H, M, EHF>(
    ws_eventing_handler: &H,
    context_message: ContextMessage,
    extension_handler_factory: &EHF,
) -> Result<(), NotificationSinkError>
where
    H: NotificationSinkTyped<M>,
    M: SoapEventMessage + ComplexXmlTypeReadInner,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    let (parts, body) = context_message.request.into_parts();

    // validate content type
    let _content_type: AcceptableContentType = parts
        .headers
        .get(header::CONTENT_TYPE)
        .ok_or(SoapError::InvalidContentType)?
        .try_into()?;

    // start by parsing soap envelope and header
    let body_vec = body
        .collect()
        .await
        .map_err(|_| SoapError::ResponseBodyMissing)?
        .to_bytes();
    let mut buf = Vec::with_capacity(body_vec.len());
    let mut reader =
        XmlReader::create_custom(body_vec.as_ref(), extension_handler_factory.create());

    let _soap_message: SoapMessage =
        find_start_element_reader!(SoapMessage, SOAP_ENVELOPE_TAG, reader, buf)?;

    // dispatch based on body
    loop {
        match reader.read_next(&mut buf) {
            Ok((ref ns, Event::Start(ref e))) => {
                let m_ns = M::ns_tag();
                break match (ns, e.local_name().into_inner()) {
                    // handle WS-Eventing Tags
                    SOAP_FAULT_TAG_REF => {
                        match Fault::from_xml_complex(SOAP_FAULT_TAG, e, &mut reader) {
                            Ok(fault) => Err(SoapError::SoapFault(fault)),
                            Err(err) => Err(SoapError::ParserError(err)),
                        }?;
                    }
                    // Dispatch to Subscription
                    WSE_SUBSCRIPTION_END_TAG_REF => {
                        let subscription_end = SubscriptionEnd::from_xml_complex(
                            WSE_SUBSCRIPTION_END_TAG,
                            e,
                            &mut reader,
                        )?;
                        Err(NotificationSinkError::SubscriptionEnd {
                            status: subscription_end.status,
                        })?;
                    }
                    (x, _) if x == &m_ns => {
                        let msg =
                            <M as ComplexXmlTypeReadInner>::from_xml_complex(m_ns, e, &mut reader)?;
                        ws_eventing_handler.receive_notification(msg).await?;

                        // respond
                        let mut payload: Response<Bytes> = Default::default();

                        // TODO: These are probably stupid for notifications?
                        payload
                            .headers_mut()
                            .insert(header::ACCEPT, MEDIA_TYPE_SOAP_HEADER.clone());
                        payload
                            .headers_mut()
                            .insert(header::CONTENT_TYPE, MEDIA_TYPE_SOAP_HEADER.clone());

                        oneshot_log_error(
                            context_message.response_sender,
                            ContextMessageResponse {
                                status_code: StatusCode::ACCEPTED,
                                payload,
                            },
                        );
                    }
                    (_, _) => Err(ParserError::UnexpectedParserStartState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: "EventSinkHandler".to_string(),
                    })?,
                };
            }
            Ok((ref _ns, Event::DocType(ref _e))) => {}
            Ok((ref _ns, Event::Decl(ref _e))) => {}
            Ok((ref _ns, Event::Comment(ref _e))) => {}
            other => Err(SoapError::ParserError(ParserError::UnexpectedParserEvent {
                event: format!("{:?}", other),
            }))?,
        }
    }

    Ok(())
}

#[async_trait]
impl<H, M, EHF> NotificationSink for WsEventingSink<H, M, EHF>
where
    H: NotificationSinkTyped<M> + Send + Sync,
    M: SoapEventMessage + ComplexXmlTypeReadInner + Send + Sync,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    async fn receive_notification(
        &self,
        notification: ContextMessage,
    ) -> Result<(), NotificationSinkError> {
        handle(
            &self.event_handler,
            notification,
            &self.extension_handler_factory,
        )
        .await
    }
}

impl<H, M, EHF> WsEventingSink<H, M, EHF>
where
    H: NotificationSinkTyped<M>,
    M: SoapEventMessage + ComplexXmlTypeReadInner,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    pub fn new(handler: H, extension_handler_factory: EHF) -> Self {
        Self {
            phantom: Default::default(),
            event_handler: handler,
            extension_handler_factory,
        }
    }
}
