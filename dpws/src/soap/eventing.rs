use crate::xml::messages::biceps_ext::{
    BICEPS_MSG_NAMESPACE, BICEPS_MSG_NAMESPACE_BYTES, DESCRIPTION_MODIFICATION_REPORT_TAG,
    EPISODIC_ALERT_REPORT_TAG, EPISODIC_COMPONENT_REPORT_TAG, EPISODIC_CONTEXT_REPORT_TAG,
    EPISODIC_METRIC_REPORT_TAG, EPISODIC_OPERATIONAL_STATE_REPORT_TAG,
    OPERATION_INVOKED_REPORT_TAG, SYSTEM_ERROR_REPORT_TAG, WAVEFORM_STREAM_TAG,
};
use crate::xml::messages::common::{
    SoapEventMessage, WSE_GET_STATUS_TAG, WSE_RENEW_TAG, WSE_SUBSCRIBE_TAG, WSE_UNSUBSCRIBE_TAG,
    WS_EVENTING_NAMESPACE, WS_EVENTING_NAMESPACE_BYTES,
};
use crate::xml::messages::ws_eventing::renew::{GetStatus, Renew, Unsubscribe};
use crate::xml::messages::ws_eventing::subscribe::Subscribe;
use protosdc_biceps::biceps::{
    DescriptionModificationReport, EpisodicAlertReport, EpisodicComponentReport,
    EpisodicContextReport, EpisodicMetricReport, EpisodicOperationalStateReport,
    OperationInvokedReport, PeriodicAlertReport, PeriodicComponentReport, PeriodicContextReport,
    PeriodicMetricReport, PeriodicOperationalStateReport, SystemErrorReport, WaveformStream,
};
use protosdc_biceps::types::AnyContent;
use protosdc_xml::{GenericXmlReaderComplexTypeRead, ParserError};
use quick_xml::events::BytesStart;
use quick_xml::name::{Namespace, ResolveResult};
use std::io::BufRead;

// for server dispatching
#[derive(Debug, Clone)]
pub enum WsEventingRequest {
    Subscribe(Box<Subscribe>),
    GetStatus(Box<GetStatus>),
    Renew(Box<Renew>),
    Unsubscribe(Box<Unsubscribe>),
}

#[derive(Debug, Clone)]
pub enum DpwsEpisodicReport {
    Alert(Box<EpisodicAlertReport>),
    Component(Box<EpisodicComponentReport>),
    Context(Box<EpisodicContextReport>),
    Metric(Box<EpisodicMetricReport>),
    Operation(Box<EpisodicOperationalStateReport>),
    Waveform(Box<WaveformStream>),
    Description(Box<DescriptionModificationReport>),
    OperationInvokedReport(Box<OperationInvokedReport>),
    SystemErrorReport(Box<SystemErrorReport>),
}

#[derive(Debug, Clone)]
pub enum PeriodicReport {
    Alert(Box<PeriodicAlertReport>),
    Component(Box<PeriodicComponentReport>),
    Context(Box<PeriodicContextReport>),
    Metric(Box<PeriodicMetricReport>),
    Operation(Box<PeriodicOperationalStateReport>),
}

#[derive(Debug)]
pub enum BicepsReport {
    Episodic(DpwsEpisodicReport),
    Periodic(PeriodicReport),
}

#[derive(Debug)]
pub enum EventSinkMessages {
    WsEventing(WsEventingRequest),
    Biceps(BicepsReport),
}

pub trait ComplexXmlTypeReadInner: Sized {
    fn from_xml_complex<B: BufRead>(
        tag_namespace: ResolveResult<'static>,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Box<(dyn AnyContent + Send + Sync + 'static)>>,
    ) -> Result<Self, ParserError>;
}

impl ComplexXmlTypeReadInner for WsEventingRequest {
    fn from_xml_complex<B: BufRead>(
        tag_ns: ResolveResult<'static>,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Box<(dyn AnyContent + Send + Sync + 'static)>>,
    ) -> Result<Self, ParserError> {
        // dispatch based on tag
        match (tag_ns, event.local_name().into_inner()) {
            WSE_SUBSCRIBE_TAG => Ok(Self::Subscribe(Box::new(Subscribe::from_xml_complex(
                WSE_SUBSCRIBE_TAG,
                event,
                reader,
            )?))),
            WSE_GET_STATUS_TAG => Ok(Self::GetStatus(Box::new(GetStatus::from_xml_complex(
                WSE_GET_STATUS_TAG,
                event,
                reader,
            )?))),
            WSE_RENEW_TAG => Ok(Self::Renew(Box::new(Renew::from_xml_complex(
                WSE_RENEW_TAG,
                event,
                reader,
            )?))),
            WSE_UNSUBSCRIBE_TAG => Ok(Self::Unsubscribe(Box::new(Unsubscribe::from_xml_complex(
                WSE_UNSUBSCRIBE_TAG,
                event,
                reader,
            )?))),
            _ => Err(ParserError::OneOfParserFallthrough {
                parser_name: "Event did not contain a WSE Request".to_string(),
            }),
        }
    }
}

impl ComplexXmlTypeReadInner for BicepsReport {
    fn from_xml_complex<B: BufRead>(
        tag_ns: ResolveResult<'static>,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Box<(dyn AnyContent + Send + Sync + 'static)>>,
    ) -> Result<Self, ParserError> {
        match (tag_ns, event.local_name().into_inner()) {
            EPISODIC_ALERT_REPORT_TAG => Ok(Self::Episodic(DpwsEpisodicReport::Alert(Box::new(
                EpisodicAlertReport::from_xml_complex(EPISODIC_ALERT_REPORT_TAG, event, reader)?,
            )))),
            EPISODIC_COMPONENT_REPORT_TAG => Ok(Self::Episodic(DpwsEpisodicReport::Component(
                Box::new(EpisodicComponentReport::from_xml_complex(
                    EPISODIC_COMPONENT_REPORT_TAG,
                    event,
                    reader,
                )?),
            ))),
            EPISODIC_CONTEXT_REPORT_TAG => Ok(Self::Episodic(DpwsEpisodicReport::Context(
                Box::new(EpisodicContextReport::from_xml_complex(
                    EPISODIC_CONTEXT_REPORT_TAG,
                    event,
                    reader,
                )?),
            ))),
            EPISODIC_METRIC_REPORT_TAG => Ok(Self::Episodic(DpwsEpisodicReport::Metric(Box::new(
                EpisodicMetricReport::from_xml_complex(EPISODIC_METRIC_REPORT_TAG, event, reader)?,
            )))),
            EPISODIC_OPERATIONAL_STATE_REPORT_TAG => {
                Ok(Self::Episodic(DpwsEpisodicReport::Operation(Box::new(
                    EpisodicOperationalStateReport::from_xml_complex(
                        EPISODIC_OPERATIONAL_STATE_REPORT_TAG,
                        event,
                        reader,
                    )?,
                ))))
            }
            WAVEFORM_STREAM_TAG => Ok(Self::Episodic(DpwsEpisodicReport::Waveform(Box::new(
                WaveformStream::from_xml_complex(WAVEFORM_STREAM_TAG, event, reader)?,
            )))),
            DESCRIPTION_MODIFICATION_REPORT_TAG => {
                Ok(Self::Episodic(DpwsEpisodicReport::Description(Box::new(
                    DescriptionModificationReport::from_xml_complex(
                        DESCRIPTION_MODIFICATION_REPORT_TAG,
                        event,
                        reader,
                    )?,
                ))))
            }
            OPERATION_INVOKED_REPORT_TAG => {
                Ok(Self::Episodic(DpwsEpisodicReport::OperationInvokedReport(
                    Box::new(OperationInvokedReport::from_xml_complex(
                        OPERATION_INVOKED_REPORT_TAG,
                        event,
                        reader,
                    )?),
                )))
            }
            SYSTEM_ERROR_REPORT_TAG => Ok(Self::Episodic(DpwsEpisodicReport::SystemErrorReport(
                Box::new(SystemErrorReport::from_xml_complex(
                    SYSTEM_ERROR_REPORT_TAG,
                    event,
                    reader,
                )?),
            ))),
            _ => Err(ParserError::OneOfParserFallthrough {
                parser_name:
                    "Event did not contain an episodic report (periodic currently unhandled)"
                        .to_string(),
            }),
        }
    }
}

impl ComplexXmlTypeReadInner for EventSinkMessages {
    fn from_xml_complex<B: BufRead>(
        tag_ns: ResolveResult<'static>,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Box<(dyn AnyContent + Send + Sync + 'static)>>,
    ) -> Result<Self, ParserError> {
        match tag_ns {
            ResolveResult::Bound(Namespace(WS_EVENTING_NAMESPACE_BYTES)) => Ok(Self::WsEventing(
                WsEventingRequest::from_xml_complex(tag_ns, event, reader)?,
            )),
            ResolveResult::Bound(Namespace(BICEPS_MSG_NAMESPACE_BYTES)) => Ok(Self::Biceps(
                BicepsReport::from_xml_complex(tag_ns, event, reader)?,
            )),
            _ => Err(ParserError::OneOfParserFallthrough {
                parser_name: "Event did not contain a message for the event sink".to_string(),
            }),
        }
    }
}

impl SoapEventMessage for BicepsReport {
    fn ns_tag() -> ResolveResult<'static> {
        ResolveResult::Bound(Namespace(BICEPS_MSG_NAMESPACE_BYTES))
    }

    fn ns_tag_str() -> Option<&'static str> {
        Some(BICEPS_MSG_NAMESPACE)
    }
}

impl SoapEventMessage for WsEventingRequest {
    fn ns_tag() -> ResolveResult<'static> {
        ResolveResult::Bound(Namespace(WS_EVENTING_NAMESPACE_BYTES))
    }

    fn ns_tag_str() -> Option<&'static str> {
        Some(WS_EVENTING_NAMESPACE)
    }
}
