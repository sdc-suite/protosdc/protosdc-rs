use crate::common::extension_handling::ExtensionHandlerFactory;
use crate::http::client::common::HttpClient;
use crate::soap::common::{AcceptableContentType, SoapError, MEDIA_TYPE_SOAP_HEADER};
use crate::xml::common::{Others, SoapHeaderBuilder};
use crate::xml::messages::addressing::{WsaEndpointReference, XmlElement};
use crate::xml::messages::common::{
    SoapElement, SOAP_BODY_TAG_REF, SOAP_ENVELOPE_TAG, SOAP_ENVELOPE_TAG_REF, SOAP_FAULT_TAG,
    SOAP_FAULT_TAG_REF,
};
use crate::xml::messages::soap::envelope::SoapMessage;
use crate::xml::messages::soap::fault::Fault;
use async_trait::async_trait;
use bytes::Bytes;
use const_format::concatcp;
use http::header::HeaderName;
use http::{header, HeaderMap, HeaderValue, Response};
use protosdc_xml::{
    find_start_element_reader, GenericXmlReaderComplexTypeRead, ParserError, XmlReader,
};
use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};
use quick_xml::events::Event;

const PROTOSDC_DPWS_VERSION: &str = env!("CARGO_PKG_VERSION");
const PROTOSDC_DPWS_COMMIT: &str = env!("VERGEN_GIT_SHA");
const PROTOSDC_DPWS_BRANCH: &str = env!("VERGEN_GIT_BRANCH");
const PROTOSDC_DPWS_COMMENT: &str = concatcp!(
    "protosdc-rs dpws ",
    PROTOSDC_DPWS_VERSION,
    " (",
    PROTOSDC_DPWS_COMMIT,
    ") ",
    PROTOSDC_DPWS_BRANCH
);

static USER_AGENT_KEY: HeaderName = HeaderName::from_static("x-user-agent");
static USER_AGENT_VALUE: HeaderValue = HeaderValue::from_static(PROTOSDC_DPWS_COMMENT);

#[async_trait]
pub trait SoapClient {
    /// Sends a request to the soap client target endpoint reference and requires a response.
    ///
    /// # Arguments
    ///
    /// * `header_builder`: Soap Message header. To will be overwritten by client, reference parameters added if required.
    /// * `body`: body element serialized into the soap Body tag upon sending.
    ///
    /// returns: Result<(SoapMessage, RESP), SoapError>
    async fn request_response<REQ, RESP>(
        &self,
        header_builder: SoapHeaderBuilder<Others>,
        body: REQ,
    ) -> Result<(SoapMessage, RESP), SoapError>
    where
        REQ: ComplexXmlTypeWrite + Send + Sync,
        RESP: SoapElement + Send + Sync;

    /// Sends a request to the soap client target endpoint reference not requiring content in the soap:Body element.
    ///
    /// If content is present, it will be parsed according to `RESP` and must be valid for `RESP`.
    /// `NoopResponse` can be used to type this method when not expecting a body.
    ///
    /// # Arguments
    ///
    /// * `header_builder`: Soap Message header. To will be overwritten by client, reference parameters added if required.
    /// * `body`: body element serialized into the soap Body tag upon sending.
    ///
    /// returns: Result<(SoapMessage, RESP), SoapError>
    async fn request_response_opt_body<REQ, RESP>(
        &self,
        header_builder: SoapHeaderBuilder<Others>,
        body: REQ,
    ) -> Result<(SoapMessage, Option<RESP>), SoapError>
    where
        REQ: ComplexXmlTypeWrite + Send + Sync,
        RESP: SoapElement + Send + Sync;

    /// Sends a request to the soap client target endpoint reference not expecting a SOAP response at all.
    ///
    /// # Arguments
    ///
    /// * `body`: pre-serialized soap message to send out
    ///
    /// returns: Result<(), SoapError>
    async fn notify_serialized(&self, body: Bytes) -> Result<(), SoapError>;

    /// Sends a request to the soap client target endpoint reference not requiring content in the soap:Body element.
    ///
    /// If content is present, it will be parsed according to `RESP` and must be valid for `RESP`.
    /// `NoopResponse` can be used to type this method when not expecting a body.
    ///
    /// # Arguments
    ///
    /// * `body`: pre-serialized soap message to send out
    ///
    /// returns: Result<(SoapMessage, RESP), SoapError>
    async fn request_response_opt_body_serialized<RESP>(
        &self,
        body: Bytes,
    ) -> Result<(SoapMessage, Option<RESP>), SoapError>
    where
        RESP: SoapElement + Send + Sync;

    fn target_url(&self) -> &str;
}

/// A `SoapClient` that can be used to create a new `SoapClient` with a new target address but the
/// same http client below it.
pub trait SoapClientCloneable: SoapClient {
    fn new_url(&self, url: impl AsRef<str>) -> Self;
    fn new_epr(&self, epr: WsaEndpointReference) -> Self;
}

#[derive(Clone, Debug)]
pub struct SoapClientImpl<HTTP, EHF>
where
    HTTP: HttpClient + Sync + Clone,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    http_client: HTTP,
    url: String,
    reference_parameters: Vec<XmlElement>,
    pub(crate) extension_handler_factory: EHF,
}

impl<HTTP, EHF> SoapClientImpl<HTTP, EHF>
where
    HTTP: HttpClient + Send + Sync + Clone,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    pub fn new(client: HTTP, addr: WsaEndpointReference, extension_handler_factory: EHF) -> Self {
        Self {
            http_client: client,
            url: addr.address.value,
            reference_parameters: addr
                .reference_parameters
                .map(|it| it.parameters)
                .unwrap_or_default(),
            extension_handler_factory,
        }
    }

    pub fn new_string(client: HTTP, addr: impl AsRef<str>, extension_handler_factory: EHF) -> Self {
        Self {
            http_client: client,
            url: addr.as_ref().to_string(),
            reference_parameters: Vec::with_capacity(0),
            extension_handler_factory,
        }
    }

    async fn soap_request(&self, body: Bytes) -> Result<Response<Bytes>, SoapError> {
        let mut headers = HeaderMap::new();
        headers.insert(header::ACCEPT, MEDIA_TYPE_SOAP_HEADER.clone());
        headers.insert(header::CONTENT_TYPE, MEDIA_TYPE_SOAP_HEADER.clone());
        headers.insert(USER_AGENT_KEY.clone(), USER_AGENT_VALUE.clone());

        Ok(self.http_client.post(headers, &self.url, body).await?)
    }

    #[cfg(feature = "http_access")]
    pub async fn send_soap_request(&self, body: Bytes) -> Result<Response<Bytes>, SoapError> {
        self.soap_request(body).await
    }
}

#[async_trait]
impl<HTTP, EHF> SoapClient for SoapClientImpl<HTTP, EHF>
where
    HTTP: HttpClient + Send + Sync + Clone,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    async fn request_response<REQ, RESP>(
        &self,
        header_builder: SoapHeaderBuilder<Others>,
        body: REQ,
    ) -> Result<(SoapMessage, RESP), SoapError>
    where
        REQ: ComplexXmlTypeWrite + Send + Sync,
        RESP: SoapElement + Send + Sync,
    {
        let (msg, body_opt) = self.request_response_opt_body(header_builder, body).await?;

        Ok((msg, body_opt.ok_or(SoapError::ResponseBodyMissing)?))
    }

    async fn request_response_opt_body<REQ, RESP>(
        &self,
        header_builder: SoapHeaderBuilder<Others>,
        body: REQ,
    ) -> Result<(SoapMessage, Option<RESP>), SoapError>
    where
        REQ: ComplexXmlTypeWrite + Send + Sync,
        RESP: SoapElement + Send + Sync,
    {
        let msg = SoapMessage {
            header: header_builder
                .to(&self.url)
                .reference_parameters_response(&self.reference_parameters)
                .build(),
        };

        let mut writer = XmlWriter::new(Vec::new());
        msg.to_xml_complex(&mut writer, body)?;
        let data: Bytes = writer.into_inner().into();

        self.request_response_opt_body_serialized(data).await
    }

    async fn notify_serialized(&self, body: Bytes) -> Result<(), SoapError> {
        let response = self.soap_request(body).await?;

        let (parts, body) = response.into_parts();

        if parts.status.is_success() {
            return Ok(());
        }

        if body.is_empty() {
            Err(SoapError::ErrorResponse(parts.status))?;
        }

        // read body and put it into an error
        let text = String::from_utf8_lossy(body.as_ref());

        Err(SoapError::ErrorResponseBody {
            status: parts.status,
            body_string: text.to_string(),
        })
    }

    async fn request_response_opt_body_serialized<RESP>(
        &self,
        body: Bytes,
    ) -> Result<(SoapMessage, Option<RESP>), SoapError>
    where
        RESP: SoapElement + Send + Sync,
    {
        let response = self.soap_request(body).await?;

        let (parts, body) = response.into_parts();

        // validate content type
        let _content_type: AcceptableContentType = parts
            .headers
            .get(header::CONTENT_TYPE)
            .ok_or(SoapError::InvalidContentType)?
            .try_into()?;

        // check status code and size
        if !parts.status.is_success() && body.is_empty() {
            Err(SoapError::ErrorResponse(parts.status))?;
        }

        let mut reader =
            XmlReader::create_custom(body.as_ref(), self.extension_handler_factory.create());
        let mut buf = Vec::with_capacity(body.len());

        // start by parsing soap envelope and header
        let soap_message: SoapMessage =
            find_start_element_reader!(SoapMessage, SOAP_ENVELOPE_TAG, reader, buf)?;

        // check next event and dispatch to parser
        let resp: Option<RESP> =
            loop {
                match reader.read_next(&mut buf) {
                    Ok((ref ns, Event::Start(ref e))) => {
                        break match (ns, e.local_name().into_inner()) {
                            SOAP_FAULT_TAG_REF => Err(SoapError::SoapFault(
                                Fault::from_xml_complex(SOAP_FAULT_TAG, e, &mut reader)?,
                            )),
                            _ => Ok(Some(RESP::from_xml_complex(RESP::tag(), e, &mut reader)?)),
                        }
                    }
                    Ok((ref ns, Event::End(ref e))) => match (ns, e.local_name().into_inner()) {
                        SOAP_ENVELOPE_TAG_REF => {
                            break Ok(None);
                        }
                        SOAP_BODY_TAG_REF => {
                            break Ok(None);
                        }
                        _ => {
                            break Err(ParserError::UnexpectedParserEvent {
                                event: format!("End: {:?}", e.local_name()),
                            }
                            .into())
                        }
                    },
                    Ok((ref _ns, Event::DocType(ref _e))) => {}
                    Ok((ref _ns, Event::Decl(ref _e))) => {}
                    Ok((ref _ns, Event::Comment(ref _e))) => {}
                    other => {
                        break Err(ParserError::UnexpectedParserEvent {
                            event: format!("other {:?}", other),
                        }
                        .into())
                    }
                }
            }?;

        Ok((soap_message, resp))
    }

    fn target_url(&self) -> &str {
        &self.url
    }
}

impl<HTTP, EHF> SoapClientCloneable for SoapClientImpl<HTTP, EHF>
where
    HTTP: HttpClient + Send + Sync + Clone,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    fn new_url(&self, url: impl AsRef<str>) -> Self {
        Self::new_string(
            self.http_client.clone(),
            url,
            self.extension_handler_factory.clone(),
        )
    }

    fn new_epr(&self, epr: WsaEndpointReference) -> Self {
        Self::new(
            self.http_client.clone(),
            epr,
            self.extension_handler_factory.clone(),
        )
    }
}
