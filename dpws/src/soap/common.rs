use crate::http::client::common::HttpClientError;
use crate::http::server::common::{ContextMessage, RegistryError};
use crate::xml::messages::common::{
    WS_ADDRESSING_NAMESPACE, WS_EVENTING_NAMESPACE, WS_MEX_NAMESPACE, WS_TRANSFER_NAMESPACE,
};
use crate::xml::messages::soap::fault::{Fault, FaultCode, FaultCodeEnum, FaultReason};
use async_trait::async_trait;
use const_format::concatcp;
use http::{header, HeaderMap, HeaderValue, Response, StatusCode};
use protosdc::provider::localization_storage::LocalizationStorageError;
use protosdc::provider::mapper::MappingError;
use protosdc_xml::ParserError;
use protosdc_xml::WriterError;
use thiserror::Error;

pub const MEDIA_TYPE_SOAP: &str = "application/soap+xml";
pub const MEDIA_TYPE_WSDL: &str = "text/xml";
pub const MEDIA_TYPE_XML: &str = "application/xml";
pub const CHARSET_UTF_8: &str = "charset=utf-8";

pub const MEDIA_TYPE_SOAP_UTF_8: &str = concatcp!(MEDIA_TYPE_SOAP, ";", CHARSET_UTF_8);

pub static MEDIA_TYPE_SOAP_HEADER: HeaderValue = HeaderValue::from_static(MEDIA_TYPE_SOAP);
pub static MEDIA_TYPE_SOAP_HEADER_UTF_8: HeaderValue =
    HeaderValue::from_static(MEDIA_TYPE_SOAP_UTF_8);
pub static MEDIA_TYPE_WSDL_HEADER: HeaderValue = HeaderValue::from_static(MEDIA_TYPE_WSDL);
pub static MEDIA_TYPE_XML_HEADER: HeaderValue = HeaderValue::from_static(MEDIA_TYPE_XML);

pub const SOAP_FAULT_ACTION_STR: &str = concatcp!(WS_ADDRESSING_NAMESPACE, "/fault");

pub const WSA_ACTION_GET_METADATA_REQUEST_STR: &str =
    concatcp!(WS_MEX_NAMESPACE, "/GetMetadata/Request");
pub const WSA_ACTION_GET_METADATA_RESPONSE_STR: &str =
    concatcp!(WS_MEX_NAMESPACE, "/GetMetadata/Response");

pub const WSE_ACTION_SUBSCRIBE_STR: &str = concatcp!(WS_EVENTING_NAMESPACE, "/Subscribe");
pub const WSE_ACTION_SUBSCRIBE_RESPONSE_STR: &str =
    concatcp!(WS_EVENTING_NAMESPACE, "/SubscribeResponse");
pub const WSE_ACTION_RENEW_STR: &str = concatcp!(WS_EVENTING_NAMESPACE, "/Renew");
pub const WSE_ACTION_RENEW_RESPONSE_STR: &str = concatcp!(WS_EVENTING_NAMESPACE, "/RenewResponse");
pub const WSE_ACTION_GET_STATUS_STR: &str = concatcp!(WS_EVENTING_NAMESPACE, "/GetStatus");
pub const WSE_ACTION_GET_STATUS_RESPONSE_STR: &str =
    concatcp!(WS_EVENTING_NAMESPACE, "/GetStatusResponse");
pub const WSE_ACTION_UNSUBSCRIBE_STR: &str = concatcp!(WS_EVENTING_NAMESPACE, "/Unsubscribe");
pub const WSE_ACTION_UNSUBSCRIBE_RESPONSE_STR: &str =
    concatcp!(WS_EVENTING_NAMESPACE, "/UnsubscribeResponse");
pub const WSE_ACTION_SUBSCRIPTION_END_STR: &str =
    concatcp!(WS_EVENTING_NAMESPACE, "/SubscriptionEnd");

pub const WSE_STATUS_DELIVERY_FAILURE: &str = concatcp!(WS_EVENTING_NAMESPACE, "/DeliveryFailure");
pub const WSE_STATUS_SOURCE_SHUTTING_DOWN: &str =
    concatcp!(WS_EVENTING_NAMESPACE, "/SourceShuttingDown");
pub const WSE_STATUS_SOURCE_CANCELLING: &str =
    concatcp!(WS_EVENTING_NAMESPACE, "/SourceCancelling");

pub const WST_ACTION_GET: &str = concatcp!(WS_TRANSFER_NAMESPACE, "/Get");
pub const WST_ACTION_GET_RESPONSE: &str = concatcp!(WS_TRANSFER_NAMESPACE, "/GetResponse");

#[derive(Debug, Error)]
pub enum SoapError {
    #[error("A soap fault occurred: {0:?}")]
    SoapFault(Fault),
    #[error("A transport error occurred")]
    TransportError,
    #[error(transparent)]
    WriterError(#[from] WriterError),
    #[error(transparent)]
    ParserError(#[from] ParserError),
    #[error(transparent)]
    HttpClientError(#[from] HttpClientError),
    #[error("Invalid content type")]
    InvalidContentType,
    #[error("Response contained no body and a status code {0} indicating an error")]
    ErrorResponse(StatusCode),
    #[error("Response contained status code {status} indicating an error, body: {body_string}")]
    ErrorResponseBody {
        status: StatusCode,
        body_string: String,
    },
    #[error(transparent)]
    RegistryError(#[from] RegistryError),
    #[error("Response body is missing")]
    ResponseBodyMissing,
    #[error(transparent)]
    ProviderMappingError(#[from] MappingError),
    #[error("An error occurred: {0}")]
    Other(String),
}

impl From<LocalizationStorageError> for SoapError {
    fn from(value: LocalizationStorageError) -> Self {
        match value {
            LocalizationStorageError::UnknownLanguage(err) => SoapError::SoapFault(
                Fault::builder()
                    .code(FaultCode::builder().value(FaultCodeEnum::Sender).build())
                    .reason(FaultReason::simple_text("en", err))
                    .build(),
            ),
            LocalizationStorageError::UnsupportedQuery(err) => SoapError::SoapFault(
                Fault::builder()
                    .code(FaultCode::builder().value(FaultCodeEnum::Sender).build())
                    .reason(FaultReason::simple_text("en", err))
                    .build(),
            ),
        }
    }
}

pub(crate) fn simple_fault(text: impl Into<String>, code: FaultCodeEnum) -> Fault {
    Fault::builder()
        .code(FaultCode::builder().value(code).build())
        .reason(FaultReason::simple_text("en", text))
        .build()
}

impl From<SoapError> for Fault {
    fn from(soap_error: SoapError) -> Fault {
        match soap_error {
            SoapError::SoapFault(f) => f,
            SoapError::TransportError => simple_fault(
                "A transport error occurred".to_string(),
                FaultCodeEnum::Sender,
            ),
            SoapError::WriterError(err) => simple_fault(
                format!("An error occurred while writing a message: {:#?}", err),
                FaultCodeEnum::Receiver,
            ),
            SoapError::ParserError(err) => simple_fault(
                format!("An error occurred while parsing a message: {:#?}", err),
                FaultCodeEnum::Sender,
            ),
            SoapError::HttpClientError(err) => simple_fault(
                format!("A transport error occurred: {:#?}", err),
                FaultCodeEnum::Receiver,
            ),
            SoapError::InvalidContentType => {
                simple_fault(format!("{:?}", soap_error), FaultCodeEnum::Sender)
            }
            SoapError::ErrorResponse(_) => {
                simple_fault(format!("{:?}", soap_error), FaultCodeEnum::Sender)
            }
            SoapError::RegistryError(err) => simple_fault(
                format!(
                    "An error occurred communication with the server registry: {:#?}",
                    err
                ),
                FaultCodeEnum::Receiver,
            ),
            SoapError::ResponseBodyMissing => {
                simple_fault(format!("{:?}", soap_error), FaultCodeEnum::Sender)
            }
            SoapError::ProviderMappingError(err) => simple_fault(
                format!("An error occurred while parsing a message: {:#?}", err),
                FaultCodeEnum::Sender,
            ),
            SoapError::ErrorResponseBody { .. } => {
                simple_fault(format!("{:?}", soap_error), FaultCodeEnum::Sender)
            }
            SoapError::Other(text) => simple_fault(
                format!("An unknown error occurred: {:?}", text),
                FaultCodeEnum::Receiver,
            ),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum AcceptableContentType {
    SOAP,
    WSDL,
    XML,
}

impl TryFrom<&HeaderValue> for AcceptableContentType {
    type Error = SoapError;

    fn try_from(value: &HeaderValue) -> Result<Self, Self::Error> {
        let prefix = value.to_str().map(|it| it.split(';').next());
        match prefix {
            Ok(Some(MEDIA_TYPE_SOAP)) => Ok(Self::SOAP),
            Ok(Some(MEDIA_TYPE_WSDL)) => Ok(Self::WSDL),
            Ok(Some(MEDIA_TYPE_XML)) => Ok(Self::XML),
            _ => Err(SoapError::InvalidContentType),
        }
    }
}

#[async_trait]
pub trait RequestResponseHandler {
    async fn process_message(&self, context: ContextMessage) -> Result<(), SoapError>;
}

pub fn validate_content_type(headers: &HeaderMap) -> Result<AcceptableContentType, SoapError> {
    let act: AcceptableContentType = headers
        .get(header::CONTENT_TYPE)
        .ok_or(SoapError::InvalidContentType)?
        .try_into()?;
    Ok(act)
}

pub(crate) fn simple_sender_fault(text: impl Into<String>) -> Fault {
    Fault::builder()
        .code(FaultCode::builder().value(FaultCodeEnum::Sender).build())
        .reason(FaultReason::simple_text("en", text))
        .build()
}

pub(crate) fn create_soap_http_message(body: bytes::Bytes) -> Response<bytes::Bytes> {
    let mut payload = Response::new(body);

    payload
        .headers_mut()
        .insert(header::ACCEPT, MEDIA_TYPE_SOAP_HEADER.clone());
    payload
        .headers_mut()
        .insert(header::CONTENT_TYPE, MEDIA_TYPE_SOAP_HEADER.clone());

    payload
}

#[cfg(test)]
mod test {
    use crate::soap::common::AcceptableContentType;
    use http::HeaderValue;

    #[test]
    fn test_parsing_headers() -> anyhow::Result<()> {
        let soap_xml = vec![
            HeaderValue::from_static("application/soap+xml"),
            HeaderValue::from_static("application/soap+xml; charset=utf=8"),
            HeaderValue::from_static("application/soap+xml;charset=utf=8"),
        ];

        for header in soap_xml {
            let acceptable: AcceptableContentType = (&header)
                .try_into()
                .unwrap_or_else(|_| panic!("Could not load acceptable type for {:?}", header));
            assert_eq!(AcceptableContentType::SOAP, acceptable);
        }

        Ok(())
    }
}
