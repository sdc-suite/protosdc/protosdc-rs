use std::io;

use axum::middleware::AddExtension;
use axum::Extension;
use axum_server::accept::Accept;

#[cfg(feature = "tls_openssl")]
use axum_server::accept::DefaultAcceptor;

#[cfg(all(not(feature = "tls_openssl"), feature = "tls_rustls"))]
use axum_server::tls_rustls::RustlsAcceptor;

use futures::future::BoxFuture;
use log::{error, trace};
use tokio::io::{AsyncRead, AsyncWrite};
use tokio_rustls::rustls::pki_types::CertificateDer;
#[cfg(all(not(feature = "tls_openssl"), feature = "tls_rustls"))]
use tokio_rustls::server::TlsStream;

use tower::Layer;
use x509_parser::certificate::X509Certificate;

#[derive(Debug, Clone)]
pub(crate) struct TlsData {
    pub(crate) peer_certificates: Option<Vec<CertificateDer<'static>>>,
}

#[cfg(feature = "tls_openssl")]
#[derive(Debug, Clone)]
pub(crate) struct CustomOpensslAcceptor {
    pub(crate) inner: axum_server::tls_openssl::OpenSSLAcceptor,
}

#[cfg(feature = "tls_openssl")]
impl CustomOpensslAcceptor {
    pub(crate) fn new(inner: axum_server::tls_openssl::OpenSSLAcceptor) -> Self {
        Self { inner }
    }
}

#[cfg(feature = "tls_openssl")]
impl<I, S> Accept<I, S> for CustomOpensslAcceptor
where
    I: AsyncRead + AsyncWrite + Unpin + Send + 'static,
    S: Send + 'static,
{
    type Stream = tokio_openssl::SslStream<<DefaultAcceptor as Accept<I, S>>::Stream>;
    type Service = AddExtension<S, TlsData>;
    type Future = BoxFuture<'static, io::Result<(Self::Stream, Self::Service)>>;

    fn accept(&self, stream: I, service: S) -> Self::Future {
        let acceptor = self.inner.clone();

        Box::pin(async move {
            let (stream, service) = acceptor.accept(stream, service).await.map_err(|err| {
                error!("Error accepting TLS connection: {}", err);
                err
            })?;

            let peer_certs_orig = stream.ssl().peer_certificate();

            trace!("Peer certificates: {:?}", peer_certs_orig);

            let peer_certs = peer_certs_orig
                .map(|it| it.to_der())
                .map_or(Ok(None), |v| v.map(Some))
                .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?
                .map(|it| vec![Certificate::from_der(it)]);

            let tls_data = TlsData {
                peer_certificates: peer_certs
                    .map(|inner| inner.into_iter().map(|cert| From::from(cert.pem)).collect()),
            };
            let service = Extension(tls_data).layer(service);

            Ok((stream, service))
        })
    }
}

/// Represents a custom acceptor for TLS connections which makes the peer certificate available to
/// handlers as [TlsData].
#[cfg(all(not(feature = "tls_openssl"), feature = "tls_rustls"))]
#[derive(Debug, Clone)]
pub(crate) struct CustomRustlsAcceptor {
    pub(crate) inner: RustlsAcceptor,
}

#[cfg(all(not(feature = "tls_openssl"), feature = "tls_rustls"))]
impl CustomRustlsAcceptor {
    pub(crate) fn new(inner: RustlsAcceptor) -> Self {
        Self { inner }
    }
}

#[cfg(all(not(feature = "tls_openssl"), feature = "tls_rustls"))]
impl<I, S> Accept<I, S> for CustomRustlsAcceptor
where
    I: AsyncRead + AsyncWrite + Unpin + Send + 'static,
    S: Send + 'static,
{
    type Stream = TlsStream<I>;
    type Service = AddExtension<S, TlsData>;
    type Future = BoxFuture<'static, io::Result<(Self::Stream, Self::Service)>>;

    fn accept(&self, stream: I, service: S) -> Self::Future {
        let acceptor = self.inner.clone();

        Box::pin(async move {
            let (stream, service) = acceptor.accept(stream, service).await?;
            let server_conn = stream.get_ref().1;

            let peer_certs = server_conn.peer_certificates().map(|it| it.to_vec());

            trace!("Peer certificates: {:?}", peer_certs);

            let tls_data = TlsData {
                peer_certificates: peer_certs,
            };
            let service = Extension(tls_data).layer(service);

            Ok((stream, service))
        })
    }
}

/// Represents a X509 certificate.
#[derive(Debug, Clone)]
pub struct Certificate {
    pub(crate) pem: Vec<u8>,
}

impl Certificate {
    /// Parse a DER encoded X509 Certificate.
    ///
    /// The provided DER should include at least one DER encoded certificate.
    pub fn from_der(pem: impl AsRef<[u8]>) -> Self {
        let pem = pem.as_ref().to_vec();
        Self { pem }
    }

    /// Get a immutable reference to underlying certificate
    pub fn get_ref(&self) -> &[u8] {
        self.pem.as_slice()
    }

    /// Get a mutable reference to underlying certificate
    pub fn get_mut(&mut self) -> &mut [u8] {
        self.pem.as_mut()
    }

    /// Consumes `self`, returning the underlying certificate
    pub fn into_inner(self) -> Vec<u8> {
        self.pem
    }
}

impl AsRef<[u8]> for Certificate {
    fn as_ref(&self) -> &[u8] {
        self.pem.as_ref()
    }
}

pub fn get_x509<'a>(cert: &'a CertificateDer<'a>) -> Option<X509Certificate<'a>> {
    match x509_parser::parse_x509_certificate(cert.as_ref()) {
        Err(err) => {
            error!("Error parsing x509 certificate from request {}", err);
            None
        }
        Ok((_, c)) => Some(c),
    }
}
