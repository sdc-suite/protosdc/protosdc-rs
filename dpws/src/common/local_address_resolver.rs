use std::net::IpAddr;
use thiserror::Error;
use tokio::net::TcpStream;
use url::Url;

#[derive(Debug, Error)]
pub enum ResolverError {
    #[error(transparent)]
    UrlParse(#[from] url::ParseError),
    #[error(transparent)]
    Io(#[from] std::io::Error),
    #[error("Host fragment is missing")]
    NoHostFragment,
    #[error("Port fragment is missing")]
    NoPortFragment,
    #[error("No local address in tcp stream")]
    NoLocalAddress,
}

/// This function attempts to resolve the local network address (`SocketAddr`) used for a given remote endpoint.
///
/// # Arguments
///
/// * `remote`: - A string representation of the remote URL. It must include both host and port. For example: "http://localhost:8000"
///
/// returns: Result<IpAddr, ResolverError>
pub(crate) async fn try_resolve_local_address(remote: &str) -> Result<IpAddr, ResolverError> {
    let remote_url = Url::parse(remote)?;
    let tcp_stream = TcpStream::connect(format!(
        "{}:{}",
        remote_url.host_str().ok_or(ResolverError::NoHostFragment)?,
        remote_url.port().ok_or(ResolverError::NoPortFragment)?
    ))
    .await?;
    Ok(tcp_stream.local_addr()?.ip())
}
