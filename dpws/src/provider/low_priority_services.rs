use crate::common::extension_handling::ExtensionHandlerFactory;
use crate::http::client::reqwest::ReqwestClientAccess;
use crate::http::server::axum::{AxumRegistryAccess, AxumServerAccess};
use crate::http::server::common::{ContextMessageResponse, Server, ServerError};
use crate::soap::common::{RequestResponseHandler, SoapError, MEDIA_TYPE_XML_HEADER};
use crate::soap::dpws::device::dpws_device::DeviceSettings;
use crate::soap::dpws::device::hosted_service::{HostedService, HostedServiceInfo};
use crate::soap::dpws::ws_eventing::source::{
    ActionBasedEventSourceImpl, DefaultEventSourceImpl, WsEventingSource,
};
use crate::soap::eventing::ComplexXmlTypeReadInner;
use crate::soap::soap_server::{
    create_soap_http_response_with_headers, create_soap_response, RequestResponseHandlerTyped,
    TransportInfo,
};
use crate::xml::messages::addressing::{WsaAttributedURIType, WsaEndpointReference};
use crate::xml::messages::biceps_ext::{
    BICEPS_ACTION_GET_LOCALIZED_TEXT_RESPONSE, BICEPS_ACTION_GET_SUPPORTED_LANGUAGES_RESPONSE,
    BICEPS_MSG_NAMESPACE, BICEPS_MSG_NAMESPACE_BYTES, BICEPS_PORT_TYPE_LOCALIZATION,
    GET_LOCALIZED_TEXT_TAG, GET_SUPPORTED_LANGUAGES_TAG,
};
use crate::xml::messages::common::{HttpUri, SoapEventMessage};
use crate::xml::messages::soap::envelope::SoapHeader;
use crate::xml::messages::soap::fault::{Fault, FaultCode, FaultCodeEnum, FaultReason};
use async_trait::async_trait;
use biceps::provider::access::local_mdib_access::LocalMdibAccess;
use bytes::Bytes;
use futures::{stream, StreamExt};
use http::header::CONTENT_TYPE;
use http::{Response, StatusCode};
use log::{error, warn};
use protosdc::provider::localization_storage::{
    LocalizationStorage, LocalizedStorageText, TextWidth,
};
use protosdc_biceps::biceps::{
    AbstractGetResponse, GetLocalizedText, GetLocalizedTextResponse, GetSupportedLanguages,
    GetSupportedLanguagesResponse,
};
use protosdc_biceps::types::{AnyContent, ProtoLanguage};
use protosdc_xml::{ExpandedName, GenericXmlReaderComplexTypeRead, ParserError};
use quick_xml::events::BytesStart;
use quick_xml::name::{Namespace, ResolveResult};
use std::io::BufRead;
use std::sync::Arc;
use tokio::sync::RwLock;
use tokio::task::JoinHandle;
use tokio_stream::wrappers::ReceiverStream;

pub const SERVICE_ID: &str = "LowPriorityServices";
pub const WSDL: &str = include_str!("lowpriorityservices.wsdl");

#[derive(Clone, Debug)]
pub enum LowPriorityServiceMessages {
    GetSupportedLanguages(GetSupportedLanguages),
    GetLocalizedText(GetLocalizedText),
}

impl SoapEventMessage for LowPriorityServiceMessages {
    fn ns_tag() -> ResolveResult<'static> {
        ResolveResult::Bound(Namespace(BICEPS_MSG_NAMESPACE_BYTES))
    }

    fn ns_tag_str() -> Option<&'static str> {
        Some(BICEPS_MSG_NAMESPACE)
    }
}

impl ComplexXmlTypeReadInner for LowPriorityServiceMessages {
    fn from_xml_complex<B: BufRead>(
        tag_ns: ResolveResult<'static>,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Box<(dyn AnyContent + Send + Sync + 'static)>>,
    ) -> Result<Self, ParserError> {
        Ok(match (tag_ns, event.local_name().into_inner()) {
            GET_SUPPORTED_LANGUAGES_TAG => {
                Self::GetSupportedLanguages(GetSupportedLanguages::from_xml_complex(
                    GET_SUPPORTED_LANGUAGES_TAG,
                    event,
                    reader,
                )?)
            }
            GET_LOCALIZED_TEXT_TAG => Self::GetLocalizedText(GetLocalizedText::from_xml_complex(
                GET_LOCALIZED_TEXT_TAG,
                event,
                reader,
            )?),
            // TODO: Other types
            _ => Err(ParserError::OneOfParserFallthrough {
                parser_name: "Event did not contain a low priority request message".to_string(),
            })?,
        })
    }
}

type LowPriorityEventSource<MDIB, LOC, EHF> = WsEventingSource<
    LowPriorityServicesMessageHandler<MDIB, LOC>,
    LowPriorityServiceMessages,
    DefaultEventSourceImpl<EHF>,
    LowPriorityServicesHostedInfo,
    EHF,
>;

pub struct LowPriorityServices<MDIB, LOC, EHF>
where
    MDIB: LocalMdibAccess + Send + Sync + 'static + Clone,
    LOC: LocalizationStorage<LocalizedStorageText> + Send + Sync + 'static,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    metadata: LowPriorityServicesHostedInfo,
    _ws_eventing_source: Arc<RwLock<LowPriorityEventSource<MDIB, LOC, EHF>>>,
    _processing_task: JoinHandle<()>,
}

impl<MDIB, LOC, EHF> LowPriorityServices<MDIB, LOC, EHF>
where
    MDIB: LocalMdibAccess + Send + Sync + 'static + Clone,
    LOC: LocalizationStorage<LocalizedStorageText> + Send + Sync + 'static,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    pub async fn new(
        mdib: MDIB,
        server_registry: AxumRegistryAccess,
        server_handler: AxumServerAccess,
        http_client: ReqwestClientAccess,
        device_settings: &DeviceSettings<EHF>,
        localized_text_storage: Option<LOC>,
    ) -> Result<Self, ServerError> {
        let mut hosted_service_registration = server_handler
            .register_context_path(&format!(
                "{}/hosted/{}",
                device_settings.server_prefix, SERVICE_ID
            ))
            .await
            .expect("Could not register hosted service endpoint for low_priority_services");
        let mut wsdl_registration = server_handler
            .register_context_path(&format!(
                "{}/hosted/{}/wsdl",
                device_settings.server_prefix, SERVICE_ID
            ))
            .await
            .expect("Could not register wsdl hosted service endpoint for low_priority_services");

        let endpoint_reference = WsaEndpointReference {
            attributes: Default::default(),
            address: WsaAttributedURIType {
                attributes: Default::default(),
                value: hosted_service_registration.address.address.to_string(),
            },
            reference_parameters: None,
            metadata: None,
            children: vec![],
        };

        let mut types = vec![];
        if localized_text_storage.is_some() {
            types.push(ExpandedName::from(BICEPS_PORT_TYPE_LOCALIZATION))
        }

        let metadata = LowPriorityServicesHostedInfo {
            host_epr: device_settings.endpoint_reference.clone(),
            host_types: device_settings.types.iter().cloned().collect(),
            endpoint_reference,
            types,
            wsdl_location: wsdl_registration.address.address.into(),
        };

        let handler = LowPriorityServicesMessageHandler {
            mdib_access: mdib,
            localization_storage: localized_text_storage,
        };

        let event_source: DefaultEventSourceImpl<EHF> = ActionBasedEventSourceImpl::new(
            server_registry,
            server_handler,
            http_client,
            device_settings.extension_handler_factory.clone(),
        )
        .await?;

        let ws_eventing_source = Arc::new(RwLock::new(WsEventingSource::new(
            handler,
            event_source,
            metadata.clone(),
            device_settings.extension_handler_factory.clone(),
        )));

        let task = tokio::spawn({
            let eventing_source = ws_eventing_source.clone();
            async move {
                while let Some(msg) = hosted_service_registration.channel.recv().await {
                    if let Err(err) = eventing_source.read().await.process_message(msg).await {
                        error!("Unhandled error processing request. Errors should never propagate here. {:?}", err)
                    }
                }
            }
        });

        // task drops when path is unregistered on shutdown, no need to store it
        tokio::spawn({
            async move {
                while let Some(msg) = wsdl_registration.channel.recv().await {
                    let mut resp = Response::new(bytes::Bytes::from(WSDL.as_bytes()));
                    resp.headers_mut()
                        .insert(CONTENT_TYPE, MEDIA_TYPE_XML_HEADER.clone());

                    let send_result = msg.response_sender.send(ContextMessageResponse {
                        status_code: StatusCode::OK,
                        payload: resp,
                    });
                    if let Err(err) = send_result {
                        warn!("Error transmitting WSDL: {:?}", err);
                    }
                }
            }
        });

        Ok(Self {
            metadata,
            _ws_eventing_source: ws_eventing_source,
            _processing_task: task,
        })
    }
}

struct LowPriorityServicesMessageHandler<MDIB, LOC>
where
    MDIB: LocalMdibAccess + Send + Sync + Clone + 'static,
    LOC: LocalizationStorage<LocalizedStorageText> + Send + Sync + 'static,
{
    mdib_access: MDIB,
    localization_storage: Option<LOC>,
}

impl<MDIB, LOC> LowPriorityServicesMessageHandler<MDIB, LOC>
where
    MDIB: LocalMdibAccess + Send + Sync + Clone + 'static,
    LOC: LocalizationStorage<LocalizedStorageText> + Send + Sync + 'static,
{
    async fn get_storage(&self) -> Result<&LOC, SoapError> {
        match &self.localization_storage {
            None => Err(SoapError::SoapFault(
                Fault::builder()
                    .code(FaultCode::builder().value(FaultCodeEnum::Sender).build())
                    .reason(FaultReason::simple_text(
                        "en",
                        "LocalizationService unavailable",
                    ))
                    .build(),
            )),
            Some(it) => Ok(it),
        }
    }

    async fn get_supported_languages(&self) -> Result<GetSupportedLanguagesResponse, SoapError> {
        let supported = self.get_storage().await?.get_languages().await;
        let mdib_version = self.mdib_access.mdib_version().await;
        Ok(GetSupportedLanguagesResponse {
            abstract_get_response: AbstractGetResponse {
                extension_element: None,
                mdib_version_group_attr: mdib_version.into(),
            },
            lang: Vec::from_iter(supported.into_iter().map(|l| ProtoLanguage { language: l })),
        })
    }

    // let x: TextWidth = it.into(); inference is incorrect
    //noinspection RsTypeCheck
    async fn get_localized_text(
        &self,
        req: GetLocalizedText,
    ) -> Result<GetLocalizedTextResponse, SoapError> {
        let storage = self.get_storage().await?;
        let mdib_version = self.mdib_access.mdib_version().await;

        let requested_refs = match &req.r#ref.is_empty() {
            true => None,
            false => Some(req.r#ref.into_iter().map(|it| it.string).collect()),
        };
        let requested_languages = match &req.lang.is_empty() {
            true => None,
            false => Some(req.lang.into_iter().map(|it| it.language).collect()),
        };
        let requested_width = match &req.text_width.is_empty() {
            true => None,
            false => req
                .text_width
                .into_iter()
                .map(|it| {
                    let x: TextWidth = it.into();
                    x
                })
                .max(),
        };

        let request = storage.query(
            requested_refs,
            req.version.map(|it| it.version_counter.unsigned_long),
            requested_languages,
            requested_width,
            req.number_of_lines.into_iter().max(),
        );
        let texts = ReceiverStream::new(request.await?)
            .flat_map(|it| stream::iter(it.into_iter()))
            .map(|it| it.into())
            .collect()
            .await;

        Ok(GetLocalizedTextResponse {
            abstract_get_response: AbstractGetResponse {
                extension_element: None,
                mdib_version_group_attr: mdib_version.into(),
            },
            text: texts,
        })
    }
}

#[async_trait]
impl<MDIB, LOC> RequestResponseHandlerTyped<LowPriorityServiceMessages>
    for LowPriorityServicesMessageHandler<MDIB, LOC>
where
    MDIB: LocalMdibAccess + Send + Sync + Clone + 'static,
    LOC: LocalizationStorage<LocalizedStorageText> + Send + Sync + 'static,
{
    async fn request_response(
        &self,
        _transport_info: TransportInfo,
        soap_header: SoapHeader,
        body: LowPriorityServiceMessages,
    ) -> Result<Response<Bytes>, SoapError> {
        Ok(match body {
            LowPriorityServiceMessages::GetSupportedLanguages(_) => {
                create_soap_http_response_with_headers(create_soap_response(
                    &soap_header,
                    BICEPS_ACTION_GET_SUPPORTED_LANGUAGES_RESPONSE,
                    self.get_supported_languages().await?,
                )?)
            }
            LowPriorityServiceMessages::GetLocalizedText(req) => {
                create_soap_http_response_with_headers(create_soap_response(
                    &soap_header,
                    BICEPS_ACTION_GET_LOCALIZED_TEXT_RESPONSE,
                    self.get_localized_text(req).await?,
                )?)
            }
        })
    }
}

impl<MDIB, LOC, EHF> HostedService for LowPriorityServices<MDIB, LOC, EHF>
where
    MDIB: LocalMdibAccess + Send + Sync + 'static + Clone,
    LOC: LocalizationStorage<LocalizedStorageText> + Send + Sync + 'static,
    EHF: ExtensionHandlerFactory + Send + Sync,
{
    fn service_id(&self) -> &'static str {
        SERVICE_ID
    }

    fn endpoint_reference(&self) -> WsaEndpointReference {
        self.metadata.endpoint_reference()
    }

    fn types(&self) -> Vec<ExpandedName> {
        self.metadata.types()
    }

    fn wsdl(&self) -> &'static [u8] {
        WSDL.as_bytes()
    }
}

#[derive(Clone, Debug)]
pub struct LowPriorityServicesHostedInfo {
    host_epr: WsaEndpointReference,
    host_types: Vec<ExpandedName>,
    endpoint_reference: WsaEndpointReference,
    types: Vec<ExpandedName>,
    wsdl_location: HttpUri,
}

impl HostedServiceInfo for LowPriorityServicesHostedInfo {
    fn service_id(&self) -> &'static str {
        SERVICE_ID
    }

    fn host_endpoint_reference(&self) -> WsaEndpointReference {
        self.host_epr.clone()
    }

    fn host_types(&self) -> Vec<ExpandedName> {
        self.host_types.clone()
    }

    fn endpoint_reference(&self) -> WsaEndpointReference {
        self.endpoint_reference.clone()
    }

    fn types(&self) -> Vec<ExpandedName> {
        self.types.clone()
    }

    fn wsdl(&self) -> &'static [u8] {
        WSDL.as_bytes()
    }

    fn wsdl_location(&self) -> HttpUri {
        self.wsdl_location.clone()
    }
}
