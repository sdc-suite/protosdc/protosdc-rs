use std::marker::PhantomData;

use async_trait::async_trait;
use thiserror::Error;
use uuid::Uuid;

use crate::common::extension_handling::ExtensionHandlerFactory;
use biceps::common::mdib_version::MdibVersionBuilder;
use biceps::provider::access::local_mdib_access::{
    LocalMdibAccess, LocalMdibAccessImpl, LocalMdibAccessReadTransaction,
};
use common::{Service, ServiceDelegate, ServiceError, ServiceState};
use network::udp_binding::{create_udp_binding, UdpBindingError, UdpBindingImpl};
use protosdc::provider::device::DefaultLocalMdibAccessImpl;
use protosdc::provider::localization_storage::{LocalizationStorage, LocalizedStorageText};
use protosdc::provider::sco::{OperationInvocationReceiver, ScoController};

use crate::discovery::common::Endpoint;
use crate::discovery::constants::WS_DISCOVERY_PORT;
use crate::discovery::provider::discovery_provider::{
    DpwsDiscoveryProcessor, DpwsDiscoveryProcessorImpl, DpwsDiscoveryProvider,
    DpwsDiscoveryProviderImpl,
};
use crate::discovery::provider::discovery_proxy_provider::DpwsDiscoveryProxyProviderImpl;
use crate::http::client::reqwest::{ReqwestClientAccess, ReqwestClientError, ReqwestHttpClient};
use crate::http::server::axum::AxumRegistry;
use crate::http::server::common::{RegistryError, ServerError, ServerRegistry};
use crate::provider::high_priority_services::HighPriorityServices;
use crate::provider::low_priority_services::LowPriorityServices;
use crate::provider::sdc_provider_plugin::SdcProviderPlugin;
use crate::soap::dpws::device::dpws_device::DeviceSettings;
use crate::soap::dpws::device::hosting_service::{
    HostingService, HostingServiceError, HostingServiceImpl,
};
use crate::soap::soap_client::SoapClientImpl;
use crate::xml::messages::addressing::WsaEndpointReference;
use crate::xml::messages::common::GLUE_SCOPE;

pub trait DeviceMdibType:
    LocalMdibAccess + Sync + Send + Clone + LocalMdibAccessReadTransaction + 'static
{
}
pub trait DeviceDpwsDiscoveryProcessor:
    DpwsDiscoveryProcessor + Sync + Send + Clone + 'static
{
}

pub trait DeviceDpwsDiscoveryProvider: DpwsDiscoveryProvider + Send + Sync + 'static {}

// blanket impls
impl<T> DeviceMdibType for T where
    T: LocalMdibAccess + Sync + Send + Clone + LocalMdibAccessReadTransaction + 'static
{
}
impl<T> DeviceDpwsDiscoveryProcessor for T where
    T: DpwsDiscoveryProcessor + Sync + Send + Clone + 'static
{
}

impl<T> DeviceDpwsDiscoveryProvider for T where T: DpwsDiscoveryProvider + Sync + Send + 'static {}

pub struct SdcDeviceContext<V, T>
where
    V: DeviceMdibType,
    T: DeviceDpwsDiscoveryProvider + Clone,
{
    pub mdib_access: V,
    pub discovery_provider: T,
}

#[derive(Debug, Error)]
pub enum SdcDeviceError {
    #[error(transparent)]
    Reqwest(#[from] ReqwestClientError),
    #[error(transparent)]
    Registry(#[from] RegistryError),
    #[error(transparent)]
    Server(#[from] ServerError),
    #[error(transparent)]
    HostingService(#[from] HostingServiceError),
    #[error(transparent)]
    UdpBinding(#[from] UdpBindingError),
}

pub struct SdcDevice<T, V, OIR, HOSTING, PLUGIN>
where
    T: DeviceDpwsDiscoveryProvider + Clone,
    V: 'static + LocalMdibAccess + Sync + Send + Clone + LocalMdibAccessReadTransaction,
    OIR: OperationInvocationReceiver<V> + Send + Sync + 'static,
    HOSTING: HostingService,
    PLUGIN: SdcProviderPlugin,
{
    service_delegate: ServiceDelegate,
    pub local_mdib: V, // TODO: No pub?
    pub hosting_service: HOSTING,
    sdc_device_plugins: PLUGIN,
    pub discovery_provider: T,
    phantom_oir: PhantomData<OIR>,
}

impl<OIR, PLUGIN, EHF>
    SdcDevice<
        DpwsDiscoveryProviderImpl<
            UdpBindingImpl,
            DpwsDiscoveryProcessorImpl,
            DpwsDiscoveryProxyProviderImpl<SoapClientImpl<ReqwestClientAccess, EHF>>,
        >,
        DefaultLocalMdibAccessImpl,
        OIR,
        HostingServiceImpl,
        PLUGIN,
    >
where
    OIR: OperationInvocationReceiver<DefaultLocalMdibAccessImpl> + Send + Sync + 'static + Clone,
    PLUGIN: SdcProviderPlugin,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    pub async fn new<LOC>(
        operation_invocation_receiver: OIR,
        device_settings: DeviceSettings<EHF>,
        plugins: PLUGIN,
        localization_storage: Option<LOC>,
    ) -> Result<Self, SdcDeviceError>
    where
        LOC: LocalizationStorage<LocalizedStorageText> + Send + Sync + 'static,
    {
        let sequence_id = Uuid::new_v4().to_string();
        let initial_version = MdibVersionBuilder::default().seq(sequence_id).create();
        let mdib = LocalMdibAccessImpl::new(initial_version);

        let registry = AxumRegistry::new(device_settings.crypto.clone());
        let http_client = ReqwestHttpClient::new(device_settings.crypto.clone())?;

        let device_server = registry
            .access()
            .init_server(device_settings.network_interface)
            .await?;

        let sco_controller = ScoController::default();

        let high_hosted = Box::new(
            HighPriorityServices::new(
                mdib.clone(),
                operation_invocation_receiver.clone(),
                sco_controller,
                registry.access(),
                device_server.clone(),
                http_client.get_access(),
                &device_settings,
            )
            .await?,
        );

        let low_hosted = Box::new(
            LowPriorityServices::new(
                mdib.clone(),
                registry.access(),
                device_server,
                http_client.get_access(),
                &device_settings,
                localization_storage,
            )
            .await?,
        );

        let endpoint: Endpoint = Endpoint {
            scopes: vec![GLUE_SCOPE.to_string()],
            types: device_settings.types.iter().cloned().collect(),
            endpoint_reference: device_settings.endpoint_reference.clone(),
            x_addrs: vec![],
            metadata_version: 0,
        };

        let disco = match &device_settings.discovery_proxy_address {
            None => None,
            Some(addr) => Some(DpwsDiscoveryProxyProviderImpl::new(SoapClientImpl::new(
                http_client.get_access(),
                WsaEndpointReference::builder()
                    .address(addr.clone())
                    .build(),
                device_settings.extension_handler_factory.clone(),
            ))),
        };
        let discovery_processor = DpwsDiscoveryProcessorImpl::new(endpoint, None);

        let hosting_service = HostingServiceImpl::new(
            &device_settings,
            vec![high_hosted, low_hosted],
            registry.access(),
            discovery_processor.clone(),
        )
        .await?;

        // update discovery info after starting the hosted service
        discovery_processor
            .update_physical_address(
                hosting_service
                    .x_addrs()
                    .await
                    .iter()
                    .cloned()
                    .map(|it| it.into())
                    .collect(),
            )
            .await;

        let udp_binding = create_udp_binding(
            device_settings.network_interface.ip(),
            Some(WS_DISCOVERY_PORT),
        )
        .await?;

        let discovery_provider =
            DpwsDiscoveryProviderImpl::new(udp_binding, discovery_processor.clone(), disco);

        Ok(Self {
            service_delegate: Default::default(),
            local_mdib: mdib,
            hosting_service,
            discovery_provider,
            sdc_device_plugins: plugins,
            phantom_oir: Default::default(),
        })
    }
}

#[async_trait]
impl<T, V, OIR, HOSTING, PLUGIN> Service for SdcDevice<T, V, OIR, HOSTING, PLUGIN>
where
    T: DeviceDpwsDiscoveryProvider + Clone,
    V: 'static
        + LocalMdibAccess
        + Sync
        + Send
        + Clone
        + LocalMdibAccessReadTransaction
        + DeviceMdibType,
    OIR: OperationInvocationReceiver<V> + Send + Sync + 'static,
    HOSTING: HostingService + Send + Sync,
    PLUGIN: SdcProviderPlugin + Send + Sync,
{
    async fn start(&mut self) -> Result<(), ServiceError> {
        let device_context = SdcDeviceContext {
            mdib_access: self.local_mdib.clone(),
            discovery_provider: self.discovery_provider.clone(),
        };

        self.service_delegate.start().await?;
        self.sdc_device_plugins
            .before_startup(&device_context)
            .await?;
        self.service_delegate.started().await?;
        self.sdc_device_plugins
            .after_startup(&device_context)
            .await?;
        Ok(())
    }

    async fn stop(&mut self) -> Result<(), ServiceError> {
        let device_context = SdcDeviceContext {
            mdib_access: self.local_mdib.clone(),
            discovery_provider: self.discovery_provider.clone(),
        };

        self.service_delegate.stop().await?;
        self.sdc_device_plugins
            .before_shutdown(&device_context)
            .await?;
        self.service_delegate.stopped().await?;
        self.sdc_device_plugins
            .after_shutdown(&device_context)
            .await?;
        Ok(())
    }

    fn is_running(&self) -> bool {
        self.service_delegate.is_running()
    }

    fn state(&self) -> &ServiceState {
        self.service_delegate.state()
    }
}
