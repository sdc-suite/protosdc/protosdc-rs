use std::io::BufRead;
use std::sync::Arc;

use async_trait::async_trait;
use bytes::Bytes;
use futures::FutureExt;
use http::header::CONTENT_TYPE;
use http::{Response, StatusCode};
use log::{error, warn};
use protosdc_biceps::biceps::instance_identifier_mod::{ExtensionAttr, RootAttr};
use protosdc_biceps::biceps::{
    AbstractGetResponse, Activate, ActivateResponse, GetContextStates, GetContextStatesByFilter,
    GetContextStatesByIdentification, GetContextStatesResponse, GetMdDescription,
    GetMdDescriptionResponse, GetMdState, GetMdStateResponse, GetMdib, GetMdibResponse,
    InstanceIdentifier, SetAlertState, SetAlertStateResponse, SetComponentState,
    SetComponentStateResponse, SetContextState, SetContextStateResponse, SetMetricState,
    SetMetricStateResponse, SetString, SetStringResponse, SetValue, SetValueResponse,
};
use protosdc_biceps::types::{AnyContent, ProtoUri};
use protosdc_xml::{ExpandedName, GenericXmlReaderComplexTypeRead, ParserError};
use quick_xml::events::BytesStart;
use quick_xml::name::{Namespace, ResolveResult};
use tokio::sync::RwLock;
use tokio::task::JoinHandle;

use crate::common::extension_handling::ExtensionHandlerFactory;
use biceps::common::access::mdib_access::MdibAccess;
use biceps::provider::access::local_mdib_access::{
    LocalMdibAccess, LocalMdibAccessReadTransaction,
};
use common::crypto::der_to_pem;
use protosdc::common::reports::EpisodicReport;
use protosdc::provider::common::{ANONYMOUS_EXTENSION, ROOT_PEM_STRING_SDC, ROOT_SDC};
use protosdc::provider::mapper;
use protosdc::provider::report_generator::ReportGenerator;
use protosdc::provider::sco::{OperationInvocationReceiver, ScoController};

use crate::http::client::reqwest::ReqwestClientAccess;
use crate::http::server::axum::{AxumRegistryAccess, AxumServerAccess};
use crate::http::server::common::{ContextMessageResponse, Server, ServerError};
use crate::soap::common::{RequestResponseHandler, SoapError, MEDIA_TYPE_XML_HEADER};
use crate::soap::dpws::device::dpws_device::DeviceSettings;
use crate::soap::dpws::device::hosted_service::{HostedService, HostedServiceInfo};
use crate::soap::dpws::ws_eventing::source::{
    ActionBasedEventSourceImpl, DefaultEventSourceImpl, EventSource, WsEventingSource,
};
use crate::soap::eventing::ComplexXmlTypeReadInner;
use crate::soap::soap_server::{
    create_soap_http_response_with_headers, create_soap_response, RequestResponseHandlerTyped,
    TransportInfo,
};
use crate::xml::messages::addressing::WsaEndpointReference;
use crate::xml::messages::biceps_ext::{
    ACTIVATE_TAG, BICEPS_ACTION_ACTIVATE_RESPONSE, BICEPS_ACTION_DESCRIPTION_MODIFICATION_REPORT,
    BICEPS_ACTION_EPISODIC_ALERT_REPORT, BICEPS_ACTION_EPISODIC_COMPONENT_REPORT,
    BICEPS_ACTION_EPISODIC_CONTEXT_REPORT, BICEPS_ACTION_EPISODIC_METRIC_REPORT,
    BICEPS_ACTION_EPISODIC_OPERATIONAL_STATE_REPORT, BICEPS_ACTION_GET_CONTEXT_STATES_RESPONSE,
    BICEPS_ACTION_GET_MDIB_RESPONSE, BICEPS_ACTION_GET_MD_DESCRIPTION_RESPONSE,
    BICEPS_ACTION_GET_MD_STATE_RESPONSE, BICEPS_ACTION_OPERATION_INVOKED_REPORT,
    BICEPS_ACTION_SET_ALERT_STATE_RESPONSE, BICEPS_ACTION_SET_COMPONENT_STATE_RESPONSE,
    BICEPS_ACTION_SET_CONTEXT_STATE_RESPONSE, BICEPS_ACTION_SET_METRIC_STATE_RESPONSE,
    BICEPS_ACTION_SET_STRING_RESPONSE, BICEPS_ACTION_SET_VALUE_RESPONSE,
    BICEPS_ACTION_WAVEFORM_STREAM, BICEPS_MSG_NAMESPACE, BICEPS_MSG_NAMESPACE_BYTES,
    BICEPS_PORT_TYPE_CONTEXT, BICEPS_PORT_TYPE_DESCRIPTION_EVENT, BICEPS_PORT_TYPE_GET,
    BICEPS_PORT_TYPE_SET, BICEPS_PORT_TYPE_STATE_EVENT, BICEPS_PORT_TYPE_WAVEFORM,
    GET_CONTEXT_STATES_BY_FILTER_TAG, GET_CONTEXT_STATES_BY_IDENTIFICATION_TAG,
    GET_CONTEXT_STATES_TAG, GET_MDIB_TAG, GET_MD_DESCRIPTION_TAG, GET_MD_STATE_TAG,
    SET_ALERT_STATE_TAG, SET_COMPONENT_STATE_TAG, SET_CONTEXT_STATE_TAG, SET_METRIC_STATE_TAG,
    SET_STRING_TAG, SET_VALUE_TAG,
};
use crate::xml::messages::common::{HttpUri, SoapEventMessage};
use crate::xml::messages::soap::envelope::SoapHeader;
use crate::xml::messages::soap::fault::{Fault, FaultCode, FaultCodeEnum, FaultReason};

pub const SERVICE_ID: &str = "HighPriorityServices";
//noinspection SpellCheckingInspection
pub const WSDL: &str = include_str!("highpriorityservices.wsdl");

#[derive(Clone, Debug)]
pub enum HighPriorityServiceMessages {
    GetMdib(Box<GetMdib>),
    GetMdDescription(Box<GetMdDescription>),
    GetMdState(Box<GetMdState>),

    GetContextStates(Box<GetContextStates>),
    GetContextStatesByFilter(Box<GetContextStatesByFilter>),
    GetContextStatesByIdentification(Box<GetContextStatesByIdentification>),

    SetValue(Box<SetValue>),
    SetString(Box<SetString>),
    Activate(Box<Activate>),
    SetComponentState(Box<SetComponentState>),
    SetContextState(Box<SetContextState>),
    SetAlertState(Box<SetAlertState>),
    SetMetricState(Box<SetMetricState>),
}

impl SoapEventMessage for HighPriorityServiceMessages {
    fn ns_tag() -> ResolveResult<'static> {
        ResolveResult::Bound(Namespace(BICEPS_MSG_NAMESPACE_BYTES))
    }

    fn ns_tag_str() -> Option<&'static str> {
        Some(BICEPS_MSG_NAMESPACE)
    }
}

impl ComplexXmlTypeReadInner for HighPriorityServiceMessages {
    fn from_xml_complex<B: BufRead>(
        tag_ns: ResolveResult<'static>,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Box<(dyn AnyContent + Send + Sync + 'static)>>,
    ) -> Result<Self, ParserError> {
        Ok(match (tag_ns, event.local_name().into_inner()) {
            GET_MDIB_TAG => Self::GetMdib(Box::new(GetMdib::from_xml_complex(
                GET_MDIB_TAG,
                event,
                reader,
            )?)),
            GET_MD_DESCRIPTION_TAG => Self::GetMdDescription(Box::new(
                GetMdDescription::from_xml_complex(GET_MD_DESCRIPTION_TAG, event, reader)?,
            )),
            GET_MD_STATE_TAG => Self::GetMdState(Box::new(GetMdState::from_xml_complex(
                GET_MD_STATE_TAG,
                event,
                reader,
            )?)),
            GET_CONTEXT_STATES_TAG => Self::GetContextStates(Box::new(
                GetContextStates::from_xml_complex(GET_CONTEXT_STATES_TAG, event, reader)?,
            )),
            GET_CONTEXT_STATES_BY_FILTER_TAG => Self::GetContextStatesByFilter(Box::new(
                GetContextStatesByFilter::from_xml_complex(
                    GET_CONTEXT_STATES_BY_FILTER_TAG,
                    event,
                    reader,
                )?,
            )),
            GET_CONTEXT_STATES_BY_IDENTIFICATION_TAG => Self::GetContextStatesByIdentification(
                Box::new(GetContextStatesByIdentification::from_xml_complex(
                    GET_CONTEXT_STATES_BY_IDENTIFICATION_TAG,
                    event,
                    reader,
                )?),
            ),
            ACTIVATE_TAG => Self::Activate(Box::new(Activate::from_xml_complex(
                ACTIVATE_TAG,
                event,
                reader,
            )?)),
            SET_ALERT_STATE_TAG => Self::SetAlertState(Box::new(SetAlertState::from_xml_complex(
                SET_ALERT_STATE_TAG,
                event,
                reader,
            )?)),
            SET_CONTEXT_STATE_TAG => Self::SetContextState(Box::new(
                SetContextState::from_xml_complex(SET_CONTEXT_STATE_TAG, event, reader)?,
            )),
            SET_METRIC_STATE_TAG => Self::SetMetricState(Box::new(
                SetMetricState::from_xml_complex(SET_METRIC_STATE_TAG, event, reader)?,
            )),
            SET_COMPONENT_STATE_TAG => Self::SetComponentState(Box::new(
                SetComponentState::from_xml_complex(SET_COMPONENT_STATE_TAG, event, reader)?,
            )),
            SET_STRING_TAG => Self::SetString(Box::new(SetString::from_xml_complex(
                SET_STRING_TAG,
                event,
                reader,
            )?)),
            SET_VALUE_TAG => Self::SetValue(Box::new(SetValue::from_xml_complex(
                SET_VALUE_TAG,
                event,
                reader,
            )?)),
            _ => Err(ParserError::OneOfParserFallthrough {
                parser_name: "Event did not contain a high priority request message".to_string(),
            })?,
        })
    }
}

type HighPriorityEventSource<MDIB, OIR, EHF> = WsEventingSource<
    HighPriorityServicesMessageHandler<MDIB, OIR, EHF>,
    HighPriorityServiceMessages,
    DefaultEventSourceImpl<EHF>,
    HighPriorityServicesHostedInfo,
    EHF,
>;

pub struct HighPriorityServices<MDIB, OIR, EHF>
where
    MDIB: LocalMdibAccess + Send + Sync + LocalMdibAccessReadTransaction + 'static + Clone,
    OIR: OperationInvocationReceiver<MDIB> + Send + Sync + 'static + Clone,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    metadata: HighPriorityServicesHostedInfo,
    _ws_eventing_source: Arc<RwLock<HighPriorityEventSource<MDIB, OIR, EHF>>>,
    _report_generator: ReportGenerator,
    _processing_task: JoinHandle<()>,
    _generator_task: JoinHandle<()>,
    _operation_task: JoinHandle<()>,
}

impl<MDIB, OIR, EHF> HighPriorityServices<MDIB, OIR, EHF>
where
    MDIB: LocalMdibAccess + Send + Sync + LocalMdibAccessReadTransaction + 'static + Clone,
    OIR: OperationInvocationReceiver<MDIB> + Send + Sync + 'static + Clone,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    pub async fn new(
        mdib: MDIB,
        operation_invocation_receiver: OIR,
        sco_controller: ScoController,
        server_registry: AxumRegistryAccess,
        server_handler: AxumServerAccess,
        http_client: ReqwestClientAccess,
        device_settings: &DeviceSettings<EHF>,
    ) -> Result<Self, ServerError> {
        let mut hosted_service_registration = server_handler
            .register_context_path(&format!(
                "{}/hosted/{}",
                device_settings.server_prefix, SERVICE_ID
            ))
            .await
            .expect("Could not register hosted service endpoint for high_priority_services");
        let mut wsdl_registration = server_handler
            .register_context_path(&format!(
                "{}/hosted/{}/wsdl",
                device_settings.server_prefix, SERVICE_ID
            ))
            .await
            .expect("Could not register wsdl hosted service endpoint for high_priority_services");

        let endpoint_reference = WsaEndpointReference::builder()
            .address(hosted_service_registration.address.address.to_string())
            .build();

        let types = vec![
            ExpandedName::from(BICEPS_PORT_TYPE_GET),
            ExpandedName::from(BICEPS_PORT_TYPE_SET),
            ExpandedName::from(BICEPS_PORT_TYPE_CONTEXT),
            ExpandedName::from(BICEPS_PORT_TYPE_DESCRIPTION_EVENT),
            ExpandedName::from(BICEPS_PORT_TYPE_STATE_EVENT),
            ExpandedName::from(BICEPS_PORT_TYPE_WAVEFORM),
        ];

        let metadata = HighPriorityServicesHostedInfo {
            host_epr: device_settings.endpoint_reference.clone(),
            host_types: device_settings.types.iter().cloned().collect(),
            endpoint_reference,
            types,
            wsdl_location: wsdl_registration.address.address.into(),
        };

        let mut report_generator = ReportGenerator::new(mdib.subscribe().await);
        // let mut sco_controller = ScoController::default();

        let mut sco_controller_sub = sco_controller.subscribe().await;

        let handler = HighPriorityServicesMessageHandler {
            mdib_access: mdib,
            operation_invocation_receiver,
            sco_controller,
            _extension_handler_factory: device_settings.extension_handler_factory.clone(),
        };

        let event_source: DefaultEventSourceImpl<EHF> = ActionBasedEventSourceImpl::new(
            server_registry,
            server_handler,
            http_client,
            device_settings.extension_handler_factory.clone(),
        )
        .await?;

        let ws_eventing_source = Arc::new(RwLock::new(WsEventingSource::new(
            handler,
            event_source,
            metadata.clone(),
            device_settings.extension_handler_factory.clone(),
        )));

        let task = tokio::spawn({
            let eventing_source = ws_eventing_source.clone();
            async move {
                while let Some(msg) = hosted_service_registration.channel.recv().await {
                    if let Err(err) = eventing_source.read().await.process_message(msg).await {
                        error!("Unhandled error processing request. Errors should never propagate here. {:?}", err)
                    }
                }
            }
        });

        // task drops when path is unregistered on shutdown, no need to store it
        tokio::spawn({
            async move {
                while let Some(msg) = wsdl_registration.channel.recv().await {
                    let mut resp = Response::new(bytes::Bytes::from(WSDL.as_bytes()));
                    resp.headers_mut()
                        .insert(CONTENT_TYPE, MEDIA_TYPE_XML_HEADER.clone());

                    let send_result = msg.response_sender.send(ContextMessageResponse {
                        status_code: StatusCode::OK,
                        payload: resp,
                    });
                    if let Err(err) = send_result {
                        warn!("Error transmitting WSDL: {:?}", err);
                    }
                }
            }
        });

        let mut report_sub = report_generator.subscribe().await;
        let report_task = tokio::spawn({
            let eventing_source = ws_eventing_source.clone();
            async move {
                while let Some(msg) = report_sub.recv().await {
                    let send_result = match msg {
                        EpisodicReport::Alert(rep) => {
                            eventing_source
                                .read()
                                .await
                                .event_source
                                .send_notification(BICEPS_ACTION_EPISODIC_ALERT_REPORT, *rep)
                                .await
                        }
                        EpisodicReport::Component(rep) => {
                            eventing_source
                                .read()
                                .await
                                .event_source
                                .send_notification(BICEPS_ACTION_EPISODIC_COMPONENT_REPORT, *rep)
                                .await
                        }
                        EpisodicReport::Context(rep) => {
                            eventing_source
                                .read()
                                .await
                                .event_source
                                .send_notification(BICEPS_ACTION_EPISODIC_CONTEXT_REPORT, *rep)
                                .await
                        }
                        EpisodicReport::Metric(rep) => {
                            eventing_source
                                .read()
                                .await
                                .event_source
                                .send_notification(BICEPS_ACTION_EPISODIC_METRIC_REPORT, *rep)
                                .await
                        }
                        EpisodicReport::Operation(rep) => {
                            eventing_source
                                .read()
                                .await
                                .event_source
                                .send_notification(
                                    BICEPS_ACTION_EPISODIC_OPERATIONAL_STATE_REPORT,
                                    *rep,
                                )
                                .await
                        }
                        EpisodicReport::Waveform(rep) => {
                            eventing_source
                                .read()
                                .await
                                .event_source
                                .send_notification(BICEPS_ACTION_WAVEFORM_STREAM, *rep)
                                .await
                        }
                        EpisodicReport::Description(rep) => {
                            eventing_source
                                .read()
                                .await
                                .event_source
                                .send_notification(
                                    BICEPS_ACTION_DESCRIPTION_MODIFICATION_REPORT,
                                    *rep,
                                )
                                .await
                        }
                    };

                    if let Err(err) = send_result {
                        error!(
                            "Unhandled error distributing notification request. {:?}",
                            err
                        )
                    }
                }
            }
        });

        let operation_report_task = tokio::spawn({
            let eventing_source = ws_eventing_source.clone();
            async move {
                while let Some(msg) = sco_controller_sub.recv().await {
                    let send_result = eventing_source
                        .read()
                        .await
                        .event_source
                        .send_notification(BICEPS_ACTION_OPERATION_INVOKED_REPORT, msg)
                        .await;

                    if let Err(err) = send_result {
                        error!(
                            "Unhandled error distributing notification request. {:?}",
                            err
                        )
                    }
                }
            }
        });

        Ok(Self {
            metadata,
            _ws_eventing_source: ws_eventing_source,
            _report_generator: report_generator,
            _processing_task: task,
            _generator_task: report_task,
            _operation_task: operation_report_task,
        })
    }
}

struct HighPriorityServicesMessageHandler<MDIB, OIR, EHF>
where
    MDIB: LocalMdibAccess + Send + Sync + Clone + 'static,
    OIR: OperationInvocationReceiver<MDIB> + Send + Sync + 'static + Clone,
    EHF: ExtensionHandlerFactory + Send + Sync,
{
    mdib_access: MDIB,
    operation_invocation_receiver: OIR,
    sco_controller: ScoController,
    _extension_handler_factory: EHF,
}

async fn get_source(transport_info: &TransportInfo) -> InstanceIdentifier {
    let cert_identifier = match &transport_info.peer_certificates {
        None => None,
        Some(certs) => certs.first().map(|cert| der_to_pem(cert.as_ref())),
    };

    match cert_identifier {
        None => InstanceIdentifier {
            extension_element: None,
            r#type: None,
            identifier_name: vec![],
            root_attr: Some(RootAttr {
                any_u_r_i: ProtoUri {
                    uri: (*ROOT_SDC).to_string(),
                },
            }),
            extension_attr: Some(ExtensionAttr {
                string: ANONYMOUS_EXTENSION.to_string(),
            }),
        },
        Some(identifier) => InstanceIdentifier {
            extension_element: None,
            r#type: None,
            identifier_name: vec![],
            root_attr: Some(RootAttr {
                any_u_r_i: ProtoUri {
                    uri: (*ROOT_PEM_STRING_SDC).to_string(),
                },
            }),
            extension_attr: Some(ExtensionAttr { string: identifier }),
        },
    }
}

impl<MDIB, OIR, EHF> HighPriorityServicesMessageHandler<MDIB, OIR, EHF>
where
    MDIB: LocalMdibAccess + Send + Sync + LocalMdibAccessReadTransaction + 'static + Clone,
    OIR: OperationInvocationReceiver<MDIB> + Send + Sync + 'static + Clone,
    EHF: ExtensionHandlerFactory + Send + Sync,
{
    async fn get_mdib(&self) -> Result<GetMdibResponse, SoapError> {
        self.mdib_access
            .read_transaction(|access| {
                async move {
                    let mdib = mapper::map_mdib(&access).await?;
                    Ok(GetMdibResponse {
                        abstract_get_response: AbstractGetResponse {
                            extension_element: None,
                            mdib_version_group_attr: mdib.mdib_version_group_attr.clone(),
                        },
                        mdib,
                    })
                }
                .boxed()
            })
            .await
    }

    async fn get_md_description(
        &self,
        get_md_description: Box<GetMdDescription>,
    ) -> Result<GetMdDescriptionResponse, SoapError> {
        self.mdib_access
            .read_transaction(|access| {
                async move {
                    let filter = get_md_description
                        .handle_ref
                        .into_iter()
                        .map(|it| it.string)
                        .collect::<Vec<_>>();

                    let md_description =
                        mapper::map_md_description(&self.mdib_access, &filter).await?;
                    Ok(GetMdDescriptionResponse {
                        abstract_get_response: AbstractGetResponse {
                            extension_element: None,
                            mdib_version_group_attr: access.mdib_version().await.into(),
                        },
                        md_description,
                    })
                }
                .boxed()
            })
            .await
    }

    async fn get_md_state(
        &self,
        _get_md_state: Box<GetMdState>,
    ) -> Result<GetMdStateResponse, SoapError> {
        self.mdib_access
            .read_transaction(|access| {
                async move {
                    // don't care about filter
                    let md_state = mapper::map_md_state(&access, &[]).await?;
                    Ok(GetMdStateResponse {
                        abstract_get_response: AbstractGetResponse {
                            extension_element: None,
                            mdib_version_group_attr: access.mdib_version().await.into(),
                        },
                        md_state,
                    })
                }
                .boxed()
            })
            .await
    }

    async fn get_context_states(
        &self,
        _get_context_states: Box<GetContextStates>,
    ) -> Result<GetContextStatesResponse, SoapError> {
        self.mdib_access
            .read_transaction(|access| {
                async move {
                    // don't care about filter
                    let context_state = access.context_states().await;
                    Ok(GetContextStatesResponse {
                        abstract_get_response: AbstractGetResponse {
                            extension_element: None,
                            mdib_version_group_attr: access.mdib_version().await.into(),
                        },
                        context_state,
                    })
                }
                .boxed()
            })
            .await
    }

    async fn activate(
        &self,
        activate: Box<Activate>,
        transport_info: TransportInfo,
    ) -> Result<ActivateResponse, SoapError> {
        let operation_handle = activate.abstract_set.operation_handle_ref.string.clone();
        let receiver = self.operation_invocation_receiver.clone();

        let call_result = self
            .sco_controller
            .process_incoming_set_operation(
                operation_handle.clone(),
                get_source(&transport_info).await,
                self.mdib_access.clone(),
                |context| async {
                    receiver
                        .activate(operation_handle, context, *activate)
                        .await
                },
            )
            .await;

        Ok(ActivateResponse {
            abstract_set_response: call_result.into(),
        })
    }

    async fn set_value(
        &self,
        set_value: Box<SetValue>,
        transport_info: TransportInfo,
    ) -> Result<SetValueResponse, SoapError> {
        let operation_handle = set_value.abstract_set.operation_handle_ref.string.clone();
        let receiver = self.operation_invocation_receiver.clone();

        let call_result = self
            .sco_controller
            .process_incoming_set_operation(
                operation_handle.clone(),
                get_source(&transport_info).await,
                self.mdib_access.clone(),
                |context| async {
                    receiver
                        .set_value(operation_handle, context, *set_value)
                        .await
                },
            )
            .await;

        Ok(SetValueResponse {
            abstract_set_response: call_result.into(),
        })
    }

    async fn set_string(
        &self,
        set_string: Box<SetString>,
        transport_info: TransportInfo,
    ) -> Result<SetStringResponse, SoapError> {
        let operation_handle = set_string.abstract_set.operation_handle_ref.string.clone();
        let receiver = self.operation_invocation_receiver.clone();

        let call_result = self
            .sco_controller
            .process_incoming_set_operation(
                operation_handle.clone(),
                get_source(&transport_info).await,
                self.mdib_access.clone(),
                |context| async {
                    receiver
                        .set_string(operation_handle, context, *set_string)
                        .await
                },
            )
            .await;

        Ok(SetStringResponse {
            abstract_set_response: call_result.into(),
        })
    }

    async fn set_context_state(
        &self,
        set_context_state: Box<SetContextState>,
        transport_info: TransportInfo,
    ) -> Result<SetContextStateResponse, SoapError> {
        let operation_handle = set_context_state
            .abstract_set
            .operation_handle_ref
            .string
            .clone();
        let receiver = self.operation_invocation_receiver.clone();

        let call_result = self
            .sco_controller
            .process_incoming_set_operation(
                operation_handle.clone(),
                get_source(&transport_info).await,
                self.mdib_access.clone(),
                |context| async {
                    receiver
                        .set_context_state(operation_handle, context, *set_context_state)
                        .await
                },
            )
            .await;

        Ok(SetContextStateResponse {
            abstract_set_response: call_result.into(),
        })
    }

    async fn set_component_state(
        &self,
        set_component_state: Box<SetComponentState>,
        transport_info: TransportInfo,
    ) -> Result<SetComponentStateResponse, SoapError> {
        let operation_handle = set_component_state
            .abstract_set
            .operation_handle_ref
            .string
            .clone();
        let receiver = self.operation_invocation_receiver.clone();

        let call_result = self
            .sco_controller
            .process_incoming_set_operation(
                operation_handle.clone(),
                get_source(&transport_info).await,
                self.mdib_access.clone(),
                |context| async {
                    receiver
                        .set_component_state(operation_handle, context, *set_component_state)
                        .await
                },
            )
            .await;

        Ok(SetComponentStateResponse {
            abstract_set_response: call_result.into(),
        })
    }

    async fn set_alert_state(
        &self,
        set_alert_state: Box<SetAlertState>,
        transport_info: TransportInfo,
    ) -> Result<SetAlertStateResponse, SoapError> {
        let operation_handle = set_alert_state
            .abstract_set
            .operation_handle_ref
            .string
            .clone();
        let receiver = self.operation_invocation_receiver.clone();

        let call_result = self
            .sco_controller
            .process_incoming_set_operation(
                operation_handle.clone(),
                get_source(&transport_info).await,
                self.mdib_access.clone(),
                |context| async {
                    receiver
                        .set_alert_state(operation_handle, context, *set_alert_state)
                        .await
                },
            )
            .await;

        Ok(SetAlertStateResponse {
            abstract_set_response: call_result.into(),
        })
    }

    async fn set_metric_state(
        &self,
        set_metric_state: Box<SetMetricState>,
        transport_info: TransportInfo,
    ) -> Result<SetMetricStateResponse, SoapError> {
        let operation_handle = set_metric_state
            .abstract_set
            .operation_handle_ref
            .string
            .clone();
        let receiver = self.operation_invocation_receiver.clone();

        let call_result = self
            .sco_controller
            .process_incoming_set_operation(
                operation_handle.clone(),
                get_source(&transport_info).await,
                self.mdib_access.clone(),
                |context| async {
                    receiver
                        .set_metric_state(operation_handle, context, *set_metric_state)
                        .await
                },
            )
            .await;

        Ok(SetMetricStateResponse {
            abstract_set_response: call_result.into(),
        })
    }
}

#[async_trait]
impl<MDIB, OIR, EHF> RequestResponseHandlerTyped<HighPriorityServiceMessages>
    for HighPriorityServicesMessageHandler<MDIB, OIR, EHF>
where
    MDIB: LocalMdibAccess + Sync + Send + LocalMdibAccessReadTransaction + 'static + Clone,
    OIR: OperationInvocationReceiver<MDIB> + Send + Sync + 'static + Clone,
    EHF: ExtensionHandlerFactory + Send + Sync,
{
    async fn request_response(
        &self,
        transport_info: TransportInfo,
        soap_header: SoapHeader,
        body: HighPriorityServiceMessages,
    ) -> Result<Response<Bytes>, SoapError> {
        Ok(match body {
            HighPriorityServiceMessages::GetMdib(_) => {
                create_soap_http_response_with_headers(create_soap_response(
                    &soap_header,
                    BICEPS_ACTION_GET_MDIB_RESPONSE,
                    self.get_mdib().await?,
                )?)
            }
            HighPriorityServiceMessages::GetMdDescription(x) => {
                create_soap_http_response_with_headers(create_soap_response(
                    &soap_header,
                    BICEPS_ACTION_GET_MD_DESCRIPTION_RESPONSE,
                    self.get_md_description(x).await?,
                )?)
            }
            HighPriorityServiceMessages::GetMdState(x) => {
                create_soap_http_response_with_headers(create_soap_response(
                    &soap_header,
                    BICEPS_ACTION_GET_MD_STATE_RESPONSE,
                    self.get_md_state(x).await?,
                )?)
            }
            HighPriorityServiceMessages::GetContextStates(x) => {
                create_soap_http_response_with_headers(create_soap_response(
                    &soap_header,
                    BICEPS_ACTION_GET_CONTEXT_STATES_RESPONSE,
                    self.get_context_states(x).await?,
                )?)
            }
            HighPriorityServiceMessages::Activate(x) => {
                create_soap_http_response_with_headers(create_soap_response(
                    &soap_header,
                    BICEPS_ACTION_ACTIVATE_RESPONSE,
                    self.activate(x, transport_info).await?,
                )?)
            }
            HighPriorityServiceMessages::SetValue(x) => {
                create_soap_http_response_with_headers(create_soap_response(
                    &soap_header,
                    BICEPS_ACTION_SET_VALUE_RESPONSE,
                    self.set_value(x, transport_info).await?,
                )?)
            }
            HighPriorityServiceMessages::SetString(x) => {
                create_soap_http_response_with_headers(create_soap_response(
                    &soap_header,
                    BICEPS_ACTION_SET_STRING_RESPONSE,
                    self.set_string(x, transport_info).await?,
                )?)
            }
            HighPriorityServiceMessages::SetContextState(x) => {
                create_soap_http_response_with_headers(create_soap_response(
                    &soap_header,
                    BICEPS_ACTION_SET_CONTEXT_STATE_RESPONSE,
                    self.set_context_state(x, transport_info).await?,
                )?)
            }
            HighPriorityServiceMessages::SetComponentState(x) => {
                create_soap_http_response_with_headers(create_soap_response(
                    &soap_header,
                    BICEPS_ACTION_SET_COMPONENT_STATE_RESPONSE,
                    self.set_component_state(x, transport_info).await?,
                )?)
            }
            HighPriorityServiceMessages::SetAlertState(x) => {
                create_soap_http_response_with_headers(create_soap_response(
                    &soap_header,
                    BICEPS_ACTION_SET_ALERT_STATE_RESPONSE,
                    self.set_alert_state(x, transport_info).await?,
                )?)
            }
            HighPriorityServiceMessages::SetMetricState(x) => {
                create_soap_http_response_with_headers(create_soap_response(
                    &soap_header,
                    BICEPS_ACTION_SET_METRIC_STATE_RESPONSE,
                    self.set_metric_state(x, transport_info).await?,
                )?)
            }
            HighPriorityServiceMessages::GetContextStatesByFilter(_)
            | HighPriorityServiceMessages::GetContextStatesByIdentification(_) => {
                Err(SoapError::SoapFault(
                    Fault::builder()
                        .code(FaultCode::builder().value(FaultCodeEnum::Receiver).build())
                        .reason(FaultReason::simple_text("en", "Unimplemented actions"))
                        .build(),
                ))?
            }
        })
    }
}

impl<MDIB, OIR, EHF> HostedService for HighPriorityServices<MDIB, OIR, EHF>
where
    MDIB: LocalMdibAccess + Send + Sync + LocalMdibAccessReadTransaction + 'static + Clone,
    OIR: OperationInvocationReceiver<MDIB> + Send + Sync + 'static + Clone,
    EHF: ExtensionHandlerFactory + Send + Sync,
{
    fn service_id(&self) -> &'static str {
        SERVICE_ID
    }

    fn endpoint_reference(&self) -> WsaEndpointReference {
        self.metadata.endpoint_reference()
    }

    fn types(&self) -> Vec<ExpandedName> {
        self.metadata.types()
    }

    fn wsdl(&self) -> &'static [u8] {
        WSDL.as_bytes()
    }
}

#[derive(Clone, Debug)]
pub(crate) struct HighPriorityServicesHostedInfo {
    pub(crate) host_epr: WsaEndpointReference,
    pub(crate) host_types: Vec<ExpandedName>,
    pub(crate) endpoint_reference: WsaEndpointReference,
    pub(crate) types: Vec<ExpandedName>,
    pub(crate) wsdl_location: HttpUri,
}

impl HostedServiceInfo for HighPriorityServicesHostedInfo {
    fn service_id(&self) -> &'static str {
        SERVICE_ID
    }

    fn host_endpoint_reference(&self) -> WsaEndpointReference {
        self.host_epr.clone()
    }

    fn host_types(&self) -> Vec<ExpandedName> {
        self.host_types.clone()
    }

    fn endpoint_reference(&self) -> WsaEndpointReference {
        self.endpoint_reference.clone()
    }

    fn types(&self) -> Vec<ExpandedName> {
        self.types.clone()
    }

    fn wsdl(&self) -> &'static [u8] {
        WSDL.as_bytes()
    }

    fn wsdl_location(&self) -> HttpUri {
        self.wsdl_location.clone()
    }
}
