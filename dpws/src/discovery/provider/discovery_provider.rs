use crate::discovery::common as discovery_common;
use crate::discovery::constants::WSA_UDP_TO;
use crate::discovery::consumer::discovery_consumer::{DpwsDiscoveryError, DpwsDiscoveryMessage};
use crate::discovery::provider::discovery_proxy_provider::DpwsDiscoveryProxyProvider;
use crate::discovery::{actions, util};
use crate::xml::common::{create_header_for_action, create_header_reply_for_action};
use crate::xml::messages::addressing::WsaEndpointReference;
use crate::xml::messages::common::HttpUri;
use crate::xml::messages::discovery::{
    AppSequence, Bye, ExpandedNameList, Hello, HelloProbeMatchResolveMatch, Probe, ProbeMatch,
    ProbeMatches, Resolve, ResolveMatches,
};
use crate::xml::messages::soap::envelope::{SoapHeader, SoapMessage};
use async_trait::async_trait;
use lazy_static::lazy_static;
use log::{debug, error, trace};
use network::udp_binding::{OutboundUdpMessage, UdpBinding, UdpHeader, UdpMessage};
use protosdc_xml::XmlWriter;
use regex::Regex;
use std::collections::HashMap;
use std::fmt::{Debug, Display, Formatter};
use std::net::SocketAddr;
use std::sync::Arc;
use std::time::{SystemTime, UNIX_EPOCH};
use tokio::sync::mpsc::Sender;
use tokio::sync::{broadcast, Mutex};
use tokio::task;
use tokio_util::sync::{CancellationToken, DropGuard};

const BUFFER_SIZE: usize = 100;

lazy_static! {
    static ref FORBIDDEN_SEGMENT: Regex = Regex::new(r"/\./|/\.\./").expect("Regex invalid");
}

#[async_trait]
pub trait DpwsDiscoveryProcessor {
    /// Processes an incoming probe request.
    ///
    /// # Arguments
    ///
    /// * `probe`: the probe message to match against
    ///
    /// returns: Option<ProbeMatches> present if there was a match, absent otherwise
    async fn process_probe(&self, probe: &Probe) -> Option<(AppSequence, ProbeMatches)>;

    /// Processes an incoming resolve request.
    ///
    /// # Arguments
    ///
    /// * `resolve`: the resolve message to match against
    ///
    /// returns: Option<ResolveMatches> present if the resolve matches, empty otherwise
    async fn process_resolve(&self, resolve: &Resolve) -> Option<(AppSequence, ResolveMatches)>;

    async fn hello(&self) -> (AppSequence, Hello);
    async fn bye(&self) -> (AppSequence, Bye);

    async fn endpoint_reference(&self) -> WsaEndpointReference;

    /// Updates the scopes of the provider.
    ///
    /// # Arguments
    ///
    /// * `scopes`: new scopes of the provider
    ///
    /// returns: bool true if an update needs to be sent out, false otherwise
    async fn update_scopes(&self, scopes: Vec<String>) -> bool;

    /// Updates the physical addresses of the provider, triggers hello message if possible.
    ///
    /// # Arguments
    ///
    /// * `addresses`: new addresses of the provider
    ///
    /// returns: bool true if an update needs to be sent out, false otherwise
    async fn update_physical_address(&self, addresses: Vec<HttpUri>) -> bool;

    /// Updates the scopes and physical addresses of the provider, triggers hello message if possible.
    ///
    /// # Arguments
    ///
    /// * `scopes`: new scopes of the provider
    /// * `addresses`: new addresses of the provider
    ///
    /// returns: bool true if an update needs to be sent out, false otherwise
    async fn update(&self, scopes: Vec<String>, addresses: Vec<HttpUri>) -> bool;
}

#[derive(Debug)]
struct DpwsDiscoveryProcessorImplInner {
    endpoint: discovery_common::Endpoint,
    app_sequence: AppSequence,
}

#[derive(Clone, Debug)]
pub struct DpwsDiscoveryProcessorImpl {
    inner: Arc<Mutex<DpwsDiscoveryProcessorImplInner>>,
}

impl DpwsDiscoveryProcessorImpl {
    pub fn new(endpoint: discovery_common::Endpoint, app_sequence: Option<AppSequence>) -> Self {
        let a_s = app_sequence.unwrap_or_else(|| {
            let start = SystemTime::now();
            let since_the_epoch = start
                .duration_since(UNIX_EPOCH)
                .expect("Time went backwards");

            AppSequence {
                attributes: Default::default(),
                instance_id: since_the_epoch.as_secs(),
                sequence_id: None,
                message_number: 0,
            }
        });

        Self {
            inner: Arc::new(Mutex::new(DpwsDiscoveryProcessorImplInner {
                endpoint,
                app_sequence: a_s,
            })),
        }
    }
}

#[async_trait]
impl DpwsDiscoveryProcessor for DpwsDiscoveryProcessorImpl {
    async fn process_probe(&self, probe: &Probe) -> Option<(AppSequence, ProbeMatches)> {
        let mut inner = self.inner.lock().await;
        if !match_probe(&inner.endpoint, probe).await {
            return None;
        }

        inner.app_sequence.message_number += 1;

        Some((
            inner.app_sequence.clone(),
            ProbeMatches {
                attributes: Default::default(),
                probe_match: vec![ProbeMatch {
                    probe_match: create_match_type(&inner.endpoint).await,
                }],
                children: vec![],
            },
        ))
    }

    async fn process_resolve(&self, resolve: &Resolve) -> Option<(AppSequence, ResolveMatches)> {
        let mut inner = self.inner.lock().await;

        // check if any epr matches ours
        let any_epr_match = resolve
            .endpoint_references
            .iter()
            .any(|epr| epr.address.value == inner.endpoint.endpoint_reference.address.value);
        if !any_epr_match {
            return None;
        }
        inner.app_sequence.message_number += 1;

        Some((
            inner.app_sequence.clone(),
            ResolveMatches {
                attributes: Default::default(),
                resolve_match: Some(create_match_type(&inner.endpoint).await),
                children: vec![],
            },
        ))
    }

    async fn hello(&self) -> (AppSequence, Hello) {
        let mut inner = self.inner.lock().await;

        inner.app_sequence.message_number += 1;

        (
            inner.app_sequence.clone(),
            Hello {
                hello: create_match_type(&inner.endpoint).await,
            },
        )
    }

    async fn bye(&self) -> (AppSequence, Bye) {
        let mut inner = self.inner.lock().await;

        inner.app_sequence.message_number += 1;

        (
            inner.app_sequence.clone(),
            create_match_type(&inner.endpoint).await.into(),
        )
    }

    async fn endpoint_reference(&self) -> WsaEndpointReference {
        let inner = self.inner.lock().await;
        inner.endpoint.endpoint_reference.clone()
    }

    async fn update_scopes(&self, scopes: Vec<String>) -> bool {
        let mut inner = self.inner.lock().await;
        if scopes != inner.endpoint.scopes {
            inner.endpoint.scopes = scopes;
            inner.endpoint.metadata_version += 1;
            return true;
        }
        false
    }

    async fn update_physical_address(&self, addresses: Vec<HttpUri>) -> bool {
        let mut inner = self.inner.lock().await;
        if addresses != inner.endpoint.x_addrs {
            inner.endpoint.x_addrs = addresses;
            inner.endpoint.metadata_version += 1;
            return true;
        }
        false
    }

    async fn update(&self, scopes: Vec<String>, addresses: Vec<HttpUri>) -> bool {
        let mut inner = self.inner.lock().await;
        if scopes != inner.endpoint.scopes || addresses != inner.endpoint.x_addrs {
            inner.endpoint.scopes = scopes;
            inner.endpoint.x_addrs = addresses;
            inner.endpoint.metadata_version += 1;
            return true;
        }
        false
    }
}

// useful alias
pub trait ThreadDpwsDiscoveryProcessor: DpwsDiscoveryProcessor + Send + Sync {}
// blanket impl
impl<T> ThreadDpwsDiscoveryProcessor for T where T: DpwsDiscoveryProcessor + Send + Sync {}

#[async_trait]
pub trait DpwsDiscoveryProvider {
    /// Updates the scopes of the provider, triggers hello message if possible and necessary..
    ///
    /// # Arguments
    ///
    /// * `scopes`: new scopes of the provider
    async fn update_scopes(&mut self, scopes: Vec<String>);

    /// Updates the physical addresses of the provider, triggers hello message if possible and necessary.
    ///
    /// # Arguments
    ///
    /// * `addresses`: new addresses of the provider
    async fn update_physical_address(&mut self, addresses: Vec<HttpUri>);

    /// Updates the scopes and physical addresses of the provider, triggers hello message if possible and necessary.
    ///
    /// # Arguments
    ///
    /// * `scopes`: new scopes of the provider
    /// * `addresses`: new addresses of the provider
    async fn update(&mut self, scopes: Vec<String>, addresses: Vec<HttpUri>);

    /// Triggers hello message if possible.
    async fn hello(&mut self) -> Result<(), DpwsDiscoveryError>;

    /// Returns the Endpoint information as seen by other network peers.
    async fn endpoint_reference(&self) -> WsaEndpointReference;
}

#[derive(Debug)]
struct DpwsDiscoveryProviderImplInner {
    _udp_parsing_task: task::JoinHandle<()>,
    _udp_processing_task: task::JoinHandle<()>,
    _drop_guard: DropGuard,
}

#[derive(Clone, Debug)]
pub struct DpwsDiscoveryProviderImpl<T, D, DISCO>
where
    T: UdpBinding + Send + Sync + Clone,
    D: DpwsDiscoveryProcessor + Send + Sync + Clone,
    DISCO: DpwsDiscoveryProxyProvider + Send + Sync + Clone,
{
    udp_binding: T,

    discovery_processor: D,
    discovery_proxy: Option<DISCO>,
    _inner: Arc<Mutex<DpwsDiscoveryProviderImplInner>>,
}

impl<T, D, DISCO> Display for DpwsDiscoveryProviderImpl<T, D, DISCO>
where
    T: UdpBinding + Send + Display + Sync + Clone,
    D: DpwsDiscoveryProcessor + Send + Sync + Clone + 'static,
    DISCO: DpwsDiscoveryProxyProvider + Send + Sync + Debug + Clone,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "DiscoveryProviderImpl (udp_binding {}, disco: {:?})",
            self.udp_binding, self.discovery_proxy
        )
    }
}

async fn handle_message<D>(
    message: DpwsDiscoveryMessage,
    message_channel: &Sender<UdpMessage>,
    discovery_processor: &D,
) where
    D: DpwsDiscoveryProcessor + Send + Sync + Clone + 'static,
{
    match message {
        DpwsDiscoveryMessage::Probe {
            udp_header,
            soap_header,
            probe,
        } => {
            trace!("DpwsDiscoveryMessage::Probe");
            process_probe(
                discovery_processor,
                udp_header,
                soap_header,
                probe,
                message_channel,
            )
            .await;
        }
        DpwsDiscoveryMessage::Resolve {
            udp_header,
            soap_header,
            resolve,
        } => {
            trace!("DpwsDiscoveryMessage::Resolve");
            process_resolve(
                discovery_processor,
                udp_header,
                soap_header,
                resolve,
                message_channel,
            )
            .await;
        }
        _ => {} // don't care
    }
}

async fn run_udp_processing<D>(
    message_receiver: broadcast::Receiver<DpwsDiscoveryMessage>,
    message_channel: Sender<UdpMessage>,
    discovery_processor: D,
    cancellation_token: CancellationToken,
) where
    D: DpwsDiscoveryProcessor + Send + Sync + Clone + 'static,
{
    discovery_common::run_udp_processing(
        message_receiver,
        |msg| handle_message(msg, &message_channel, &discovery_processor),
        cancellation_token,
    )
    .await
}

impl<T, D, DISCO> DpwsDiscoveryProviderImpl<T, D, DISCO>
where
    T: UdpBinding + Send + Sync + Clone,
    D: DpwsDiscoveryProcessor + Send + Sync + Clone + 'static,
    DISCO: DpwsDiscoveryProxyProvider + Send + Sync + Clone,
{
    pub fn new(udp_binding: T, discovery_processor: D, discovery_proxy: Option<DISCO>) -> Self {
        let receiver = udp_binding.subscribe();
        let message_channel = udp_binding.get_message_channel_sender();
        let (message_sender, message_receiver) = broadcast::channel(BUFFER_SIZE);

        let cancellation_token = CancellationToken::new();

        // kickstart processing udp messages
        let udp_parsing_task =
            discovery_common::create_udp_task(receiver, message_sender, cancellation_token.clone());

        let run = run_udp_processing(
            message_receiver,
            message_channel,
            discovery_processor.clone(),
            cancellation_token.clone(),
        );
        let udp_processing_task = tokio::spawn(run);

        Self {
            udp_binding,
            discovery_processor,
            discovery_proxy,
            _inner: Arc::new(Mutex::new(DpwsDiscoveryProviderImplInner {
                _udp_processing_task: udp_processing_task,
                _udp_parsing_task: udp_parsing_task,
                _drop_guard: cancellation_token.drop_guard(),
            })),
        }
    }
}

#[async_trait]
impl<T, D, DISCO> DpwsDiscoveryProvider for DpwsDiscoveryProviderImpl<T, D, DISCO>
where
    T: UdpBinding + Display + Send + Sync + Clone,
    D: DpwsDiscoveryProcessor + Send + Sync + Clone,
    DISCO: DpwsDiscoveryProxyProvider + Send + Sync + Clone,
{
    async fn update_scopes(&mut self, scopes: Vec<String>) {
        if self.discovery_processor.update_scopes(scopes).await {
            // ignore error
            self.hello()
                .await
                .map_err(|err| error!("Unexpected error discovery: {}", err))
                .ok();
        }
    }

    async fn update_physical_address(&mut self, addresses: Vec<HttpUri>) {
        if self
            .discovery_processor
            .update_physical_address(addresses)
            .await
        {
            // ignore error
            self.hello()
                .await
                .map_err(|err| error!("Unexpected error discovery: {}", err))
                .ok();
        }
    }

    async fn update(&mut self, scopes: Vec<String>, addresses: Vec<HttpUri>) {
        if self.discovery_processor.update(scopes, addresses).await {
            // ignore error
            self.hello()
                .await
                .map_err(|err| error!("Unexpected error discovery: {}", err))
                .ok();
        }
    }

    async fn hello(&mut self) -> Result<(), DpwsDiscoveryError> {
        debug!("Sending hello message");
        let (app_sequence, hello_msg) = self.discovery_processor.hello().await;

        // Disco
        if let Some(disco) = &self.discovery_proxy {
            debug!("Sending hello via disco");
            if let Err(err) = disco.hello(hello_msg.clone()).await {
                error!("Error contacting disco: {:?}", err)
            };
        }

        // UDP
        let mut message = SoapMessage {
            header: create_header_for_action(actions::HELLO, WSA_UDP_TO),
        };

        message.header.app_sequence = Some(app_sequence);

        let mut writer = XmlWriter::new(vec![]);
        message.to_xml_complex(&mut writer, hello_msg)?;
        util::send_multicast(
            self.udp_binding.get_message_channel_sender(),
            writer.into_inner(),
        )
        .await?;

        Ok(())
    }

    async fn endpoint_reference(&self) -> WsaEndpointReference {
        self.discovery_processor.endpoint_reference().await
    }
}

async fn create_match_type(
    endpoint_inner: &discovery_common::Endpoint,
) -> HelloProbeMatchResolveMatch {
    HelloProbeMatchResolveMatch {
        attributes: HashMap::with_capacity(0),
        endpoint_reference: endpoint_inner.endpoint_reference.clone(),
        types: Some(ExpandedNameList::from(endpoint_inner.types.as_slice()).into()),
        scopes: Some(endpoint_inner.scopes.as_slice().into()),
        x_addrs: Some(endpoint_inner.x_addrs.as_slice().into()),
        metadata_version: endpoint_inner.metadata_version,
        children: vec![],
    }
}

pub enum MatchBy {
    RFC3986,
    STRCMP0,
}

impl TryFrom<&str> for MatchBy {
    type Error = ();

    //noinspection HttpUrlsUsage
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/rfc3986" => Ok(Self::RFC3986),
            "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/strcmp0" => Ok(Self::STRCMP0),
            _ => Err(()),
        }
    }
}

impl Default for MatchBy {
    fn default() -> Self {
        Self::RFC3986
    }
}

async fn match_probe(endpoint: &discovery_common::Endpoint, probe: &Probe) -> bool {
    let scopes_matching = match &probe.scopes {
        None => true,
        Some(probe_scopes) => {
            let matcher: MatchBy = match probe_scopes
                .match_by
                .as_ref()
                .map_or_else(|| Ok(MatchBy::default()), |x| MatchBy::try_from(x.as_str()))
            {
                Ok(it) => it,
                Err(_) => {
                    error!("Invalid MatchBy in Probe");
                    return false;
                }
            };
            is_scopes_matching(
                endpoint
                    .scopes
                    .iter()
                    .map(|it| it.as_str())
                    .collect::<Vec<&str>>()
                    .as_slice(),
                probe_scopes
                    .scopes
                    .list
                    .iter()
                    .map(|it| it.as_str())
                    .collect::<Vec<&str>>()
                    .as_slice(),
                matcher,
            )
            .await
        }
    };

    let types_matching = match &probe.types {
        None => true,
        Some(probe_types) => {
            is_matching(
                endpoint.types.as_slice(),
                probe_types.types.list.as_slice(),
                |o1, o2| o1 == o2,
            )
            .await
        }
    };

    scopes_matching && types_matching
}

async fn is_matching<T>(superset: &[T], subset: &[T], comparator: impl Fn(&T, &T) -> bool) -> bool {
    superset.len() >= subset.len()
        && superset
            .iter()
            .filter(|qname1| subset.iter().any(|qname2| comparator(qname1, qname2)))
            .count()
            == subset.len()
}

fn str_compare(superset_string: &&str, subset_string: &&str) -> bool {
    superset_string == subset_string
}

fn uri_compare(superset_string: &&str, subset_string: &&str) -> bool {
    let superset_str = *superset_string;
    let subset_str = *subset_string;

    let superset_uri = match http::Uri::try_from(*superset_string) {
        Ok(uri) => uri,
        Err(_) => return false,
    };

    let subset_uri = match http::Uri::try_from(*subset_string) {
        Ok(uri) => uri,
        Err(_) => return false,
    };

    if (!superset_str.is_empty() && FORBIDDEN_SEGMENT.is_match(superset_str))
        || (!subset_str.is_empty() && FORBIDDEN_SEGMENT.is_match(subset_str))
    {
        return false;
    }

    if superset_str == subset_str {
        return true;
    }
    if superset_uri.scheme() != subset_uri.scheme() {
        return false;
    }
    if superset_uri.authority() != subset_uri.authority() {
        return false;
    }

    let superset_segments: Vec<&str> = superset_uri.path().split_terminator('/').collect();
    let subset_segments: Vec<&str> = subset_uri.path().split_terminator('/').collect();

    if subset_segments.len() > superset_segments.len() {
        return false;
    }

    for i in 0..superset_segments.len() {
        match (superset_segments.get(i), subset_segments.get(i)) {
            (Some(superset), Some(subset)) => {
                if superset != subset {
                    return false;
                }
            }
            (None, Some(_)) => return false, // should be covered by length check already
            (_, _) => break,
        }
    }

    // TODO: compare schemeSpecificPart only if authority is null

    true
}

async fn is_scopes_matching(scopes: &[&str], probe_scopes: &[&str], matcher: MatchBy) -> bool {
    match matcher {
        MatchBy::RFC3986 => is_matching(scopes, probe_scopes, uri_compare).await,
        MatchBy::STRCMP0 => is_matching(scopes, probe_scopes, str_compare).await,
    }
}

async fn process_probe<D: DpwsDiscoveryProcessor>(
    discovery_processor: &D,
    udp_header: UdpHeader,
    soap_header: SoapHeader,
    probe: Probe,
    udp_message_channel: &Sender<UdpMessage>,
) {
    debug!("process_probe from {:?}", udp_header);
    let message_id = match &soap_header.message_id {
        None => {
            error!("MessageID missing in probe");
            return;
        }
        Some(message_id) => message_id,
    };

    let (app_sequence, probe_matches) = match discovery_processor.process_probe(&probe).await {
        Some(it) => it,
        None => return,
    };

    debug!(
        "Inbound probe from {:?} is applicable, sending matches",
        udp_header
    );

    let mut soap_header =
        create_header_reply_for_action(actions::PROBE_MATCHES, WSA_UDP_TO, &message_id.value);

    soap_header.app_sequence = Some(app_sequence);

    let soap_message = SoapMessage {
        header: soap_header,
    };

    let mut writer = XmlWriter::new(vec![]);
    if let Err(err) = soap_message.to_xml_complex(&mut writer, probe_matches) {
        error!("Error serializing message. {}", err);
        return;
    }

    let outbound = OutboundUdpMessage {
        data: writer.into_inner(),
        address: SocketAddr::new(udp_header.source_address, udp_header.source_port),
    };

    udp_message_channel
        .send(UdpMessage::OutboundUnicast {
            message: outbound,
            response: None,
        })
        .await
        .map_err(|err| error!("Unexpected error discovery: {}", err))
        .ok();
}

async fn process_resolve<D: DpwsDiscoveryProcessor>(
    discovery_processor: &D,
    udp_header: UdpHeader,
    soap_header: SoapHeader,
    resolve: Resolve,
    udp_message_channel: &Sender<UdpMessage>,
) {
    debug!("process_resolve");
    let message_id = match &soap_header.message_id {
        None => {
            error!("MessageID missing in resolve");
            return;
        }
        Some(message_id) => message_id,
    };

    let (app_sequence, resolve_matches) = match discovery_processor.process_resolve(&resolve).await
    {
        Some(it) => it,
        None => return,
    };

    debug!("Inbound resolve is applicable, sending matches");

    let mut soap_header =
        create_header_reply_for_action(actions::RESOLVE_MATCHES, WSA_UDP_TO, &message_id.value);

    soap_header.app_sequence = Some(app_sequence);

    let soap_message = SoapMessage {
        header: soap_header,
    };

    let mut writer = XmlWriter::new(vec![]);
    if let Err(err) = soap_message.to_xml_complex(&mut writer, resolve_matches) {
        error!("Error serializing message. {}", err);
        return;
    }

    let outbound = OutboundUdpMessage {
        data: writer.into_inner(),
        address: SocketAddr::new(udp_header.source_address, udp_header.source_port),
    };

    udp_message_channel
        .send(UdpMessage::OutboundUnicast {
            message: outbound,
            response: None,
        })
        .await
        .map_err(|err| error!("Unexpected error discovery: {}", err))
        .ok();
}

#[cfg(test)]
mod test {
    use crate::discovery::provider::discovery_provider::{
        is_scopes_matching, MatchBy, FORBIDDEN_SEGMENT,
    };
    use std::collections::HashMap;

    //noinspection HttpUrlsUsage
    const DEFAULT_SUPERSET: &str = "http://a.de/abc/d//e";

    async fn does_match(superset: &str, subset: &str) -> bool {
        let superset_array = [superset];
        let subset_array = [subset];
        is_scopes_matching(&superset_array, &subset_array, MatchBy::RFC3986).await
    }

    async fn does_match_default(subset: &str) -> bool {
        does_match(DEFAULT_SUPERSET, subset).await
    }

    pub fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn test_forbidden_segment_regex() -> anyhow::Result<()> {
        init();

        let ok = vec!["abcd/.../efgh", "abcd//efgh"];
        let not_ok = vec!["abc/../de", "abc/./de"];

        for x in ok {
            assert!(!FORBIDDEN_SEGMENT.is_match(x));
        }

        for x in not_ok {
            assert!(FORBIDDEN_SEGMENT.is_match(x));
        }

        Ok(())
    }

    //noinspection HttpUrlsUsage
    #[tokio::test]
    async fn test_matching_rfc3986_subsets() {
        init();

        let matching_subsets = vec![
            "http://a.de/abc/d//e",       // equals
            "HTTP://a.de/abc/d//e",       // case-insensitive schema
            "http://A.dE/abc/d//e",       // case-insensitive authority
            "http://a.de/abc",            // match by segment
            "http://a.de/abc/d",          // match by segment
            "http://a.de/abc/d/",         // match by segment, trailing slash ignored
            "http://a.de/abc/d//",        // match by segment, trailing slashes ignored
            "http://a.de",                // match if subset has no segments
            "http://a.de/abc?a=x&b=y",    // query parameters are ignored during comparison
            "http://a.de/abc#fragement1", // fragments are ignored
        ];

        for subset in matching_subsets {
            assert!(
                does_match_default(subset).await,
                "{} was not a subset of {}",
                subset,
                DEFAULT_SUPERSET
            )
        }

        let not_matching_subsets: HashMap<&str, &str> = [
            ("urn:oid", "urs:oid"), // not equal
            // TODO
            // ("http://a.de/abc/../d", "http://a.de/abc/../d"),         // equals but contains ".."
            ("http://a.de", "http://a.de/a"), // superset path is null
            ("https://127.0.0.1:46581", "https://127.0.0.1:465"), // port doesn't match
            ("sdc.mds.pkp:/1.2.840", "sdc.mds.pkp:/a.2.840"), // scheme specifics not equal
            // TODO
            // ("sdc.mds.pkp:/2.2.840.10004.20701.1.1", "sdc.mds.pkp:/"), // scheme specifics is null
            ("sdc.mds.pkp:/a.2.840", "sdc.mds.pkp:/A.2.840"), // case-sensitive scheme specifics
            ("http://user:pass@a.de/abc", "http://a.de/abc"), // authority includes user info
        ]
        .into_iter()
        .collect();

        for (superset, subset) in not_matching_subsets {
            if superset.contains("/./") {
                println!("{}", superset);
            }
            assert!(
                !does_match(superset, subset).await,
                "{} was a subset of {}",
                subset,
                superset
            )
        }
    }
}
