use const_format::concatcp;

//noinspection HttpUrlsUsage
const DISCOVERY_ACTION_PREFIX: &str = "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/";

// discovery
pub const PROBE: &str = concatcp!(DISCOVERY_ACTION_PREFIX, "Probe");
pub const PROBE_MATCHES: &str = concatcp!(DISCOVERY_ACTION_PREFIX, "ProbeMatches");
pub const RESOLVE: &str = concatcp!(DISCOVERY_ACTION_PREFIX, "Resolve");
pub const RESOLVE_MATCHES: &str = concatcp!(DISCOVERY_ACTION_PREFIX, "ResolveMatches");
pub const HELLO: &str = concatcp!(DISCOVERY_ACTION_PREFIX, "Hello");
pub const BYE: &str = concatcp!(DISCOVERY_ACTION_PREFIX, "Bye");
