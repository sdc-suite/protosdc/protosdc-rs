use crate::xml::messages::common::WS_DISCOVERY_NAMESPACE;
use protosdc_xml::QNameSlice;
use quick_xml::name::{Namespace, ResolveResult};

pub const WS_DISCOVERY_PORT: u16 = 3702;

/// WS-Addressing To-field for UDP multicast sinks.
pub const WSA_UDP_TO: &str = "urn:docs-oasis-open-org:ws-dd:ns:discovery:2009:01";

pub const HELLO_TAG: QNameSlice = (
    ResolveResult::Bound(Namespace(WS_DISCOVERY_NAMESPACE.as_bytes())),
    b"Hello",
);
pub const BYE_TAG: QNameSlice = (
    ResolveResult::Bound(Namespace(WS_DISCOVERY_NAMESPACE.as_bytes())),
    b"Bye",
);
pub const PROBE_TAG: QNameSlice = (
    ResolveResult::Bound(Namespace(WS_DISCOVERY_NAMESPACE.as_bytes())),
    b"Probe",
);
pub const PROBE_MATCHES_TAG: QNameSlice = (
    ResolveResult::Bound(Namespace(WS_DISCOVERY_NAMESPACE.as_bytes())),
    b"ProbeMatches",
);
pub const RESOLVE_TAG: QNameSlice = (
    ResolveResult::Bound(Namespace(WS_DISCOVERY_NAMESPACE.as_bytes())),
    b"Resolve",
);
pub const RESOLVE_MATCHES_TAG: QNameSlice = (
    ResolveResult::Bound(Namespace(WS_DISCOVERY_NAMESPACE.as_bytes())),
    b"ResolveMatches",
);
