use crate::common::extension_handling::ReplacementExtensionHandlerFactory;
use crate::discovery::actions;
use crate::discovery::consumer::discovery_consumer::{
    create_probe, create_resolve, DpwsDiscoveryConsumer, DpwsDiscoveryError, DpwsDiscoveryEvent,
    ProbeOptions,
};
use crate::http::server::common::{Server, ServerRegistry};
use crate::soap::common::SoapError;
use crate::soap::dpws::client::event_sink::{EventSink, EventSinkError, EventSinkImpl};
use crate::soap::dpws::client::notification_sink::{NotificationSinkError, NotificationSinkTyped};
use crate::soap::dpws::subscription_manager::SinkSubscriptionManagerImpl;
use crate::soap::dpws::ws_eventing::sink::WsEventingSink;
use crate::soap::eventing::ComplexXmlTypeReadInner;
use crate::soap::soap_client::SoapClientCloneable;
use crate::xml::common::SoapHeaderBuilder;
use crate::xml::messages::common::{
    SoapEventMessage, BYE_TAG, HELLO_TAG, WS_DISCOVERY_NAMESPACE, WS_DISCOVERY_NAMESPACE_BYTES,
};
use crate::xml::messages::discovery::{Bye, Hello, Probe, ProbeMatches, Resolve, ResolveMatches};
use crate::xml::messages::soap::fault::{Fault, FaultCode, FaultCodeEnum, FaultReason};
use crate::xml::messages::ws_eventing::common::Filter;
use async_trait::async_trait;
use common::broadcast_event_log_error;
use log::error;
use protosdc_biceps::types::AnyContent;
use protosdc_xml::{GenericXmlReaderComplexTypeRead, ParserError};
use quick_xml::events::BytesStart;
use quick_xml::name::{Namespace, ResolveResult};
use std::io::BufRead;
use std::marker::PhantomData;
use std::net::SocketAddr;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::broadcast::{Receiver, Sender};
use tokio::sync::{broadcast, Mutex};

pub const DIALECT: &str = "http://discoproxy";

#[derive(Clone, Debug, PartialEq)]
pub enum SubscriptionMessage {
    Hello(Hello),
    Bye(Bye),
}

impl SoapEventMessage for SubscriptionMessage {
    fn ns_tag() -> ResolveResult<'static> {
        ResolveResult::Bound(Namespace(WS_DISCOVERY_NAMESPACE_BYTES))
    }

    fn ns_tag_str() -> Option<&'static str> {
        Some(WS_DISCOVERY_NAMESPACE)
    }
}

impl ComplexXmlTypeReadInner for SubscriptionMessage {
    fn from_xml_complex<B: BufRead>(
        tag_ns: ResolveResult<'static>,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Box<(dyn AnyContent + Send + Sync + 'static)>>,
    ) -> Result<Self, ParserError> {
        match (tag_ns, event.local_name().into_inner()) {
            HELLO_TAG => Ok(Self::Hello(Hello::from_xml_complex(
                HELLO_TAG, event, reader,
            )?)),
            BYE_TAG => Ok(Self::Bye(Bye::from_xml_complex(BYE_TAG, event, reader)?)),
            _ => Err(ParserError::OneOfParserFallthrough {
                parser_name: "Event did not contain an hello or bye message".to_string(),
            }),
        }
    }
}

/// Represents a consumer of DPWS discovery proxy.
#[async_trait]
pub trait DpwsDiscoveryProxyConsumer {
    async fn probe(&self, probe: Probe) -> Result<ProbeMatches, SoapError>;

    async fn resolve(&self, resolve: Resolve) -> Result<ResolveMatches, SoapError>;

    async fn subscribe(&self) -> Result<Receiver<SubscriptionMessage>, SoapError>;
}

/// # Warning
/// This implementation does not represent a finished specification, avoid using it.
pub struct DpwsDiscoveryProxyConsumerImpl<SC, REG, SERVER>
where
    SC: SoapClientCloneable + Send + Sync,
    REG: ServerRegistry<SERVER> + Send + Sync + Clone,
    SERVER: Server,
{
    soap_client: SC,
    registry: REG,
    discovery_proxy_address: String,
    host_address: SocketAddr,
    discovery_event_sender: Sender<DpwsDiscoveryEvent>,
    _server: PhantomData<SERVER>,
}

impl<SC, REG, SERVER> DpwsDiscoveryProxyConsumerImpl<SC, REG, SERVER>
where
    SC: SoapClientCloneable + Send + Sync,
    REG: ServerRegistry<SERVER> + Send + Sync + Clone,
    SERVER: Server,
{
    pub fn new(
        disco_address: String,
        host_address: SocketAddr,
        soap_client: SC,
        registry: REG,
    ) -> Self {
        let (rx, _) = broadcast::channel(10);
        Self {
            soap_client,
            registry,
            discovery_proxy_address: disco_address,
            host_address,
            discovery_event_sender: rx,
            _server: PhantomData,
        }
    }
}
#[async_trait]
impl<SC, REG, SERVER> DpwsDiscoveryProxyConsumer for DpwsDiscoveryProxyConsumerImpl<SC, REG, SERVER>
where
    SC: SoapClientCloneable + Send + Sync + 'static,
    REG: ServerRegistry<SERVER> + Send + Sync + Clone,
    SERVER: Server + Send + Sync + Clone + 'static,
{
    async fn probe(&self, probe: Probe) -> Result<ProbeMatches, SoapError> {
        self.soap_client
            .request_response(SoapHeaderBuilder::new().action(actions::PROBE), probe)
            .await
            .map(|it| it.1)
    }

    async fn resolve(&self, resolve: Resolve) -> Result<ResolveMatches, SoapError> {
        self.soap_client
            .request_response(SoapHeaderBuilder::new().action(actions::RESOLVE), resolve)
            .await
            .map(|it| it.1)
    }

    async fn subscribe(&self) -> Result<Receiver<SubscriptionMessage>, SoapError> {
        let event_sink: EventSinkImpl<_, _, _, SinkSubscriptionManagerImpl> = EventSinkImpl::new(
            self.soap_client
                .new_url(self.discovery_proxy_address.as_str()),
            self.host_address,
            self.registry.clone(),
        );

        let filter = Some(Filter {
            attributes: Default::default(),
            dialect: Some(DIALECT.to_string()),
            filters: None,
        });

        let (tx, rx) = broadcast::channel(100);

        event_sink
            .subscribe(
                filter,
                Duration::from_secs(3600),
                WsEventingSink::new(
                    DiscoveryProxySink {
                        processor: Arc::new(Mutex::new(tx)),
                    },
                    ReplacementExtensionHandlerFactory,
                ),
            )
            .await
            .map_err(|err| {
                error!("{:?}", err);
                match err {
                    EventSinkError::SoapError(err) => err,
                    EventSinkError::SubscriptionNotFound(err) => SoapError::Other(err),
                    EventSinkError::ExpiresMissing => {
                        SoapError::Other("Expires missing".to_string())
                    }
                }
            })?;

        Ok(rx)
    }
}

struct DiscoveryProxySink {
    processor: Arc<Mutex<Sender<SubscriptionMessage>>>,
}

#[async_trait]
impl NotificationSinkTyped<SubscriptionMessage> for DiscoveryProxySink {
    async fn receive_notification(
        &self,
        notification: SubscriptionMessage,
    ) -> Result<(), NotificationSinkError> {
        self.processor
            .lock()
            .await
            .send(notification)
            .map_err(|_err| {
                NotificationSinkError::SoapError(SoapError::SoapFault(
                    Fault::builder()
                        .code(FaultCode::builder().value(FaultCodeEnum::Receiver).build())
                        .reason(FaultReason::simple_text("en", "Receiver ended"))
                        .build(),
                ))
            })?;
        Ok(())
    }
}

#[async_trait]
impl<SC, REG, SERVER> DpwsDiscoveryConsumer for DpwsDiscoveryProxyConsumerImpl<SC, REG, SERVER>
where
    SC: SoapClientCloneable + Send + Sync + 'static,
    REG: ServerRegistry<SERVER> + Send + Sync + Clone,
    SERVER: Server + Send + Sync + Clone + 'static,
{
    fn subscribe(&self) -> Receiver<DpwsDiscoveryEvent> {
        self.discovery_event_sender.subscribe()
    }

    async fn probe(
        &self,
        options: ProbeOptions<'_>,
    ) -> Result<Vec<ProbeMatches>, DpwsDiscoveryError> {
        let response =
            DpwsDiscoveryProxyConsumer::probe(self, create_probe(options.types, options.scopes))
                .await
                .map(|it| vec![it])?;
        for resp in &response {
            broadcast_event_log_error(
                &self.discovery_event_sender,
                DpwsDiscoveryEvent::ProbeMatches(resp.clone()),
            );
        }
        Ok(response)
    }

    async fn resolve(
        &self,
        epr: &str,
        _max_wait: Option<Duration>,
    ) -> Result<ResolveMatches, DpwsDiscoveryError> {
        Ok(DpwsDiscoveryProxyConsumer::resolve(self, create_resolve(&[epr])).await?)
    }

    // async fn directed_probe() -> Result<ProbeMatches, DpwsDiscoveryError> {
    //     todo!()
    // }
}
