use async_trait::async_trait;
use log::{debug, error, trace};
use protosdc_mapping::MappingError;
use std::collections::HashMap;
use std::fmt::Debug;
use std::ops::Sub;
use std::time::{Duration, Instant};

use network::udp_binding::{UdpBinding, UdpHeader};
use thiserror::Error;
use tokio::task;

use crate::xml::messages::discovery::{Bye, Hello, Probe, ProbeMatches, Resolve, ResolveMatches};

use crate::discovery::common as discovery_common;
use crate::discovery::{actions, constants, util};
use crate::soap::common::SoapError;
use crate::xml::common::create_header_for_action;
use crate::xml::messages::addressing::{WsaAttributedURIType, WsaEndpointReference};
use crate::xml::messages::soap::envelope::{SoapHeader, SoapMessage};
use protosdc::discovery::common::error::DiscoveryError;
use protosdc_xml::{ExpandedName, WriterError, XmlWriter};
use tokio::sync::broadcast::{
    channel as BroadcastChannel, Receiver as BroadcastReceiver, Sender as BroadcastSender,
};
use tokio::time::timeout;
use tokio_stream::wrappers::BroadcastStream;
use tokio_stream::StreamExt;
use tokio_util::sync::{CancellationToken, DropGuard};
use typed_builder::TypedBuilder;

pub const DEFAULT_MAX_WAIT: Duration = Duration::from_secs(10);
const BUFFER_SIZE: usize = 100;

#[derive(Clone, Debug)]
pub enum DpwsDiscoveryMessage {
    Probe {
        udp_header: UdpHeader,
        soap_header: SoapHeader,
        probe: Probe,
    },
    ProbeMatches {
        udp_header: UdpHeader,
        soap_header: SoapHeader,
        probe_matches: ProbeMatches,
    },
    Resolve {
        udp_header: UdpHeader,
        soap_header: SoapHeader,
        resolve: Resolve,
    },
    ResolveMatches {
        udp_header: UdpHeader,
        soap_header: SoapHeader,
        resolve_matches: ResolveMatches,
    },
    Hello {
        udp_header: UdpHeader,
        soap_header: SoapHeader,
        hello: Hello,
    },
    Bye {
        udp_header: UdpHeader,
        soap_header: SoapHeader,
        bye: Bye,
    },
}

#[derive(Debug, Clone)]
pub enum DpwsDiscoveryEvent {
    ProbeMatches(ProbeMatches),
    Hello(Hello),
    Bye(Bye),
    SearchTimeout { search_id: String },
}

#[derive(Debug)]
pub struct DpwsDiscoveryConsumerImpl<T>
where
    T: UdpBinding + Send,
{
    udp_binding: T,
    _drop_guard: DropGuard,
    _udp_parsing_task: task::JoinHandle<()>,
    _udp_processing_task: task::JoinHandle<()>,
    discovery_event_sender: BroadcastSender<DpwsDiscoveryEvent>,
    discovery_message_sender: BroadcastSender<DpwsDiscoveryMessage>,
}

async fn handle_message(
    message: DpwsDiscoveryMessage,
    event_sender: &BroadcastSender<DpwsDiscoveryEvent>,
) {
    match message {
        DpwsDiscoveryMessage::Hello { hello, .. } => {
            event_sender
                .send(DpwsDiscoveryEvent::Hello(hello))
                .map_err(|_err| trace!("udp_processing_task: No receivers for hello"))
                .ok();
        }
        DpwsDiscoveryMessage::Bye { bye, .. } => {
            event_sender
                .send(DpwsDiscoveryEvent::Bye(bye))
                .map_err(|_err| trace!("udp_processing_task: No receivers for bye"))
                .ok();
        }
        // don't care
        _ => {}
    }
}
async fn run_udp_processing(
    message_receiver: BroadcastReceiver<DpwsDiscoveryMessage>,
    event_sender: BroadcastSender<DpwsDiscoveryEvent>,
    cancellation_token: CancellationToken,
) {
    discovery_common::run_udp_processing(
        message_receiver,
        |msg| handle_message(msg, &event_sender),
        cancellation_token,
    )
    .await
}

impl<T> DpwsDiscoveryConsumerImpl<T>
where
    T: UdpBinding + Send,
{
    pub fn new(udp_binding: T) -> Self {
        let (discovery_event_sender, _) = BroadcastChannel(BUFFER_SIZE);
        let (discovery_message_sender, _) = BroadcastChannel(BUFFER_SIZE);

        let receiver = udp_binding.subscribe();

        let event_sender = discovery_event_sender.clone();
        let message_receiver = discovery_message_sender.subscribe();

        let cancellation_token = CancellationToken::new();

        // kickstart processing udp messages
        let udp_parsing_task = discovery_common::create_udp_task(
            receiver,
            discovery_message_sender.clone(),
            cancellation_token.clone(),
        );

        let run = run_udp_processing(message_receiver, event_sender, cancellation_token.clone());
        let udp_processing_task = tokio::spawn(run);

        Self {
            udp_binding,
            _udp_parsing_task: udp_parsing_task,
            _udp_processing_task: udp_processing_task,
            discovery_event_sender,
            discovery_message_sender,
            _drop_guard: cancellation_token.drop_guard(),
        }
    }
}

#[derive(Debug, Error)]
pub enum DpwsDiscoveryError {
    #[error(transparent)]
    DiscoveryError(#[from] DiscoveryError),
    #[error(transparent)]
    MappingError(#[from] MappingError),
    #[error(transparent)]
    WriterError(#[from] WriterError),
    #[error(transparent)]
    SoapError(#[from] SoapError),
    #[error("No response received")]
    NoResponse,
}

#[derive(TypedBuilder)]
pub struct ProbeOptions<'a> {
    #[builder(default)]
    pub(crate) types: &'a [ExpandedName],
    #[builder(default)]
    pub(crate) scopes: &'a [String],
    #[builder(default, setter(strip_option))]
    pub(crate) max_responses: Option<usize>,
    #[builder(default=DEFAULT_MAX_WAIT)]
    pub(crate) max_wait: Duration,
}

/// Trait representing the DPWS discovery functionality for a consumer.
#[async_trait]
pub trait DpwsDiscoveryConsumer {
    fn subscribe(&self) -> BroadcastReceiver<DpwsDiscoveryEvent>;

    /// Sends a probe request to discover devices using DPWS (Device Profile for Web Services) discovery.
    ///
    /// # Arguments
    ///
    /// * `types` - A slice of `ExpandedName` representing the types of devices to discover.
    /// * `scopes` - A slice of `String` representing the scopes to filter the discovery on.
    /// * `max_responses` - An optional `usize` indicating the maximum number of responses to wait for. If `None`, all responses will be collected.
    /// * `max_wait` - An optional `Duration` indicating the maximum duration to wait for responses. If `None`, a default duration will be used.
    ///
    /// # Returns
    ///
    /// Returns a `Result` containing a vector of `ProbeMatches` representing the discovered devices, or a `DpwsDiscoveryError` if an error occurs.
    async fn probe(
        &self,
        options: ProbeOptions<'_>,
    ) -> Result<Vec<ProbeMatches>, DpwsDiscoveryError>;

    /// Sends a resolve request to discover a device using its endpoint reference.
    ///
    /// # Arguments
    ///
    /// - `epr`: A string representing the Endpoint Reference to be resolved.
    ///
    /// - `max_wait`: An optional `Duration` to specify the maximum time
    ///   to wait for the resolution. If `Some`, this function will wait
    ///   until the resolution is completed or the specified duration has passed.
    ///   If `None`, a default duration will be used.
    ///
    /// # Returns
    ///
    /// A `Result` containing the resolve matches response if
    /// the resolution is successful, or a `DpwsDiscoveryError` if it fails.
    async fn resolve(
        &self,
        epr: &str,
        max_wait: Option<Duration>,
    ) -> Result<ResolveMatches, DpwsDiscoveryError>;

    // TODO: consider implementation
    // async fn directed_probe() -> Result<ProbeMatches, DpwsDiscoveryError>;
}

pub(crate) fn create_probe(types: &[ExpandedName], scopes: &[String]) -> Probe {
    Probe {
        attributes: Default::default(),
        types: Some(types.into()),
        scopes: Some(scopes.into()),
        children: Vec::with_capacity(0),
    }
}

pub(crate) fn create_resolve(epr: &[&str]) -> Resolve {
    Resolve {
        attributes: Default::default(),
        children: Vec::with_capacity(0),
        endpoint_references: epr
            .iter()
            .map(|it| {
                let addr: WsaAttributedURIType = WsaAttributedURIType::from(*it);
                WsaEndpointReference {
                    attributes: HashMap::with_capacity(0),
                    address: addr,
                    reference_parameters: None,
                    metadata: None,
                    children: Vec::with_capacity(0),
                }
            })
            .collect(),
    }
}

#[async_trait]
impl<T> DpwsDiscoveryConsumer for DpwsDiscoveryConsumerImpl<T>
where
    T: UdpBinding + Sync + Send,
{
    fn subscribe(&self) -> BroadcastReceiver<DpwsDiscoveryEvent> {
        self.discovery_event_sender.subscribe()
    }

    async fn probe(
        &self,
        options: ProbeOptions<'_>,
    ) -> Result<Vec<ProbeMatches>, DpwsDiscoveryError> {
        let header = create_header_for_action(actions::PROBE, constants::WSA_UDP_TO);
        let message_id = header.message_id.as_ref().unwrap().value.to_string();

        let probe = create_probe(options.types, options.scopes);
        let soap_message = SoapMessage { header };

        let mut writer = XmlWriter::new(vec![]);
        soap_message.to_xml_complex(&mut writer, probe)?;

        let search_id = format!("search_{}", message_id);
        let search_task = run_probe(
            search_id,
            message_id,
            options.max_responses,
            options.max_wait,
            self.discovery_message_sender.subscribe(),
            self.discovery_event_sender.clone(),
        );

        util::send_multicast(
            self.udp_binding.get_message_channel_sender(),
            writer.into_inner(),
        )
        .await?;

        Ok(search_task.await)
    }

    async fn resolve(
        &self,
        epr: &str,
        max_wait: Option<Duration>,
    ) -> Result<ResolveMatches, DpwsDiscoveryError> {
        let header = create_header_for_action(actions::RESOLVE, constants::WSA_UDP_TO);
        let message_id = header.message_id.as_ref().unwrap().value.to_string();

        let arr = [epr];
        let resolve = create_resolve(arr.as_slice());
        let soap_message = SoapMessage { header };

        let mut writer = XmlWriter::new(vec![]);
        soap_message.to_xml_complex(&mut writer, resolve)?;

        let search_task = run_resolve(
            message_id,
            max_wait.unwrap_or(DEFAULT_MAX_WAIT),
            self.discovery_message_sender.subscribe(),
        );

        util::send_multicast(
            self.udp_binding.get_message_channel_sender(),
            writer.into_inner(),
        )
        .await?;

        Ok(search_task.await?)
    }

    // async fn directed_probe() -> Result<ProbeMatches, DpwsDiscoveryError> {
    //     todo!()
    // }
}

async fn run_resolve(
    message_id: String,
    max_wait: Duration,
    message_receiver: BroadcastReceiver<DpwsDiscoveryMessage>,
) -> Result<ResolveMatches, DpwsDiscoveryError> {
    let mut message_stream = BroadcastStream::new(message_receiver);
    let start = Instant::now();

    let mut remaining = max_wait.sub(start.elapsed());
    while let Ok(Some(item)) = timeout(remaining, message_stream.next()).await {
        remaining = max_wait.sub(start.elapsed());
        match item {
            Ok(DpwsDiscoveryMessage::ResolveMatches {
                soap_header,
                resolve_matches,
                ..
            }) => {
                debug!("Processing DpwsDiscoveryMessage::ResolveMatches in run_resolve");
                let addressing_matches = match soap_header.relates_to {
                    Some(a) => a.value.as_str() == message_id,
                    None => false,
                };
                if !addressing_matches {
                    continue;
                }
                return Ok(resolve_matches);
            }
            Ok(_) => {
                // don't care, not a resolve
            }
            Err(err) => {
                error!("Error during discovery {:?}", err)
            }
        }
    }
    Err(DpwsDiscoveryError::NoResponse)
}

async fn run_probe(
    search_id: String,
    message_id: String,
    max_response: Option<usize>,
    max_wait: Duration,
    message_receiver: BroadcastReceiver<DpwsDiscoveryMessage>,
    event_sender: BroadcastSender<DpwsDiscoveryEvent>,
) -> Vec<ProbeMatches> {
    let mut message_stream = BroadcastStream::new(message_receiver);
    let mut data = vec![];
    let start = Instant::now();
    let mut response_count = 0;

    let mut remaining = max_wait.sub(start.elapsed());
    'outer: while let Ok(Some(item)) = timeout(remaining, message_stream.next()).await {
        remaining = max_wait.sub(start.elapsed());
        match item {
            Ok(DpwsDiscoveryMessage::ProbeMatches {
                soap_header,
                probe_matches,
                ..
            }) => {
                debug!("Processing DpwsDiscoveryMessage::ProbeMatches in run_probe");
                let addressing_matches = match soap_header.relates_to {
                    Some(a) => a.value.as_str() == message_id,
                    None => false,
                };
                if !addressing_matches {
                    continue;
                }
                match event_sender.send(DpwsDiscoveryEvent::ProbeMatches(probe_matches.clone())) {
                    Ok(_) => {}
                    Err(err) => trace!("No receivers for event messages, dropping: {:?}", err),
                };
                data.push(probe_matches);
                response_count += 1;
                if let Some(max) = max_response {
                    if response_count >= max {
                        break 'outer;
                    }
                }
            }
            Ok(_) => {
                // don't care, not a probe
            }
            Err(err) => {
                error!("Error during discovery {:?}", err)
            }
        }
    }
    match event_sender.send(DpwsDiscoveryEvent::SearchTimeout {
        search_id: search_id.clone(),
    }) {
        Ok(_) => {}
        Err(err) => trace!("No receivers for event messages, dropping: {:?}", err),
    };
    data
}
