use crate::discovery::actions;
use crate::discovery::constants::{
    BYE_TAG, HELLO_TAG, PROBE_MATCHES_TAG, PROBE_TAG, RESOLVE_MATCHES_TAG, RESOLVE_TAG,
};
use crate::discovery::consumer::discovery_consumer::DpwsDiscoveryMessage;
use crate::xml::messages::addressing::WsaEndpointReference;
use crate::xml::messages::common::{HttpUri, SOAP_ENVELOPE_TAG};
use crate::xml::messages::discovery::{Bye, Hello, Probe, ProbeMatches, Resolve, ResolveMatches};
use crate::xml::messages::soap::envelope::SoapMessage;
use log::{debug, error, trace, warn};
use network::udp_binding::InboundUdpMessage;
use protosdc_xml::{
    find_start_element_reader, ExpandedName, GenericXmlReaderComplexTypeRead, ParserError,
    XmlReader,
};
use std::future::Future;
use tokio::sync::broadcast::{Receiver, Sender};
use tokio::task::JoinHandle;
use tokio_stream::wrappers::BroadcastStream;
use tokio_stream::StreamExt;
use tokio_util::sync::CancellationToken;

pub(crate) async fn run_udp_processing<F, Fut>(
    message_receiver: Receiver<DpwsDiscoveryMessage>,
    mut handler: F,
    cancellation_token: CancellationToken,
) where
    F: FnMut(DpwsDiscoveryMessage) -> Fut,
    Fut: Future<Output = ()>,
{
    let mut udp_stream = BroadcastStream::new(message_receiver);
    loop {
        tokio::select! {
            _ = cancellation_token.cancelled() => {
                debug!("run_udp_processing cancelled");
                break;
            }
            msg = udp_stream.next() => {
                match msg {
                    Some(Ok(item)) => {
                        handler(item).await
                    }
                    _ => {
                        debug!("run_udp_processing stream ended");
                        break;
                    }
                }
            }
        }
    }
}

pub(crate) fn create_udp_task(
    receiver: Receiver<InboundUdpMessage>,
    message_sender: Sender<DpwsDiscoveryMessage>,
    cancellation_token: CancellationToken,
) -> JoinHandle<()> {
    // kickstart processing udp messages
    tokio::spawn(async move {
        let mut udp_stream = BroadcastStream::new(receiver);

        loop {
            tokio::select! {
                _ = cancellation_token.cancelled() => {
                    debug!("udp_task cancelled");
                    break
                }
                next = udp_stream.next() => {
                    match next {
                        None => {
                            debug!("udp_task empty next, cancelling");
                            break
                        },
                        Some(item_opt) => {
                            let item = match item_opt {
                                Ok(item) => item,
                                Err(err) => {
                                    error!("err: {:?}", err);
                                    break;
                                }
                            };
                            // parse udp message
                            trace!("inbound udp message: {:?}", item);

                            let mut reader = XmlReader::create_typed(item.data.as_slice());
                            let mut buf = Vec::with_capacity(item.data.len());

                            // start by parsing soap envelope and header
                            let soap_message: Result<SoapMessage, ParserError> =
                                find_start_element_reader!(SoapMessage, SOAP_ENVELOPE_TAG, reader, buf);

                            match soap_message {
                                Err(err) => error!("Error parsing soap message: {}", err),
                                Ok(message) => {
                                    // check action, parse body
                                    match &message.header.action {
                                        None => {
                                            warn!("Message with no action, no idea what is happening. Sender: {:?}, message header {:?}", item.udp_header, &message.header)
                                        }
                                        Some(action) => {
                                            let discovery_message = match action.value.as_str() {
                                                actions::PROBE => {
                                                    let probe: Result<Probe, ParserError> =
                                                        find_start_element_reader!(Probe, PROBE_TAG, reader, buf);
                                                    probe.map(|it| DpwsDiscoveryMessage::Probe {
                                                        udp_header: item.udp_header,
                                                        soap_header: message.header,
                                                        probe: it,
                                                    })
                                                }
                                                actions::PROBE_MATCHES => {
                                                    let probe_matches: Result<ProbeMatches, ParserError> = find_start_element_reader!(
                                                        ProbeMatches,
                                                        PROBE_MATCHES_TAG,
                                                        reader,
                                                        buf
                                                    );
                                                    probe_matches.map(|it| DpwsDiscoveryMessage::ProbeMatches {
                                                        udp_header: item.udp_header,
                                                        soap_header: message.header,
                                                        probe_matches: it,
                                                    })
                                                }
                                                actions::RESOLVE => {
                                                    let resolve: Result<Resolve, ParserError> = find_start_element_reader!(
                                                        Resolve,
                                                        RESOLVE_TAG,
                                                        reader,
                                                        buf
                                                    );
                                                    resolve.map(|it| DpwsDiscoveryMessage::Resolve {
                                                        udp_header: item.udp_header,
                                                        soap_header: message.header,
                                                        resolve: it,
                                                    })
                                                }
                                                actions::RESOLVE_MATCHES => {
                                                    let resolve_matches: Result<ResolveMatches, ParserError> = find_start_element_reader!(
                                                        ResolveMatches,
                                                        RESOLVE_MATCHES_TAG,
                                                        reader,
                                                        buf
                                                    );
                                                    resolve_matches.map(|it| DpwsDiscoveryMessage::ResolveMatches {
                                                        udp_header: item.udp_header,
                                                        soap_header: message.header,
                                                        resolve_matches: it,
                                                    })
                                                }
                                                actions::HELLO => {
                                                    let hello: Result<Hello, ParserError> =
                                                        find_start_element_reader!(Hello, HELLO_TAG, reader, buf);
                                                    hello.map(|it| DpwsDiscoveryMessage::Hello {
                                                        udp_header: item.udp_header,
                                                        soap_header: message.header,
                                                        hello: it,
                                                    })
                                                }
                                                actions::BYE => {
                                                    let bye: Result<Bye, ParserError> =
                                                        find_start_element_reader!(Bye, BYE_TAG, reader, buf);
                                                    bye.map(|it| DpwsDiscoveryMessage::Bye {
                                                        udp_header: item.udp_header,
                                                        soap_header: message.header,
                                                        bye: it,
                                                    })
                                                }
                                                _ => Err(ParserError::Other),
                                            };

                                            trace!("Received message {:?}", discovery_message);

                                            if let Ok(msg) = discovery_message {
                                                message_sender
                                                    .send(msg)
                                                    .map_err(|err| {
                                                        error!("Unexpected error in event distribution: {}", err)
                                                    })
                                                    .ok();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    })
}

/// Represents an endpoint in a service.
#[derive(Clone, Debug)]
pub struct Endpoint {
    pub scopes: Vec<String>,
    pub types: Vec<ExpandedName>,
    pub endpoint_reference: WsaEndpointReference,
    // there is no way around it, this must be a proper http uri
    pub x_addrs: Vec<HttpUri>,
    pub metadata_version: u32,
}
