use crate::xml::messages::addressing::XmlElement;
use crate::xml::messages::common::{
    write_attributes, DO_NOT_CARE_QNAME, DPWS_FIRMWARE_VERSION, DPWS_FIRMWARE_VERSION_TAG_REF,
    DPWS_FRIENDLY_NAME_TAG, DPWS_FRIENDLY_NAME_TAG_REF, DPWS_FRIENDLY_NAME_TAG_STR,
    DPWS_MANUFACTURER_TAG, DPWS_MANUFACTURER_TAG_REF, DPWS_MANUFACTURER_TAG_STR,
    DPWS_MANUFACTURER_URL, DPWS_MANUFACTURER_URL_TAG_REF, DPWS_MODEL_NAME_TAG,
    DPWS_MODEL_NAME_TAG_REF, DPWS_MODEL_NAME_TAG_STR, DPWS_MODEL_NUMBER, DPWS_MODEL_NUMBER_TAG_REF,
    DPWS_MODEL_URL, DPWS_MODEL_URL_TAG_REF, DPWS_NAMESPACE, DPWS_PRESENTATION_URL,
    DPWS_PRESENTATION_URL_TAG_REF, DPWS_SERIAL_NUMBER, DPWS_SERIAL_NUMBER_TAG_REF,
    DPWS_THIS_DEVICE, DPWS_THIS_MODEL,
};
use crate::xml::messages::dpws::common::LocalizedStringType;
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{ComplexXmlTypeWrite, QNameStr, WriterError, XmlWriter};
use protosdc_xml::{ExpandedName, GenericXmlReaderComplexTypeRead, ParserError};
use quick_xml::events::{BytesStart, Event};
use std::collections::HashMap;
use std::io::BufRead;
use typed_builder::TypedBuilder;

#[derive(Clone, Debug, PartialEq, TypedBuilder)]
pub struct ThisModel {
    #[builder(default)]
    pub attributes: HashMap<ExpandedName, String>,
    pub manufacturer: Vec<LocalizedStringType>, // at least one
    #[builder(default, setter(strip_option, into))]
    pub manufacturer_url: Option<String>,
    pub model_name: Vec<LocalizedStringType>, // at least one
    #[builder(default, setter(strip_option, into))]
    pub model_number: Option<String>,
    #[builder(default, setter(strip_option, into))]
    pub model_url: Option<String>,
    #[builder(default, setter(strip_option, into))]
    pub presentation_url: Option<String>,
    #[builder(default)]
    pub children: Vec<XmlElement>,
}

#[derive(Clone, Debug, PartialEq, TypedBuilder)]
pub struct ThisDevice {
    #[builder(default)]
    pub attributes: HashMap<ExpandedName, String>,
    pub friendly_name: Vec<LocalizedStringType>, // at least one
    #[builder(default, setter(strip_option, into))]
    pub firmware_version: Option<String>,
    #[builder(default, setter(strip_option, into))]
    pub serial_number: Option<String>,
    #[builder(default)]
    pub children: Vec<XmlElement>,
}

impl GenericXmlReaderComplexTypeRead for ThisModel {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            ManufacturerStart,
            ManufacturerEnd,
            ManufacturerUrlStart,
            ManufacturerUrlEnd,
            ModelNameStart,
            ModelNameEnd,
            ModelNumberStart,
            ModelNumberEnd,
            ModelUrlStart,
            ModelUrlEnd,
            PresentationUrlStart,
            PresentationUrlEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;

        let mut manufacturer: Vec<LocalizedStringType> = vec![];
        let mut manufacturer_url: Option<String> = None;
        let mut model_name: Vec<LocalizedStringType> = vec![];
        let mut model_number: Option<String> = None;
        let mut model_url: Option<String> = None;
        let mut presentation_url: Option<String> = None;
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];

        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, DPWS_MANUFACTURER_TAG_REF)
                        | (State::ManufacturerEnd, DPWS_MANUFACTURER_TAG_REF) => {
                            state = State::ManufacturerStart;
                            manufacturer.push(LocalizedStringType::from_xml_complex(
                                DPWS_MANUFACTURER_TAG,
                                e,
                                reader,
                            )?);
                            state = State::ManufacturerEnd;
                        }
                        (State::ManufacturerEnd, DPWS_MANUFACTURER_URL_TAG_REF) => {
                            state = State::ManufacturerUrlStart;
                        }
                        (State::ManufacturerEnd, DPWS_MODEL_NAME_TAG_REF)
                        | (State::ManufacturerUrlEnd, DPWS_MODEL_NAME_TAG_REF)
                        | (State::ModelNameEnd, DPWS_MODEL_NAME_TAG_REF) => {
                            state = State::ModelNameStart;
                            model_name.push(LocalizedStringType::from_xml_complex(
                                DPWS_MODEL_NAME_TAG,
                                e,
                                reader,
                            )?);
                            state = State::ModelNameEnd;
                        }
                        (State::ModelNameEnd, DPWS_MODEL_NUMBER_TAG_REF) => {
                            state = State::ModelNumberStart;
                        }
                        (State::ModelNameEnd, DPWS_MODEL_URL_TAG_REF)
                        | (State::ModelNumberEnd, DPWS_MODEL_URL_TAG_REF) => {
                            state = State::ModelUrlStart;
                        }
                        (State::ModelNameEnd, DPWS_PRESENTATION_URL_TAG_REF)
                        | (State::ModelNumberEnd, DPWS_PRESENTATION_URL_TAG_REF)
                        | (State::ModelUrlEnd, DPWS_PRESENTATION_URL_TAG_REF) => {
                            state = State::PresentationUrlStart;
                        }
                        (State::ModelNameEnd, _)
                        | (State::ModelNumberEnd, _)
                        | (State::ModelUrlEnd, _)
                        | (State::PresentationUrlEnd, _)
                        | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::ManufacturerUrlStart, DPWS_MANUFACTURER_URL_TAG_REF) => {
                        state = State::ManufacturerUrlEnd;
                    }
                    (State::ModelNumberStart, DPWS_MODEL_NUMBER_TAG_REF) => {
                        state = State::ModelNumberEnd;
                    }
                    (State::ModelUrlStart, DPWS_MODEL_URL_TAG_REF) => {
                        state = State::ModelUrlEnd;
                    }
                    (State::PresentationUrlStart, DPWS_PRESENTATION_URL_TAG_REF) => {
                        state = State::PresentationUrlEnd;
                    }
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            manufacturer,
                            manufacturer_url,
                            model_name,
                            model_number,
                            model_url,
                            presentation_url,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::ManufacturerUrlStart => {
                        manufacturer_url = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        )
                    }
                    State::ModelNumberStart => {
                        model_number = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        );
                    }
                    State::ModelUrlStart => {
                        model_url = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        )
                    }
                    State::PresentationUrlStart => {
                        presentation_url = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        )
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for ThisModel {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(DPWS_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| DPWS_THIS_MODEL, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        for manufacturer in &self.manufacturer {
            manufacturer.to_xml_complex(Some(DPWS_MANUFACTURER_TAG_STR), writer, false)?;
        }
        if let Some(manufacturer_url) = &self.manufacturer_url {
            writer.write_start(Some(DPWS_NAMESPACE), DPWS_MANUFACTURER_URL)?;
            writer.write_text(manufacturer_url.as_str())?;
            writer.write_end(Some(DPWS_NAMESPACE), DPWS_MANUFACTURER_URL)?;
        }
        for model_name in &self.model_name {
            model_name.to_xml_complex(Some(DPWS_MODEL_NAME_TAG_STR), writer, false)?;
        }
        if let Some(model_number) = &self.model_number {
            writer.write_start(Some(DPWS_NAMESPACE), DPWS_MODEL_NUMBER)?;
            writer.write_text(model_number)?;
            writer.write_end(Some(DPWS_NAMESPACE), DPWS_MODEL_NUMBER)?;
        }
        if let Some(model_url) = &self.model_url {
            writer.write_start(Some(DPWS_NAMESPACE), DPWS_MODEL_URL)?;
            writer.write_text(model_url.as_str())?;
            writer.write_end(Some(DPWS_NAMESPACE), DPWS_MODEL_URL)?;
        }
        if let Some(presentation_url) = &self.presentation_url {
            writer.write_start(Some(DPWS_NAMESPACE), DPWS_PRESENTATION_URL)?;
            writer.write_text(presentation_url.as_str())?;
            writer.write_end(Some(DPWS_NAMESPACE), DPWS_PRESENTATION_URL)?;
        }
        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl GenericXmlReaderComplexTypeRead for ThisDevice {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            FriendlyNameStart,
            FriendlyNameEnd,
            FirmwareVersionStart,
            FirmwareVersionEnd,
            SerialNumberStart,
            SerialNumberEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;

        let mut friendly_names: Vec<LocalizedStringType> = vec![];
        let mut firmware_version: Option<String> = None;
        let mut serial_number: Option<String> = None;
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];

        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, DPWS_FRIENDLY_NAME_TAG_REF)
                        | (State::FriendlyNameEnd, DPWS_FRIENDLY_NAME_TAG_REF) => {
                            state = State::FriendlyNameStart;
                            friendly_names.push(LocalizedStringType::from_xml_complex(
                                DPWS_FRIENDLY_NAME_TAG,
                                e,
                                reader,
                            )?);
                            state = State::FriendlyNameEnd;
                        }
                        (State::FriendlyNameEnd, DPWS_FIRMWARE_VERSION_TAG_REF) => {
                            state = State::FirmwareVersionStart;
                        }
                        (State::FriendlyNameEnd, DPWS_SERIAL_NUMBER_TAG_REF)
                        | (State::FirmwareVersionEnd, DPWS_SERIAL_NUMBER_TAG_REF) => {
                            state = State::SerialNumberStart;
                        }
                        (State::FriendlyNameEnd, _)
                        | (State::FirmwareVersionEnd, _)
                        | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::FirmwareVersionStart, DPWS_FIRMWARE_VERSION_TAG_REF) => {
                        state = State::FirmwareVersionEnd;
                    }
                    (State::SerialNumberStart, DPWS_SERIAL_NUMBER_TAG_REF) => {
                        state = State::SerialNumberEnd;
                    }
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            friendly_name: friendly_names,
                            firmware_version,
                            serial_number,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::FirmwareVersionStart => {
                        firmware_version = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        );
                    }
                    State::SerialNumberStart => {
                        serial_number = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        );
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for ThisDevice {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(DPWS_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| DPWS_THIS_DEVICE, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        for friendly_name in &self.friendly_name {
            friendly_name.to_xml_complex(Some(DPWS_FRIENDLY_NAME_TAG_STR), writer, false)?;
        }
        if let Some(firmware_version) = &self.firmware_version {
            writer.write_start(Some(DPWS_NAMESPACE), DPWS_FIRMWARE_VERSION)?;
            writer.write_text(firmware_version)?;
            writer.write_end(Some(DPWS_NAMESPACE), DPWS_FIRMWARE_VERSION)?;
        }
        if let Some(serial_number) = &self.serial_number {
            writer.write_start(Some(DPWS_NAMESPACE), DPWS_SERIAL_NUMBER)?;
            writer.write_text(serial_number)?;
            writer.write_end(Some(DPWS_NAMESPACE), DPWS_SERIAL_NUMBER)?;
        }
        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::xml::messages::common::{
        DPWS_THIS_DEVICE_TAG, DPWS_THIS_DEVICE_TAG_STR, DPWS_THIS_MODEL_TAG,
        DPWS_THIS_MODEL_TAG_STR,
    };
    use crate::xml::messages::dpws::this::{ThisDevice, ThisModel};
    use protosdc_xml::{find_start_element_reader, GenericXmlReaderComplexTypeRead, XmlReader};
    use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};

    //noinspection HttpUrlsUsage
    fn test_this_model(this_model: &ThisModel) {
        assert_eq!(3, this_model.manufacturer.len());
        assert_eq!(
            "http://www.example.com",
            this_model.manufacturer_url.as_ref().unwrap().as_str()
        );

        assert_eq!(1, this_model.model_name.len());
        let model_name = this_model.model_name.first().unwrap();
        assert!(model_name.lang.is_none());
        assert_eq!("PEU", model_name.value.as_str());
        assert!(model_name.attributes.is_empty());

        assert_eq!(
            "54-32-1",
            this_model.model_number.as_ref().unwrap().as_str()
        );
        assert_eq!(
            "http://www.example.com",
            this_model.presentation_url.as_ref().unwrap().as_str()
        );
    }

    //noinspection HttpUrlsUsage
    #[test]
    fn round_trip_model() -> anyhow::Result<()> {
        let this_model = {
            let input = "                <dpws:ThisModel xmlns:dpws=\"http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01\">
                    <dpws:Manufacturer xml:lang=\"en\">Provider Example Inc.</dpws:Manufacturer>
                    <dpws:Manufacturer xml:lang=\"de\">Beispiel Provider AG</dpws:Manufacturer>
                    <dpws:Manufacturer xml:lang=\"cn\">范例公司</dpws:Manufacturer>
                    <dpws:ManufacturerUrl>http://www.example.com</dpws:ManufacturerUrl>
                    <dpws:ModelName>PEU</dpws:ModelName>
                    <dpws:ModelNumber>54-32-1</dpws:ModelNumber>
                    <dpws:PresentationUrl>http://www.example.com</dpws:PresentationUrl>
                </dpws:ThisModel>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];
            let this_model: ThisModel =
                find_start_element_reader!(ThisModel, DPWS_THIS_MODEL_TAG, reader, buf)?;
            test_this_model(&this_model);
            this_model
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        this_model.to_xml_complex(Some(DPWS_THIS_MODEL_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let this_model_again: ThisModel =
            find_start_element_reader!(ThisModel, DPWS_THIS_MODEL_TAG, reader, buf)?;
        test_this_model(&this_model_again);

        Ok(())
    }

    fn test_this_device(this_device: &ThisDevice) {
        assert_eq!(1, this_device.friendly_name.len());
        assert_eq!(
            "v1.2.3",
            this_device.firmware_version.as_ref().unwrap().as_str()
        );
        assert_eq!(
            "1234-5678-9101-1121",
            this_device.serial_number.as_ref().unwrap().as_str()
        );
    }

    #[test]
    fn roundtrip_device() -> anyhow::Result<()> {
        let this_device = {
            let input = "                <dpws:ThisDevice xmlns:dpws=\"http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01\">
                    <dpws:FriendlyName xml:lang=\"en\">Provider Example Unit</dpws:FriendlyName>
                    <dpws:FirmwareVersion>v1.2.3</dpws:FirmwareVersion>
                    <dpws:SerialNumber>1234-5678-9101-1121</dpws:SerialNumber>
                </dpws:ThisDevice>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];
            let this_device: ThisDevice =
                find_start_element_reader!(ThisDevice, DPWS_THIS_DEVICE_TAG, reader, buf)?;
            test_this_device(&this_device);
            this_device
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        this_device.to_xml_complex(Some(DPWS_THIS_DEVICE_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let this_device_again: ThisDevice =
            find_start_element_reader!(ThisDevice, DPWS_THIS_DEVICE_TAG, reader, buf)?;
        test_this_device(&this_device_again);

        Ok(())
    }
}
