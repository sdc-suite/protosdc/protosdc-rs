use crate::xml::messages::addressing::{WsaEndpointReference, XmlElement};
use crate::xml::messages::common::{
    write_attributes, DO_NOT_CARE_QNAME, DPWS_HOST, DPWS_HOSTED, DPWS_NAMESPACE, DPWS_SERVICE_ID,
    DPWS_SERVICE_ID_TAG_REF, DPWS_TYPES, DPWS_TYPES_TAG, DPWS_TYPES_TAG_REF, ENDPOINT_REFERENCE,
    ENDPOINT_REFERENCE_TAG, ENDPOINT_REFERENCE_TAG_REF,
};
use crate::xml::messages::dpws::common::Types;
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{ComplexXmlTypeWrite, QNameStr, WriterError, XmlWriter};
use protosdc_xml::{ExpandedName, GenericXmlReaderComplexTypeRead, ParserError};
use quick_xml::events::{BytesStart, Event};
use std::collections::HashMap;
use std::io::BufRead;

#[derive(Clone, Debug, PartialEq)]
pub struct Host {
    pub attributes: HashMap<ExpandedName, String>,
    pub endpoint_reference: WsaEndpointReference,
    pub types: Option<Types>,
    pub children: Vec<XmlElement>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Hosted {
    pub attributes: HashMap<ExpandedName, String>,
    pub endpoint_reference: Vec<WsaEndpointReference>, // at least one
    pub types: Types,
    pub service_id: String,
    pub children: Vec<XmlElement>,
}

impl GenericXmlReaderComplexTypeRead for Host {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            EndpointReferenceStart,
            EndpointReferenceEnd,
            TypesStart,
            TypesEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;

        let mut endpoint_reference: Option<WsaEndpointReference> = None;
        let mut types: Option<Types> = None;
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];

        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, ENDPOINT_REFERENCE_TAG_REF) => {
                            state = State::EndpointReferenceStart;
                            endpoint_reference = Some(WsaEndpointReference::from_xml_complex(
                                ENDPOINT_REFERENCE_TAG,
                                e,
                                reader,
                            )?);
                            state = State::EndpointReferenceEnd;
                        }
                        (State::EndpointReferenceEnd, DPWS_TYPES_TAG_REF) => {
                            state = State::TypesStart;
                            types = Some(Types::from_xml_complex(DPWS_TYPES_TAG, e, reader)?);
                            state = State::TypesEnd;
                        }
                        (State::EndpointReferenceEnd, _)
                        | (State::TypesEnd, _)
                        | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            endpoint_reference: endpoint_reference.ok_or(
                                ParserError::MandatoryElementMissing {
                                    element_name: ENDPOINT_REFERENCE.to_string(),
                                },
                            )?,
                            types,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => Err(ParserError::UnexpectedParserTextEventState {
                    parser_state: format!("{:?}", state),
                })?,
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for Host {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(DPWS_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| DPWS_HOST, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        self.endpoint_reference
            .to_xml_complex(None, writer, false)?;
        if let Some(types) = &self.types {
            types.to_xml_complex(None, writer, false)?;
        }
        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl GenericXmlReaderComplexTypeRead for Hosted {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            EndpointReferenceStart,
            EndpointReferenceEnd,
            TypesStart,
            TypesEnd,
            ServiceIdStart,
            ServiceIdEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;

        let mut endpoint_references: Vec<WsaEndpointReference> = vec![];
        let mut types: Option<Types> = None;
        let mut service_id: Option<String> = None;
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];

        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, ENDPOINT_REFERENCE_TAG_REF)
                        | (State::EndpointReferenceEnd, ENDPOINT_REFERENCE_TAG_REF) => {
                            state = State::EndpointReferenceStart;
                            endpoint_references.push(WsaEndpointReference::from_xml_complex(
                                ENDPOINT_REFERENCE_TAG,
                                e,
                                reader,
                            )?);
                            state = State::EndpointReferenceEnd;
                        }
                        (State::EndpointReferenceEnd, DPWS_TYPES_TAG_REF) => {
                            state = State::TypesStart;
                            types = Some(Types::from_xml_complex(DPWS_TYPES_TAG, e, reader)?);
                            state = State::TypesEnd;
                        }
                        (State::TypesEnd, DPWS_SERVICE_ID_TAG_REF) => {
                            state = State::ServiceIdStart;
                        }
                        (State::ServiceIdEnd, _) | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::ServiceIdStart, DPWS_SERVICE_ID_TAG_REF) => {
                        state = State::ServiceIdEnd;
                    }
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            endpoint_reference: endpoint_references,
                            types: types.ok_or(ParserError::MandatoryElementMissing {
                                element_name: DPWS_TYPES.to_string(),
                            })?,
                            service_id: service_id.ok_or(ParserError::MandatoryElementMissing {
                                element_name: DPWS_SERVICE_ID.to_string(),
                            })?,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::ServiceIdStart => {
                        service_id = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        );
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for Hosted {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(DPWS_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| DPWS_HOSTED, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        for endpoint_reference in &self.endpoint_reference {
            endpoint_reference.to_xml_complex(None, writer, false)?;
        }
        self.types.to_xml_complex(None, writer, false)?;

        writer.write_start(Some(DPWS_NAMESPACE), DPWS_SERVICE_ID)?;
        writer.write_text(self.service_id.as_str())?;
        writer.write_end(Some(DPWS_NAMESPACE), DPWS_SERVICE_ID)?;

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::xml::messages::biceps_ext::SDC_NAMESPACE;
    use crate::xml::messages::common::{
        DPWS_HOSTED_TAG, DPWS_HOSTED_TAG_STR, DPWS_HOST_TAG, DPWS_HOST_TAG_STR,
    };
    use crate::xml::messages::dpws::host::{Host, Hosted};
    use protosdc_xml::{find_start_element_reader, GenericXmlReaderComplexTypeRead, XmlReader};
    use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};

    fn test_host(host: &Host) {
        assert_eq!(
            "urn:uuid:7a09f8a4-42a0-4aaa-852c-932e7e8401fc",
            host.endpoint_reference.address.value.as_str()
        );
        assert_eq!(2, host.types.as_ref().unwrap().list.list.len())
    }

    #[test]
    fn round_trip_host() -> anyhow::Result<()> {
        let host = {
            let input = "                    <dpws:Host xmlns:dpws=\"http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">
                        <wsa:EndpointReference>
                            <wsa:Address>urn:uuid:7a09f8a4-42a0-4aaa-852c-932e7e8401fc</wsa:Address>
                        </wsa:EndpointReference>
                        <dpws:Types xmlns=\"http://standards.ieee.org/downloads/11073/11073-20702-2016\">dpws:Device MedicalDevice
                </dpws:Types>
                </dpws:Host>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];
            let host: Host = find_start_element_reader!(Host, DPWS_HOST_TAG, reader, buf)?;
            test_host(&host);
            host
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        host.to_xml_complex(Some(DPWS_HOST_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let host_again: Host = find_start_element_reader!(Host, DPWS_HOST_TAG, reader, buf)?;
        test_host(&host_again);

        Ok(())
    }

    fn test_hosted(hosted: &Hosted) {
        assert_eq!(
            "https://192.168.0.105:58173/7a09f8a4-42a0-4aaa-852c-932e7e8401fc/HighPriorityServices",
            hosted
                .endpoint_reference
                .first()
                .unwrap()
                .address
                .value
                .as_str()
        );
        assert_eq!("my:HighPriorityServices", hosted.service_id.as_str());

        assert_eq!(7, hosted.types.list.list.len());
        assert!(hosted
            .types
            .list
            .list
            .iter()
            .any(|it| it.local_name == "GetService"
                && it.namespace == Some(SDC_NAMESPACE.to_string())))
    }

    fn round_trip_hosted_common<F: Fn(&Hosted)>(input: &str, test_fn: F) -> anyhow::Result<()> {
        let hosted = {
            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];
            let hosted: Hosted = find_start_element_reader!(Hosted, DPWS_HOSTED_TAG, reader, buf)?;
            test_fn(&hosted);
            hosted
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        hosted.to_xml_complex(Some(DPWS_HOSTED_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let hosted_again: Hosted =
            find_start_element_reader!(Hosted, DPWS_HOSTED_TAG, reader, buf)?;
        test_fn(&hosted_again);

        Ok(())
    }

    #[test]
    fn round_trip_hosted() -> anyhow::Result<()> {
        let input = "                    <dpws:Hosted xmlns:dpws=\"http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:sdc=\"http://standards.ieee.org/downloads/11073/11073-20701-2018\">
                        <wsa:EndpointReference>
                            <wsa:Address>https://192.168.0.105:58173/7a09f8a4-42a0-4aaa-852c-932e7e8401fc/HighPriorityServices</wsa:Address>
                        </wsa:EndpointReference>
                        <dpws:Types>sdc:GetService sdc:SetService sdc:ContainmentTreeService sdc:ContextService sdc:DescriptionEventService sdc:StateEventService sdc:WaveformService</dpws:Types>
                        <dpws:ServiceId>my:HighPriorityServices</dpws:ServiceId>
                    </dpws:Hosted>";
        round_trip_hosted_common(input, test_hosted)
    }

    #[test]
    fn round_trip_hosted_empty_types() -> anyhow::Result<()> {
        let input = "                    <dpws:Hosted xmlns:dpws=\"http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:sdc=\"http://standards.ieee.org/downloads/11073/11073-20701-2018\">
                        <wsa:EndpointReference>
                            <wsa:Address>https://192.168.0.105:58173/7a09f8a4-42a0-4aaa-852c-932e7e8401fc/HighPriorityServices</wsa:Address>
                        </wsa:EndpointReference>
                        <dpws:Types></dpws:Types>
                        <dpws:ServiceId>my:HighPriorityServices</dpws:ServiceId>
                    </dpws:Hosted>";
        round_trip_hosted_common(input, |it| assert_eq!(0, it.types.list.list.len()))
    }
}
