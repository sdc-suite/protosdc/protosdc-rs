use crate::xml::messages::common::{
    write_attributes, DPWS_NAMESPACE, DPWS_TYPES, XML_LANG_PREFIXED, XML_LANG_TAG_STR,
    XML_NAMESPACE,
};
use crate::xml::messages::discovery::ExpandedNameList;
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{ComplexXmlTypeWrite, QNameStr, WriterError, XmlWriter};
use protosdc_xml::{
    ExpandedName, GenericXmlReaderComplexTypeRead, GenericXmlReaderSimpleTypeRead, ParserError,
};
use quick_xml::events::{BytesStart, Event};
use std::collections::HashMap;
use std::io::BufRead;
use typed_builder::TypedBuilder;

#[derive(Clone, Debug, PartialEq, TypedBuilder)]
pub struct LocalizedStringType {
    #[builder(default)]
    pub attributes: HashMap<ExpandedName, String>,
    #[builder(default, setter(strip_option, into))]
    pub lang: Option<String>,
    #[builder(setter(into))]
    pub value: String,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Types {
    pub list: ExpandedNameList,
}

impl GenericXmlReaderComplexTypeRead for LocalizedStringType {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        // collect attributes
        let mut attributes: HashMap<ExpandedName, String> =
            collect_attributes_reader(event, reader)?;
        // extract known attributes
        let lang = attributes.remove(&ExpandedName {
            namespace: Some(XML_NAMESPACE.to_string()),
            local_name: XML_LANG_TAG_STR.1.to_string(),
        });

        // read value
        state = State::Start;

        let mut value: Option<String> = None;

        let mut buf = vec![];

        loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    Err(ParserError::UnexpectedParserStartState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            lang,
                            value: value.unwrap_or("".to_string()),
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => {
                    value = Some(
                        e.unescape()
                            .map_err(ParserError::QuickXMLError)?
                            .to_string(),
                    );
                }
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for LocalizedStringType {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(DPWS_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| "LocalizedStringType", |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        if let Some(lang) = &self.lang {
            // yay edge case...
            // TODO: add xml namespace handling to writer
            writer.add_attribute((XML_LANG_PREFIXED, lang.as_str()))?;
        }
        write_attributes(writer, &self.attributes)?;

        writer.write_text(&self.value)?;

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl GenericXmlReaderComplexTypeRead for Types {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        _event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        // read value
        state = State::Start;

        let mut value: Option<ExpandedNameList> = None;

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    Err(ParserError::UnexpectedParserStartState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            list: value.unwrap_or_default(),
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => {
                    value = Some(ExpandedNameList::from_xml_simple(
                        e.unescape().map_err(ParserError::QuickXMLError)?.as_bytes(),
                        reader,
                    )?)
                }
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for Types {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(DPWS_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| DPWS_TYPES, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;

        let list = self.list.to_xml_simple(writer)?;
        writer.write_text(list.as_str())?;

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::xml::messages::common::{DPWS_MANUFACTURER_TAG, DPWS_MANUFACTURER_TAG_STR};
    use crate::xml::messages::dpws::common::LocalizedStringType;
    use protosdc_xml::{find_start_element_reader, GenericXmlReaderComplexTypeRead, XmlReader};
    use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};

    fn test_localized_string(localized_string: &LocalizedStringType) {
        assert_eq!(0, localized_string.attributes.len());
        assert_eq!(Some("en".to_string()), localized_string.lang);
        assert_eq!("Provider Example Inc.".to_string(), localized_string.value);
    }

    #[test]
    fn round_trip() -> anyhow::Result<()> {
        let localized_string = {
            let input = "<dpws:Manufacturer xmlns:dpws=\"http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01\" xml:lang=\"en\">Provider Example Inc.</dpws:Manufacturer>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];
            let localized_string: LocalizedStringType = find_start_element_reader!(
                LocalizedStringType,
                DPWS_MANUFACTURER_TAG,
                reader,
                buf
            )?;
            test_localized_string(&localized_string);
            localized_string
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        localized_string.to_xml_complex(Some(DPWS_MANUFACTURER_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let localized_string_again: LocalizedStringType =
            find_start_element_reader!(LocalizedStringType, DPWS_MANUFACTURER_TAG, reader, buf)?;
        test_localized_string(&localized_string_again);

        Ok(())
    }
}
