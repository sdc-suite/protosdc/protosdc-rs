use const_format::concatcp;
use lazy_static::lazy_static;
use log::trace;
use protosdc_mapping::MappingError;
use protosdc_xml::{GenericXmlReaderSimpleTypeRead, ParserError};
use protosdc_xml::{SimpleXmlTypeWrite, WriterError, XmlWriter};
use regex::{Match, Regex};
use std::cmp::min;
use std::io::BufRead;
use std::time::Duration;

const SIGN_GROUP: &str = "sign";
const SIGN_FRAGMENT: &str = concatcp!(r"(?P<", SIGN_GROUP, r">-)?");
const YEAR_GROUP: &str = "year";
const YEAR_FRAGMENT: &str = concatcp!(r"(?P<", YEAR_GROUP, r">[0-9]+)");

const MONTH_GROUP: &str = "month";
const MONTH_FRAGMENT: &str = concatcp!(r"(?P<", MONTH_GROUP, r">[0-9]+)");

const DAY_GROUP: &str = "day";
const DAY_FRAGMENT: &str = concatcp!(r"(?P<", DAY_GROUP, r">[0-9]+)");

const HOUR_GROUP: &str = "hour";
const HOUR_FRAGMENT: &str = concatcp!(r"(?P<", HOUR_GROUP, r">[0-9]+)");

const MINUTE_GROUP: &str = "minute";
const MINUTE_FRAGMENT: &str = concatcp!(r"(?P<", MINUTE_GROUP, r">[0-9]+)");

const FRACTION_GROUP: &str = "millisecond";
const FRACTION_GROUP_FRAGMENT: &str = concatcp!(r"(?P<", FRACTION_GROUP, r">[0-9]+)");
const SECOND_GROUP: &str = "second";
const SECOND_FRAGMENT: &str = concatcp!(
    r"(?P<",
    SECOND_GROUP,
    r">[0-9]+)((\.",
    FRACTION_GROUP_FRAGMENT,
    r")?)"
);

const DURATION_REGEX: &str = concatcp!(
    SIGN_FRAGMENT,
    r"P(",
    YEAR_FRAGMENT,
    r"Y)?(",
    MONTH_FRAGMENT,
    r"M)?(",
    DAY_FRAGMENT,
    r"D)?(T(",
    HOUR_FRAGMENT,
    r"H)?(",
    MINUTE_FRAGMENT,
    r"M)?(",
    SECOND_FRAGMENT,
    r"S)?)?"
);
// const DURATION_REGEX: &str = r"-?P[0-9]+Y?([0-9]+M)?([0-9]+D)?(T([0-9]+H)?([0-9]+M)?([0-9]+(\.[0-9]+)?S)?)?";

lazy_static! {
    pub(crate) static ref DURATION_PARSER: Regex = Regex::new(DURATION_REGEX).unwrap();
    pub(crate) static ref DURATION_VALIDATOR1: Regex = Regex::new(r".*[YMDHS].*").unwrap();
    pub(crate) static ref DURATION_VALIDATOR2: Regex = Regex::new(r".*[^T]").unwrap();
}

#[derive(Clone, Debug, PartialEq)]
pub struct XsdDuration {
    negative: bool,
    years: Option<u32>,
    months: Option<u32>,
    days: Option<u32>,
    hours: Option<u32>,
    minutes: Option<u32>,
    seconds: Option<u64>,
    microseconds: Option<u32>,
}

pub(crate) fn parse_u32_opt(
    opt: &Option<Match>,
    calling_type_name: &str,
    element_name: &str,
) -> Result<Option<u32>, MappingError> {
    match opt {
        None => Ok(None),
        Some(value) => {
            let as_str = value.as_str();
            let parsed = as_str
                .parse::<u32>()
                .map_err(|_| MappingError::FromProtoGeneric {
                    message: format!(
                        "{} in {} could not be parsed to u32",
                        element_name, calling_type_name
                    ),
                })?;
            Ok(Some(parsed))
        }
    }
}

pub(crate) fn parse_u64_opt(
    opt: &Option<Match>,
    calling_type_name: &str,
    element_name: &str,
) -> Result<Option<u64>, MappingError> {
    match opt {
        None => Ok(None),
        Some(value) => {
            let as_str = value.as_str();
            let parsed = as_str
                .parse::<u64>()
                .map_err(|_| MappingError::FromProtoGeneric {
                    message: format!(
                        "{} in {} could not be parsed to u64",
                        element_name, calling_type_name
                    ),
                })?;
            Ok(Some(parsed))
        }
    }
}

impl GenericXmlReaderSimpleTypeRead for XsdDuration {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    fn from_xml_simple<B: BufRead>(
        data: &[u8],
        _reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        let parsed = std::str::from_utf8(data).map_err(|_| ParserError::ElementError {
            element_name: "XsdDuration".to_string(),
        })?;

        parsed.try_into().map_err(ParserError::MappingError)
    }
}

impl SimpleXmlTypeWrite for XsdDuration {
    fn to_xml_simple(&self, _writer: &mut XmlWriter<Vec<u8>>) -> Result<Vec<u8>, WriterError> {
        let data: String = self.clone().into();
        Ok(data.into_bytes())
    }
}

impl TryFrom<&str> for XsdDuration {
    type Error = MappingError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        parse_duration(value)
    }
}

impl From<Duration> for XsdDuration {
    fn from(value: Duration) -> Self {
        Self {
            negative: false,
            years: None,
            months: None,
            days: None,
            hours: None,
            minutes: None,
            seconds: Some(value.as_secs()),
            microseconds: Some(value.subsec_micros()),
        }
    }
}

impl From<XsdDuration> for Duration {
    // TODO: This is a poor approximation
    fn from(val: XsdDuration) -> Duration {
        let years = val.years.unwrap_or(0) as u64;
        let months = val.months.unwrap_or(0) as u64;
        let days = val.days.unwrap_or(0) as u64;
        let hours = val.hours.unwrap_or(0) as u64;
        let minutes = val.minutes.unwrap_or(0) as u64;
        let seconds = val.seconds.unwrap_or(0);
        let microseconds = val.microseconds.unwrap_or(0) as u64;

        Duration::from_micros(
            // years, months and days as days, approx.
            ((years * 365 + months * 30 + days) * 24 * 60 * 60
                + hours * 60 * 60
                + minutes * 60
                + seconds)
                * 1_000_000
                + microseconds,
        )
    }
}

impl From<XsdDuration> for String {
    fn from(value: XsdDuration) -> Self {
        let mut data: String = "".to_string();

        if value.negative {
            data.push('-');
        }
        data.push('P');
        if let Some(y) = value.years {
            data.push_str(y.to_string().as_str());
            data.push('Y');
        }
        if let Some(m) = value.months {
            data.push_str(m.to_string().as_str());
            data.push('M');
        }
        if let Some(d) = value.days {
            data.push_str(d.to_string().as_str());
            data.push('D');
        }
        data.push('T');
        if let Some(h) = value.hours {
            data.push_str(h.to_string().as_str());
            data.push('H');
        }
        if let Some(m) = value.minutes {
            data.push_str(m.to_string().as_str());
            data.push('M');
        }
        if let Some(s) = value.seconds {
            data.push_str(s.to_string().as_str());
            if let Some(frac) = value.microseconds {
                // pad up to 6 zeroes
                data.push_str(format!(".{:0>6}", frac).as_str());
            }
            data.push('S');
        }
        data
    }
}

pub fn parse_duration(value: &str) -> Result<XsdDuration, MappingError> {
    trace!("Parsing duration {value}");
    let validator1_result = DURATION_VALIDATOR1.is_match(value);
    let validator2_result = DURATION_VALIDATOR2.is_match(value);

    if !validator1_result || !validator2_result {
        Err(MappingError::FromProtoGeneric {
            message: "XsdDuration validator failed".to_string(),
        })?
    }

    let regex_parsed = DURATION_PARSER.captures(value);
    match regex_parsed {
        None => Err(MappingError::FromProtoGeneric {
            message: "date did not pass validation".to_string(),
        }),
        Some(capture) => {
            let sign = capture.name(SIGN_GROUP);
            let year: Option<u32> = parse_u32_opt(&capture.name(YEAR_GROUP), "duration", "year")?;
            let month: Option<u32> =
                parse_u32_opt(&capture.name(MONTH_GROUP), "duration", "month")?;
            let day: Option<u32> = parse_u32_opt(&capture.name(DAY_GROUP), "duration", "day")?;
            let hour: Option<u32> = parse_u32_opt(&capture.name(HOUR_GROUP), "duration", "hour")?;
            let minute: Option<u32> =
                parse_u32_opt(&capture.name(MINUTE_GROUP), "duration", "minute")?;
            let second: Option<u64> =
                parse_u64_opt(&capture.name(SECOND_GROUP), "duration", "second")?;

            let milliseconds_raw = capture.name(FRACTION_GROUP);
            let microseconds = milliseconds_raw
                .map(|ms| {
                    // truncate if it is too long
                    let last_digit = min(5, ms.as_str().len() - 1);
                    // the pow factor determines how to convert the value, which might
                    // be 5, i.e. 500ms, or 500206, i.e. 500.206 ms, into microseconds, meaning how
                    // much does it pad the value on the right
                    let pow_factor: u32 = 6 - (last_digit as u32) - 1;
                    ms.as_str()[0..last_digit + 1]
                        .parse::<u32>()
                        // pad if last digit was not 6
                        .map(|it| it * 10_u32.pow(pow_factor))
                        .map_err(|_| MappingError::FromProtoGeneric {
                            message: "milliseconds could not be parsed to u32".to_string(),
                        })
                })
                .map_or(Ok(None), |r| r.map(Some))?;

            Ok(XsdDuration {
                negative: sign.is_some(),
                years: year,
                months: month,
                days: day,
                hours: hour,
                minutes: minute,
                seconds: second,
                microseconds,
            })
        }
    }
}

#[cfg(test)]
mod test {
    use crate::xml::messages::duration::XsdDuration;
    use common::test_util::init_logging;
    use protosdc_mapping::MappingError;

    #[test]
    fn test_round_trip() -> anyhow::Result<()> {
        init_logging();
        let input: Vec<(&str, Option<&str>)> = vec![
            ("PT0.001S", Some("PT0.001000S")),
            ("P7Y5M12DT10H2M15.0606S", Some("P7Y5M12DT10H2M15.060600S")),
            ("PT1H0S", None),
            ("PT1H05500S", Some("PT1H5500S")),
            ("PT1.0S", Some("PT1.000000S")),
            ("P6000YT", None),
            ("PT4M59.9999568S", Some("PT4M59.999956S")),
        ];

        for (data, expected) in input {
            let parsed: XsdDuration = data.try_into()?;
            let written: String = parsed.into();
            match expected {
                None => assert_eq!(data, written.as_str()),
                Some(exp) => assert_eq!(exp, written.as_str()),
            }
        }
        Ok(())
    }

    #[test]
    fn test_invalid() -> anyhow::Result<()> {
        init_logging();
        let input = vec!["PT", "P", "T", "", "á²³¤€^"];

        for data in input {
            let parsed: Result<XsdDuration, MappingError> = data.try_into();
            assert!(parsed.is_err())
        }

        Ok(())
    }
}
