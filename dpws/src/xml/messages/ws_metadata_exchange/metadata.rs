use crate::xml::messages::addressing::{WsaEndpointReference, XmlElement};
use crate::xml::messages::common::{
    soap_element_impl, write_attributes, DO_NOT_CARE_QNAME, DPWS_RELATIONSHIP_TAG,
    DPWS_RELATIONSHIP_TAG_REF, DPWS_THIS_DEVICE_TAG, DPWS_THIS_DEVICE_TAG_REF, DPWS_THIS_MODEL_TAG,
    DPWS_THIS_MODEL_TAG_REF, MEX_DIALECT, MEX_IDENTIFIER, MEX_LOCATION, MEX_LOCATION_TAG,
    MEX_LOCATION_TAG_REF, MEX_LOCATION_TAG_STR, MEX_METADATA, MEX_METADATA_REFERENCE,
    MEX_METADATA_REFERENCE_TAG, MEX_METADATA_REFERENCE_TAG_REF, MEX_METADATA_REFERENCE_TAG_STR,
    MEX_METADATA_SECTION, MEX_METADATA_SECTION_TAG, MEX_METADATA_SECTION_TAG_REF,
    MEX_METADATA_SECTION_TAG_STR, WS_MEX_NAMESPACE,
};
use crate::xml::messages::dpws::relationship::Relationship;
use crate::xml::messages::dpws::this::{ThisDevice, ThisModel};
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::ParserError::UnexpectedParserTextEventState;
use protosdc_xml::{ComplexXmlTypeWrite, QNameStr, WriterError, XmlWriter};
use protosdc_xml::{ExpandedName, GenericXmlReaderComplexTypeRead, ParserError};
use quick_xml::events::{BytesStart, Event};
use std::collections::HashMap;
use std::io::BufRead;

#[derive(Clone, Debug, PartialEq)]
pub struct Metadata {
    pub(crate) attributes: HashMap<ExpandedName, String>,
    pub(crate) metadata_sections: Vec<MetadataSection>,
    pub(crate) children: Vec<XmlElement>,
}
soap_element_impl!(Metadata, MEX_METADATA);

#[derive(Clone, Debug, PartialEq)]
pub enum MetadataPayload {
    MetadataReference(MetadataReference),
    Location(Location),
    // likely cases for DPWS
    Relationship(Relationship),
    ThisDevice(ThisDevice),
    ThisModel(ThisModel),
    InlineWsdl(bytes::Bytes),
    Other(XmlElement),
}

#[derive(Clone, Debug, PartialEq)]
pub struct MetadataSection {
    pub(crate) attributes: HashMap<ExpandedName, String>,
    pub(crate) payload: MetadataPayload,
    pub(crate) dialect: String,
    pub(crate) identifier: Option<String>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct MetadataReference {
    endpoint_reference: WsaEndpointReference,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Location {
    pub location: Option<String>,
}

impl GenericXmlReaderComplexTypeRead for Location {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        _event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        // read value
        state = State::Start;

        let mut location: Option<String> = None;

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    Err(ParserError::UnexpectedParserStartState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self { location });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => {
                    location = Some(
                        e.unescape()
                            .map_err(ParserError::QuickXMLError)?
                            .to_string(),
                    );
                }
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for Location {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_MEX_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| MEX_LOCATION, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        if let Some(loc) = &self.location {
            writer.write_text(loc.as_str())?;
        }
        writer.write_end(element_namespace, element_tag)?;

        Ok(())
    }
}

impl GenericXmlReaderComplexTypeRead for MetadataReference {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        let epr = WsaEndpointReference::from_xml_complex(tag_name, event, reader)?;
        Ok(Self {
            endpoint_reference: epr,
        })
    }
}

impl ComplexXmlTypeWrite for MetadataReference {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_MEX_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| MEX_METADATA_REFERENCE, |(_, it)| it);

        self.endpoint_reference.to_xml_complex(
            Some((element_namespace, element_tag)),
            writer,
            xsi_type,
        )
    }
}

impl GenericXmlReaderComplexTypeRead for MetadataSection {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            PayloadStart,
            PayloadEnd,
        }
        let mut state = State::Root;

        // read value
        state = State::Start;

        let mut attributes = collect_attributes_reader(event, reader)?;
        // extract known attributes
        let dialect = attributes
            .remove(&ExpandedName {
                namespace: None,
                local_name: MEX_DIALECT.to_string(),
            })
            .ok_or(ParserError::MandatoryAttributeMissing {
                attribute_name: MEX_DIALECT.to_string(),
            })?;

        let identifier = attributes.remove(&ExpandedName {
            namespace: None,
            local_name: MEX_IDENTIFIER.to_string(),
        });

        let mut metadata_payload: Option<MetadataPayload> = None;

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, MEX_METADATA_REFERENCE_TAG_REF) => {
                            state = State::PayloadStart;
                            metadata_payload = Some(MetadataPayload::MetadataReference(
                                MetadataReference::from_xml_complex(
                                    MEX_METADATA_REFERENCE_TAG,
                                    e,
                                    reader,
                                )?,
                            ));
                            state = State::PayloadEnd;
                        }
                        (State::Start, MEX_LOCATION_TAG_REF) => {
                            state = State::PayloadStart;
                            metadata_payload = Some(MetadataPayload::Location(
                                Location::from_xml_complex(MEX_LOCATION_TAG, e, reader)?,
                            ));
                            state = State::PayloadEnd;
                        }
                        (State::Start, DPWS_THIS_DEVICE_TAG_REF) => {
                            state = State::PayloadStart;
                            metadata_payload = Some(MetadataPayload::ThisDevice(
                                ThisDevice::from_xml_complex(DPWS_THIS_DEVICE_TAG, e, reader)?,
                            ));
                            state = State::PayloadEnd;
                        }
                        (State::Start, DPWS_THIS_MODEL_TAG_REF) => {
                            state = State::PayloadStart;
                            metadata_payload = Some(MetadataPayload::ThisModel(
                                ThisModel::from_xml_complex(DPWS_THIS_MODEL_TAG, e, reader)?,
                            ));
                            state = State::PayloadEnd;
                        }
                        (State::Start, DPWS_RELATIONSHIP_TAG_REF) => {
                            state = State::PayloadStart;
                            metadata_payload = Some(MetadataPayload::Relationship(
                                Relationship::from_xml_complex(DPWS_RELATIONSHIP_TAG, e, reader)?,
                            ));
                            state = State::PayloadEnd;
                        }
                        (State::Start, _) => {
                            state = State::PayloadStart;
                            metadata_payload = Some(MetadataPayload::Other(
                                XmlElement::from_xml_complex(DO_NOT_CARE_QNAME, e, reader)?,
                            ));
                            state = State::PayloadEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            dialect,
                            identifier,
                            payload: metadata_payload.ok_or(
                                ParserError::MandatoryElementMissing {
                                    element_name: "MetadataSection payload".to_string(),
                                },
                            )?,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => Err(UnexpectedParserTextEventState {
                    parser_state: format!("{:?}", state),
                })?,
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for MetadataSection {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_MEX_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| MEX_METADATA_SECTION, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        writer.add_attribute((MEX_DIALECT, self.dialect.as_str()))?;
        if let Some(identifier) = &self.identifier {
            writer.add_attribute((MEX_IDENTIFIER, identifier.as_str()))?;
        }
        write_attributes(writer, &self.attributes)?;

        match &self.payload {
            MetadataPayload::MetadataReference(x) => {
                x.to_xml_complex(Some(MEX_METADATA_REFERENCE_TAG_STR), writer, false)?;
            }
            MetadataPayload::Location(x) => {
                x.to_xml_complex(Some(MEX_LOCATION_TAG_STR), writer, false)?;
            }
            MetadataPayload::Relationship(x) => x.to_xml_complex(None, writer, false)?,
            MetadataPayload::Other(x) => x.to_xml_complex(None, writer, false)?,
            MetadataPayload::ThisDevice(x) => x.to_xml_complex(None, writer, false)?,
            MetadataPayload::ThisModel(x) => x.to_xml_complex(None, writer, false)?,
            MetadataPayload::InlineWsdl(data) => {
                writer.write_bytes(data.as_ref())?;
            }
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl GenericXmlReaderComplexTypeRead for Metadata {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            MetadataSectionStart,
            MetadataSectionEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        // read value
        state = State::Start;

        let attributes = collect_attributes_reader(event, reader)?;
        // extract known attributes

        let mut metadata_sections: Vec<MetadataSection> = vec![];
        let mut children: Vec<XmlElement> = Vec::with_capacity(0); // unlikely, optimize!

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, MEX_METADATA_SECTION_TAG_REF)
                        | (State::MetadataSectionEnd, MEX_METADATA_SECTION_TAG_REF) => {
                            state = State::MetadataSectionStart;
                            metadata_sections.push(MetadataSection::from_xml_complex(
                                MEX_METADATA_SECTION_TAG,
                                e,
                                reader,
                            )?);
                            state = State::MetadataSectionEnd;
                        }
                        (State::Start, _) | (State::MetadataSectionEnd, _) | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            metadata_sections,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => Err(UnexpectedParserTextEventState {
                    parser_state: format!("{:?}", state),
                })?,
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for Metadata {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_MEX_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| MEX_METADATA, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        for section in &self.metadata_sections {
            section.to_xml_complex(Some(MEX_METADATA_SECTION_TAG_STR), writer, false)?;
        }

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}
