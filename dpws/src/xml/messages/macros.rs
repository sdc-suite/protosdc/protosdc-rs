/// Utility macro to fast forward an XML reader to the next start event and attempt parsing it as
/// the provided type
#[macro_export]
macro_rules! find_start_element {
    ($struct_name:ident, $qname:ident, $reader:ident, $buf:ident) => {{
        loop {
            match $reader.read_next(&mut $buf) {
                Ok((ref _ns, quick_xml::events::Event::Start(ref e))) => {
                    break $struct_name::from_xml_complex($qname, e, &mut $reader);
                }
                Ok((ref _ns, quick_xml::events::Event::DocType(ref _e))) => {}
                Ok((ref _ns, quick_xml::events::Event::Decl(ref _e))) => {}
                Ok((ref _ns, quick_xml::events::Event::Comment(ref _e))) => {}
                other => {
                    break Err(protosdc_xml::ParserError::UnexpectedParserEvent {
                        event: format!("{:?}", other),
                    })
                }
            };
        }
    }};
}

// export macro crate-wide
// pub use find_start_element;
