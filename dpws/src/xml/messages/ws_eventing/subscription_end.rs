use crate::xml::messages::addressing::{WsaEndpointReference, XmlElement};
use crate::xml::messages::common::{
    soap_element_impl, write_attributes, DO_NOT_CARE_QNAME, WSE_REASON_TAG, WSE_REASON_TAG_REF,
    WSE_REASON_TAG_STR, WSE_STATUS, WSE_STATUS_TAG_REF, WSE_SUBSCRIPTION_END,
    WSE_SUBSCRIPTION_MANAGER, WSE_SUBSCRIPTION_MANAGER_TAG, WSE_SUBSCRIPTION_MANAGER_TAG_REF,
    WSE_SUBSCRIPTION_MANAGER_TAG_STR, WS_EVENTING_NAMESPACE,
};
use crate::xml::messages::dpws::common::LocalizedStringType;
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{ComplexXmlTypeWrite, QNameStr, WriterError, XmlWriter};
use protosdc_xml::{ExpandedName, GenericXmlReaderComplexTypeRead, ParserError};
use quick_xml::events::{BytesStart, Event};
use std::collections::HashMap;
use std::io::BufRead;

#[derive(Clone, Debug, PartialEq)]
pub struct SubscriptionEnd {
    pub attributes: HashMap<ExpandedName, String>,
    pub subscription_manager: WsaEndpointReference,
    pub status: String,
    pub reason: Option<LocalizedStringType>, // technically not the correct type, but whatever...
    pub children: Vec<XmlElement>,
}
soap_element_impl!(SubscriptionEnd, WSE_SUBSCRIPTION_END);

impl GenericXmlReaderComplexTypeRead for SubscriptionEnd {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            SubscriptionManagerStart,
            SubscriptionManagerEnd,
            StatusStart,
            StatusEnd,
            ReasonStart,
            ReasonEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        state = State::Start;

        let mut subscription_manager: Option<WsaEndpointReference> = None;
        let mut status: Option<String> = None;
        let mut reason: Option<LocalizedStringType> = None;
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, WSE_SUBSCRIPTION_MANAGER_TAG_REF) => {
                            state = State::SubscriptionManagerStart;
                            subscription_manager = Some(WsaEndpointReference::from_xml_complex(
                                WSE_SUBSCRIPTION_MANAGER_TAG,
                                e,
                                reader,
                            )?);
                            state = State::SubscriptionManagerEnd;
                        }
                        (State::SubscriptionManagerEnd, WSE_STATUS_TAG_REF) => {
                            state = State::StatusStart;
                        }
                        (State::StatusEnd, WSE_REASON_TAG_REF) => {
                            state = State::ReasonStart;
                            reason = Some(LocalizedStringType::from_xml_complex(
                                WSE_REASON_TAG,
                                e,
                                reader,
                            )?);
                            state = State::ReasonEnd;
                        }
                        (State::StatusEnd, _) | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::StatusStart, WSE_STATUS_TAG_REF) => {
                        state = State::StatusEnd;
                    }
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;

                        return Ok(Self {
                            attributes,
                            subscription_manager: subscription_manager.ok_or(
                                ParserError::MandatoryElementMissing {
                                    element_name: WSE_SUBSCRIPTION_MANAGER.to_string(),
                                },
                            )?,
                            status: status.ok_or(ParserError::MandatoryElementMissing {
                                element_name: WSE_STATUS.to_string(),
                            })?,
                            reason,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::StatusStart => {
                        status = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        );
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for SubscriptionEnd {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_EVENTING_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| WSE_SUBSCRIPTION_END, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        self.subscription_manager.to_xml_complex(
            Some(WSE_SUBSCRIPTION_MANAGER_TAG_STR),
            writer,
            false,
        )?;

        writer.write_start(Some(WS_EVENTING_NAMESPACE), WSE_STATUS)?;
        writer.write_text(&self.status)?;
        writer.write_end(Some(WS_EVENTING_NAMESPACE), WSE_STATUS)?;

        if let Some(reason) = &self.reason {
            reason.to_xml_complex(Some(WSE_REASON_TAG_STR), writer, false)?;
        }

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::xml::messages::common::{WSE_SUBSCRIPTION_END_TAG, WSE_SUBSCRIPTION_END_TAG_STR};
    use crate::xml::messages::ws_eventing::subscription_end::SubscriptionEnd;
    use protosdc_xml::{find_start_element_reader, GenericXmlReaderComplexTypeRead, XmlReader};
    use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};

    fn test_subscription_end(subscription_end: &SubscriptionEnd) {
        assert_eq!(
            "somewhere",
            subscription_end.subscription_manager.address.value.as_str()
        );
        assert_eq!("bad_news", subscription_end.status.as_str());
    }

    #[test]
    fn round_trip_subscribe_response() -> anyhow::Result<()> {
        let subscription_end = {
            let input = "        <wse:SubscriptionEnd xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:wse=\"http://schemas.xmlsoap.org/ws/2004/08/eventing\">
                <wse:SubscriptionManager>
                    <wsa:Address>somewhere</wsa:Address>
                </wse:SubscriptionManager>
                <wse:Status>bad_news</wse:Status>
            </wse:SubscriptionEnd>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];
            let subscription_end: SubscriptionEnd =
                find_start_element_reader!(SubscriptionEnd, WSE_SUBSCRIPTION_END_TAG, reader, buf)?;
            test_subscription_end(&subscription_end);
            subscription_end
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        subscription_end.to_xml_complex(Some(WSE_SUBSCRIPTION_END_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let subscription_end_again: SubscriptionEnd =
            find_start_element_reader!(SubscriptionEnd, WSE_SUBSCRIPTION_END_TAG, reader, buf)?;
        test_subscription_end(&subscription_end_again);

        Ok(())
    }
}
