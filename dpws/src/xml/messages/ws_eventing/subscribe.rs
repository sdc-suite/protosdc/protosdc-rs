use crate::xml::messages::addressing::{WsaEndpointReference, XmlElement};
use crate::xml::messages::common::{
    soap_element_impl, write_attributes, DO_NOT_CARE_QNAME, WSE_DELIVERY, WSE_DELIVERY_TAG,
    WSE_DELIVERY_TAG_REF, WSE_DELIVERY_TAG_STR, WSE_END_TO_TAG, WSE_END_TO_TAG_REF,
    WSE_END_TO_TAG_STR, WSE_EXPIRES, WSE_EXPIRES_TAG_REF, WSE_FILTER_TAG, WSE_FILTER_TAG_REF,
    WSE_FILTER_TAG_STR, WSE_MODE, WSE_NOTIFY_TO_TAG, WSE_NOTIFY_TO_TAG_REF, WSE_NOTIFY_TO_TAG_STR,
    WSE_SUBSCRIBE, WS_EVENTING_NAMESPACE,
};
use crate::xml::messages::duration::XsdDuration;
use crate::xml::messages::ws_eventing::common::Filter;
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{ComplexXmlTypeWrite, QNameStr, SimpleXmlTypeWrite, WriterError, XmlWriter};
use protosdc_xml::{
    ExpandedName, GenericXmlReaderComplexTypeRead, GenericXmlReaderSimpleTypeRead, ParserError,
};
use quick_xml::events::{BytesStart, Event};
use std::collections::HashMap;
use std::io::BufRead;

#[derive(Clone, Debug, PartialEq)]
pub struct Subscribe {
    pub attributes: HashMap<ExpandedName, String>,

    pub end_to: Option<WsaEndpointReference>,
    pub delivery: Delivery,
    pub expires: Option<XsdDuration>,
    pub filter: Option<Filter>,
    pub children: Vec<XmlElement>,
}
soap_element_impl!(Subscribe, WSE_SUBSCRIBE);

#[derive(Clone, Debug, PartialEq)]
pub struct Delivery {
    pub attributes: HashMap<ExpandedName, String>,
    pub mode: Option<String>,
    pub notify_to: Vec<NotifyTo>,
    pub children: Vec<XmlElement>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct NotifyTo {
    pub endpoint_reference: WsaEndpointReference,
}

impl GenericXmlReaderComplexTypeRead for Subscribe {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            EndToStart,
            EndToEnd,
            DeliveryStart,
            DeliveryEnd,
            ExpiresStart,
            ExpiresEnd,
            FilterStart,
            FilterEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;

        let mut end_to: Option<WsaEndpointReference> = None;
        let mut delivery: Option<Delivery> = None;
        let mut expires: Option<XsdDuration> = None;
        let mut filter: Option<Filter> = None;
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];

        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, WSE_END_TO_TAG_REF) => {
                            state = State::EndToStart;
                            end_to = Some(WsaEndpointReference::from_xml_complex(
                                WSE_END_TO_TAG,
                                e,
                                reader,
                            )?);
                            state = State::EndToEnd;
                        }
                        (State::Start, WSE_DELIVERY_TAG_REF)
                        | (State::EndToEnd, WSE_DELIVERY_TAG_REF) => {
                            state = State::DeliveryStart;
                            delivery =
                                Some(Delivery::from_xml_complex(WSE_DELIVERY_TAG, e, reader)?);
                            state = State::DeliveryEnd;
                        }
                        (State::DeliveryEnd, WSE_EXPIRES_TAG_REF) => {
                            state = State::ExpiresStart;
                        }
                        (State::DeliveryEnd, WSE_FILTER_TAG_REF)
                        | (State::ExpiresEnd, WSE_FILTER_TAG_REF) => {
                            state = State::FilterStart;
                            filter = Some(Filter::from_xml_complex(WSE_FILTER_TAG, e, reader)?);
                            state = State::FilterEnd;
                        }
                        (State::DeliveryEnd, _)
                        | (State::ExpiresEnd, _)
                        | (State::FilterEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::ExpiresStart, WSE_EXPIRES_TAG_REF) => {
                        state = State::ExpiresEnd;
                    }
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            end_to,
                            delivery: delivery.ok_or(ParserError::MandatoryElementMissing {
                                element_name: WSE_DELIVERY.to_string(),
                            })?,
                            expires,
                            filter,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::ExpiresStart => {
                        expires = Some(XsdDuration::from_xml_simple(
                            e.unescape().map_err(ParserError::QuickXMLError)?.as_bytes(),
                            reader,
                        )?);
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for Subscribe {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_EVENTING_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| WSE_SUBSCRIBE, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;
        if let Some(end_to) = &self.end_to {
            end_to.to_xml_complex(Some(WSE_END_TO_TAG_STR), writer, false)?;
        }
        self.delivery
            .to_xml_complex(Some(WSE_DELIVERY_TAG_STR), writer, false)?;
        if let Some(expires) = &self.expires {
            let data = expires.to_xml_simple(writer)?;
            writer.write_start(Some(WS_EVENTING_NAMESPACE), WSE_EXPIRES)?;
            writer.write_bytes(&data)?;
            writer.write_end(Some(WS_EVENTING_NAMESPACE), WSE_EXPIRES)?;
        }

        if let Some(filter) = &self.filter {
            filter.to_xml_complex(Some(WSE_FILTER_TAG_STR), writer, false)?;
        }

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;

        Ok(())
    }
}

impl GenericXmlReaderComplexTypeRead for Delivery {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        // collect attributes
        let mut attributes: HashMap<ExpandedName, String> =
            collect_attributes_reader(event, reader)?;
        // extract known attributes
        let mode = attributes.remove(&ExpandedName {
            namespace: None,
            local_name: WSE_MODE.to_string(),
        });

        // read value
        state = State::Start;

        let mut notify_to: Vec<NotifyTo> = vec![];
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];

        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, WSE_NOTIFY_TO_TAG_REF)
                        | (State::AnyEnd, WSE_NOTIFY_TO_TAG_REF) => {
                            state = State::AnyStart;
                            notify_to.push(NotifyTo::from_xml_complex(
                                WSE_NOTIFY_TO_TAG,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (State::Start, _) | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            mode,
                            notify_to,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => Err(ParserError::UnexpectedParserTextEventState {
                    parser_state: format!("{:?}", state),
                })?,
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for Delivery {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_EVENTING_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| WSE_DELIVERY, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;
        if let Some(mode) = &self.mode {
            writer.add_attribute((WSE_MODE, mode.as_str()))?;
        }

        for notify_to in &self.notify_to {
            notify_to.to_xml_complex(Some(WSE_NOTIFY_TO_TAG_STR), writer, false)?;
        }

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;

        Ok(())
    }
}

impl GenericXmlReaderComplexTypeRead for NotifyTo {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    fn from_xml_complex<B: BufRead>(
        _tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        let epr = WsaEndpointReference::from_xml_complex(WSE_NOTIFY_TO_TAG, event, reader)?;
        Ok(Self {
            endpoint_reference: epr,
        })
    }
}

impl ComplexXmlTypeWrite for NotifyTo {
    fn to_xml_complex(
        &self,
        _tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        self.endpoint_reference
            .to_xml_complex(Some(WSE_NOTIFY_TO_TAG_STR), writer, false)
    }
}

#[cfg(test)]
mod test {
    use crate::xml::messages::common::{WSE_SUBSCRIBE_TAG, WSE_SUBSCRIBE_TAG_STR};
    use crate::xml::messages::ws_eventing::common::FilterContent;
    use crate::xml::messages::ws_eventing::subscribe::Subscribe;
    use protosdc_xml::{find_start_element_reader, GenericXmlReaderComplexTypeRead, XmlReader};
    use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};

    //noinspection HttpUrlsUsage
    fn test_subscribe(subscribe: &Subscribe) {
        assert_eq!(
            "https://192.168.0.105:58187/EventSink/EndTo/8040bad3-78c3-4969-a4eb-c1c16377d351",
            subscribe.end_to.as_ref().unwrap().address.value.as_str()
        );
        assert_eq!(
            "http://schemas.xmlsoap.org/ws/2004/08/eventing/DeliveryModes/Push",
            subscribe.delivery.mode.as_ref().unwrap().as_str()
        );
        assert_eq!(
            "https://192.168.0.105:58188/EventSink/NotifyTo/8040bad3-78c3-4969-a4eb-c1c16377d351",
            subscribe
                .delivery
                .notify_to
                .first()
                .unwrap()
                .endpoint_reference
                .address
                .value
                .as_str()
        );

        let expires: String = subscribe.expires.as_ref().unwrap().clone().into();
        assert_eq!("PT1M", expires.as_str());

        assert_eq!(
            "http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01/Action",
            subscribe
                .filter
                .as_ref()
                .unwrap()
                .dialect
                .as_ref()
                .unwrap()
                .as_str()
        );

        assert!(subscribe.filter.is_some());
        let filter = subscribe.filter.as_ref().unwrap().filters.as_ref().unwrap();

        assert!(matches!(filter, FilterContent::Text(..)));

        let filter_text = match filter {
            FilterContent::Element(_) => panic!("No element expected"),
            FilterContent::Text(text) => text,
        };

        let actions: Vec<String> = filter_text.split(' ').map(|it| it.to_string()).collect();
        assert_eq!(9, actions.len());
        assert!(actions.contains(&"http://standards.ieee.org/downloads/11073/11073-20701-2018/StateEventService/EpisodicAlertReport".to_string()))
    }

    fn test_subscribe_element(subscribe: &Subscribe) {
        assert_eq!(
            "urn:absoluteInsanity",
            subscribe
                .filter
                .as_ref()
                .unwrap()
                .dialect
                .as_ref()
                .unwrap()
                .as_str()
        );

        let filter = subscribe.filter.as_ref().unwrap().filters.as_ref().unwrap();

        assert!(matches!(filter, FilterContent::Element(..)));

        let filter_element = match filter {
            FilterContent::Element(elem) => elem,
            FilterContent::Text(_) => panic!("No text expected"),
        };

        assert_eq!(
            "urn:absoluteInsanity",
            filter_element.tag.namespace.as_ref().unwrap().as_str()
        );
        assert_eq!("dumbElement", filter_element.tag.local_name.as_str());
    }

    //noinspection HttpUrlsUsage
    #[test]
    fn round_trip_subscribe_text() -> anyhow::Result<()> {
        let subscribe = {
            let input = "        <wse:Subscribe xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:wse=\"http://schemas.xmlsoap.org/ws/2004/08/eventing\">
                <wse:EndTo>
                    <wsa:Address>https://192.168.0.105:58187/EventSink/EndTo/8040bad3-78c3-4969-a4eb-c1c16377d351</wsa:Address>
                </wse:EndTo>
                <wse:Delivery Mode=\"http://schemas.xmlsoap.org/ws/2004/08/eventing/DeliveryModes/Push\">
                    <wse:NotifyTo>
                        <wsa:Address>https://192.168.0.105:58188/EventSink/NotifyTo/8040bad3-78c3-4969-a4eb-c1c16377d351</wsa:Address>
                    </wse:NotifyTo>
                </wse:Delivery>
                <wse:Expires>PT1M</wse:Expires>
                <wse:Filter Dialect=\"http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01/Action\">http://standards.ieee.org/downloads/11073/11073-20701-2018/StateEventService/EpisodicAlertReport http://standards.ieee.org/downloads/11073/11073-20701-2018/StateEventService/EpisodicComponentReport http://standards.ieee.org/downloads/11073/11073-20701-2018/ContextService/EpisodicContextReport http://standards.ieee.org/downloads/11073/11073-20701-2018/StateEventService/EpisodicMetricReport http://standards.ieee.org/downloads/11073/11073-20701-2018/StateEventService/EpisodicOperationalStateReport http://standards.ieee.org/downloads/11073/11073-20701-2018/DescriptionEventService/DescriptionModificationReport http://standards.ieee.org/downloads/11073/11073-20701-2018/SetService/OperationInvokedReport http://standards.ieee.org/downloads/11073/11073-20701-2018/StateEventService/SystemErrorReport http://standards.ieee.org/downloads/11073/11073-20701-2018/WaveformService/WaveformStream</wse:Filter>
            </wse:Subscribe>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];
            let subscribe: Subscribe =
                find_start_element_reader!(Subscribe, WSE_SUBSCRIBE_TAG, reader, buf)?;
            test_subscribe(&subscribe);
            subscribe
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        subscribe.to_xml_complex(Some(WSE_SUBSCRIBE_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let subscribe_again: Subscribe =
            find_start_element_reader!(Subscribe, WSE_SUBSCRIBE_TAG, reader, buf)?;
        test_subscribe(&subscribe_again);

        Ok(())
    }

    //noinspection HttpUrlsUsage
    #[test]
    fn round_trip_subscribe_element() -> anyhow::Result<()> {
        let subscribe = {
            let input = "        <wse:Subscribe xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:wse=\"http://schemas.xmlsoap.org/ws/2004/08/eventing\">
                <wse:EndTo>
                    <wsa:Address>https://192.168.0.105:58187/EventSink/EndTo/8040bad3-78c3-4969-a4eb-c1c16377d351</wsa:Address>
                </wse:EndTo>
                <wse:Delivery Mode=\"http://schemas.xmlsoap.org/ws/2004/08/eventing/DeliveryModes/Push\">
                    <wse:NotifyTo>
                        <wsa:Address>https://192.168.0.105:58188/EventSink/NotifyTo/8040bad3-78c3-4969-a4eb-c1c16377d351</wsa:Address>
                    </wse:NotifyTo>
                </wse:Delivery>
                <wse:Expires>PT1M</wse:Expires>
                <wse:Filter Dialect=\"urn:absoluteInsanity\"><a:dumbElement xmlns:a=\"urn:absoluteInsanity\"/></wse:Filter>
            </wse:Subscribe>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];
            let subscribe: Subscribe =
                find_start_element_reader!(Subscribe, WSE_SUBSCRIBE_TAG, reader, buf)?;
            test_subscribe_element(&subscribe);
            subscribe
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        subscribe.to_xml_complex(Some(WSE_SUBSCRIBE_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let subscribe_again: Subscribe =
            find_start_element_reader!(Subscribe, WSE_SUBSCRIBE_TAG, reader, buf)?;
        test_subscribe_element(&subscribe_again);

        Ok(())
    }
}
