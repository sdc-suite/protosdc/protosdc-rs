use crate::xml::messages::addressing::{WsaAttributedURIType, WsaEndpointReference, XmlElement};
use crate::xml::messages::common::{
    write_attributes, ACTION_TAG, ACTION_TAG_REF, ACTION_TAG_STR, APP_SEQUENCE_TAG,
    APP_SEQUENCE_TAG_REF, APP_SEQUENCE_TAG_STR, DO_NOT_CARE_QNAME, FROM_TAG, FROM_TAG_REF,
    FROM_TAG_STR, MESSAGE_ID_TAG, MESSAGE_ID_TAG_REF, MESSAGE_ID_TAG_STR, RELATES_TO_TAG,
    RELATES_TO_TAG_REF, RELATES_TO_TAG_STR, SOAP_1_2_NAMESPACE, SOAP_BODY, SOAP_BODY_TAG_REF,
    SOAP_ENVELOPE, SOAP_HEADER, SOAP_HEADER_TAG, SOAP_HEADER_TAG_REF, TO_TAG, TO_TAG_REF,
    TO_TAG_STR,
};
use crate::xml::messages::discovery::AppSequence;
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{ComplexXmlTypeWrite, QNameStr, WriterError, XmlWriter};
use protosdc_xml::{ExpandedName, GenericXmlReaderComplexTypeRead, ParserError};
use quick_xml::events::{BytesStart, Event};
use std::collections::HashMap;
use std::io::BufRead;

pub struct SoapMessage {
    pub header: SoapHeader,
}

#[derive(Clone, Debug, PartialEq)]
pub struct SoapHeader {
    pub attributes: HashMap<ExpandedName, String>,
    pub to: Option<WsaAttributedURIType>,
    pub message_id: Option<WsaAttributedURIType>,
    pub from: Option<WsaEndpointReference>,
    pub action: Option<WsaAttributedURIType>,
    pub relates_to: Option<WsaAttributedURIType>,
    pub app_sequence: Option<AppSequence>,
    pub children: Vec<XmlElement>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct SoapEmptyBody {}

impl ComplexXmlTypeWrite for SoapEmptyBody {
    fn to_xml_complex(
        &self,
        _tag_name: Option<QNameStr>,
        _writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        Ok(())
    }
}

impl SoapMessage {
    pub fn to_xml_complex<T: ComplexXmlTypeWrite>(
        &self,
        writer: &mut XmlWriter<Vec<u8>>,
        body_writer: T,
    ) -> Result<(), WriterError> {
        writer.write_start(Some(SOAP_1_2_NAMESPACE), SOAP_ENVELOPE)?;

        self.header.to_xml_complex(None, writer, false)?;

        writer.write_start(Some(SOAP_1_2_NAMESPACE), SOAP_BODY)?;
        body_writer.to_xml_complex(None, writer, false)?;
        writer.write_end(Some(SOAP_1_2_NAMESPACE), SOAP_BODY)?;

        writer.write_end(Some(SOAP_1_2_NAMESPACE), SOAP_ENVELOPE)?;
        Ok(())
    }

    pub fn to_xml_complex_bytes(
        &self,
        writer: &mut XmlWriter<Vec<u8>>,
        body: bytes::Bytes,
    ) -> Result<(), WriterError> {
        writer.write_start(Some(SOAP_1_2_NAMESPACE), SOAP_ENVELOPE)?;

        self.header.to_xml_complex(None, writer, false)?;

        writer.write_start(Some(SOAP_1_2_NAMESPACE), SOAP_BODY)?;
        writer.write_bytes(&body)?;
        writer.write_end(Some(SOAP_1_2_NAMESPACE), SOAP_BODY)?;

        writer.write_end(Some(SOAP_1_2_NAMESPACE), SOAP_ENVELOPE)?;
        Ok(())
    }
}

// parses header, fast forwards to body content
impl GenericXmlReaderComplexTypeRead for SoapMessage {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        _event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        let mut header: Option<SoapHeader> = None;

        state = State::Start;

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (_, SOAP_HEADER_TAG_REF) => {
                            header =
                                Some(SoapHeader::from_xml_complex(SOAP_HEADER_TAG, e, reader)?);
                        }
                        (_, SOAP_BODY_TAG_REF) => {
                            return Ok(Self {
                                header: header.ok_or(ParserError::MandatoryElementMissing {
                                    element_name: SOAP_HEADER.to_string(),
                                })?,
                            });
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }
                Ok((ref ns, Event::End(ref e))) => match (state, ns, e.local_name().into_inner()) {
                    (_, ns, local_name) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            header: header.ok_or(ParserError::MandatoryElementMissing {
                                element_name: SOAP_HEADER.to_string(),
                            })?,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => {
                    Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?;
                }
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for SoapHeader {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;

        let mut action: Option<WsaAttributedURIType> = None;
        let mut message_id: Option<WsaAttributedURIType> = None;
        let mut to: Option<WsaAttributedURIType> = None;
        let mut from: Option<WsaEndpointReference> = None;
        let mut relates_to: Option<WsaAttributedURIType> = None;
        let mut app_sequence: Option<AppSequence> = None;
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (_, ACTION_TAG_REF) => {
                            action = Some(WsaAttributedURIType::from_xml_complex(
                                ACTION_TAG, e, reader,
                            )?);
                        }
                        (_, MESSAGE_ID_TAG_REF) => {
                            message_id = Some(WsaAttributedURIType::from_xml_complex(
                                MESSAGE_ID_TAG,
                                e,
                                reader,
                            )?);
                        }
                        (_, TO_TAG_REF) => {
                            to = Some(WsaAttributedURIType::from_xml_complex(TO_TAG, e, reader)?);
                        }
                        (_, FROM_TAG_REF) => {
                            from =
                                Some(WsaEndpointReference::from_xml_complex(FROM_TAG, e, reader)?);
                        }
                        (_, RELATES_TO_TAG_REF) => {
                            relates_to = Some(WsaAttributedURIType::from_xml_complex(
                                RELATES_TO_TAG,
                                e,
                                reader,
                            )?);
                        }
                        (_, APP_SEQUENCE_TAG_REF) => {
                            app_sequence =
                                Some(AppSequence::from_xml_complex(APP_SEQUENCE_TAG, e, reader)?);
                        }
                        (_, _) => {
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                        }
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, ns, e.local_name().into_inner()) {
                    (_, ns, local_name) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            action,
                            message_id,
                            to,
                            children,
                            from,
                            relates_to,
                            app_sequence,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => {
                    Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?;
                }
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for SoapHeader {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(SOAP_1_2_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| SOAP_HEADER, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        if let Some(to) = &self.to {
            to.to_xml_complex(Some(TO_TAG_STR), writer, false)?;
        }
        if let Some(message_id) = &self.message_id {
            message_id.to_xml_complex(Some(MESSAGE_ID_TAG_STR), writer, false)?;
        }
        if let Some(from) = &self.from {
            from.to_xml_complex(Some(FROM_TAG_STR), writer, false)?;
        }
        if let Some(action) = &self.action {
            action.to_xml_complex(Some(ACTION_TAG_STR), writer, false)?;
        }
        if let Some(relates_to) = &self.relates_to {
            relates_to.to_xml_complex(Some(RELATES_TO_TAG_STR), writer, false)?;
        }
        if let Some(app_sequence) = &self.app_sequence {
            app_sequence.to_xml_complex(Some(APP_SEQUENCE_TAG_STR), writer, false)?;
        }

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::discovery::actions::HELLO;
    use crate::xml::messages::addressing::WsaAttributedURIType;
    use crate::xml::messages::common::SOAP_HEADER_TAG;
    use crate::xml::messages::soap::envelope::SoapHeader;
    use protosdc_xml::{find_start_element_reader, GenericXmlReaderComplexTypeRead, XmlReader};
    use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};
    use std::collections::HashMap;

    fn test_soap_header(soap_header: &SoapHeader) {
        assert_eq!(
            Some(WsaAttributedURIType {
                attributes: HashMap::with_capacity(0),
                value: HELLO.to_string()
            }),
            soap_header.action
        );

        assert_eq!(
            Some(WsaAttributedURIType {
                attributes: HashMap::with_capacity(0),
                value: "urn:uuid:e5c65f74-b4eb-4bf1-8c26-27493a8cac7f".to_string(),
            }),
            soap_header.message_id
        );

        assert_eq!(
            Some(WsaAttributedURIType {
                attributes: HashMap::with_capacity(0),
                value: "urn:docs-oasis-open-org:ws-dd:ns:discovery:2009:01".to_string()
            }),
            soap_header.to
        );

        assert_eq!(
            Some(WsaAttributedURIType {
                attributes: HashMap::with_capacity(0),
                value: "urn:uuid:d3331754-4869-4c32-974b-e2619df26043".to_string()
            }),
            soap_header.relates_to
        );

        assert_eq!(
            1671646710,
            soap_header.app_sequence.as_ref().unwrap().instance_id
        )
    }

    #[test]
    fn test_parse_ri_soap_header() -> anyhow::Result<()> {
        let soap_header = {
            let input = "<s12:Header xmlns:s12=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wsd=\"http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">
        <wsa:Action>http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/Hello</wsa:Action>
        <wsa:MessageID>urn:uuid:e5c65f74-b4eb-4bf1-8c26-27493a8cac7f</wsa:MessageID>
        <wsa:To>urn:docs-oasis-open-org:ws-dd:ns:discovery:2009:01</wsa:To>
        <wsa:RelatesTo>urn:uuid:d3331754-4869-4c32-974b-e2619df26043</wsa:RelatesTo>
        <wsd:AppSequence InstanceId=\"1671646710\" MessageNumber=\"1\"/>
</s12:Header>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let soap_header: SoapHeader =
                find_start_element_reader!(SoapHeader, SOAP_HEADER_TAG, reader, buf)?;
            test_soap_header(&soap_header);
            soap_header
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        soap_header.to_xml_complex(None, &mut writer, false)?;
        let writer_buf = writer.inner();

        println!("{}", std::str::from_utf8(writer_buf).unwrap());

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let soap_header: SoapHeader =
            find_start_element_reader!(SoapHeader, SOAP_HEADER_TAG, reader, buf)?;
        test_soap_header(&soap_header);

        Ok(())
    }
}
