use crate::xml::messages::addressing::XmlElement;
use crate::xml::messages::common::{
    soap_element_impl, write_attributes, DO_NOT_CARE_QNAME, SOAP_1_2_NAMESPACE, SOAP_CODE,
    SOAP_CODE_DATA_ENCODING_UNKNOWN, SOAP_CODE_DATA_ENCODING_UNKNOWN_TAG_REF,
    SOAP_CODE_MUST_UNDERSTAND, SOAP_CODE_MUST_UNDERSTAND_TAG_REF, SOAP_CODE_RECEIVER,
    SOAP_CODE_RECEIVER_TAG_REF, SOAP_CODE_SENDER, SOAP_CODE_SENDER_TAG_REF, SOAP_CODE_TAG,
    SOAP_CODE_TAG_REF, SOAP_CODE_TAG_STR, SOAP_CODE_VERSION_MISMATCH,
    SOAP_CODE_VERSION_MISMATCH_TAG_REF, SOAP_DETAIL_TAG, SOAP_DETAIL_TAG_REF, SOAP_DETAIL_TAG_STR,
    SOAP_FAULT, SOAP_NODE, SOAP_NODE_TAG_REF, SOAP_REASON, SOAP_REASON_TAG, SOAP_REASON_TAG_REF,
    SOAP_REASON_TAG_STR, SOAP_ROLE, SOAP_ROLE_TAG_REF, SOAP_SUBCODE_TAG, SOAP_SUBCODE_TAG_REF,
    SOAP_SUBCODE_TAG_STR, SOAP_TEXT, SOAP_TEXT_TAG, SOAP_TEXT_TAG_REF, SOAP_TEXT_TAG_STR,
    SOAP_VALUE, SOAP_VALUE_TAG_REF, XML_LANG_PREFIXED, XML_LANG_TAG_STR, XML_NAMESPACE,
};
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{ComplexXmlTypeWrite, QNameStr, SimpleXmlTypeWrite, WriterError, XmlWriter};
use protosdc_xml::{
    ExpandedName, GenericXmlReaderComplexTypeRead, GenericXmlReaderSimpleTypeRead, ParserError,
};
use quick_xml::events::{BytesStart, Event};
use quick_xml::name::QName;
use std::collections::HashMap;
use std::io::BufRead;
use typed_builder::TypedBuilder;

#[derive(Clone, Debug, PartialEq)]
pub struct Detail {
    pub attributes: HashMap<ExpandedName, String>,
    pub children: Vec<XmlElement>,
}

#[derive(Clone, Debug, PartialEq)]
pub enum FaultCodeEnum {
    DataEncodingUnknown,
    MustUnderstand,
    Receiver,
    Sender,
    VersionMismatch,
}

#[derive(Clone, Debug, PartialEq, TypedBuilder)]
pub struct FaultCode {
    pub value: FaultCodeEnum,
    #[builder(default)]
    pub subcode: Option<Subcode>,
}

#[derive(Clone, Debug, PartialEq, TypedBuilder)]
pub struct Subcode {
    value: ExpandedName,
    #[builder(default)]
    subcode: Option<Box<Subcode>>,
}

#[derive(Clone, Debug, PartialEq, TypedBuilder)]
pub struct FaultReason {
    text: Vec<ReasonText>,
}

impl FaultReason {
    pub fn simple_text(lang: impl Into<String>, text: impl Into<String>) -> Self {
        Self {
            text: vec![ReasonText {
                lang: lang.into(),
                value: text.into(),
            }],
        }
    }
}

#[derive(Clone, Debug, PartialEq, TypedBuilder)]
pub struct ReasonText {
    lang: String,
    value: String,
}

#[derive(Clone, Debug, PartialEq, TypedBuilder)]
pub struct Fault {
    code: FaultCode,
    reason: FaultReason,
    #[builder(default, setter(strip_option, into))]
    node: Option<String>,
    #[builder(default, setter(strip_option, into))]
    role: Option<String>,
    #[builder(default, setter(strip_option, into))]
    detail: Option<Detail>,
}
soap_element_impl!(Fault, SOAP_FAULT);

//
// parsers
//
impl GenericXmlReaderComplexTypeRead for Fault {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        _event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            CodeStart,
            CodeEnd,
            ReasonStart,
            ReasonEnd,
            NodeStart,
            NodeEnd,
            RoleStart,
            RoleEnd,
            DetailStart,
            DetailEnd,
        }
        let mut state = State::Root;

        // read value
        state = State::Start;

        let mut code: Option<FaultCode> = None;
        let mut reason: Option<FaultReason> = None;
        let mut node: Option<String> = None;
        let mut role: Option<String> = None;
        let mut detail: Option<Detail> = None;

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, SOAP_CODE_TAG_REF) => {
                            state = State::CodeStart;
                            code = Some(FaultCode::from_xml_complex(SOAP_CODE_TAG, e, reader)?);
                            state = State::CodeEnd
                        }
                        (State::CodeEnd, SOAP_REASON_TAG_REF) => {
                            state = State::ReasonStart;
                            reason =
                                Some(FaultReason::from_xml_complex(SOAP_REASON_TAG, e, reader)?);
                            state = State::ReasonEnd;
                        }
                        (State::ReasonEnd, SOAP_NODE_TAG_REF) => {
                            state = State::NodeStart;
                        }
                        (State::ReasonEnd, SOAP_ROLE_TAG_REF)
                        | (State::NodeEnd, SOAP_ROLE_TAG_REF) => {
                            state = State::RoleStart;
                        }
                        (State::ReasonEnd, SOAP_DETAIL_TAG_REF)
                        | (State::NodeEnd, SOAP_DETAIL_TAG_REF)
                        | (State::RoleEnd, SOAP_DETAIL_TAG_REF) => {
                            state = State::DetailStart;
                            detail = Some(Detail::from_xml_complex(SOAP_DETAIL_TAG, e, reader)?);
                            state = State::DetailEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(e.local_name().as_ref().to_vec())
                            {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::NodeStart, SOAP_NODE_TAG_REF) => {
                        state = State::NodeEnd;
                    }
                    (State::RoleStart, SOAP_ROLE_TAG_REF) => {
                        state = State::RoleEnd;
                    }
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            code: code.ok_or(ParserError::MandatoryElementMissing {
                                element_name: SOAP_CODE.to_string(),
                            })?,
                            reason: reason.ok_or(ParserError::MandatoryElementMissing {
                                element_name: SOAP_REASON.to_string(),
                            })?,
                            node,
                            role,
                            detail,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::NodeStart => {
                        node = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        );
                    }
                    State::RoleStart => {
                        role = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        );
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for FaultCode {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        _event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            ValueStart,
            ValueEnd,
            SubcodeStart,
            SubcodeEnd,
        }
        let mut state = State::Root;

        // read value
        state = State::Start;

        let mut value: Option<FaultCodeEnum> = None;
        let mut subcode: Option<Subcode> = None;

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, SOAP_VALUE_TAG_REF) => {
                            state = State::ValueStart;
                        }
                        (State::ValueEnd, SOAP_SUBCODE_TAG_REF) => {
                            state = State::SubcodeStart;
                            subcode = Some(Subcode::from_xml_complex(SOAP_SUBCODE_TAG, e, reader)?);
                            state = State::SubcodeEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::ValueStart, SOAP_VALUE_TAG_REF) => {
                        state = State::ValueEnd;
                    }
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            value: value.ok_or(ParserError::MandatoryElementMissing {
                                element_name: SOAP_VALUE.to_string(),
                            })?,
                            subcode,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::ValueStart => {
                        value = Some(FaultCodeEnum::from_xml_simple(
                            e.unescape().map_err(ParserError::QuickXMLError)?.as_bytes(),
                            reader,
                        )?);
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for FaultReason {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        _event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            TextStart,
            TextEnd,
        }
        let mut state = State::Root;

        // read value
        state = State::Start;

        let mut text: Vec<ReasonText> = vec![]; // at least one

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, SOAP_TEXT_TAG_REF) | (State::TextEnd, SOAP_TEXT_TAG_REF) => {
                            state = State::TextStart;
                            text.push(ReasonText::from_xml_complex(SOAP_TEXT_TAG, e, reader)?);
                            state = State::TextEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::TextEnd, (ns, local_name))
                        if &tag_name.0 == ns && tag_name.1 == local_name =>
                    {
                        state = State::End;
                        return Ok(Self { text });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => Err(ParserError::UnexpectedParserTextEventState {
                    parser_state: format!("{:?}", state),
                })?,
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for Detail {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        let attributes = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;

        let mut children: Vec<XmlElement> = vec![]; // at least one

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, _) | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => Err(ParserError::UnexpectedParserTextEventState {
                    parser_state: format!("{:?}", state),
                })?,
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for Subcode {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        _event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            ValueStart,
            ValueEnd,
            SubcodeStart,
            SubcodeEnd,
        }
        let mut state = State::Root;

        // read value
        state = State::Start;

        let mut value: Option<ExpandedName> = None;
        let mut subcode: Option<Box<Subcode>> = None;

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, SOAP_VALUE_TAG_REF) => {
                            state = State::ValueStart;
                        }
                        (State::ValueEnd, SOAP_SUBCODE_TAG_REF) => {
                            state = State::SubcodeStart;
                            subcode = Some(Box::new(Subcode::from_xml_complex(
                                SOAP_SUBCODE_TAG,
                                e,
                                reader,
                            )?));
                            state = State::SubcodeEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::ValueStart, SOAP_VALUE_TAG_REF) => {
                        state = State::ValueEnd;
                    }
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            value: value.ok_or(ParserError::MandatoryElementMissing {
                                element_name: SOAP_VALUE.to_string(),
                            })?,
                            subcode,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::ValueStart => {
                        let x = e.unescape().map_err(ParserError::QuickXMLError)?;
                        let result = reader.resolve_attribute(QName(x.as_bytes()));
                        value = Some(result.try_into()?);
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for ReasonText {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        let mut attributes = collect_attributes_reader(event, reader)?;

        // extract known attributes
        let lang = attributes
            .remove(&ExpandedName {
                namespace: Some(XML_NAMESPACE.to_string()),
                local_name: XML_LANG_TAG_STR.1.to_string(),
            })
            .ok_or(ParserError::MandatoryAttributeMissing {
                attribute_name: XML_LANG_PREFIXED.to_string(),
            })?;
        // fail if any other attribute is present
        for (key, value) in attributes {
            Err(ParserError::UnexpectedAttribute {
                element_name: SOAP_REASON.to_string(),
                attribute_name: format!("{:?}", key),
                attribute_value: value,
            })?
        }
        // read value
        state = State::Start;

        let mut value: Option<String> = None;

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    Err(ParserError::UnexpectedParserStartState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?
                }

                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            lang,
                            value: value.unwrap_or_default(), // default to empty
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::Start => {
                        let result = e
                            .unescape()
                            .map_err(ParserError::QuickXMLError)?
                            .to_string();
                        value = Some(result);
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderSimpleTypeRead for FaultCodeEnum {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    fn from_xml_simple<B: BufRead>(
        data: &[u8],
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        let resolved = reader.resolve_attribute(QName(data));
        Ok(match (&resolved.0, resolved.1.into_inner()) {
            SOAP_CODE_DATA_ENCODING_UNKNOWN_TAG_REF => Self::DataEncodingUnknown,
            SOAP_CODE_MUST_UNDERSTAND_TAG_REF => Self::MustUnderstand,
            SOAP_CODE_RECEIVER_TAG_REF => Self::Receiver,
            SOAP_CODE_SENDER_TAG_REF => Self::Sender,
            SOAP_CODE_VERSION_MISMATCH_TAG_REF => Self::VersionMismatch,
            _ => Err(ParserError::ElementError {
                element_name: "FaultCodeEnum value unknown".to_string(),
            })?,
        })
    }
}

//
// writers
//
impl ComplexXmlTypeWrite for Fault {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(SOAP_1_2_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| SOAP_FAULT, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;

        self.code
            .to_xml_complex(Some(SOAP_CODE_TAG_STR), writer, false)?;
        self.reason
            .to_xml_complex(Some(SOAP_REASON_TAG_STR), writer, false)?;

        if let Some(node) = &self.node {
            writer.write_start(Some(SOAP_1_2_NAMESPACE), SOAP_NODE)?;
            writer.write_text(node.as_str())?;
            writer.write_end(Some(SOAP_1_2_NAMESPACE), SOAP_NODE)?;
        }

        if let Some(role) = &self.role {
            writer.write_start(Some(SOAP_1_2_NAMESPACE), SOAP_ROLE)?;
            writer.write_text(role.as_str())?;
            writer.write_end(Some(SOAP_1_2_NAMESPACE), SOAP_ROLE)?;
        }

        if let Some(detail) = &self.detail {
            detail.to_xml_complex(Some(SOAP_DETAIL_TAG_STR), writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl SimpleXmlTypeWrite for FaultCodeEnum {
    fn to_xml_simple(&self, writer: &mut XmlWriter<Vec<u8>>) -> Result<Vec<u8>, WriterError> {
        let local_name = match self {
            FaultCodeEnum::DataEncodingUnknown => SOAP_CODE_DATA_ENCODING_UNKNOWN,
            FaultCodeEnum::MustUnderstand => SOAP_CODE_MUST_UNDERSTAND,
            FaultCodeEnum::Receiver => SOAP_CODE_RECEIVER,
            FaultCodeEnum::Sender => SOAP_CODE_SENDER,
            FaultCodeEnum::VersionMismatch => SOAP_CODE_VERSION_MISMATCH,
        };

        let tag = writer.qualify_tag_add_ns(Some(SOAP_1_2_NAMESPACE), local_name)?;

        Ok(tag.into_bytes())
    }
}

impl ComplexXmlTypeWrite for FaultCode {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(SOAP_1_2_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| SOAP_CODE, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;

        writer.write_start(Some(SOAP_1_2_NAMESPACE), SOAP_VALUE)?;
        let value = self.value.to_xml_simple(writer)?;
        writer.write_bytes(&value)?;
        writer.write_end(Some(SOAP_1_2_NAMESPACE), SOAP_VALUE)?;

        if let Some(subcode) = &self.subcode {
            subcode.to_xml_complex(Some(SOAP_SUBCODE_TAG_STR), writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl ComplexXmlTypeWrite for FaultReason {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(SOAP_1_2_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| SOAP_REASON, |(_, it)| it);
        writer.write_start(element_namespace, element_tag)?;

        for reason_text in &self.text {
            reason_text.to_xml_complex(Some(SOAP_TEXT_TAG_STR), writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl ComplexXmlTypeWrite for ReasonText {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(SOAP_1_2_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| SOAP_TEXT, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        writer.add_attribute((XML_LANG_PREFIXED, self.lang.as_str()))?;

        writer.write_text(&self.value)?;

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl ComplexXmlTypeWrite for Detail {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(SOAP_1_2_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| SOAP_CODE, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl ComplexXmlTypeWrite for Subcode {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(SOAP_1_2_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| SOAP_CODE, |(_, it)| it);
        writer.write_start(element_namespace, element_tag)?;

        writer.write_start(Some(SOAP_1_2_NAMESPACE), SOAP_VALUE)?;
        let value = writer.qualify_tag_add_ns(
            self.value.namespace.as_deref(),
            self.value.local_name.as_str(),
        )?;
        writer.write_text(&value)?;
        writer.write_end(Some(SOAP_1_2_NAMESPACE), SOAP_VALUE)?;

        if let Some(subcode) = &self.subcode {
            subcode.to_xml_complex(Some(SOAP_SUBCODE_TAG_STR), writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::xml::messages::common::SOAP_FAULT_TAG;
    use crate::xml::messages::soap::fault::{Fault, FaultCodeEnum};
    use protosdc_xml::{find_start_element_reader, GenericXmlReaderComplexTypeRead, XmlReader};
    use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};

    //noinspection HttpUrlsUsage
    fn test_fault(fault: &Fault) {
        assert_eq!(FaultCodeEnum::DataEncodingUnknown, fault.code.value);
        assert_eq!(
            "BROKEN",
            fault
                .code
                .subcode
                .as_ref()
                .unwrap()
                .value
                .local_name
                .as_str()
        );
        assert_eq!(
            "http://example.org",
            fault
                .code
                .subcode
                .as_ref()
                .unwrap()
                .value
                .namespace
                .as_ref()
                .unwrap()
                .as_str()
        );

        assert_eq!(2, fault.reason.text.len());

        assert_eq!("http://example.org/", fault.node.as_ref().unwrap().as_str());
        assert_eq!("http://example.org/", fault.role.as_ref().unwrap().as_str());

        assert_eq!(
            "So there I was, writing a SOAP parser on Dec. 30...",
            fault
                .detail
                .as_ref()
                .unwrap()
                .children
                .first()
                .unwrap()
                .text
                .as_ref()
                .unwrap()
                .as_str()
        )
    }

    //noinspection HttpUrlsUsage
    #[test]
    fn round_trip_soap_fault() -> anyhow::Result<()> {
        let fault = {
            let input = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>
<SOAP:Fault xmlns:SOAP=\"http://www.w3.org/2003/05/soap-envelope\">
            <SOAP:Code>
                <SOAP:Value>SOAP:DataEncodingUnknown</SOAP:Value>
                <SOAP:Subcode>
                <SOAP:Value xmlns:SO=\"http://example.org\">SO:BROKEN</SOAP:Value>
                </SOAP:Subcode>
                </SOAP:Code>
                <SOAP:Reason>
                <SOAP:Text xml:lang=\"en\">broke</SOAP:Text>
                <SOAP:Text xml:lang=\"de\">putt</SOAP:Text>
                </SOAP:Reason>
                <SOAP:Node>http://example.org/</SOAP:Node>
            <SOAP:Role>http://example.org/</SOAP:Role>
            <SOAP:Detail>
                <SO:AdditionalFunny xmlns:SO=\"http://example.org\">So there I was, writing a SOAP parser on Dec. 30...</SO:AdditionalFunny>
            </SOAP:Detail>
            </SOAP:Fault>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let fault: Fault = find_start_element_reader!(Fault, SOAP_FAULT_TAG, reader, buf)?;
            test_fault(&fault);
            fault
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        fault.to_xml_complex(None, &mut writer, false)?;
        let writer_buf = writer.inner();

        println!("{}", std::str::from_utf8(writer_buf).unwrap());

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let fault_again: Fault = find_start_element_reader!(Fault, SOAP_FAULT_TAG, reader, buf)?;
        test_fault(&fault_again);

        Ok(())
    }
}
