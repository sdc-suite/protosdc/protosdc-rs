use std::collections::HashMap;
use std::fmt::{Display, Formatter};

use const_format::concatcp;
use http::Uri;
use protosdc_biceps::types::AnyContent;
use protosdc_mapping::MappingError;
use protosdc_xml::{ComplexXmlTypeWrite, WriterError, XmlWriter};
use protosdc_xml::{ExpandedName, GenericXmlReaderComplexTypeRead, QNameSlice};
use quick_xml::name::ResolveResult;

pub const SOAP_1_2_NAMESPACE: &str = "http://www.w3.org/2003/05/soap-envelope";
pub const WS_ADDRESSING_NAMESPACE: &str = "http://www.w3.org/2005/08/addressing";
//noinspection HttpUrlsUsage
pub const WS_DISCOVERY_NAMESPACE: &str = "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01";
pub const WS_MEX_NAMESPACE: &str = "http://schemas.xmlsoap.org/ws/2004/09/mex";
pub const WS_EVENTING_NAMESPACE: &str = "http://schemas.xmlsoap.org/ws/2004/08/eventing";
pub const WS_TRANSFER_NAMESPACE: &str = "http://schemas.xmlsoap.org/ws/2004/09/transfer";

pub const SOAP_1_2_NAMESPACE_BYTES: &[u8] = SOAP_1_2_NAMESPACE.as_bytes();
pub const WS_ADDRESSING_NAMESPACE_BYTES: &[u8] = WS_ADDRESSING_NAMESPACE.as_bytes();
pub const WS_DISCOVERY_NAMESPACE_BYTES: &[u8] = WS_DISCOVERY_NAMESPACE.as_bytes();
pub const WS_MEX_NAMESPACE_BYTES: &[u8] = WS_MEX_NAMESPACE.as_bytes();
pub const WS_EVENTING_NAMESPACE_BYTES: &[u8] = WS_EVENTING_NAMESPACE.as_bytes();

//noinspection HttpUrlsUsage
pub const DPWS_NAMESPACE: &str = "http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01";
//noinspection HttpUrlsUsage
pub const MDPWS_NAMESPACE: &str = "http://standards.ieee.org/downloads/11073/11073-20702-2016";

pub const DPWS_NAMESPACE_BYTES: &[u8] = DPWS_NAMESPACE.as_bytes();
pub const MDPWS_NAMESPACE_BYTES: &[u8] = MDPWS_NAMESPACE.as_bytes();

pub const XML_NAMESPACE: &str = "http://www.w3.org/XML/1998/namespace";
pub const XML_NAMESPACE_BYTES: &[u8] = XML_NAMESPACE.as_bytes();
pub const XML_NAMESPACE_PREFIX: &str = "xml";
pub const XML_NAMESPACE_PREFIX_BYTES: &[u8] = XML_NAMESPACE_PREFIX.as_bytes();

macro_rules! soap_element_impl {
    ($struct_name:ident, $constant_name:ident) => {
        impl crate::xml::messages::common::SoapElement for $struct_name {
            fn tag() -> protosdc_xml::QNameSlice {
                paste::paste! {
                    crate::xml::messages::common::[<$constant_name _TAG>]
                }
            }
            fn tag_str() -> (Option<&'static str>, &'static str) {
                paste::paste! {
                    crate::xml::messages::common::[<$constant_name _TAG_STR>]
                }
            }
            fn ns_tag() -> quick_xml::name::ResolveResult<'static> {
                paste::paste! {
                    crate::xml::messages::common::[<$constant_name _TAG>].0
                }
            }
            fn ns_tag_str() -> Option<&'static str> {
                paste::paste! {
                    crate::xml::messages::common::[<$constant_name _TAG_STR>].0
                }
            }
        }
    };
}
pub(crate) use soap_element_impl;

/// Represents an element that can be inside a soap body, needed for dispatching
pub trait SoapElement: ComplexXmlTypeWrite
where
    Self:
        GenericXmlReaderComplexTypeRead<Extension = Box<(dyn AnyContent + Send + Sync + 'static)>>,
{
    fn tag() -> QNameSlice;
    fn tag_str() -> (Option<&'static str>, &'static str);
    fn ns_tag() -> ResolveResult<'static>;
    fn ns_tag_str() -> Option<&'static str>;
}

pub trait SoapEventMessage {
    fn ns_tag() -> ResolveResult<'static>;
    fn ns_tag_str() -> Option<&'static str>;
}

impl<T> SoapEventMessage for T
where
    T: SoapElement,
{
    fn ns_tag() -> ResolveResult<'static> {
        T::ns_tag()
    }

    fn ns_tag_str() -> Option<&'static str> {
        T::ns_tag_str()
    }
}

macro_rules! create_tag_constants {
    ($namespace:ident, $struct_name:ident) => {
        paste::paste! {
            #[allow(dead_code)]
            pub const [<$struct_name _BYTES>]: &[u8] = $struct_name.as_bytes();
            #[allow(dead_code)]
            pub const [<$struct_name _TAG>]: protosdc_xml::QNameSlice = (quick_xml::name::ResolveResult::Bound(quick_xml::name::Namespace($namespace.as_bytes())), [<$struct_name _BYTES>]);
            #[allow(dead_code)]
            pub const [<$struct_name _TAG_REF>]: (&quick_xml::name::ResolveResult<'static>, &[u8]) = (&[<$struct_name _TAG>].0, [<$struct_name _BYTES>]);
            #[allow(dead_code)]
            pub const [<$struct_name _TAG_STR>]: (Option<&str>, &str) = (Some($namespace), $struct_name);
        }
    };
    ($struct_name:ident) => {
        paste::paste! {
            #[allow(dead_code)]
            pub const [<$struct_name _BYTES>]: &[u8] = $struct_name.as_bytes();
            #[allow(dead_code)]
            pub const [<$struct_name _TAG>]: protosdc_xml::QNameSlice = (quick_xml::name::ResolveResult::Unbound, [<$struct_name _BYTES>]);
            #[allow(dead_code)]
            pub const [<$struct_name _TAG_REF>]: (&quick_xml::name::ResolveResult<'static>, &[u8]) = (&[<$struct_name _TAG>].0, [<$struct_name _BYTES>]);
            #[allow(dead_code)]
            pub const [<$struct_name _TAG_STR>]: (Option<&str>, &str) = (None, $struct_name);
        }
    };
}

// export macro
pub(crate) use create_tag_constants;

pub const SOAP_ENVELOPE: &str = "Envelope";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_ENVELOPE);
pub const SOAP_HEADER: &str = "Header";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_HEADER);
pub const SOAP_BODY: &str = "Body";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_BODY);
pub const SOAP_FAULT: &str = "Fault";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_FAULT);
pub const SOAP_CODE: &str = "Code";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_CODE);
pub const SOAP_REASON: &str = "Reason";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_REASON);
pub const SOAP_NODE: &str = "Node";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_NODE);
pub const SOAP_ROLE: &str = "Role";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_ROLE);
pub const SOAP_DETAIL: &str = "Detail";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_DETAIL);
pub const SOAP_TEXT: &str = "Text";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_TEXT);

pub const SOAP_CODE_DATA_ENCODING_UNKNOWN: &str = "DataEncodingUnknown";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_CODE_DATA_ENCODING_UNKNOWN);
pub const SOAP_CODE_MUST_UNDERSTAND: &str = "MustUnderstand";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_CODE_MUST_UNDERSTAND);
pub const SOAP_CODE_RECEIVER: &str = "Receiver";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_CODE_RECEIVER);
pub const SOAP_CODE_SENDER: &str = "Sender";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_CODE_SENDER);
pub const SOAP_CODE_VERSION_MISMATCH: &str = "VersionMismatch";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_CODE_VERSION_MISMATCH);

pub const SOAP_VALUE: &str = "Value";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_VALUE);
//noinspection SpellCheckingInspection
pub const SOAP_SUBCODE: &str = "Subcode";
create_tag_constants!(SOAP_1_2_NAMESPACE, SOAP_SUBCODE);

pub const MESSAGE_ID: &str = "MessageID";
create_tag_constants!(WS_ADDRESSING_NAMESPACE, MESSAGE_ID);
pub const ACTION: &str = "Action";
create_tag_constants!(WS_ADDRESSING_NAMESPACE, ACTION);
pub const FROM: &str = "From";
create_tag_constants!(WS_ADDRESSING_NAMESPACE, FROM);
pub const TO: &str = "To";
create_tag_constants!(WS_ADDRESSING_NAMESPACE, TO);
pub const RELATES_TO: &str = "RelatesTo";
create_tag_constants!(WS_ADDRESSING_NAMESPACE, RELATES_TO);
pub const ADDRESS: &str = "Address";
create_tag_constants!(WS_ADDRESSING_NAMESPACE, ADDRESS);
pub const REFERENCE_PARAMETERS: &str = "ReferenceParameters";
create_tag_constants!(WS_ADDRESSING_NAMESPACE, REFERENCE_PARAMETERS);
pub const METADATA: &str = "Metadata";
create_tag_constants!(WS_ADDRESSING_NAMESPACE, METADATA);
pub const ATTRIBUTED_URI_TYPE: &str = "AttributedURIType";
create_tag_constants!(WS_ADDRESSING_NAMESPACE, ATTRIBUTED_URI_TYPE);
pub const ENDPOINT_REFERENCE: &str = "EndpointReference";
create_tag_constants!(WS_ADDRESSING_NAMESPACE, ENDPOINT_REFERENCE);
pub const WSA_IS_REFERENCE_PARAMETER: &str = "IsReferenceParameter";
create_tag_constants!(WS_ADDRESSING_NAMESPACE, WSA_IS_REFERENCE_PARAMETER);

pub const PROBE: &str = "Probe";
create_tag_constants!(WS_DISCOVERY_NAMESPACE, PROBE);
pub const PROBE_MATCHES: &str = "ProbeMatches";
create_tag_constants!(WS_DISCOVERY_NAMESPACE, PROBE_MATCHES);
pub const PROBE_MATCH: &str = "ProbeMatch";
create_tag_constants!(WS_DISCOVERY_NAMESPACE, PROBE_MATCH);
pub const RESOLVE: &str = "Resolve";
create_tag_constants!(WS_DISCOVERY_NAMESPACE, RESOLVE);
pub const RESOLVE_MATCHES: &str = "ResolveMatches";
create_tag_constants!(WS_DISCOVERY_NAMESPACE, RESOLVE_MATCHES);
pub const RESOLVE_MATCH: &str = "ResolveMatch";
create_tag_constants!(WS_DISCOVERY_NAMESPACE, RESOLVE_MATCH);
pub const HELLO: &str = "Hello";
create_tag_constants!(WS_DISCOVERY_NAMESPACE, HELLO);
pub const BYE: &str = "Bye";
create_tag_constants!(WS_DISCOVERY_NAMESPACE, BYE);
pub const TYPES: &str = "Types";
create_tag_constants!(WS_DISCOVERY_NAMESPACE, TYPES);
pub const SCOPES: &str = "Scopes";
create_tag_constants!(WS_DISCOVERY_NAMESPACE, SCOPES);
pub const X_ADDRS: &str = "XAddrs";
create_tag_constants!(WS_DISCOVERY_NAMESPACE, X_ADDRS);
pub const METADATA_VERSION: &str = "MetadataVersion";
create_tag_constants!(WS_DISCOVERY_NAMESPACE, METADATA_VERSION);
pub const APP_SEQUENCE: &str = "AppSequence";
create_tag_constants!(WS_DISCOVERY_NAMESPACE, APP_SEQUENCE);

pub const WSDD_INSTANCE_ID: &str = "InstanceId";
create_tag_constants!(WSDD_INSTANCE_ID);
pub const WSDD_SEQUENCE_ID: &str = "SequenceId";
create_tag_constants!(WSDD_SEQUENCE_ID);
pub const WSDD_MESSAGE_NUMBER: &str = "MessageNumber";
create_tag_constants!(WSDD_MESSAGE_NUMBER);

pub const MATCH_BY: &str = "MatchBy";
create_tag_constants!(MATCH_BY);

pub const MEX_GET_METADATA: &str = "GetMetadata";
create_tag_constants!(WS_MEX_NAMESPACE, MEX_GET_METADATA);
pub const MEX_METADATA: &str = "Metadata";
create_tag_constants!(WS_MEX_NAMESPACE, MEX_METADATA);
pub const MEX_DIALECT: &str = "Dialect";
create_tag_constants!(WS_MEX_NAMESPACE, MEX_DIALECT);
pub const MEX_IDENTIFIER: &str = "Identifier";
create_tag_constants!(WS_MEX_NAMESPACE, MEX_IDENTIFIER);
pub const MEX_LOCATION: &str = "Location";
create_tag_constants!(WS_MEX_NAMESPACE, MEX_LOCATION);
pub const MEX_METADATA_REFERENCE: &str = "MetadataReference";
create_tag_constants!(WS_MEX_NAMESPACE, MEX_METADATA_REFERENCE);
pub const MEX_METADATA_SECTION: &str = "MetadataSection";
create_tag_constants!(WS_MEX_NAMESPACE, MEX_METADATA_SECTION);

pub const DPWS_RELATIONSHIP: &str = "Relationship";
create_tag_constants!(DPWS_NAMESPACE, DPWS_RELATIONSHIP);
pub const DPWS_MANUFACTURER: &str = "Manufacturer";
create_tag_constants!(DPWS_NAMESPACE, DPWS_MANUFACTURER);
pub const DPWS_MANUFACTURER_URL: &str = "ManufacturerUrl";
create_tag_constants!(DPWS_NAMESPACE, DPWS_MANUFACTURER_URL);
pub const DPWS_MODEL_NAME: &str = "ModelName";
create_tag_constants!(DPWS_NAMESPACE, DPWS_MODEL_NAME);
pub const DPWS_MODEL_NUMBER: &str = "ModelNumber";
create_tag_constants!(DPWS_NAMESPACE, DPWS_MODEL_NUMBER);
pub const DPWS_MODEL_URL: &str = "ModelUrl";
create_tag_constants!(DPWS_NAMESPACE, DPWS_MODEL_URL);
pub const DPWS_PRESENTATION_URL: &str = "PresentationUrl";
create_tag_constants!(DPWS_NAMESPACE, DPWS_PRESENTATION_URL);
pub const DPWS_FRIENDLY_NAME: &str = "FriendlyName";
create_tag_constants!(DPWS_NAMESPACE, DPWS_FRIENDLY_NAME);
pub const DPWS_FIRMWARE_VERSION: &str = "FirmwareVersion";
create_tag_constants!(DPWS_NAMESPACE, DPWS_FIRMWARE_VERSION);
pub const DPWS_SERIAL_NUMBER: &str = "SerialNumber";
create_tag_constants!(DPWS_NAMESPACE, DPWS_SERIAL_NUMBER);
pub const DPWS_TYPES: &str = "Types";
create_tag_constants!(DPWS_NAMESPACE, DPWS_TYPES);
pub const DPWS_SERVICE_ID: &str = "ServiceId";
create_tag_constants!(DPWS_NAMESPACE, DPWS_SERVICE_ID);
pub const DPWS_TYPE: &str = "Type";
create_tag_constants!(DPWS_TYPE);

pub const DPWS_HOST: &str = "Host";
create_tag_constants!(DPWS_NAMESPACE, DPWS_HOST);
pub const DPWS_HOSTED: &str = "Hosted";
create_tag_constants!(DPWS_NAMESPACE, DPWS_HOSTED);

pub const DPWS_THIS_MODEL: &str = "ThisModel";
create_tag_constants!(DPWS_NAMESPACE, DPWS_THIS_MODEL);
pub const DPWS_THIS_DEVICE: &str = "ThisDevice";
create_tag_constants!(DPWS_NAMESPACE, DPWS_THIS_DEVICE);

pub const DPWS_THIS_MODEL_DIALECT: &str = concatcp!(DPWS_NAMESPACE, "/", DPWS_THIS_MODEL);
pub const DPWS_THIS_DEVICE_DIALECT: &str = concatcp!(DPWS_NAMESPACE, "/", DPWS_THIS_DEVICE);
pub const DPWS_RELATIONSHIP_DIALECT: &str = concatcp!(DPWS_NAMESPACE, "/", DPWS_RELATIONSHIP);

pub const WSDL_DIALECT: &str = "http://schemas.xmlsoap.org/wsdl/";

pub const DPWS_TYPE_HOST: &str = concatcp!(DPWS_NAMESPACE, "/", "host");

pub const DPWS_DEVICE_TYPE: ConstQualifiedName<'static> = ConstQualifiedName {
    namespace: Some(DPWS_NAMESPACE),
    local_name: "Device",
};

pub const MDPWS_MEDICAL_DEVICE_TYPE: ConstQualifiedName<'static> = ConstQualifiedName {
    namespace: Some(MDPWS_NAMESPACE),
    local_name: "MedicalDevice",
};

pub const GLUE_SCOPE: &str = "sdc.mds.pkp:1.2.840.10004.20701.1.1";

pub const WSE_NOTIFY_TO: &str = "NotifyTo";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_NOTIFY_TO);
pub const WSE_END_TO: &str = "EndTo";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_END_TO);
pub const WSE_DELIVERY: &str = "Delivery";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_DELIVERY);
pub const WSE_EXPIRES: &str = "Expires";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_EXPIRES);
pub const WSE_FILTER: &str = "Filter";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_FILTER);
pub const WSE_STATUS: &str = "Status";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_STATUS);
pub const WSE_REASON: &str = "Reason";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_REASON);
pub const WSE_SUBSCRIBE: &str = "Subscribe";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_SUBSCRIBE);
pub const WSE_SUBSCRIBE_RESPONSE: &str = "SubscribeResponse";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_SUBSCRIBE_RESPONSE);
pub const WSE_SUBSCRIPTION_END: &str = "SubscriptionEnd";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_SUBSCRIPTION_END);
pub const WSE_RENEW: &str = "Renew";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_RENEW);
pub const WSE_RENEW_RESPONSE: &str = "RenewResponse";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_RENEW_RESPONSE);
pub const WSE_GET_STATUS: &str = "GetStatus";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_GET_STATUS);
pub const WSE_GET_STATUS_RESPONSE: &str = "GetStatusResponse";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_GET_STATUS_RESPONSE);
pub const WSE_UNSUBSCRIBE: &str = "Unsubscribe";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_UNSUBSCRIBE);

pub const WSE_DIALECT: &str = "Dialect";
create_tag_constants!(WSE_DIALECT);

pub const WSE_SUBSCRIPTION_MANAGER: &str = "SubscriptionManager";
create_tag_constants!(WS_EVENTING_NAMESPACE, WSE_SUBSCRIPTION_MANAGER);

pub const WSE_MODE: &str = "Mode";
create_tag_constants!(WSE_MODE);

pub const XML_LANG: &str = "lang";
create_tag_constants!(XML_NAMESPACE, XML_LANG);
pub const XML_LANG_PREFIXED: &str = "xml:lang";

pub const DO_NOT_CARE_QNAME: QNameSlice = (ResolveResult::Unbound, b"");

pub const WSE_DELIVERY_MODE_PUSH: &str = concatcp!(WS_EVENTING_NAMESPACE, "/DeliveryModes/Push");

pub const DPWS_ACTION_DIALECT: &str = concatcp!(DPWS_NAMESPACE, "/Action");

pub const NOOP_NAMESPACE: &str = "NoopNamespace";
pub const NOOP_RESPONSE: &str = "NoopResponse";
create_tag_constants!(NOOP_NAMESPACE, NOOP_RESPONSE);

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct ConstQualifiedName<'a> {
    pub namespace: Option<&'a str>,
    pub local_name: &'a str,
}

impl PartialEq<ExpandedName> for ConstQualifiedName<'_> {
    fn eq(&self, other: &ExpandedName) -> bool {
        match (self.namespace, &other.namespace) {
            (Some(s), Some(o)) => {
                if s != o.as_str() {
                    return false;
                }
            }
            // is match
            (None, None) => {}
            // is no match
            _ => return false,
        }

        self.local_name == other.local_name.as_str()
    }
}

impl PartialEq<ConstQualifiedName<'_>> for ExpandedName {
    fn eq(&self, other: &ConstQualifiedName<'_>) -> bool {
        other == self // trigger the implemented comparison
    }
}

impl From<ConstQualifiedName<'_>> for ExpandedName {
    fn from(value: ConstQualifiedName<'_>) -> Self {
        ExpandedName {
            namespace: value.namespace.map(|it| it.to_string()),
            local_name: value.local_name.to_string(),
        }
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct HttpUri {
    pub(crate) uri: Uri,
}

impl Display for HttpUri {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.uri)
    }
}

impl From<Uri> for HttpUri {
    fn from(value: Uri) -> Self {
        Self { uri: value }
    }
}

impl TryFrom<String> for HttpUri {
    type Error = MappingError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Ok(Self {
            uri: http::Uri::try_from(&value).map_err(|err| MappingError::FromProtoGeneric {
                message: format!("{:?}", err),
            })?,
        })
    }
}

impl TryFrom<&str> for HttpUri {
    type Error = MappingError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Ok(Self {
            uri: http::Uri::try_from(value).map_err(|err| MappingError::FromProtoGeneric {
                message: format!("{:?}", err),
            })?,
        })
    }
}

// impl GenericXmlReaderSimpleTypeRead for QualifiedName {
//     type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
//     fn from_xml_simple<B: BufRead>(
//         data: &[u8],
//         reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
//     ) -> Result<Self, ParserError> {
//         reader.resolve_element(QName(data)).try_into()
//     }
// }
//
// impl TryFrom<(Option<&[u8]>, &[u8])> for QualifiedName {
//     type Error = ParserError;
//
//     fn try_from(value: (Option<&[u8]>, &[u8])) -> Result<Self, Self::Error> {
//         let (resolved_namespace, local_name) = value;
//         let key = match resolved_namespace {
//             None => QualifiedName {
//                 namespace: None,
//                 local_name: std::str::from_utf8(local_name)
//                     .map(|it| it.to_string())
//                     .map_err(|_err| ParserError::Other)?,
//             },
//             Some(namespace) => QualifiedName {
//                 namespace: Some(
//                     std::str::from_utf8(namespace)
//                         .map(|it| it.to_string())
//                         .map_err(|_err| ParserError::Other)?,
//                 ),
//                 local_name: std::str::from_utf8(local_name)
//                     .map(|it| it.to_string())
//                     .map_err(|_err| ParserError::Other)?,
//             },
//         };
//         Ok(key)
//     }
// }
//
// impl TryFrom<(ResolveResult<'_>, LocalName<'_>)> for QualifiedName {
//     type Error = ParserError;
//
//     fn try_from(value: (ResolveResult, LocalName)) -> Result<Self, Self::Error> {
//         let (resolved_namespace, local_name) = value;
//         let key = match resolved_namespace {
//             ResolveResult::Unbound => QualifiedName {
//                 namespace: None,
//                 local_name: std::str::from_utf8(local_name.into_inner())
//                     .map(|it| it.to_string())
//                     .map_err(|_err| ParserError::Other)?,
//             },
//             ResolveResult::Bound(Namespace(namespace)) => QualifiedName {
//                 namespace: Some(
//                     std::str::from_utf8(namespace)
//                         .map(|it| it.to_string())
//                         .map_err(|_err| ParserError::Other)?,
//                 ),
//                 local_name: std::str::from_utf8(local_name.into_inner())
//                     .map(|it| it.to_string())
//                     .map_err(|_err| ParserError::Other)?,
//             },
//             ResolveResult::Unknown(unk) => match unk.as_slice() {
//                 // hardcode the xml prefix
//                 XML_NAMESPACE_PREFIX_BYTES => QualifiedName {
//                     namespace: Some(XML_NAMESPACE.to_string()),
//                     local_name: std::str::from_utf8(local_name.into_inner())
//                         .map(|it| it.to_string())
//                         .map_err(|_err| ParserError::Other)?,
//                 },
//                 _ => Err(ParserError::UnexpectedParserEvent {
//                     event: format!("{{ResolveResult::Unknown({:?})}}, {:?}", unk, local_name),
//                 })?,
//             },
//         };
//         Ok(key)
//     }
// }

pub fn write_attributes(
    writer: &mut XmlWriter<Vec<u8>>,
    attributes: &HashMap<ExpandedName, String>,
) -> Result<(), WriterError> {
    for (attribute_key, attribute_value) in attributes {
        writer.add_attribute_qname(
            attribute_key.namespace.as_deref(),
            &attribute_key.local_name,
            None,
            attribute_value,
        )?;
    }
    Ok(())
}

#[cfg(test)]
mod test {
    use crate::xml::messages::common::HttpUri;

    #[test]
    fn test_parse_uri() {
        let values = vec!["HighPriorityServices"];

        for v in values {
            let parsed = HttpUri::try_from(v.to_string()).unwrap();
            assert_eq!(v, parsed.uri.to_string().as_str())
        }
    }
}
