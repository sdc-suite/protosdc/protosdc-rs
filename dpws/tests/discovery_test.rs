use common::test_util::init_logging;
use dpws::discovery::common::Endpoint;
use dpws::discovery::consumer::discovery_consumer::{
    DpwsDiscoveryConsumer, DpwsDiscoveryConsumerImpl, ProbeOptions,
};
use dpws::discovery::provider::discovery_provider::{
    DpwsDiscoveryProcessorImpl, DpwsDiscoveryProviderImpl,
};
use dpws::discovery::provider::discovery_proxy_provider::DummyDpwsDiscoveryProxyProviderImpl;
use dpws::xml::messages::addressing::{WsaAttributedURIType, WsaEndpointReference};
use network::mock_udp_binding::MockUdpBinding;
use std::time::Duration;
use tokio::sync::broadcast;

#[tokio::test]
async fn test_discovery_probe_resolve() -> anyhow::Result<()> {
    init_logging();

    let (sender, _) = broadcast::channel(1);

    let provider_epr: String = "https://example.org/".to_string();

    let mock_endpoint = Endpoint {
        scopes: vec![],
        types: vec![],
        endpoint_reference: WsaEndpointReference {
            attributes: Default::default(),
            address: WsaAttributedURIType {
                attributes: Default::default(),
                value: provider_epr.clone(),
            },
            reference_parameters: None,
            metadata: None,
            children: vec![],
        },
        x_addrs: vec![],
        metadata_version: 0,
    };

    let discovery_processor = DpwsDiscoveryProcessorImpl::new(mock_endpoint, None);

    let binding = MockUdpBinding::new_with_channel(sender.clone());
    let consumer = DpwsDiscoveryConsumerImpl::new(binding);
    let binding = MockUdpBinding::new_with_channel(sender.clone());
    let disco: Option<DummyDpwsDiscoveryProxyProviderImpl> = None;
    let _provider = DpwsDiscoveryProviderImpl::new(binding, discovery_processor, disco);

    let results = consumer
        .probe(
            ProbeOptions::builder()
                .max_wait(Duration::from_secs(1))
                .build(),
        )
        .await?;

    assert_eq!(1, results.len());
    assert_eq!(
        provider_epr.as_str(),
        results
            .first()
            .unwrap()
            .probe_match
            .first()
            .unwrap()
            .probe_match
            .endpoint_reference
            .address
            .value
            .as_str()
    );

    let resolve_results = consumer
        .resolve(&provider_epr, Some(Duration::from_secs(100)))
        .await?;

    assert_eq!(
        provider_epr.as_str(),
        resolve_results
            .resolve_match
            .as_ref()
            .unwrap()
            .endpoint_reference
            .address
            .value
            .as_str()
    );

    Ok(())
}
