use itertools::Itertools;
use std::net::IpAddr;
use std::time::Duration;

use log::info;
use protosdc_biceps::biceps::InvocationState;

use common::test_util::init_logging;
use common::Service;
use common_test::crypto::generate_dpws_certificates;
use common_test::tests::sco::sco_types::{
    AsyncOperationInvocationReceiver, DirectOperationInvocationReceiver,
};
use dpws::soap::dpws::device::hosting_service::HostingService;
use network::platform::test_util::determine_adapter_address_for_multicast;
use network::udp_binding::IPV4_MULTICAST_ADDRESS;
use protosdc::provider::common::ROOT_PEM_STRING_SDC;
use protosdc::provider::sco_util::{InvocationStateTypes, NotFailedInvocationStateTypes};

use crate::test_util::{connect_consumer, create_device_generic};

#[cfg(test)]
pub mod test_util;

#[tokio::test]
async fn test_dpws_with_synchronous_operation() -> anyhow::Result<()> {
    init_logging();

    let receiver = DirectOperationInvocationReceiver {
        response_state: InvocationStateTypes::NotFailed(NotFailedInvocationStateTypes::Fin),
    };
    let final_state: InvocationState = receiver.response_state.clone().into();

    // find address which does ipv4 multicast
    let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
    let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

    let mut device = create_device_generic(receiver, adapter_address, None, None).await?;
    device.start().await?;

    let to_connect = device.hosting_service.endpoint_reference().await;
    let remote_device = connect_consumer(adapter_address, to_connect, None).await?;

    let mut sco = remote_device.set_service.expect("No set service found");

    info!("Got ScoController");

    common_test::tests::sco::sco_tests::test_with_synchronous_operation(&mut sco, &final_state)
        .await
}

#[tokio::test]
async fn test_dpws_with_asynchronous_operation() -> anyhow::Result<()> {
    init_logging();

    let transitions = vec![
        InvocationStateTypes::NotFailed(NotFailedInvocationStateTypes::Wait),
        InvocationStateTypes::NotFailed(NotFailedInvocationStateTypes::Start),
        InvocationStateTypes::NotFailed(NotFailedInvocationStateTypes::FinMod),
    ];

    let receiver = AsyncOperationInvocationReceiver {
        invocation_states: transitions.clone(),
        time_between_steps: Duration::from_secs(1),
    };

    let transitions_invocation_state = receiver
        .invocation_states
        .iter()
        .cloned()
        .map(|it| it.into())
        .collect_vec();

    // find address which does ipv4 multicast
    let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
    let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

    let mut device = create_device_generic(receiver, adapter_address, None, None).await?;
    device.start().await?;

    let to_connect = device.hosting_service.endpoint_reference().await;
    let remote_device = connect_consumer(adapter_address, to_connect, None).await?;

    let mut sco = remote_device.set_service.expect("No set service found");
    info!("Got ScoController");

    common_test::tests::sco::sco_tests::test_with_asynchronous_operation(
        &mut sco,
        &transitions_invocation_state,
    )
    .await
}

#[tokio::test]
async fn test_dpws_tls_invocation_source() -> anyhow::Result<()> {
    init_logging();

    let receiver = DirectOperationInvocationReceiver {
        response_state: InvocationStateTypes::NotFailed(NotFailedInvocationStateTypes::Fin),
    };
    let final_state: InvocationState = receiver.response_state.clone().into();

    // find address which does ipv4 multicast
    let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
    let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

    let certificates = generate_dpws_certificates()?;

    let mut device = create_device_generic(
        receiver,
        adapter_address,
        Some(certificates.provider_crypto_config()),
        None,
    )
    .await?;
    device.start().await?;

    let to_connect = device.hosting_service.endpoint_reference().await;
    let remote_device = connect_consumer(
        adapter_address,
        to_connect,
        Some(certificates.consumer_crypto_config()),
    )
    .await?;

    let mut sco = remote_device.set_service.expect("No set service found");

    info!("Got ScoController");
    tokio::time::sleep(Duration::from_secs(1)).await;

    common_test::tests::sco::sco_tests::test_tls_invocation_source(
        &mut sco,
        &final_state,
        &certificates,
        ROOT_PEM_STRING_SDC,
    )
    .await
}
