use crate::provider::sdc_provider::{
    DeviceDiscoveryAccess, DeviceMdibType, SdcDeviceContext, SdcProviderPlugin,
};
use async_trait::async_trait;
use biceps::common::access::mdib_access::{MdibAccess, MdibAccessEvent};
use biceps::common::context::location_detail_query_mapper::{
    create_with_location_detail_query, LocationDetail as ContextLocationDetail,
};
use biceps::provider::access::local_mdib_access::{
    LocalMdibAccess, LocalMdibAccessReadTransaction,
};
use futures::FutureExt;
use itertools::Itertools;
use log::{error, info};
use protosdc_biceps::biceps::{
    context_association_mod, AbstractContextStateOneOf, InstanceIdentifierOneOf,
};
use std::collections::HashSet;
use tokio::task::JoinHandle;
use typed_builder::TypedBuilder;

pub struct ProviderPluginChain<C, N>
where
    C: SdcProviderPlugin,
    N: SdcProviderPlugin,
{
    current: C,
    next: N,
}

impl<C, N> ProviderPluginChain<C, N>
where
    C: SdcProviderPlugin + Send + Sync,
    N: SdcProviderPlugin + Send + Sync,
{
    pub fn create(current: C, next: N) -> Self {
        Self { current, next }
    }
}

#[async_trait::async_trait]
impl<C, N> SdcProviderPlugin for ProviderPluginChain<C, N>
where
    C: SdcProviderPlugin + Send + Sync,
    N: SdcProviderPlugin + Send + Sync,
{
    async fn before_startup<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess + Clone,
    {
        self.current.before_startup(context).await?;
        self.next.before_startup(context).await
    }

    async fn after_startup<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess + Clone,
    {
        self.current.after_startup(context).await?;
        self.next.after_startup(context).await
    }

    async fn before_shutdown<M, D>(
        &mut self,
        context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess + Clone,
    {
        self.current.before_shutdown(context).await?;
        self.next.before_shutdown(context).await
    }

    async fn after_shutdown<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess + Clone,
    {
        self.current.after_shutdown(context).await?;
        self.next.after_shutdown(context).await
    }
}

/// Chains a variable amount of plugins into one [ProviderPluginChain].
#[macro_export]
macro_rules! provider_chain_plugins {
    // The pattern for a single `eval`
    ($first:expr, $second:expr, $($more:expr ),*) => {
        {
            $crate::provider::plugin::ProviderPluginChain::create($first, provider_chain_plugins!($second, $($more ),*))
        }
    };

    ($first:expr, $second:expr) => {
        {
            $crate::provider::plugin::ProviderPluginChain::create($first, $second)
        }
    };
}

#[derive(TypedBuilder)]
pub struct ScopesPlugin {
    #[builder(default, setter(skip))]
    processing_task: Option<JoinHandle<()>>,
    #[builder(default)]
    pub constant_scopes: Vec<String>,
}

impl ScopesPlugin {
    pub async fn update_from_mdib<M, P>(mdib: &M, processor: &mut P, additional_scopes: &[String])
    where
        M: LocalMdibAccess + LocalMdibAccessReadTransaction + 'static,
        P: DeviceDiscoveryAccess,
    {
        let location_scopes = mdib
            .read_transaction(|access| {
                async move {
                    access
                        .context_states()
                        .await
                        .into_iter()
                        .filter_map(|it| match it {
                            AbstractContextStateOneOf::LocationContextState(it) => Some(it),
                            _ => None,
                        })
                        .filter(|it| {
                            it.abstract_context_state.context_association_attr
                                == Some(protosdc_biceps::biceps::ContextAssociation {
                                    enum_type: context_association_mod::EnumType::Assoc,
                                })
                        })
                        .flat_map(|it| {
                            let location_detail: ContextLocationDetail = match it.location_detail {
                                None => ContextLocationDetail::default(),
                                Some(it) => it.into(),
                            };

                            it.abstract_context_state.identification.into_iter().map(
                                move |ident_oo| match ident_oo {
                                    InstanceIdentifierOneOf::InstanceIdentifier(ident) => {
                                        create_with_location_detail_query(&ident, &location_detail)
                                    }
                                    InstanceIdentifierOneOf::OperatingJurisdiction(juris) => {
                                        create_with_location_detail_query(
                                            &juris.instance_identifier,
                                            &location_detail,
                                        )
                                    }
                                },
                            )
                        })
                        .filter_map(|it| match it {
                            Ok(it) => Some(it),
                            Err(err) => {
                                error!("Error while processing update: {:?}", err);
                                None
                            }
                        })
                        .collect_vec()
                }
                .boxed()
            })
            .await;

        let mut scopes = HashSet::new();
        scopes.extend(location_scopes);
        scopes.extend(additional_scopes.iter().cloned().collect_vec());

        info!("Updating scopes to {:?}", scopes);
        let transformed_scopes = scopes.into_iter().collect();
        {
            processor.update_scopes(transformed_scopes).await;
        }
    }
}

#[async_trait]
impl SdcProviderPlugin for ScopesPlugin {
    async fn before_startup<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: protosdc::provider::plugins::DeviceMdibType,
        D: DeviceDiscoveryAccess + Clone,
    {
        info!("Startup of automatic required scopes updating for device");

        let mut change_subscription = context.mdib_access.subscribe().await;
        let task_mdib = context.mdib_access.clone();
        let mut task_processor = context.discovery_access.clone();

        let constant_types = self.constant_scopes.clone();

        self.processing_task = Some(tokio::spawn(async move {
            info!("receiver_task started");
            loop {
                let message = change_subscription.recv().await;

                match message {
                    Some(msg) => match msg {
                        MdibAccessEvent::ContextStateModification { .. }
                        | MdibAccessEvent::DescriptionModification { .. } => {
                            ScopesPlugin::update_from_mdib(
                                &task_mdib,
                                &mut task_processor,
                                &constant_types,
                            )
                            .await
                        }
                        _ => {
                            // pass
                        }
                    },
                    None => {
                        error!("Channel was closed");
                        break;
                    }
                }
            }
            info!("receiver_task finished");
        }));

        Ok(())
    }

    async fn after_shutdown<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: protosdc::provider::plugins::DeviceMdibType,
        D: DeviceDiscoveryAccess,
    {
        info!("Automatic required types and scopes updating stopped");
        if let Some(it) = self.processing_task.take() {
            it.abort()
        }
        Ok(())
    }
}
