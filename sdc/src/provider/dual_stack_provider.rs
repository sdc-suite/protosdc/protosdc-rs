use crate::provider::sdc_provider::{
    DiscoveryAccess, SdcDeviceContext, SdcProvider, SdcProviderPlugin,
};
use biceps::provider::access::local_mdib_access::{
    LocalMdibAccess, LocalMdibAccessReadTransaction,
};
use common::crypto::CryptoConfig;
use common::{Service, ServiceDelegate, ServiceError, ServiceState};
use dpws::common::extension_handling::ExtensionHandlerFactory;
use dpws::discovery::provider::discovery_provider::{
    DpwsDiscoveryProcessor, DpwsDiscoveryProcessorImpl, DpwsDiscoveryProvider,
    DpwsDiscoveryProviderImpl,
};
use dpws::discovery::provider::discovery_proxy_provider::DpwsDiscoveryProxyProviderImpl;
use dpws::http::client::reqwest::{ReqwestClientAccess, ReqwestClientError, ReqwestHttpClient};
use dpws::http::server::axum::AxumRegistry;
use dpws::http::server::common::{RegistryError, ServerError, ServerRegistry};
use dpws::provider::high_priority_services::HighPriorityServices;
use dpws::soap::dpws::device::dpws_device::DeviceSettings;
use dpws::soap::dpws::device::hosting_service::{
    HostingService, HostingServiceError, HostingServiceImpl,
};
use dpws::soap::soap_client::SoapClientImpl;
use dpws::xml::messages::addressing::WsaEndpointReference;
use dpws::xml::messages::common::{DPWS_DEVICE_TYPE, MDPWS_MEDICAL_DEVICE_TYPE};
use http::Uri;
use log::info;
use network::udp_binding::{UdpBindingError, UdpBindingImpl};
use protosdc::discovery::common::error::DiscoveryError;
use protosdc::discovery::provider::discovery_provider::{DiscoveryProvider, DiscoveryProviderImpl};
use protosdc::provider::common::{
    socket_address_to_uri_http, socket_address_to_uri_https, EndpointData,
};
use protosdc::provider::device::ServerConfig;
use protosdc::provider::localization_storage::{
    LocalizationStorage, LocalizationStorageImpl, LocalizedStorageText,
};
use protosdc::provider::metadata::DeviceMetadata;
use protosdc::provider::sco::{OperationInvocationReceiver, ScoController};
use protosdc::provider::services::{PrimaryProviderServices, PrimaryProviderServicesConfig};
use std::collections::HashSet;
use std::fmt::Debug;
use std::marker::PhantomData;
use std::net::{IpAddr, SocketAddr};
use thiserror::Error;
use typed_builder::TypedBuilder;
use uuid::Uuid;
#[derive(Debug)]
pub struct DualStackDiscoveryInner<DPWS, PROTO>
where
    DPWS: DpwsDiscoveryProvider + 'static + Debug + Clone,
    PROTO: DiscoveryProvider + 'static + Debug + Clone,
{
    pub dpws_discovery: DPWS,
    pub proto_discovery: PROTO,
}

#[derive(Clone, Debug)]
pub struct DualStackDiscovery<Dpws, DpwsProcessor, Proto>
where
    Dpws: DpwsDiscoveryProvider + 'static + Debug + Clone,
    DpwsProcessor: DpwsDiscoveryProcessor + 'static + Debug + Clone,
    Proto: DiscoveryProvider + 'static + Debug + Clone,
{
    pub dpws_discovery: Dpws,
    pub dpws_processor: DpwsProcessor,
    pub proto_discovery: Proto,
    pub proto_endpoint: EndpointData,
}

impl<Dpws, Proto, DpwsProcessor> DualStackDiscovery<Dpws, DpwsProcessor, Proto>
where
    Dpws: DpwsDiscoveryProvider + 'static + Debug + Clone,
    DpwsProcessor: DpwsDiscoveryProcessor + 'static + Debug + Clone,
    Proto: DiscoveryProvider + 'static + Debug + Clone,
{
    pub fn new(
        dpws: Dpws,
        dpws_processor: DpwsProcessor,
        proto: Proto,
        proto_endpoint: EndpointData,
    ) -> Self {
        Self {
            dpws_discovery: dpws,
            dpws_processor,
            proto_discovery: proto,
            proto_endpoint,
        }
    }
}

#[async_trait::async_trait]
impl<Dpws, Proto, DpwsProcessor> DiscoveryAccess for DualStackDiscovery<Dpws, DpwsProcessor, Proto>
where
    Dpws: DpwsDiscoveryProvider + DiscoveryAccess + Send + Sync + Debug + Clone,
    DpwsProcessor: DpwsDiscoveryProcessor + 'static + Send + Sync + Debug + Clone,
    Proto: DiscoveryProvider + DiscoveryAccess + Send + Sync + Debug + Clone,
{
    async fn endpoint_reference_address(&self) -> String {
        self.dpws_discovery.endpoint_reference_address().await
    }

    async fn update_scopes(&mut self, scopes: Vec<String>) {
        DiscoveryAccess::update_scopes(&mut self.dpws_discovery, scopes.clone()).await;
        DiscoveryAccess::update_scopes(&mut self.proto_discovery, scopes).await;
    }

    async fn update_physical_address(&mut self, addresses: Vec<Uri>) {
        DiscoveryAccess::update_physical_address(&mut self.dpws_discovery, addresses.clone()).await;
        DiscoveryAccess::update_physical_address(&mut self.proto_discovery, addresses).await;
    }

    async fn update(&mut self, scopes: Vec<String>, addresses: Vec<Uri>) {
        DiscoveryAccess::update(&mut self.dpws_discovery, scopes.clone(), addresses.clone()).await;
        DiscoveryAccess::update(&mut self.proto_discovery, scopes, addresses).await;
    }
}

pub struct Provider<DISCOVERY, MDIB, HOSTING, PLUGIN, OPERATION, LOCALIZATION>
where
    DISCOVERY: DiscoveryAccess,
    MDIB: 'static + LocalMdibAccess + Sync + Send + Clone + LocalMdibAccessReadTransaction,
    HOSTING: HostingService,
    OPERATION: OperationInvocationReceiver<MDIB> + Send + Sync + 'static,
    LOCALIZATION: 'static + LocalizationStorage<LocalizedStorageText>,

    PLUGIN: SdcProviderPlugin,
{
    // common
    service_delegate: ServiceDelegate,
    discovery: DISCOVERY,
    mdib: MDIB,
    phantom_operation: PhantomData<OPERATION>,
    sdc_device_plugins: PLUGIN,

    // dpws
    _hosting_service: HOSTING,

    // proto
    primary_services: PrimaryProviderServices<OPERATION, MDIB, LOCALIZATION>,
}

#[async_trait::async_trait]
impl<DISCOVERY, MDIB, HOSTING, PLUGIN, OPERATION, LOCALIZATION> SdcProvider<MDIB, DISCOVERY>
    for Provider<DISCOVERY, MDIB, HOSTING, PLUGIN, OPERATION, LOCALIZATION>
where
    DISCOVERY: DiscoveryAccess + 'static + Clone + Send + Sync,
    MDIB: 'static + LocalMdibAccess + Sync + Send + Clone + LocalMdibAccessReadTransaction,
    HOSTING: HostingService + Send + Sync,
    OPERATION: OperationInvocationReceiver<MDIB> + Send + Sync + 'static,
    LOCALIZATION: 'static + LocalizationStorage<LocalizedStorageText> + Send + Sync,
    PLUGIN: SdcProviderPlugin + Send + Sync,
{
    async fn local_mdib(&self) -> MDIB {
        self.mdib.clone()
    }

    async fn discovery_access(&self) -> DISCOVERY {
        self.discovery.clone()
    }
}

#[async_trait::async_trait]
impl<DISCOVERY, MDIB, HOSTING, PLUGIN, OPERATION, LOCALIZATION> Service
    for Provider<DISCOVERY, MDIB, HOSTING, PLUGIN, OPERATION, LOCALIZATION>
where
    DISCOVERY: DiscoveryAccess + 'static + Clone + Send + Sync,
    MDIB: 'static + LocalMdibAccess + Sync + Send + Clone + LocalMdibAccessReadTransaction,
    HOSTING: HostingService + Send + Sync,
    OPERATION: OperationInvocationReceiver<MDIB> + Send + Sync + 'static + Clone,
    LOCALIZATION: 'static + LocalizationStorage<LocalizedStorageText> + Send + Sync,
    PLUGIN: SdcProviderPlugin + Send + Sync,
{
    async fn start(&mut self) -> Result<(), ServiceError> {
        let device_context = SdcDeviceContext {
            mdib_access: self.mdib.clone(),
            discovery_access: self.discovery.clone(),
        };

        self.service_delegate.start().await?;

        self.sdc_device_plugins
            .before_startup(&device_context)
            .await?;

        self.primary_services.start().await?;

        self.service_delegate.started().await?;

        self.sdc_device_plugins
            .after_startup(&device_context)
            .await?;

        Ok(())
    }

    async fn stop(&mut self) -> Result<(), ServiceError> {
        let device_context = SdcDeviceContext {
            mdib_access: self.mdib.clone(),
            discovery_access: self.discovery.clone(),
        };

        self.service_delegate.stop().await?;

        self.sdc_device_plugins
            .before_shutdown(&device_context)
            .await?;

        self.primary_services.stop().await?;

        self.service_delegate.stopped().await?;

        self.sdc_device_plugins
            .after_shutdown(&device_context)
            .await?;

        Ok(())
    }

    fn is_running(&self) -> bool {
        self.service_delegate.is_running()
    }

    fn state(&self) -> &ServiceState {
        self.service_delegate.state()
    }
}

pub type DefaultDpwsDiscoveryProvider<EHF> = DpwsDiscoveryProviderImpl<
    UdpBindingImpl,
    DpwsDiscoveryProcessorImpl,
    DpwsDiscoveryProxyProviderImpl<SoapClientImpl<ReqwestClientAccess, EHF>>,
>;
pub type DefaultProtoDiscoveryProvider = DiscoveryProviderImpl<UdpBindingImpl>;
pub type DefaultDpwsDiscoveryProcessor = DpwsDiscoveryProcessorImpl;
pub type DefaultDualStackDiscovery<EHF> = DualStackDiscovery<
    DefaultDpwsDiscoveryProvider<EHF>,
    DefaultProtoDiscoveryProvider,
    DefaultDpwsDiscoveryProcessor,
>;

#[derive(Debug, Error)]
pub enum ProviderError {
    #[error(transparent)]
    HostingServiceError(#[from] HostingServiceError),
    #[error(transparent)]
    ReqwestClientError(#[from] ReqwestClientError),
    #[error(transparent)]
    ServerError(#[from] ServerError),
    #[error(transparent)]
    RegistryError(#[from] RegistryError),
    #[error(transparent)]
    UdpBindingError(#[from] UdpBindingError),
    #[error(transparent)]
    DiscoveryError(#[from] DiscoveryError),
}

#[derive(Debug)]
pub struct EndpointReferenceAddress(pub String);

#[derive(Debug)]
pub struct ServerPrefix(pub String);

impl From<&str> for EndpointReferenceAddress {
    fn from(value: &str) -> Self {
        Self(value.to_string())
    }
}

impl From<String> for EndpointReferenceAddress {
    fn from(value: String) -> Self {
        Self(value)
    }
}

impl Default for ServerPrefix {
    fn default() -> Self {
        Self(Uuid::new_v4().to_string())
    }
}

#[derive(Debug, TypedBuilder)]
pub struct DualStackProviderConfig<Dpws, DpwsProcessor, Proto, Mdib, Plugin, Ehf, Operation>
where
    Dpws: DpwsDiscoveryProvider + DiscoveryAccess + 'static + Debug + Clone,
    DpwsProcessor: DpwsDiscoveryProcessor + 'static + Debug + Clone,
    Proto: DiscoveryProvider + DiscoveryAccess + 'static + Debug + Clone,
    Mdib: LocalMdibAccess + 'static + Sync + Send + Clone + LocalMdibAccessReadTransaction,
    Plugin: SdcProviderPlugin + 'static,
    Ehf: ExtensionHandlerFactory + Send + Sync,
    Operation: OperationInvocationReceiver<Mdib> + Send + Sync + 'static + Clone,
{
    #[builder(setter(into))]
    endpoint_reference_address: EndpointReferenceAddress,
    network_interface: IpAddr,

    #[builder(default = 0)]
    dpws_port: u16,
    #[builder(default = 0)]
    protosdc_port: u16,

    discovery: DualStackDiscovery<Dpws, DpwsProcessor, Proto>,
    mdib: Mdib,
    plugin: Plugin,
    crypto: Option<CryptoConfig>,
    operation: Operation,
    #[builder(default)]
    localization: Option<LocalizationStorageImpl>,

    // dpws-specific
    #[builder(default)]
    server_prefix: ServerPrefix,

    extension_handler: Ehf,
}
impl<Dpws, DpwsProcessor, Proto, Mdib, Plugin, Ehf, Operation>
    DualStackProviderConfig<Dpws, DpwsProcessor, Proto, Mdib, Plugin, Ehf, Operation>
where
    Dpws: DpwsDiscoveryProvider + DiscoveryAccess + 'static + Debug + Send + Sync + Clone,
    DpwsProcessor: DpwsDiscoveryProcessor + 'static + Send + Sync + Debug + Clone,
    Proto: DiscoveryProvider + DiscoveryAccess + 'static + Debug + Send + Sync + Clone,
    Mdib: LocalMdibAccess + 'static + Sync + Send + Clone + LocalMdibAccessReadTransaction,
    Plugin: SdcProviderPlugin + 'static + Send + Sync,
    Ehf: ExtensionHandlerFactory + 'static + Send + Sync,
    Operation: OperationInvocationReceiver<Mdib> + Send + Sync + 'static + Clone,
{
    pub async fn try_into_provider(
        mut self,
    ) -> Result<
        Provider<
            DualStackDiscovery<Dpws, DpwsProcessor, Proto>,
            Mdib,
            HostingServiceImpl,
            Plugin,
            Operation,
            LocalizationStorageImpl,
        >,
        ProviderError,
    > {
        // setup dpws
        let device_settings = DeviceSettings::builder()
            .network_interface(SocketAddr::from((self.network_interface, self.dpws_port)))
            .crypto(self.crypto.clone())
            .endpoint_reference(
                WsaEndpointReference::builder()
                    .address(self.endpoint_reference_address.0.clone())
                    .build(),
            )
            .types(HashSet::from_iter(
                vec![MDPWS_MEDICAL_DEVICE_TYPE.into(), DPWS_DEVICE_TYPE.into()].into_iter(),
            ))
            .server_prefix(self.server_prefix.0.clone())
            .extension_handler_factory(self.extension_handler)
            .build();

        let registry = AxumRegistry::new(self.crypto.clone());
        let http_client = ReqwestHttpClient::new(self.crypto.clone())?;

        let device_server = registry
            .access()
            .init_server(device_settings.network_interface)
            .await?;

        let sco_controller = ScoController::default();

        let high_hosted = Box::new(
            HighPriorityServices::new(
                self.mdib.clone(),
                self.operation.clone(),
                sco_controller.clone(),
                registry.access(),
                device_server.clone(),
                http_client.get_access(),
                &device_settings,
            )
            .await?,
        );

        let hosting = HostingServiceImpl::new(
            &device_settings,
            vec![high_hosted],
            registry.access(),
            self.discovery.dpws_processor.clone(),
        )
        .await
        .expect("Fixme");

        // update discovery info after starting the hosted service

        let x_addrs = hosting
            .x_addrs()
            .await
            .iter()
            .cloned()
            .map(|it| it.into())
            .collect();
        info!("Setting XAddrs to {:?}", x_addrs);
        DpwsDiscoveryProvider::update_physical_address(&mut self.discovery.dpws_discovery, x_addrs)
            .await;

        // proto
        let proto_socket_addr = SocketAddr::from((self.network_interface, self.protosdc_port));
        let proto_server_config = ServerConfig {
            crypto_config: self.crypto.clone(),
            server_address: proto_socket_addr,
        };

        let proto_addr = vec![match self.crypto.is_some() {
            true => socket_address_to_uri_https(&proto_socket_addr),
            false => socket_address_to_uri_http(&proto_socket_addr),
        }];
        DiscoveryProvider::update_physical_address(&mut self.discovery.proto_discovery, proto_addr)
            .await;

        let metadata = DeviceMetadata::default();

        let primary_config = PrimaryProviderServicesConfig::builder()
            .mdib_access(self.mdib.clone())
            .endpoint(self.discovery.proto_endpoint.clone())
            .endpoint_metadata(metadata.into())
            .services(vec![])
            .server_config(proto_server_config)
            .sco_controller(sco_controller)
            .operation_invocation_receiver(self.operation.clone())
            .localization_storage(self.localization)
            .build();

        let primary = PrimaryProviderServices::new(primary_config).await;

        Ok(Provider {
            service_delegate: Default::default(),
            discovery: self.discovery,
            mdib: self.mdib,
            phantom_operation: Default::default(),
            sdc_device_plugins: self.plugin,
            _hosting_service: hosting,
            primary_services: primary,
        })
    }
}

#[cfg(test)]
mod test {
    use crate::provider::dual_stack_provider::{DualStackDiscovery, DualStackProviderConfig};
    use crate::provider::sdc_provider::SdcProviderPlugin;
    use biceps::common::mdib_version::MdibVersionBuilder;
    use biceps::common::storage::mdib_storage::MdibStorageImpl;
    use biceps::provider::access::local_mdib_access::LocalMdibAccessImpl;
    use dpws::common::extension_handling::ReplacementExtensionHandlerFactory;
    use dpws::discovery::common::Endpoint;
    use dpws::discovery::provider::discovery_provider::{
        DpwsDiscoveryProcessorImpl, DpwsDiscoveryProviderImpl,
    };
    use dpws::discovery::provider::discovery_proxy_provider::DummyDpwsDiscoveryProxyProviderImpl;
    use dpws::xml::messages::addressing::{WsaAttributedURIType, WsaEndpointReference};
    use lazy_static::lazy_static;
    use network::mock_udp_binding::MockUdpBinding;
    use protosdc::discovery::provider::discovery_provider::DiscoveryProviderImpl;
    use protosdc::provider::sco::DummyOperationInvocationReceiver;
    use protosdc_proto::discovery::Endpoint as ProtoEndpoint;
    use std::net::Ipv4Addr;
    use tokio::sync::broadcast;

    use common::test_util::init_logging;
    use common::Service;
    use protosdc::provider::common::EndpointData;

    const PROVIDER_EPR: &str = "urn:uuid:abc:def";

    lazy_static! {
        pub(crate) static ref MOCK_DPWS_ENDPOINT: Endpoint = Endpoint {
            scopes: vec![],
            types: vec![],
            endpoint_reference: WsaEndpointReference {
                attributes: Default::default(),
                address: WsaAttributedURIType {
                    attributes: Default::default(),
                    value: PROVIDER_EPR.to_string(),
                },
                reference_parameters: None,
                metadata: None,
                children: vec![],
            },
            x_addrs: vec![],
            metadata_version: 0,
        };
        pub(crate) static ref PROTO_ENDPOINT: ProtoEndpoint = ProtoEndpoint {
            endpoint_identifier: PROVIDER_EPR.to_string(),
            scope: vec![],
            physical_address: vec![],
        };
    }

    #[derive(Debug)]
    struct DummyPlugin {}
    impl SdcProviderPlugin for DummyPlugin {}

    #[tokio::test]
    async fn test_builder() -> anyhow::Result<()> {
        init_logging();

        let (tx, _rx) = broadcast::channel(10);

        let dpws_processor = DpwsDiscoveryProcessorImpl::new(MOCK_DPWS_ENDPOINT.clone(), None);
        let disco: Option<DummyDpwsDiscoveryProxyProviderImpl> = None;
        let dpws_discovery = DpwsDiscoveryProviderImpl::new(
            MockUdpBinding::new_with_channel(tx.clone()),
            dpws_processor.clone(),
            disco,
        );

        let proto_endpoint_data: EndpointData = PROTO_ENDPOINT.clone().into();

        let proto_disco = DiscoveryProviderImpl::new(
            MockUdpBinding::new_with_channel(tx.clone()),
            None,
            proto_endpoint_data.clone(),
            None,
        )
        .await?;

        let initial_version = MdibVersionBuilder::default().seq("ab:cd").create();

        let mdib = LocalMdibAccessImpl::<MdibStorageImpl, _, _>::new(initial_version);

        let config = DualStackProviderConfig::builder()
            .endpoint_reference_address("abc:def")
            .discovery(DualStackDiscovery::new(
                dpws_discovery,
                dpws_processor,
                proto_disco,
                proto_endpoint_data,
            ))
            .mdib(mdib)
            .plugin(DummyPlugin {})
            .extension_handler(ReplacementExtensionHandlerFactory)
            .network_interface(Ipv4Addr::new(127, 0, 0, 1).into())
            .crypto(None)
            .operation(DummyOperationInvocationReceiver {})
            .build();

        println!("{:?}", config);

        let mut provider = config.try_into_provider().await?;

        provider.start().await?;

        Ok(())
    }
}
