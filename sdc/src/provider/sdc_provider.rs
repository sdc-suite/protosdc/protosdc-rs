use async_trait::async_trait;
use biceps::provider::access::local_mdib_access::{
    LocalMdibAccess, LocalMdibAccessReadTransaction,
};
use dpws::discovery::provider::discovery_provider::DpwsDiscoveryProvider;
use dpws::discovery::provider::discovery_proxy_provider::DpwsDiscoveryProxyProvider;
use dpws::provider::sdc_provider::DeviceDpwsDiscoveryProvider;
use dpws::soap::dpws::device::hosting_service::HostingService;
use dpws::xml::messages::common::HttpUri;
use network::udp_binding::UdpBinding;
use protosdc::discovery::provider::discovery_provider::DiscoveryProvider as ProtoDiscoveryProvider;
use protosdc::provider::device::SdcDevice as ProtoSdcDevice;
use protosdc::provider::localization_storage::{LocalizationStorage, LocalizedStorageText};
use protosdc::provider::plugins::{ProtoSdcDeviceContext, ProtoSdcProviderPlugin};
use protosdc::provider::sco::OperationInvocationReceiver;
use protosdc_proto::common::Uri;
use std::fmt::Display;

pub trait DeviceMdibType:
    LocalMdibAccess + Sync + Send + Clone + LocalMdibAccessReadTransaction + 'static
{
}

impl<T> DeviceMdibType for T where
    T: LocalMdibAccess + Sync + Send + Clone + LocalMdibAccessReadTransaction + 'static
{
}

pub trait DeviceDiscoveryAccess: DiscoveryAccess + Send + Sync + 'static {}

impl<T> DeviceDiscoveryAccess for T where T: DiscoveryAccess + Send + Sync + 'static {}

#[derive(Clone)]
struct DpwsDiscoveryAccessHolder<T>
where
    T: DpwsDiscoveryProvider + Send + Sync + 'static + Clone,
{
    discovery_access: T,
}

#[derive(Clone)]
struct ProtoDiscoveryAccessHolder<T>
where
    T: protosdc::discovery::provider::discovery_provider::DiscoveryProvider
        + Send
        + Sync
        + 'static
        + Clone,
{
    discovery_access: T,
}

#[async_trait]
impl<T> DiscoveryAccess for ProtoDiscoveryAccessHolder<T>
where
    T: protosdc::discovery::provider::discovery_provider::DiscoveryProvider
        + Send
        + Sync
        + 'static
        + Clone,
{
    async fn endpoint_reference_address(&self) -> String {
        self.discovery_access.endpoint().await.endpoint_identifier
    }

    async fn update_scopes(&mut self, scopes: Vec<String>) {
        self.discovery_access
            .update_scopes(scopes.into_iter().map(|it| Uri { value: it }).collect())
            .await
    }

    async fn update_physical_address(&mut self, addresses: Vec<http::Uri>) {
        self.discovery_access
            .update_physical_address(
                addresses
                    .into_iter()
                    .map(|it| Uri {
                        value: it.to_string(),
                    })
                    .collect(),
            )
            .await
    }

    async fn update(&mut self, scopes: Vec<String>, addresses: Vec<http::Uri>) {
        self.discovery_access
            .update(
                scopes.into_iter().map(|it| Uri { value: it }).collect(),
                addresses
                    .into_iter()
                    .map(|it| Uri {
                        value: it.to_string(),
                    })
                    .collect(),
            )
            .await
    }
}

#[async_trait]
impl<T> DiscoveryAccess for DpwsDiscoveryAccessHolder<T>
where
    T: DpwsDiscoveryProvider + Send + Sync + 'static + Clone,
{
    async fn endpoint_reference_address(&self) -> String {
        self.discovery_access
            .endpoint_reference()
            .await
            .address
            .value
    }

    async fn update_scopes(&mut self, scopes: Vec<String>) {
        self.discovery_access.update_scopes(scopes).await
    }

    async fn update_physical_address(&mut self, addresses: Vec<http::Uri>) {
        self.discovery_access
            .update_physical_address(addresses.into_iter().map(HttpUri::from).collect())
            .await
    }

    async fn update(&mut self, scopes: Vec<String>, addresses: Vec<http::Uri>) {
        self.discovery_access
            .update(scopes, addresses.into_iter().map(HttpUri::from).collect())
            .await
    }
}

#[async_trait]
pub trait DiscoveryAccess {
    async fn endpoint_reference_address(&self) -> String;

    /// Updates the scopes of the provider.
    ///
    /// # Arguments
    ///
    /// * `scopes`: new scopes of the provider
    async fn update_scopes(&mut self, scopes: Vec<String>);

    /// Updates the physical addresses of the provider, triggers hello message if possible.
    ///
    /// # Arguments
    ///
    /// * `addresses`: new addresses of the provider
    async fn update_physical_address(&mut self, addresses: Vec<http::Uri>);

    /// Updates the scopes and physical addresses of the provider, triggers hello message if possible.
    ///
    /// # Arguments
    ///
    /// * `scopes`: new scopes of the provider
    /// * `addresses`: new addresses of the provider
    ///
    /// returns: bool true if an update needs to be sent out, false otherwise
    async fn update(&mut self, scopes: Vec<String>, addresses: Vec<http::Uri>);
}

// impl DiscoveryAccessMarker for

#[async_trait]
impl<U> DiscoveryAccess
    for protosdc::discovery::provider::discovery_provider::DiscoveryProviderImpl<U>
where
    U: UdpBinding + Send + Sync + Display,
{
    async fn endpoint_reference_address(&self) -> String {
        self.endpoint().await.endpoint_identifier
    }

    async fn update_scopes(&mut self, scopes: Vec<String>) {
        ProtoDiscoveryProvider::update_scopes(
            self,
            scopes.into_iter().map(|it| Uri { value: it }).collect(),
        )
        .await
    }

    async fn update_physical_address(&mut self, addresses: Vec<http::Uri>) {
        ProtoDiscoveryProvider::update_physical_address(
            self,
            addresses
                .into_iter()
                .map(|it| Uri {
                    value: it.to_string(),
                })
                .collect(),
        )
        .await
    }

    async fn update(&mut self, scopes: Vec<String>, addresses: Vec<http::Uri>) {
        ProtoDiscoveryProvider::update(
            self,
            scopes.into_iter().map(|it| Uri { value: it }).collect(),
            addresses
                .into_iter()
                .map(|it| Uri {
                    value: it.to_string(),
                })
                .collect(),
        )
        .await
    }
}

#[async_trait]
impl<U, D, DISCO> DiscoveryAccess
    for dpws::discovery::provider::discovery_provider::DpwsDiscoveryProviderImpl<U, D, DISCO>
where
    U: UdpBinding + Send + Sync + Display + Clone,
    D: dpws::discovery::provider::discovery_provider::DpwsDiscoveryProcessor + Send + Sync + Clone,
    DISCO: DpwsDiscoveryProxyProvider + Send + Sync + Clone,
{
    async fn endpoint_reference_address(&self) -> String {
        self.endpoint_reference().await.address.value
    }

    async fn update_scopes(&mut self, scopes: Vec<String>) {
        DpwsDiscoveryProvider::update_scopes(self, scopes).await
    }

    async fn update_physical_address(&mut self, addresses: Vec<http::Uri>) {
        DpwsDiscoveryProvider::update_physical_address(
            self,
            addresses.into_iter().map(HttpUri::from).collect(),
        )
        .await
    }

    async fn update(&mut self, scopes: Vec<String>, addresses: Vec<http::Uri>) {
        DpwsDiscoveryProvider::update(
            self,
            scopes,
            addresses.into_iter().map(HttpUri::from).collect(),
        )
        .await
    }
}

pub struct SdcDeviceContext<V, T>
where
    V: DeviceMdibType,
    T: DeviceDiscoveryAccess,
{
    pub mdib_access: V,
    pub discovery_access: T,
}

#[async_trait]
pub trait SdcProviderPlugin {
    async fn before_startup<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess + Clone,
    {
        Ok(())
    }

    async fn after_startup<M, D>(&mut self, _context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess + Clone,
    {
        Ok(())
    }

    async fn before_shutdown<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess + Clone,
    {
        Ok(())
    }

    async fn after_shutdown<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess + Clone,
    {
        Ok(())
    }
}

#[async_trait]
pub trait SdcProvider<MDIB, D>
where
    MDIB: LocalMdibAccess,
    D: DeviceDiscoveryAccess,
{
    async fn local_mdib(&self) -> MDIB;

    async fn discovery_access(&self) -> D;
}

#[async_trait]
impl<MDIB, T, OIR, HOSTING, PLUGIN> SdcProvider<MDIB, T>
    for dpws::provider::sdc_provider::SdcDevice<T, MDIB, OIR, HOSTING, PLUGIN>
where
    MDIB: 'static + LocalMdibAccess + Sync + Send + Clone + LocalMdibAccessReadTransaction,
    T: DeviceDpwsDiscoveryProvider + DeviceDiscoveryAccess + Clone,
    OIR: OperationInvocationReceiver<MDIB> + Send + Sync + 'static,
    HOSTING: HostingService + Send + Sync,
    PLUGIN: dpws::provider::sdc_provider_plugin::SdcProviderPlugin + Send + Sync,
{
    async fn local_mdib(&self) -> MDIB {
        self.local_mdib.clone()
    }

    async fn discovery_access(&self) -> T {
        self.discovery_provider.clone()
    }
}

#[async_trait]
impl<MDIB, T, OIR, LOC, PLUGIN> SdcProvider<MDIB, T> for ProtoSdcDevice<T, OIR, MDIB, LOC, PLUGIN>
where
    MDIB: 'static + LocalMdibAccess + Sync + Send + Clone + LocalMdibAccessReadTransaction,
    T: ProtoDiscoveryProvider + DeviceDiscoveryAccess + Clone,
    OIR: OperationInvocationReceiver<MDIB> + Send + Sync + 'static,
    LOC: 'static + LocalizationStorage<LocalizedStorageText> + Sync,
    PLUGIN: ProtoSdcProviderPlugin + Send + Sync,
{
    async fn local_mdib(&self) -> MDIB {
        self.local_mdib.clone()
    }

    async fn discovery_access(&self) -> T {
        self.discovery_provider.clone()
    }
}

pub struct DpwsSdcProviderPluginWrapper<T>
where
    T: SdcProviderPlugin,
{
    plugin: T,
}

impl<T> From<T> for DpwsSdcProviderPluginWrapper<T>
where
    T: SdcProviderPlugin,
{
    fn from(value: T) -> Self {
        Self { plugin: value }
    }
}

#[async_trait]
impl<T> dpws::provider::sdc_provider_plugin::SdcProviderPlugin for DpwsSdcProviderPluginWrapper<T>
where
    T: SdcProviderPlugin + Send + Sync,
{
    async fn before_startup<M, D>(
        &mut self,
        context: &dpws::provider::sdc_provider::SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: dpws::provider::sdc_provider::DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        let ctx = SdcDeviceContext {
            mdib_access: context.mdib_access.clone(),
            discovery_access: DpwsDiscoveryAccessHolder {
                discovery_access: context.discovery_provider.clone(),
            },
        };
        self.plugin.before_startup(&ctx).await
    }

    async fn after_startup<M, D>(
        &mut self,
        context: &dpws::provider::sdc_provider::SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: dpws::provider::sdc_provider::DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        let ctx = SdcDeviceContext {
            mdib_access: context.mdib_access.clone(),
            discovery_access: DpwsDiscoveryAccessHolder {
                discovery_access: context.discovery_provider.clone(),
            },
        };
        self.plugin.after_startup(&ctx).await
    }

    async fn before_shutdown<M, D>(
        &mut self,
        context: &dpws::provider::sdc_provider::SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: dpws::provider::sdc_provider::DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        let ctx = SdcDeviceContext {
            mdib_access: context.mdib_access.clone(),
            discovery_access: DpwsDiscoveryAccessHolder {
                discovery_access: context.discovery_provider.clone(),
            },
        };
        self.plugin.before_shutdown(&ctx).await
    }

    async fn after_shutdown<M, D>(
        &mut self,
        context: &dpws::provider::sdc_provider::SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: dpws::provider::sdc_provider::DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        let ctx = SdcDeviceContext {
            mdib_access: context.mdib_access.clone(),
            discovery_access: DpwsDiscoveryAccessHolder {
                discovery_access: context.discovery_provider.clone(),
            },
        };
        self.plugin.after_shutdown(&ctx).await
    }
}

pub struct ProtoSdcProviderPluginWrapper<T>
where
    T: SdcProviderPlugin,
{
    plugin: T,
}

impl<T> From<T> for ProtoSdcProviderPluginWrapper<T>
where
    T: SdcProviderPlugin,
{
    fn from(value: T) -> Self {
        Self { plugin: value }
    }
}

#[async_trait]
impl<T> ProtoSdcProviderPlugin for ProtoSdcProviderPluginWrapper<T>
where
    T: SdcProviderPlugin + Send + Sync,
{
    async fn before_startup<M, D>(
        &mut self,
        context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: protosdc::provider::plugins::DeviceMdibType,
        D: protosdc::provider::plugins::DeviceDiscoveryProvider + Clone,
    {
        let ctx = SdcDeviceContext {
            mdib_access: context.mdib_access.clone(),
            discovery_access: ProtoDiscoveryAccessHolder {
                discovery_access: context.discovery_access.clone(),
            },
        };
        self.plugin.before_startup(&ctx).await
    }

    async fn after_startup<M, D>(
        &mut self,
        context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: protosdc::provider::plugins::DeviceMdibType,
        D: protosdc::provider::plugins::DeviceDiscoveryProvider + Clone,
    {
        let ctx = SdcDeviceContext {
            mdib_access: context.mdib_access.clone(),
            discovery_access: ProtoDiscoveryAccessHolder {
                discovery_access: context.discovery_access.clone(),
            },
        };
        self.plugin.after_startup(&ctx).await
    }

    async fn before_shutdown<M, D>(
        &mut self,
        context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: protosdc::provider::plugins::DeviceMdibType,
        D: protosdc::provider::plugins::DeviceDiscoveryProvider + Clone,
    {
        let ctx = SdcDeviceContext {
            mdib_access: context.mdib_access.clone(),
            discovery_access: ProtoDiscoveryAccessHolder {
                discovery_access: context.discovery_access.clone(),
            },
        };
        self.plugin.before_shutdown(&ctx).await
    }

    async fn after_shutdown<M, D>(
        &mut self,
        context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: protosdc::provider::plugins::DeviceMdibType,
        D: protosdc::provider::plugins::DeviceDiscoveryProvider + Clone,
    {
        let ctx = SdcDeviceContext {
            mdib_access: context.mdib_access.clone(),
            discovery_access: ProtoDiscoveryAccessHolder {
                discovery_access: context.discovery_access.clone(),
            },
        };
        self.plugin.after_shutdown(&ctx).await
    }
}
