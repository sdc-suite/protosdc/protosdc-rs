use async_trait::async_trait;
use biceps::common::biceps_util::MetricState;
use biceps::common::mdib_entity::{Entity, EntityBase};
use biceps::common::mdib_state_modifications::MdibStateModifications;
use biceps::entity_filter;
use chrono::Utc;
use itertools::Itertools;
use log::{debug, info, trace};
use protosdc_biceps::biceps::abstract_metric_value_mod::MetricQuality;
use protosdc_biceps::biceps::{
    generation_mode_mod, measurement_validity_mod, AbstractMetricValue, GenerationMode,
    MeasurementValidity, NumericMetricState, NumericMetricValue, Timestamp,
};
use protosdc_biceps::types::ProtoDecimal;
use sdc::provider::sdc_provider::{
    DeviceDiscoveryAccess, DeviceMdibType, SdcDeviceContext, SdcProviderPlugin,
};
use std::collections::HashSet;
use std::time::Duration;
use tokio::task::JoinHandle;

#[derive(Debug, Default)]
pub(crate) struct NumericMetricUpdater {
    update_task: Option<JoinHandle<()>>,
    exclude_mds: HashSet<String>,
}

impl NumericMetricUpdater {
    pub fn new(exclude_mds: HashSet<String>) -> Self {
        Self {
            update_task: None,
            exclude_mds,
        }
    }
}
#[async_trait]
impl SdcProviderPlugin for NumericMetricUpdater {
    async fn before_startup<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess,
    {
        info!("Starting NumericMetricUpdater");

        let mut interval = tokio::time::interval(Duration::from_secs(1));

        let mut task_mdib = context.mdib_access.clone();
        let excluded_mds = self.exclude_mds.clone();
        // determine which handles we're working on
        let applicable_entities = task_mdib
            .entities_by_type(entity_filter!(NumericMetric))
            .await;
        let applicable_states = applicable_entities
            .into_iter()
            .filter(|it| !excluded_mds.contains(&it.entity.mds()))
            .map(|it| match it.entity {
                Entity::NumericMetric(ent) => ent,
                _ => panic!("Your filter is broken."),
            })
            .collect_vec();

        let task = tokio::spawn(async move {
            let mut counter = 0;

            loop {
                let now = Utc::now();
                let now_ms = now.timestamp_millis() as u64;

                let state_updates = applicable_states
                    .iter()
                    .map(|it| NumericMetricState {
                        metric_value: Some(NumericMetricValue {
                            abstract_metric_value: AbstractMetricValue {
                                extension_element: None,
                                metric_quality: MetricQuality {
                                    extension_element: None,
                                    validity_attr: MeasurementValidity {
                                        enum_type: measurement_validity_mod::EnumType::Vld,
                                    },
                                    mode_attr: Some(GenerationMode {
                                        enum_type: generation_mode_mod::EnumType::Demo,
                                    }),
                                    qi_attr: None,
                                },
                                annotation: vec![],
                                start_time_attr: None,
                                stop_time_attr: None,
                                determination_time_attr: Some(Timestamp {
                                    unsigned_long: now_ms,
                                }),
                            },
                            value_attr: Some(ProtoDecimal::from(counter)),
                        }),
                        ..it.pair.state.clone()
                    })
                    .map(|it| it.into_abstract_metric_state_one_of())
                    .collect_vec();

                debug!("Updated states: {:?}", state_updates);

                let modifications = MdibStateModifications::Metric {
                    metric_states: state_updates,
                };

                task_mdib
                    .write_states(modifications)
                    .await
                    .expect("Could not write numeric metric state updates");

                trace!("Metric tick");
                counter += 1;
                interval.tick().await;
            }
        });

        self.update_task = Some(task);
        Ok(())
    }

    async fn before_shutdown<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess,
    {
        info!("Stopping NumericMetricUpdater");
        if let Some(task) = self.update_task.take() {
            task.abort()
        }
        Ok(())
    }
}
