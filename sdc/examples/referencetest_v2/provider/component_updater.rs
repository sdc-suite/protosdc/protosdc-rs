use async_trait::async_trait;
use biceps::common::biceps_util::{ComponentState, State};
use biceps::common::mdib_entity::{Entity, EntityBase};
use biceps::common::mdib_state_modifications::MdibStateModifications;
use log::info;
use std::collections::HashSet;
use std::time::Duration;
use tokio::task::JoinHandle;

use biceps::entity_filter;
use sdc::provider::sdc_provider::{
    DeviceDiscoveryAccess, DeviceMdibType, SdcDeviceContext, SdcProviderPlugin,
};

#[derive(Debug, Default)]
pub(crate) struct ComponentUpdater {
    update_task: Option<JoinHandle<()>>,
    exclude_mds: HashSet<String>,
}

impl ComponentUpdater {
    pub fn new(exclude_mds: HashSet<String>) -> Self {
        Self {
            update_task: None,
            exclude_mds,
        }
    }
}

#[async_trait]
impl SdcProviderPlugin for ComponentUpdater {
    async fn before_startup<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess,
    {
        info!("Starting ComponentUpdater");

        let mut interval = tokio::time::interval(Duration::from_secs(5));
        let mut task_mdib = context.mdib_access.clone();

        let clock_entity_to_update = task_mdib
            .entities_by_type(entity_filter!(Clock))
            .await
            .into_iter()
            .find(|it| !self.exclude_mds.contains(&it.entity.mds()))
            .expect("No clock found within allowed mds");

        let clock_to_update = match clock_entity_to_update.entity {
            Entity::Clock(ent) => ent.pair.state,
            _ => panic!("Your filter is broken."),
        }
        .into_abstract_component_state_one_of();

        let vmd_entity_to_update = task_mdib
            .entities_by_type(entity_filter!(Vmd))
            .await
            .into_iter()
            .find(|it| !self.exclude_mds.contains(&it.entity.mds()))
            .expect("No vmd found within allowed mds");

        let vmd_to_update = match vmd_entity_to_update.entity {
            Entity::Vmd(ent) => ent.pair.state,
            _ => panic!("Your filter is broken."),
        }
        .into_abstract_component_state_one_of();

        let task = tokio::spawn(async move {
            loop {
                info!(
                    "Updating components {} and {}",
                    clock_to_update.descriptor_handle(),
                    vmd_to_update.descriptor_handle()
                );

                let update = MdibStateModifications::Component {
                    component_states: vec![clock_to_update.clone(), vmd_to_update.clone()],
                };
                task_mdib
                    .write_states(update)
                    .await
                    .expect("Could not write component update");

                interval.tick().await;
            }
        });

        self.update_task = Some(task);
        Ok(())
    }

    async fn before_shutdown<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess,
    {
        if let Some(task) = self.update_task.take() {
            task.abort();
        }
        Ok(())
    }
}
