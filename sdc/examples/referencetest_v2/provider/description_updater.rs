use async_trait::async_trait;
use biceps::common::biceps_util::Descriptor;
use biceps::common::mdib_description_modification::MdibDescriptionModification;
use biceps::common::mdib_description_modifications::MdibDescriptionModifications;
use biceps::common::mdib_entity::Entity;
use biceps::common::mdib_pair::Pair;
use biceps::test_util::mdib_utils;
use log::info;
use protosdc_biceps::biceps::abstract_metric_value_mod::MetricQuality;
use protosdc_biceps::biceps::{
    generation_mode_mod, localized_text_width_mod, measurement_validity_mod,
    AbstractAlertDescriptor, AbstractDescriptor, AbstractMetricDescriptor, AbstractMetricValue,
    AlertConditionDescriptor, CauseInfo, CodeIdentifier, CodedValue, GenerationMode, LocalizedText,
    LocalizedTextContent, LocalizedTextWidth, MeasurementValidity, NumericMetricDescriptor,
    NumericMetricState, NumericMetricValue, RemedyInfo, SymbolicCodeName,
};
use protosdc_biceps::types::ProtoLanguage;
use rust_decimal::Decimal;
use sdc::provider::sdc_provider::{
    DeviceDiscoveryAccess, DeviceMdibType, SdcDeviceContext, SdcProviderPlugin,
};
use std::string::ToString;
use std::time::Duration;
use tokio::task::JoinHandle;

fn create_localized_text(text: String) -> LocalizedText {
    LocalizedText {
        localized_text_content: LocalizedTextContent { string: text },
        ref_attr: None,
        lang_attr: Some(ProtoLanguage {
            language: "tlh".to_string(),
        }), // klingon
        version_attr: None,
        text_width_attr: Some(LocalizedTextWidth {
            enum_type: localized_text_width_mod::EnumType::xxl,
        }),
    }
}

fn create_cause_info(text: String) -> CauseInfo {
    CauseInfo {
        extension_element: None,
        remedy_info: Some(RemedyInfo {
            extension_element: None,
            description: vec![create_localized_text(text.clone())],
        }),
        description: vec![create_localized_text(text)],
    }
}

fn create_ieee_code(code: String, ref_id: String) -> CodedValue {
    CodedValue {
        extension_element: None,
        coding_system_name: vec![],
        concept_description: vec![],
        translation: vec![],
        code_attr: CodeIdentifier { string: code },
        coding_system_attr: None,
        coding_system_version_attr: None,
        symbolic_code_name_attr: Some(SymbolicCodeName { string: ref_id }),
    }
}

#[derive(Debug, Default)]
pub struct DescriptionUpdater {
    update_task: Option<JoinHandle<()>>,
}

#[async_trait]
impl SdcProviderPlugin for DescriptionUpdater {
    async fn before_startup<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess,
    {
        info!("Starting DescriptionUpdater");

        let ml_per_min: CodedValue =
            create_ieee_code("265234".to_string(), "MDC_DIM_MILLI_L_PER_MIN".to_string());
        let ml_per_hour: CodedValue =
            create_ieee_code("265266".to_string(), "MDC_DIM_MILLI_L_PER_HR".to_string());

        let mut interval = tokio::time::interval(Duration::from_secs(9));
        let mut task_mdib = context.mdib_access.clone();

        let condition_entity = task_mdib
            .entity("alert_condition_0.vmd_0.mds_1")
            .await
            .expect("could not find required alert condition descriptor to update");

        let metric_entity = task_mdib
            .entity("numeric_metric_0.channel_0.vmd_0.mds_1")
            .await
            .expect("could not find required alert condition descriptor to update");

        let condition = match condition_entity.entity {
            Entity::AlertCondition(it) => it,
            _ => panic!("Provided entity was not an alert condition"),
        };

        let metric = match metric_entity.entity {
            Entity::NumericMetric(it) => it,
            _ => panic!("Provided entity was not a numeric metric"),
        };

        let condition_descriptor = condition.pair.descriptor.clone();
        let condition_code = condition_descriptor
            .abstract_alert_descriptor
            .abstract_descriptor
            .r#type
            .as_ref()
            .expect("no code present")
            .clone();

        let metric_descriptor = metric.pair.descriptor.clone();
        let metric_state = metric.pair.state.clone();

        let task = tokio::spawn(async move {
            let mut counter = 0;

            loop {
                let mut modification = MdibDescriptionModifications::default();

                // add alert condition update

                let current_condition_entity = task_mdib
                    .entity(&condition_descriptor.handle())
                    .await
                    .expect("condition state missing");
                let current_condition = match current_condition_entity.entity {
                    Entity::AlertCondition(it) => it,
                    _ => panic!("Provided entity was not an alert condition"),
                };
                let current_state = current_condition.pair.state;

                let updated_descriptor = {
                    AlertConditionDescriptor {
                        cause_info: vec![create_cause_info(format!(
                            "https://www.youtube.com/watch?v=VFckpH130QE {}",
                            counter
                        ))],
                        abstract_alert_descriptor: AbstractAlertDescriptor {
                            abstract_descriptor: AbstractDescriptor {
                                r#type: Some(CodedValue {
                                    concept_description: vec![create_localized_text(format!(
                                        "Hab SoSlI' Quch! {}",
                                        counter
                                    ))],
                                    ..condition_code.clone()
                                }),
                                ..condition_descriptor
                                    .abstract_alert_descriptor
                                    .abstract_descriptor
                                    .clone()
                            },
                        },
                        ..condition_descriptor.clone()
                    }
                };

                let updated_metric_descriptor = NumericMetricDescriptor {
                    abstract_metric_descriptor: AbstractMetricDescriptor {
                        unit: match counter % 2 {
                            0 => ml_per_min.clone(),
                            _ => ml_per_hour.clone(),
                        },
                        ..metric_descriptor.abstract_metric_descriptor.clone()
                    },
                    ..metric_descriptor.clone()
                };

                let updated_metric_state = NumericMetricState {
                    metric_value: Some(NumericMetricValue {
                        abstract_metric_value: AbstractMetricValue {
                            extension_element: None,
                            metric_quality: MetricQuality {
                                extension_element: None,
                                validity_attr: MeasurementValidity {
                                    enum_type: measurement_validity_mod::EnumType::Vld,
                                },
                                mode_attr: Some(GenerationMode {
                                    enum_type: generation_mode_mod::EnumType::Demo,
                                }),
                                qi_attr: None,
                            },
                            annotation: vec![],
                            start_time_attr: None,
                            stop_time_attr: None,
                            determination_time_attr: None,
                        },
                        value_attr: Some(
                            match counter % 2 {
                                0 => Decimal::from(20),
                                _ => Decimal::from(20 * 60),
                            }
                            .into(),
                        ),
                    }),
                    ..metric_state.clone()
                };

                modification
                    .add(MdibDescriptionModification::Update {
                        pair: Pair::from((updated_descriptor, current_state)),
                    })
                    .expect("Modification could not be added");

                modification
                    .add(MdibDescriptionModification::Update {
                        pair: Pair::from((updated_metric_descriptor, updated_metric_state)),
                    })
                    .expect("Modification could not be added");

                let vmd_handle = format!("added_vmd_{}", counter);
                let channel_handle = format!("added_channel_{}", counter);
                let metric_handle = format!("added_metric_{}", counter);

                modification
                    .add(MdibDescriptionModification::Insert {
                        pair: Pair::from((
                            mdib_utils::create_minimal_vmd_descriptor(&vmd_handle),
                            mdib_utils::create_minimal_vmd_state(&vmd_handle),
                        )),
                        parent: Some("mds_1".to_string()),
                    })
                    .expect("Modification could not be added");

                modification
                    .add(MdibDescriptionModification::Insert {
                        pair: Pair::from((
                            mdib_utils::create_minimal_channel_descriptor(&channel_handle),
                            mdib_utils::create_minimal_channel_state(&channel_handle),
                        )),
                        parent: Some(vmd_handle.to_string()),
                    })
                    .expect("Modification could not be added");

                modification
                    .add(MdibDescriptionModification::Insert {
                        pair: Pair::from((
                            mdib_utils::create_minimal_numeric_metric_descriptor(&metric_handle),
                            mdib_utils::create_minimal_numeric_metric_state(&metric_handle),
                        )),
                        parent: Some(channel_handle.to_string()),
                    })
                    .expect("Modification could not be added");

                task_mdib
                    .write_description(modification)
                    .await
                    .expect("Could not write updated description");

                tokio::time::sleep(Duration::from_secs(1)).await;

                let mut second_modification = MdibDescriptionModifications::default();

                second_modification
                    .add_all(vec![
                        MdibDescriptionModification::Delete {
                            handle: metric_handle.to_string(),
                        },
                        MdibDescriptionModification::Delete {
                            handle: channel_handle.to_string(),
                        },
                        MdibDescriptionModification::Delete {
                            handle: vmd_handle.to_string(),
                        },
                    ])
                    .expect("Modification could not be added");

                task_mdib
                    .write_description(second_modification)
                    .await
                    .expect("Could not write delete to description");

                counter += 1;
                interval.tick().await;
            }
        });

        self.update_task = Some(task);
        Ok(())
    }

    async fn before_shutdown<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess,
    {
        info!("Stopping DescriptionUpdater");
        if let Some(task) = self.update_task.take() {
            task.abort();
        }
        Ok(())
    }
}
