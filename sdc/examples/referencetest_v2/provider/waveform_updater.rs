use async_trait::async_trait;
use biceps::common::mdib_entity::Entity;
use biceps::common::mdib_state_modifications::MdibStateModifications;
use biceps::entity_filter;
use chrono::Utc;
use itertools::Itertools;
use log::{debug, info, trace};
use protosdc_biceps::biceps::abstract_metric_value_mod::MetricQuality;
use protosdc_biceps::biceps::{
    generation_mode_mod, measurement_validity_mod, AbstractMetricValue, GenerationMode,
    MeasurementValidity, RealTimeSampleArrayMetricState, RealTimeValueType, SampleArrayValue,
    Timestamp,
};
use protosdc_biceps::types::ProtoDecimal;
use rust_decimal::Decimal;
use sdc::provider::sdc_provider::{
    DeviceDiscoveryAccess, DeviceMdibType, SdcDeviceContext, SdcProviderPlugin,
};
use std::f64::consts::PI;
use std::time::Duration;
use tokio::task::JoinHandle;

#[derive(Debug, Default)]
pub(crate) struct WaveformUpdater {
    update_task: Option<JoinHandle<()>>,
}

#[async_trait]
impl SdcProviderPlugin for WaveformUpdater {
    async fn before_startup<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess,
    {
        info!("Starting WaveformUpdater");

        let mut interval = tokio::time::interval(Duration::from_millis(100));

        let mut task_mdib = context.mdib_access.clone();
        // determine which handles we're working on
        let applicable_entities = task_mdib
            .entities_by_type(entity_filter!(RealTimeSampleArrayMetric))
            .await;
        let applicable_states = applicable_entities
            .iter()
            .map(|it| match &it.entity {
                Entity::RealTimeSampleArrayMetric(ent) => ent.pair.state.clone(),
                _ => panic!("Your filter is broken."),
            })
            .collect_vec();

        let min_value = 0f64;
        let max_value = 50f64;
        let sample_capacity = 100;

        // sine wave
        let delta = 2f64 * PI / (sample_capacity as f64);

        let actual_values = (0..sample_capacity)
            .map(|n| {
                (((n as f64) * delta).sin() + 1f64) / 2.0f64 * (max_value - min_value) + min_value
            })
            .map(|it| {
                Decimal::try_from(it).unwrap_or_else(|_| panic!("Could not load {} as decimal", it))
            })
            .map(ProtoDecimal::from)
            .collect_vec();

        let target_sample_array = SampleArrayValue {
            abstract_metric_value: AbstractMetricValue {
                extension_element: None,
                metric_quality: MetricQuality {
                    extension_element: None,
                    validity_attr: MeasurementValidity {
                        enum_type: measurement_validity_mod::EnumType::Vld,
                    },
                    mode_attr: Some(GenerationMode {
                        enum_type: generation_mode_mod::EnumType::Demo,
                    }),
                    qi_attr: None,
                },
                annotation: vec![],
                start_time_attr: None,
                stop_time_attr: None,
                determination_time_attr: None,
            },
            apply_annotation: vec![],
            samples_attr: Some(RealTimeValueType {
                decimal: actual_values,
            }),
        };

        let task = tokio::spawn(async move {
            loop {
                let now = Utc::now();
                let now_ms = now.timestamp_millis() as u64;

                let state_updates = applicable_states
                    .iter()
                    .map(|it| RealTimeSampleArrayMetricState {
                        metric_value: Some(SampleArrayValue {
                            abstract_metric_value: AbstractMetricValue {
                                determination_time_attr: Some(Timestamp {
                                    unsigned_long: now_ms,
                                }),
                                ..target_sample_array.abstract_metric_value.clone()
                            },
                            ..target_sample_array.clone()
                        }),
                        ..it.clone()
                    })
                    .collect_vec();

                debug!("Updated states: {:?}", state_updates);

                let modifications = MdibStateModifications::Waveform {
                    waveform_states: state_updates,
                };

                task_mdib
                    .write_states(modifications)
                    .await
                    .expect("Could not write waveform state updates");

                trace!("Waveform tick");
                interval.tick().await;
            }
        });

        self.update_task = Some(task);
        Ok(())
    }

    async fn before_shutdown<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess,
    {
        info!("Stopping WaveformUpdater");
        if let Some(task) = self.update_task.take() {
            task.abort()
        }
        Ok(())
    }
}
