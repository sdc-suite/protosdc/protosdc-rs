use async_trait::async_trait;
use biceps::common::biceps_util::{OperationState, State};
use biceps::common::mdib_entity::{Entity, EntityBase};
use biceps::common::mdib_state_modifications::MdibStateModifications;
use log::info;
use protosdc_biceps::biceps::operating_mode_mod::EnumType;
use protosdc_biceps::biceps::{AbstractOperationState, ActivateOperationState, OperatingMode};
use std::collections::HashSet;
use std::time::Duration;
use tokio::task::JoinHandle;

use biceps::entity_filter;
use sdc::provider::sdc_provider::{
    DeviceDiscoveryAccess, DeviceMdibType, SdcDeviceContext, SdcProviderPlugin,
};

#[derive(Debug, Default)]
pub(crate) struct OperationUpdater {
    update_task: Option<JoinHandle<()>>,
    exclude_mds: HashSet<String>,
}

impl OperationUpdater {
    pub fn new(exclude_mds: HashSet<String>) -> Self {
        Self {
            update_task: None,
            exclude_mds,
        }
    }
}

#[async_trait]
impl SdcProviderPlugin for OperationUpdater {
    async fn before_startup<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess,
    {
        info!("Starting OperationUpdater");

        let mut interval = tokio::time::interval(Duration::from_secs(5));
        let mut task_mdib = context.mdib_access.clone();

        let activate_entity_to_update = task_mdib
            .entities_by_type(entity_filter!(ActivateOperation))
            .await
            .into_iter()
            .find(|it| !self.exclude_mds.contains(&it.entity.mds()))
            .expect("No activate found within allowed mds");

        let activate_to_update = match activate_entity_to_update.entity {
            Entity::ActivateOperation(ent) => ent.pair.state,
            _ => panic!("Your filter is broken."),
        };

        let task = tokio::spawn(async move {
            let mut next: EnumType = EnumType::En;
            loop {
                info!(
                    "Updating operation {}",
                    activate_to_update.descriptor_handle()
                );

                let update = MdibStateModifications::Operation {
                    operation_states: vec![ActivateOperationState {
                        abstract_operation_state: AbstractOperationState {
                            operating_mode_attr: OperatingMode {
                                enum_type: next.clone(),
                            },
                            ..activate_to_update.abstract_operation_state.clone()
                        },
                    }
                    .into_abstract_operation_state_one_of()],
                };
                task_mdib
                    .write_states(update)
                    .await
                    .expect("Could not write component update");

                next = match next {
                    EnumType::En => EnumType::Dis,
                    _ => EnumType::En,
                };

                interval.tick().await;
            }
        });

        self.update_task = Some(task);
        Ok(())
    }

    async fn before_shutdown<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess,
    {
        info!("Stopping OperationUpdater");
        if let Some(task) = self.update_task.take() {
            task.abort();
        }
        Ok(())
    }
}
