use async_trait::async_trait;
use biceps::common::access::mdib_access::MdibAccess;
use biceps::common::biceps_util::{AlertState, State};
use biceps::common::mdib_entity::{
    AlertCondition, AlertSignal, Entity, EntityBase, LimitAlertCondition,
};
use biceps::common::mdib_state_modifications::MdibStateModifications;
use biceps::entity_filter;
use chrono::Utc;
use futures::FutureExt;
use itertools::Itertools;
use log::{debug, info, trace};
use protosdc_biceps::biceps::{
    alert_condition_kind_mod, alert_signal_presence_mod, AbstractAlertStateOneOf,
    AlertConditionReference, AlertConditionState, AlertSignalPresence, AlertSignalState,
    AlertSystemState, HandleRef, LimitAlertConditionState, Timestamp,
};
use sdc::provider::sdc_provider::{
    DeviceDiscoveryAccess, DeviceMdibType, SdcDeviceContext, SdcProviderPlugin,
};
use std::collections::{HashMap, HashSet};
use std::time::Duration;
use tokio::task::JoinHandle;

/// Plugin which updates the AlertSystem self checks as well as toggling all alert condition presences
/// TODO: I assume this breaks when alert systems disappear while running an update
#[derive(Debug, Default)]
pub struct AlertSystemUpdater {
    update_task: Option<JoinHandle<()>>,
    exclude_mds: HashSet<String>,
}

impl AlertSystemUpdater {
    pub fn new(exclude_mds: HashSet<String>) -> Self {
        Self {
            update_task: None,
            exclude_mds,
        }
    }
}

#[async_trait]
impl SdcProviderPlugin for AlertSystemUpdater {
    async fn before_startup<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess,
    {
        info!("Starting AlertSystemUpdater");

        let mut interval = tokio::time::interval(Duration::from_secs(1));

        // start update task
        let mut task_mdib = context.mdib_access.clone();
        let excluded_mds = self.exclude_mds.clone();
        let task = tokio::spawn(async move {
            // low effort approach: update all alerts every second, check if we need to bump
            // self check during that update

            let mut presence = true;

            loop {
                let now = Utc::now();
                let now_ms = now.timestamp_millis() as u64;

                let (
                    all_alert_conditions,
                    all_limit_alert_conditions,
                    all_alert_signals,
                    all_alert_systems,
                ) = task_mdib
                    .read_transaction(|access| {
                        async move {
                            let all_alert_conditions = access
                                .entities_by_type(entity_filter!(AlertCondition))
                                .await;
                            let all_limit_alert_conditions = access
                                .entities_by_type(entity_filter!(LimitAlertCondition))
                                .await;
                            let all_alert_signals =
                                access.entities_by_type(entity_filter!(AlertSignal)).await;
                            let all_alert_systems =
                                access.entities_by_type(entity_filter!(AlertSystem)).await;
                            (
                                all_alert_conditions,
                                all_limit_alert_conditions,
                                all_alert_signals,
                                all_alert_systems,
                            )
                        }
                        .boxed()
                    })
                    .await;

                info!(
                    "all_alert_conditions: {:?}",
                    all_alert_conditions
                        .iter()
                        .map(|it| it.entity.handle())
                        .join(" ")
                );

                let condition_states: Vec<(Box<AlertCondition>, AlertConditionState)> =
                    all_alert_conditions
                        .into_iter()
                        .filter(|it| !excluded_mds.contains(&it.entity.mds()))
                        .map(|it| match it.entity {
                            Entity::AlertCondition(ent) => {
                                let cloned = ent.pair.state.clone();
                                (ent, cloned)
                            }
                            _ => panic!("Your filter is broken."),
                        })
                        .map(|(entity, clone_state)| {
                            (
                                entity,
                                AlertConditionState {
                                    presence_attr: Some(presence),
                                    determination_time_attr: Some(Timestamp {
                                        unsigned_long: now_ms,
                                    }),
                                    ..clone_state
                                },
                            )
                        })
                        .collect();

                // collect all alert conditions
                let limit_condition_states: Vec<(
                    Box<LimitAlertCondition>,
                    LimitAlertConditionState,
                )> = all_limit_alert_conditions
                    .into_iter()
                    .filter(|it| !excluded_mds.contains(&it.entity.mds()))
                    .map(|it| match it.entity {
                        Entity::LimitAlertCondition(ent) => {
                            let cloned = ent.pair.state.clone();
                            (ent, cloned)
                        }
                        _ => panic!("Your filter is broken."),
                    })
                    .map(|(entity, clone_state)| {
                        (
                            entity,
                            LimitAlertConditionState {
                                alert_condition_state: AlertConditionState {
                                    presence_attr: Some(presence),
                                    determination_time_attr: Some(Timestamp {
                                        unsigned_long: now_ms,
                                    }),
                                    ..clone_state.alert_condition_state
                                },
                                ..clone_state
                            },
                        )
                    })
                    .collect();

                let present_physiological: Vec<(String, String)> = condition_states
                    .iter()
                    .map(|it| {
                        (
                            it.0.parent.clone(),
                            it.0.handle(),
                            it.0.pair.descriptor.kind_attr.enum_type.clone(),
                            it.1.presence_attr,
                        )
                    })
                    .chain(limit_condition_states.iter().map(|it| {
                        (
                            it.0.parent.clone(),
                            it.0.handle(),
                            it.0.pair
                                .descriptor
                                .alert_condition_descriptor
                                .kind_attr
                                .enum_type
                                .clone(),
                            it.1.alert_condition_state.presence_attr,
                        )
                    }))
                    .filter(|it| it.2 == alert_condition_kind_mod::EnumType::Phy)
                    .filter(|it| it.3 == Some(true))
                    .map(|it| (it.0, it.1))
                    .collect();

                let present_technical: Vec<(String, String)> = condition_states
                    .iter()
                    .map(|it| {
                        (
                            it.0.parent.clone(),
                            it.0.handle(),
                            it.0.pair.descriptor.kind_attr.enum_type.clone(),
                            it.1.presence_attr,
                        )
                    })
                    .chain(limit_condition_states.iter().map(|it| {
                        (
                            it.0.parent.clone(),
                            it.0.handle(),
                            it.0.pair
                                .descriptor
                                .alert_condition_descriptor
                                .kind_attr
                                .enum_type
                                .clone(),
                            it.1.alert_condition_state.presence_attr,
                        )
                    }))
                    .filter(|it| it.2 == alert_condition_kind_mod::EnumType::Tec)
                    .filter(|it| it.3 == Some(true))
                    .map(|it| (it.0, it.1))
                    .collect();

                debug!("present_physiological: {:?}", present_physiological);
                debug!("present_technical: {:?}", present_technical);

                // set alert signals for conditions
                let condition_presences: HashMap<String, bool> = condition_states
                    .iter()
                    .map(|(_, state)| {
                        (
                            state
                                .abstract_alert_state
                                .abstract_state
                                .descriptor_handle_attr
                                .string
                                .clone(),
                            state.presence_attr.unwrap_or(false),
                        )
                    })
                    .collect();

                let simple_condition_signal_states: Vec<(Box<AlertSignal>, AlertSignalState)> =
                    all_alert_signals
                        .into_iter()
                        .filter(|it| !excluded_mds.contains(&it.entity.mds()))
                        .map(|it| match it.entity {
                            Entity::AlertSignal(ent) => {
                                let cloned = ent.pair.state.clone();
                                (ent, cloned)
                            }
                            _ => panic!("Your filter is broken."),
                        })
                        .map(|(entity, clone_state)| {
                            let entity_handle = entity.handle();
                            (
                                entity,
                                AlertSignalState {
                                    presence_attr: Some(AlertSignalPresence {
                                        enum_type: match condition_presences.get(&entity_handle) {
                                            None => alert_signal_presence_mod::EnumType::Off,
                                            Some(true) => alert_signal_presence_mod::EnumType::On,
                                            Some(false) => alert_signal_presence_mod::EnumType::Off,
                                        },
                                    }),
                                    ..clone_state
                                },
                            )
                        })
                        .collect();

                debug!(
                    "Setting alert signals: {}",
                    simple_condition_signal_states
                        .iter()
                        .map(|it| it.1.descriptor_handle())
                        .join(" ")
                );

                let mut system_states: Vec<AbstractAlertStateOneOf> = all_alert_systems
                    .into_iter()
                    .filter(|it| !excluded_mds.contains(&it.entity.mds()))
                    .map(|it| match it.entity {
                        Entity::AlertSystem(ent) => {
                            let cloned = ent.pair.state.clone();
                            (ent, cloned)
                        },
                        _ => panic!("Your filter is broken."),
                    })
                    // update alert presence
                    .map(|(entity, cloned_state)| {
                        let entity_handle = entity.handle();

                        (entity, AlertSystemState {
                            present_physiological_alarm_conditions_attr: Some(AlertConditionReference {
                                handle_ref: present_physiological
                                    .iter()
                                    .filter(|it| it.0 == entity_handle)
                                    .map(|it| HandleRef { string: it.1.clone() })
                                    .collect()
                            }),
                            present_technical_alarm_conditions_attr: Some(AlertConditionReference {
                                handle_ref: present_technical
                                    .iter()
                                    .filter(|it| it.0 == entity_handle)
                                    .map(|it| HandleRef { string: it.1.clone() })
                                    .collect()
                            }),
                            ..cloned_state
                        })
                    })
                    .map(|(entity, mut state)| match entity.pair.descriptor.self_check_period_attr {
                        None => state,
                        Some(period) => {
                            match &state.last_self_check_attr {
                                None => {
                                    state.last_self_check_attr = Some(Timestamp { unsigned_long: now_ms });
                                    state.self_check_count_attr = match state.self_check_count_attr {
                                        Some(it) => Some(it + 1),
                                        None => Some(1),
                                    };
                                    state
                                }
                                Some(last_check) => {
                                    // check if we're within a second, if so, update
                                    let period_ms = period.as_millis() as u64;
                                    let lhs = last_check.unsigned_long + period_ms - 1000u64;
                                    if lhs <= now_ms {
                                        info!(
                                            "updating self check for {}, period has (almost) passed: {} <= {}",
                                            &state.abstract_alert_state.abstract_state.descriptor_handle_attr.string,
                                            lhs, now_ms
                                        );
                                        trace!("last_check.unsigned_long: {}\nperiod_ms: {}", last_check.unsigned_long, period_ms);
                                        state.last_self_check_attr = Some(Timestamp { unsigned_long: now_ms });
                                        state.self_check_count_attr = match state.self_check_count_attr {
                                            Some(it) => Some(it + 1),
                                            None => Some(1),
                                        };
                                        state
                                    } else {
                                        debug!(
                                            "NOT updating self check: {} > {}",
                                            lhs, now_ms
                                        );
                                        state
                                    }
                                }
                            }
                        }
                    })
                    .map(|it| it.into_abstract_alert_state_one_of())
                    .collect();

                let conditions_one_of = condition_states
                    .into_iter()
                    .map(|it| it.1.into_abstract_alert_state_one_of())
                    .chain(
                        limit_condition_states
                            .into_iter()
                            .map(|it| it.1.into_abstract_alert_state_one_of()),
                    )
                    .collect_vec();
                system_states.extend(conditions_one_of);

                let signals_one_of = simple_condition_signal_states
                    .into_iter()
                    .map(|it| it.1.into_abstract_alert_state_one_of())
                    .collect_vec();

                system_states.extend(signals_one_of);

                info!(
                    "Writing updates for {}",
                    system_states
                        .iter()
                        .map(|it| it.descriptor_handle())
                        .join(" ")
                );
                task_mdib
                    .write_states(MdibStateModifications::Alert {
                        alert_states: system_states,
                    })
                    .await
                    .expect("Write failed?");

                debug!("new presence: {}", presence);
                presence = !presence;
                interval.tick().await;
            }
        });

        self.update_task = Some(task);
        Ok(())
    }

    async fn before_shutdown<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryAccess,
    {
        if let Some(task) = self.update_task.take() {
            task.abort();
        }

        Ok(())
    }
}
