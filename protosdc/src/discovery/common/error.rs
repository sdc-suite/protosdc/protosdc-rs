use hyper::http::uri::InvalidUri;
use thiserror::Error;
use tokio::sync::mpsc::error::SendError;

use crate::common::crypto::CryptoError;
use network::udp_binding::UdpBindingError;
use protosdc_proto::discovery::ProxyNotifyHelloAndByeRequest;

#[derive(Debug, Error)]
pub enum DiscoveryError {
    #[error("DiscoveryProxy was already connected")]
    ProxyAlreadyConnected,
    #[error(transparent)]
    TonicStatusError(#[from] tonic::Status),
    #[error(transparent)]
    TonicTransportError(#[from] tonic::transport::Error),
    #[error(transparent)]
    UDPError(#[from] UdpBindingError),
    #[error(transparent)]
    TonicSendHelloByeError(SendError<ProxyNotifyHelloAndByeRequest>),
    #[error(transparent)]
    CryptoError(#[from] CryptoError),
    #[error(transparent)]
    InvalidUri(#[from] InvalidUri),
}
