use std::fmt::{Display, Formatter};
use std::net::SocketAddr;
use std::str::FromStr;
use std::sync::Arc;

use async_trait::async_trait;
use log::{error, info};
use prost::Message;
use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio::sync::{broadcast, Mutex};
use tokio::task;
use tokio_stream::wrappers::ReceiverStream;
use url::Url;

use network::udp_binding::{
    InboundUdpMessage, OutboundUdpMessage, UdpBinding, UdpHeader, UdpMessage,
};
use protosdc_proto::addressing::Addressing;
use protosdc_proto::common::Uri;
use protosdc_proto::discovery::provider_proxy_service_client::ProviderProxyServiceClient;
use protosdc_proto::discovery::scope_matcher;
use protosdc_proto::discovery::search_filter;
use protosdc_proto::discovery::{
    discovery_message, proxy_notify_hello_and_bye_request, DiscoveryMessage, Endpoint, Hello,
    ProxyNotifyHelloAndByeRequest, ScopeMatcher, SearchFilter, SearchRequest, SearchResponse,
};
use tokio_util::sync::{CancellationToken, DropGuard};

use crate::addressing::util;
use crate::common::crypto::CryptoConfig;
use crate::common::hyper_client::{create_client, get_dummy_uri};
use crate::discovery::common::error::DiscoveryError;
use crate::discovery::common::util as discovery_util;
use crate::provider::common::EndpointData;

const BUFFER_SIZE: usize = 100;

#[async_trait]
pub trait DiscoveryProvider {
    /// Updates the scopes of the provider, triggers hello message if possible.
    ///
    /// # Arguments
    ///
    /// * `scopes`: new scopes of the provider
    async fn update_scopes(&mut self, scopes: Vec<Uri>);

    /// Updates the physical addresses of the provider, triggers hello message if possible.
    ///
    /// # Arguments
    ///
    /// * `scopes`: new scopes of the provider
    async fn update_physical_address(&mut self, addresses: Vec<Uri>);

    /// Updates the scopes and physical addresses of the provider, triggers hello message if possible.
    ///
    /// # Arguments
    ///
    /// * `scopes`: new scopes of the provider
    async fn update(&mut self, scopes: Vec<Uri>, addresses: Vec<Uri>);

    /// Triggers hello message if possible.
    async fn hello(&mut self) -> Result<(), DiscoveryError>;

    /// Returns the Endpoint information as seen by other network peers.
    async fn endpoint(&self) -> Endpoint;
}

#[derive(Debug)]
struct DiscoveryProviderInner {
    _udp_task: task::JoinHandle<()>,
    _drop_guard: DropGuard,
    _proxy_task: Option<task::JoinHandle<()>>,
}

#[derive(Clone, Debug)]
pub struct DiscoveryProviderImpl<T>
where
    T: UdpBinding + Send + Sync,
{
    udp_binding: T,

    endpoint: EndpointData,

    proxy_address: Option<String>,
    proxy_sender: Option<Sender<ProxyNotifyHelloAndByeRequest>>,

    _inner: Arc<Mutex<DiscoveryProviderInner>>,
}

impl<T> Display for DiscoveryProviderImpl<T>
where
    T: UdpBinding + Send + Display + Sync,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "DiscoveryProviderImpl (udp_binding {}, discovery_proxy: {:?})",
            self.udp_binding, self.proxy_address,
        )
    }
}

/// Matches an endpoint against search filters
///
/// # Arguments
///
/// * `&endpoint`: to match
/// * `search_filters`:  to match against
///
/// returns: bool true if the search is empty, an endpoint identifier matches, or _all_ scopes are
///               matching
async fn match_search(
    endpoint_identifier: &str,
    scopes: &[Uri],
    search_filters: &[SearchFilter],
) -> bool {
    if search_filters.is_empty() {
        return true;
    }

    let endpoint_match = search_filters
        .iter()
        .filter_map(|search| match &search.r#type {
            Some(search_filter::Type::EndpointIdentifier(identifier)) => Some(identifier),
            _ => None,
        })
        .any(|filter| endpoint_identifier == filter.as_str());

    if endpoint_match {
        return true;
    }

    let scope_matchers: Vec<&ScopeMatcher> = search_filters
        .iter()
        .filter_map(|search| match &search.r#type {
            Some(search_filter::Type::ScopeMatcher(scope)) => Some(scope),
            _ => None,
        })
        .collect();

    if scope_matchers.is_empty() {
        // TODO This seems like it doesn't cover the case correctly
        return false;
    }

    scope_matchers.iter().all(|matcher| {
        scopes.iter().any(|endpoint_scope| {
            match scope_matcher::Algorithm::try_from(matcher.algorithm) {
                Ok(scope_matcher::Algorithm::Rfc3986) => {
                    match &matcher.scope {
                        None => false,
                        Some(uri) => {
                            // TODO: Technically this isn't rfc3986, but whatwg URL
                            let url1 = Url::parse(&uri.value);
                            let url2 = Url::parse(&endpoint_scope.value);
                            match (&url1, &url2) {
                                (Ok(u1), Ok(u2)) => u1 == u2,
                                _ => {
                                    error!("Error parsing scopes.");
                                    error!("url1: {:?}", &url1);
                                    error!("url2: {:?}", &url2);
                                    false
                                }
                            }
                        }
                    }
                }
                Ok(scope_matcher::Algorithm::StringCompare) => match &matcher.scope {
                    None => false,
                    Some(uri) => uri == endpoint_scope,
                },
                Err(err) => {
                    error!(
                        "Error decoding algorithm id {}. {:?}",
                        matcher.algorithm, err
                    );
                    false
                }
            }
        })
    })
}

async fn validate_search_request(addressing: &Addressing) -> bool {
    addressing.action == crate::common::action::SEARCH_REQUEST
}

async fn handle_message(
    udp_header: UdpHeader,
    message: DiscoveryMessage,
    endpoint: &EndpointData,
    message_channel: &Sender<UdpMessage>,
) {
    if let Some(discovery_message::Type::SearchRequest(search_request)) = message.r#type {
        info!("Processing inbound search request");
        process_search(
            endpoint,
            udp_header,
            message.addressing,
            search_request,
            message_channel,
        )
        .await;
    }
}

async fn process_udp_messages(
    receiver: broadcast::Receiver<InboundUdpMessage>,
    message_channel: Sender<UdpMessage>,
    endpoint: EndpointData,
    cancellation_token: CancellationToken,
) {
    discovery_util::process_udp_messages(
        receiver,
        |header, message| handle_message(header, message, &endpoint, &message_channel),
        cancellation_token,
    )
    .await
}

impl<T> DiscoveryProviderImpl<T>
where
    T: UdpBinding + Send + Sync,
{
    pub async fn new(
        udp_binding: T,
        discovery_proxy_address: Option<String>,
        endpoint: EndpointData,
        crypto_config: Option<CryptoConfig>,
    ) -> Result<Self, DiscoveryError> {
        let (proxy_sender, proxy_receiver) = channel(BUFFER_SIZE);

        let receiver = udp_binding.subscribe();

        let cancellation_token = CancellationToken::new();

        let run = process_udp_messages(
            receiver,
            udp_binding.get_message_channel_sender(),
            endpoint.clone(),
            cancellation_token.clone(),
        );
        let udp_task = tokio::spawn(run);

        let (proxy_task, proxy_sender_actual) = match &discovery_proxy_address {
            Some(addr) => (
                Some(
                    connect_proxy(
                        addr,
                        crypto_config,
                        proxy_receiver,
                        cancellation_token.clone(),
                    )
                    .await?,
                ),
                Some(proxy_sender),
            ),
            None => (None, None),
        };

        Ok(Self {
            udp_binding,
            endpoint,
            proxy_sender: proxy_sender_actual,
            proxy_address: discovery_proxy_address,
            _inner: Arc::new(Mutex::new(DiscoveryProviderInner {
                _udp_task: udp_task,
                _proxy_task: proxy_task,
                _drop_guard: cancellation_token.drop_guard(),
            })),
        })
    }
}

async fn connect_proxy(
    proxy_address: &str,
    crypto: Option<CryptoConfig>,
    proxy_receiver: Receiver<ProxyNotifyHelloAndByeRequest>,
    cancellation_token: CancellationToken,
) -> Result<task::JoinHandle<()>, DiscoveryError> {
    let mut proxy_client = {
        let uri = hyper::Uri::from_str(proxy_address)?;
        let client = create_client(&uri, &crypto).await?;
        ProviderProxyServiceClient::with_origin(client, get_dummy_uri(&uri))
    };

    let proxy_task = tokio::spawn(async move {
        let receiver_stream = ReceiverStream::new(proxy_receiver);
        match proxy_client.notify_hello_and_bye(receiver_stream).await {
            Ok(_) => {}
            // TODO: Reconnect/Retry mechanism
            Err(err) => error!("Unexpected error discovery proxy connection: {}", err),
        };
    });

    // Start a second cancel the task when the cancellation token is dropped
    let proxy_task_abort_handle = proxy_task.abort_handle();
    tokio::spawn(async move {
        cancellation_token.cancelled().await;
        proxy_task_abort_handle.abort();
    });

    Ok(proxy_task)
}

async fn process_search(
    endpoint_data: &EndpointData,
    udp_header: UdpHeader,
    addressing_opt: Option<Addressing>,
    search: SearchRequest,
    udp_message_channel: &Sender<UdpMessage>,
) {
    let addressing = match addressing_opt {
        Some(it) => it,
        None => return,
    };
    if !validate_search_request(&addressing).await {
        return;
    }
    let endpoint = endpoint_data.endpoint.lock().await;
    if !match_search(
        &endpoint.endpoint_identifier,
        &endpoint.scope,
        &search.search_filter,
    )
    .await
    {
        return;
    }

    info!("Search is a match, responding");

    let response = DiscoveryMessage {
        addressing: Some(util::create_reply_to(
            &addressing,
            crate::common::action::SEARCH_RESPONSE,
        )),
        r#type: Some(discovery_message::Type::SearchResponse(SearchResponse {
            endpoint: vec![endpoint.clone()],
        })),
    };

    let outbound = OutboundUdpMessage {
        data: response.encode_to_vec(),
        address: SocketAddr::new(udp_header.source_address, udp_header.source_port),
    };

    udp_message_channel
        .send(UdpMessage::OutboundUnicast {
            message: outbound,
            response: None,
        })
        .await
        .map_err(|err| error!("Unexpected error discovery: {}", err))
        .ok();
}

#[async_trait]
impl<T> DiscoveryProvider for DiscoveryProviderImpl<T>
where
    T: UdpBinding + Display + Send + Sync,
{
    async fn update_scopes(&mut self, scopes: Vec<Uri>) {
        {
            let mut endpoint = self.endpoint.endpoint.lock().await;
            endpoint.scope = scopes;
        }
        // ignore error
        self.hello()
            .await
            .map_err(|err| error!("Unexpected error discovery: {}", err))
            .ok();
    }

    async fn update_physical_address(&mut self, addresses: Vec<Uri>) {
        {
            let mut endpoint = self.endpoint.endpoint.lock().await;
            endpoint.physical_address = addresses;
        }
        // ignore error
        self.hello()
            .await
            .map_err(|err| error!("Unexpected error discovery: {}", err))
            .ok();
    }

    async fn update(&mut self, scopes: Vec<Uri>, addresses: Vec<Uri>) {
        {
            let mut endpoint = self.endpoint.endpoint.lock().await;
            endpoint.scope = scopes;
            endpoint.physical_address = addresses;
        }
        // ignore error
        self.hello()
            .await
            .map_err(|err| error!("Unexpected error discovery: {}", err))
            .ok();
    }

    async fn hello(&mut self) -> Result<(), DiscoveryError> {
        info!("Sending hello message");

        let hello_msg = Hello {
            endpoint: Some(self.endpoint().await),
        };

        // UDP
        let disco_message = DiscoveryMessage {
            addressing: Some(util::create_for_action(crate::common::action::HELLO)),
            r#type: Some(discovery_message::Type::Hello(hello_msg.clone())),
        };

        discovery_util::send_multicast(
            self.udp_binding.get_message_channel_sender(),
            disco_message,
        )
        .await?;

        match &self.proxy_sender {
            None => {}
            Some(sender) => {
                let proxy_request = ProxyNotifyHelloAndByeRequest {
                    addressing: Some(util::create_for_action(crate::common::action::HELLO)),
                    r#type: Some(proxy_notify_hello_and_bye_request::Type::Hello(hello_msg)),
                };

                sender.send(proxy_request).await.map_err(|err| {
                    error!("Unexpected error discovery: {}", err);
                    DiscoveryError::TonicSendHelloByeError(err)
                })?;
            }
        };

        Ok(())
    }

    async fn endpoint(&self) -> Endpoint {
        (*self.endpoint.endpoint.lock().await).clone()
    }
}
