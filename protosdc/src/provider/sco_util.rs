use biceps::common::mdib_version::MdibVersion;
use protosdc_biceps::biceps::{
    invocation_error_mod, invocation_state_mod, InvocationError, InvocationState, LocalizedText,
};

#[derive(Clone, Debug)]
pub enum InvocationStateTypes {
    NotFailed(NotFailedInvocationStateTypes),
    Failed(FailedInvocationStateTypes),
}

#[derive(Clone, Debug)]
pub enum NotFailedInvocationStateTypes {
    Wait,
    Start,
    Fin,
    FinMod,
}

#[derive(Clone, Debug)]
//noinspection SpellCheckingInspection
pub enum FailedInvocationStateTypes {
    Cnclld,
    CnclldMan,
    Fail,
}

impl From<InvocationStateTypes> for InvocationState {
    fn from(value: InvocationStateTypes) -> Self {
        match value {
            InvocationStateTypes::NotFailed(it) => it.into(),
            InvocationStateTypes::Failed(it) => it.into(),
        }
    }
}

impl From<NotFailedInvocationStateTypes> for InvocationState {
    fn from(value: NotFailedInvocationStateTypes) -> Self {
        InvocationState {
            enum_type: match value {
                NotFailedInvocationStateTypes::Wait => invocation_state_mod::EnumType::Wait,
                NotFailedInvocationStateTypes::Start => invocation_state_mod::EnumType::Start,
                NotFailedInvocationStateTypes::Fin => invocation_state_mod::EnumType::Fin,
                NotFailedInvocationStateTypes::FinMod => invocation_state_mod::EnumType::FinMod,
            },
        }
    }
}

impl TryFrom<InvocationState> for NotFailedInvocationStateTypes {
    type Error = ();

    fn try_from(value: InvocationState) -> Result<Self, Self::Error> {
        match value.enum_type {
            invocation_state_mod::EnumType::Wait => Ok(Self::Wait),
            invocation_state_mod::EnumType::Start => Ok(Self::Start),
            invocation_state_mod::EnumType::Fin => Ok(Self::Fin),
            invocation_state_mod::EnumType::FinMod => Ok(Self::FinMod),
            _ => Err(()),
        }
    }
}

impl From<FailedInvocationStateTypes> for InvocationState {
    fn from(value: FailedInvocationStateTypes) -> Self {
        InvocationState {
            enum_type: match value {
                FailedInvocationStateTypes::Cnclld => invocation_state_mod::EnumType::Cnclld,
                FailedInvocationStateTypes::CnclldMan => invocation_state_mod::EnumType::CnclldMan,
                FailedInvocationStateTypes::Fail => invocation_state_mod::EnumType::Fail,
            },
        }
    }
}

impl TryFrom<InvocationState> for FailedInvocationStateTypes {
    type Error = ();
    fn try_from(value: InvocationState) -> Result<Self, Self::Error> {
        match value.enum_type {
            invocation_state_mod::EnumType::Cnclld => Ok(Self::Cnclld),
            invocation_state_mod::EnumType::CnclldMan => Ok(Self::CnclldMan),
            invocation_state_mod::EnumType::Fail => Ok(Self::Fail),
            _ => Err(()),
        }
    }
}

impl From<InvocationState> for InvocationStateTypes {
    fn from(value: InvocationState) -> Self {
        match value.enum_type {
            invocation_state_mod::EnumType::Wait => {
                InvocationStateTypes::NotFailed(NotFailedInvocationStateTypes::Wait)
            }
            invocation_state_mod::EnumType::Start => {
                InvocationStateTypes::NotFailed(NotFailedInvocationStateTypes::Start)
            }
            invocation_state_mod::EnumType::Fin => {
                InvocationStateTypes::NotFailed(NotFailedInvocationStateTypes::Fin)
            }
            invocation_state_mod::EnumType::FinMod => {
                InvocationStateTypes::NotFailed(NotFailedInvocationStateTypes::FinMod)
            }
            invocation_state_mod::EnumType::Cnclld => {
                InvocationStateTypes::Failed(FailedInvocationStateTypes::Cnclld)
            }
            invocation_state_mod::EnumType::CnclldMan => {
                InvocationStateTypes::Failed(FailedInvocationStateTypes::CnclldMan)
            }
            invocation_state_mod::EnumType::Fail => {
                InvocationStateTypes::Failed(FailedInvocationStateTypes::Fail)
            }
        }
    }
}

pub struct NoInvocationState;
pub struct NotFailedInvocationState(NotFailedInvocationStateTypes);
pub struct FailedInvocationState(FailedInvocationStateTypes);
pub struct FailedInvocationStateErrors {
    invocation_state: FailedInvocationStateTypes,
    invocation_error: InvocationError,
    invocation_error_message: Vec<LocalizedText>,
}

pub struct NoMdibVersion;
pub struct WithMdibVersion(MdibVersion);

pub struct NoOperationTarget;
pub struct OperationTarget(String);

#[derive(Clone, Debug)]
pub struct ReportBuilder<InvocationState, MdibVer, OperationTar> {
    invocation_state: InvocationState,
    mdib_version: MdibVer,
    operation_target: OperationTar,
}

impl ReportBuilder<NoInvocationState, NoMdibVersion, NoOperationTarget> {
    pub fn create() -> Self {
        Self {
            invocation_state: NoInvocationState,
            mdib_version: NoMdibVersion,
            operation_target: NoOperationTarget,
        }
    }
}

impl<MdibVer, OperationTarget> ReportBuilder<NoInvocationState, MdibVer, OperationTarget> {
    pub fn success(
        self,
        state: NotFailedInvocationStateTypes,
    ) -> ReportBuilder<NotFailedInvocationState, MdibVer, OperationTarget> {
        let Self {
            mdib_version,
            operation_target,
            ..
        } = self;
        ReportBuilder {
            invocation_state: NotFailedInvocationState(state),
            mdib_version,
            operation_target,
        }
    }

    pub fn failure(
        self,
        state: FailedInvocationStateTypes,
    ) -> ReportBuilder<FailedInvocationState, MdibVer, OperationTarget> {
        let Self {
            mdib_version,
            operation_target,
            ..
        } = self;
        ReportBuilder {
            invocation_state: FailedInvocationState(state),
            mdib_version,
            operation_target,
        }
    }
}

impl<MdibVer, OperationTarget> ReportBuilder<FailedInvocationState, MdibVer, OperationTarget> {
    pub fn error(
        self,
        invocation_error: invocation_error_mod::EnumType,
    ) -> ReportBuilder<FailedInvocationStateErrors, MdibVer, OperationTarget> {
        let Self {
            mdib_version,
            operation_target,
            invocation_state,
            ..
        } = self;
        ReportBuilder {
            mdib_version,
            operation_target,
            invocation_state: FailedInvocationStateErrors {
                invocation_state: invocation_state.0,
                invocation_error: InvocationError {
                    enum_type: invocation_error,
                },
                invocation_error_message: vec![],
            },
        }
    }
}

impl<MdibVer, OperationTarget>
    ReportBuilder<FailedInvocationStateErrors, MdibVer, OperationTarget>
{
    pub fn error_message(
        mut self,
        invocation_error_message: LocalizedText,
    ) -> ReportBuilder<FailedInvocationStateErrors, MdibVer, OperationTarget> {
        self.invocation_state
            .invocation_error_message
            .push(invocation_error_message);
        self
    }
}

impl<InvocationState, OperationTarget>
    ReportBuilder<InvocationState, NoMdibVersion, OperationTarget>
{
    pub fn mdib_version(
        self,
        mdib_version: MdibVersion,
    ) -> ReportBuilder<InvocationState, WithMdibVersion, OperationTarget> {
        let Self {
            invocation_state,
            operation_target,
            ..
        } = self;
        ReportBuilder {
            mdib_version: WithMdibVersion(mdib_version),
            invocation_state,
            operation_target,
        }
    }
}

impl<InvocationState, MdibVer> ReportBuilder<InvocationState, MdibVer, NoOperationTarget> {
    pub fn operation_target(
        self,
        operation_target: impl Into<String>,
    ) -> ReportBuilder<InvocationState, MdibVer, OperationTarget> {
        let Self {
            invocation_state,
            mdib_version,
            ..
        } = self;
        ReportBuilder {
            mdib_version,
            invocation_state,
            operation_target: OperationTarget(operation_target.into()),
        }
    }
}

impl ReportBuilder<NotFailedInvocationState, WithMdibVersion, NoOperationTarget> {
    pub fn build(self) -> ReportData {
        let Self {
            mdib_version,
            invocation_state,
            ..
        } = self;
        ReportData {
            invocation_state: invocation_state.0.into(),
            mdib_version: mdib_version.0,
            operation_target: None,
            // no error
            invocation_error: None,
            invocation_error_message: vec![],
        }
    }
}
impl ReportBuilder<NotFailedInvocationState, WithMdibVersion, OperationTarget> {
    pub fn build(self) -> ReportData {
        let Self {
            mdib_version,
            operation_target,
            invocation_state,
            ..
        } = self;
        ReportData {
            invocation_state: invocation_state.0.into(),
            mdib_version: mdib_version.0,
            operation_target: Some(operation_target.0),
            // no error
            invocation_error: None,
            invocation_error_message: vec![],
        }
    }
}

impl ReportBuilder<FailedInvocationStateErrors, WithMdibVersion, NoOperationTarget> {
    pub fn build(self) -> ReportData {
        let Self {
            mdib_version,
            invocation_state,
            ..
        } = self;
        ReportData {
            invocation_state: invocation_state.invocation_state.into(),
            mdib_version: mdib_version.0,
            operation_target: None,
            invocation_error: Some(invocation_state.invocation_error),
            invocation_error_message: invocation_state.invocation_error_message,
        }
    }
}
impl ReportBuilder<FailedInvocationStateErrors, WithMdibVersion, OperationTarget> {
    pub fn build(self) -> ReportData {
        let Self {
            mdib_version,
            operation_target,
            invocation_state,
            ..
        } = self;
        ReportData {
            invocation_state: invocation_state.invocation_state.into(),
            mdib_version: mdib_version.0,
            operation_target: Some(operation_target.0),
            invocation_error: Some(invocation_state.invocation_error),
            invocation_error_message: invocation_state.invocation_error_message,
        }
    }
}

#[derive(Clone, Debug)]
pub struct ReportData {
    pub invocation_state: InvocationState,
    pub invocation_error: Option<InvocationError>,
    pub invocation_error_message: Vec<LocalizedText>,
    pub mdib_version: MdibVersion,
    pub operation_target: Option<String>,
}
