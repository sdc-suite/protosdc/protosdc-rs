use std::net::SocketAddr;

use async_trait::async_trait;
use log::warn;
use protosdc_proto::discovery::Endpoint;

use biceps::common::mdib_version::MdibVersionBuilder;
use biceps::common::storage::mdib_storage::MdibStorageImpl;
use biceps::provider::access::local_mdib_access::{
    DefaultLocalDescriptionPreprocessors, DefaultLocalStatePreprocessors, LocalMdibAccess,
    LocalMdibAccessImpl, LocalMdibAccessReadTransaction,
};
use common::{Service, ServiceDelegate, ServiceError, ServiceState};
use network::udp_binding::{create_udp_binding, UdpBindingImpl, PROTOSDC_MULTICAST_PORT};

use crate::common::crypto::CryptoConfig;
use crate::discovery::provider::discovery_provider::{DiscoveryProvider, DiscoveryProviderImpl};
use crate::provider::common::{
    socket_address_to_uri_http, socket_address_to_uri_https, EndpointData,
};
use crate::provider::localization_storage::{
    LocalizationStorage, LocalizationStorageImpl, LocalizedStorageText,
};
use crate::provider::metadata::DeviceMetadata;
use crate::provider::plugins::{ProtoSdcDeviceContext, ProtoSdcProviderPlugin};
use crate::provider::sco::{OperationInvocationReceiver, ScoController};
use crate::provider::services::{PrimaryProviderServices, PrimaryProviderServicesConfig};

#[derive(Debug, Clone)]
pub struct ServerConfig {
    pub crypto_config: Option<CryptoConfig>,
    pub server_address: SocketAddr,
}

pub struct SdcDevice<T, U, V, LOC, PLUGIN>
where
    T: DiscoveryProvider + Sync + Send + Clone,
    U: OperationInvocationReceiver<V> + Sync + Send,
    V: 'static + LocalMdibAccess + Sync + Send + Clone,
    LOC: 'static + LocalizationStorage<LocalizedStorageText>,
    PLUGIN: ProtoSdcProviderPlugin + Send + Sync,
{
    service_delegate: ServiceDelegate,
    pub local_mdib: V, // TODO: No pub?
    primary_services: PrimaryProviderServices<U, V, LOC>,
    pub discovery_provider: T,
    plugins: PLUGIN,
}

impl<T, U, V, LOC, PLUGIN> SdcDevice<T, U, V, LOC, PLUGIN>
where
    T: DiscoveryProvider + Sync + Send + Clone,
    U: OperationInvocationReceiver<V> + Sync + Send,
    V: LocalMdibAccess + Sync + Send + Clone,
    LOC: LocalizationStorage<LocalizedStorageText>,
    PLUGIN: ProtoSdcProviderPlugin + Send + Sync,
{
    pub fn new_custom(
        service_delegate: ServiceDelegate,
        local_mdib: V,
        primary_services: PrimaryProviderServices<U, V, LOC>,
        discovery_provider: T,
        plugins: PLUGIN,
    ) -> Self {
        Self {
            service_delegate,
            local_mdib,
            discovery_provider,
            primary_services,
            plugins,
        }
    }
}

pub type DefaultLocalMdibAccessImpl = LocalMdibAccessImpl<
    MdibStorageImpl,
    DefaultLocalDescriptionPreprocessors<MdibStorageImpl>,
    DefaultLocalStatePreprocessors<MdibStorageImpl>,
>;

impl<U, PLUGIN>
    SdcDevice<
        DiscoveryProviderImpl<UdpBindingImpl>,
        U,
        DefaultLocalMdibAccessImpl,
        LocalizationStorageImpl,
        PLUGIN,
    >
where
    U: OperationInvocationReceiver<DefaultLocalMdibAccessImpl> + Sync + Send,
    PLUGIN: ProtoSdcProviderPlugin + Send + Sync,
{
    /// Creates a new SdcDevice.
    ///
    /// Do note that this is only a convenience constructor, you may roll your own using `new_custom`.
    ///
    /// # Arguments
    ///
    /// * `sequence_id`: sequence id of the local mdib that is created
    /// * `server_config`: configuration of the grpc server that is created
    /// * `operation_invocation_receiver`: handler for incoming operation invocations
    /// * `endpoint_reference`: endpoint reference to uniquely identify the provider
    /// * `metadata`: metadata describing the provider
    ///
    /// returns: Result<SdcDevice<DiscoveryProviderImpl<UdpBindingImpl>, U>, Error>
    #[allow(clippy::too_many_arguments)] // TODO: consider refactoring at some point
    pub async fn new(
        sequence_id: String,
        server_config: ServerConfig,
        operation_invocation_receiver: U,
        endpoint_reference: String,
        metadata: Option<DeviceMetadata>,
        discovery_proxy: Option<String>,
        localization_storage: Option<LocalizationStorageImpl>,
        plugins: PLUGIN,
    ) -> Result<Self, anyhow::Error> {
        let initial_version = MdibVersionBuilder::default().seq(sequence_id).create();

        let mdib_access = LocalMdibAccessImpl::new(initial_version);

        let endpoint_data: EndpointData = Endpoint {
            endpoint_identifier: endpoint_reference,
            scope: vec![],
            physical_address: vec![match server_config.crypto_config.is_some() {
                true => socket_address_to_uri_https(&server_config.server_address),
                false => socket_address_to_uri_http(&server_config.server_address),
            }],
        }
        .into();

        let endpoint_metadata = metadata
            .unwrap_or_else(|| {
                warn!("No metadata provided, using default");
                DeviceMetadata::default()
            })
            .into();

        let discovery_provider = DiscoveryProviderImpl::new(
            create_udp_binding(
                server_config.server_address.ip(),
                Some(PROTOSDC_MULTICAST_PORT),
            )
            .await
            .map_err(anyhow::Error::new)?,
            discovery_proxy,
            endpoint_data.clone(),
            server_config.crypto_config.clone(),
        )
        .await?;

        let sco_controller = ScoController::default();

        let primary_services_config = PrimaryProviderServicesConfig::builder()
            .mdib_access(mdib_access.clone())
            .endpoint(endpoint_data.clone())
            .endpoint_metadata(endpoint_metadata)
            .services(vec![])
            .server_config(server_config)
            .sco_controller(sco_controller)
            .operation_invocation_receiver(operation_invocation_receiver)
            .localization_storage(localization_storage)
            .build();

        let primary_services = PrimaryProviderServices::new(primary_services_config).await;

        Ok(Self::new_custom(
            ServiceDelegate::default(),
            mdib_access,
            primary_services,
            discovery_provider,
            plugins,
        ))
    }

    pub async fn endpoint(&self) -> Endpoint {
        self.discovery_provider.endpoint().await
    }
}

#[async_trait]
impl<T, U, V, LOC, PLUGIN> Service for SdcDevice<T, U, V, LOC, PLUGIN>
where
    T: 'static + DiscoveryProvider + Send + Sync + Clone,
    U: 'static + OperationInvocationReceiver<V> + Send + Sync + Clone,
    V: 'static + LocalMdibAccess + Send + Sync + Clone + LocalMdibAccessReadTransaction,
    LOC: 'static + LocalizationStorage<LocalizedStorageText> + Send + Sync,
    PLUGIN: ProtoSdcProviderPlugin + Send + Sync,
{
    async fn start(&mut self) -> Result<(), ServiceError> {
        self.service_delegate.start().await?;

        let device_context = ProtoSdcDeviceContext {
            mdib_access: self.local_mdib.clone(),
            discovery_access: self.discovery_provider.clone(),
        };

        self.plugins.before_startup(&device_context).await?;

        self.primary_services.start().await?;

        // update actual addresses for discovery
        // let actual_addresses = self.primary_services.actual_addresses();
        // self.discovery_provider.update_physical_address(actual_addresses.to_vec()).await;

        self.plugins.after_startup(&device_context).await?;

        self.service_delegate.started().await
    }

    async fn stop(&mut self) -> Result<(), ServiceError> {
        self.service_delegate.stop().await?;

        let device_context = ProtoSdcDeviceContext {
            mdib_access: self.local_mdib.clone(),
            discovery_access: self.discovery_provider.clone(),
        };

        self.plugins.before_shutdown(&device_context).await?;

        self.primary_services.stop().await?;

        self.plugins.after_shutdown(&device_context).await?;

        self.service_delegate.stopped().await
    }

    fn is_running(&self) -> bool {
        self.service_delegate.is_running()
    }

    fn state(&self) -> &ServiceState {
        self.service_delegate.state()
    }
}

#[cfg(test)]
mod test {
    use std::net::{IpAddr, SocketAddr};
    use std::time::Duration;

    use anyhow::Result;
    use env_logger;

    use common::Service;
    use network::platform::test_util::determine_adapter_address_for_multicast;
    use network::udp_binding::IPV4_MULTICAST_ADDRESS;

    use crate::common::uuid::generate_provider_epr_uuid;
    use crate::provider::device::{SdcDevice, ServerConfig};
    use crate::provider::plugins::ScopesPlugin;

    use crate::provider::metadata::DeviceMetadata;
    use crate::provider::sco::DummyOperationInvocationReceiver;

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[tokio::test]
    async fn test_device() -> Result<()> {
        init();

        // find address which does ipv4 multicast
        let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
        let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

        let server_config = ServerConfig {
            crypto_config: None,
            server_address: SocketAddr::new(adapter_address, 0),
        };

        let dummy_op_inv_rec = DummyOperationInvocationReceiver {};

        let mut device = SdcDevice::new(
            "wat:test_seq".to_string(),
            server_config,
            dummy_op_inv_rec,
            generate_provider_epr_uuid(),
            Some(DeviceMetadata::default()),
            None,
            None,
            ScopesPlugin::default(),
        )
        .await
        .unwrap();

        device.start().await?;

        tokio::time::sleep(Duration::from_secs(2)).await;

        device.stop().await?;
        Ok(())
    }
}
