use std::collections::HashMap;
use std::sync::Arc;

use log::{debug, error, info, trace, warn};
use tokio::sync::{mpsc, Mutex};
use tokio::task;

use biceps::common::access::mdib_access::MdibAccessEvent;
use biceps::common::mdib_entity::{EntityBase, MdibEntity};
use biceps::common::mdib_version::MdibVersion;
use protosdc_biceps::biceps::{
    abstract_alert_report_mod, abstract_component_report_mod, abstract_context_report_mod,
    abstract_metric_report_mod, abstract_operational_state_report_mod,
    description_modification_report_mod, description_modification_type_mod, AbstractAlertReport,
    AbstractAlertStateOneOf, AbstractComponentReport, AbstractContextReport,
    AbstractContextStateOneOf, AbstractDeviceComponentStateOneOf, AbstractMetricReport,
    AbstractMetricStateOneOf, AbstractOperationStateOneOf, AbstractOperationalStateReport,
    AbstractReport, AbstractReportPart, DescriptionModificationReport, DescriptionModificationType,
    EpisodicAlertReport, EpisodicComponentReport, EpisodicContextReport, EpisodicMetricReport,
    EpisodicOperationalStateReport, HandleRef, RealTimeSampleArrayMetricState, WaveformStream,
};

use crate::common::reports::EpisodicReport;

pub struct ReportGenerator {
    episodic_senders: Arc<Mutex<Vec<mpsc::Sender<EpisodicReport>>>>,
    // TODO: receiver task watchdog to monitor task health
    receiver_task: Option<task::JoinHandle<()>>,
}

fn build_abstract_report(mdib_version: &MdibVersion) -> AbstractReport {
    AbstractReport {
        extension_element: None,
        mdib_version_group_attr: mdib_version.into(),
    }
}

fn build_abstract_report_part(source_mds: Option<String>) -> AbstractReportPart {
    AbstractReportPart {
        extension_element: None,
        source_mds: source_mds.map(|it| HandleRef { string: it }),
    }
}

fn build_handle_ref(handle: Option<String>) -> Option<HandleRef> {
    handle.map(|it| HandleRef { string: it })
}

fn append_part(
    report: &mut DescriptionModificationReport,
    entities: &[MdibEntity],
    modification: description_modification_type_mod::EnumType,
) {
    let parts: Vec<description_modification_report_mod::ReportPart> = entities
        .iter()
        .map(|it| description_modification_report_mod::ReportPart {
            abstract_report_part: build_abstract_report_part(Some(it.entity.mds())),
            descriptor: vec![it.entity.get_abstract_descriptor()],
            state: match modification {
                description_modification_type_mod::EnumType::Del => vec![],
                _ => it.resolve_states(),
            },
            parent_descriptor_attr: build_handle_ref(it.resolve_parent()),
            modification_type_attr: Some(DescriptionModificationType {
                enum_type: modification.clone(),
            }),
        })
        .collect();

    report.report_part.extend(parts);
}

async fn process_report_episodic_alert(
    mdib_version: MdibVersion,
    states: HashMap<String, Vec<AbstractAlertStateOneOf>>,
) -> EpisodicReport {
    EpisodicReport::Alert(Box::new(EpisodicAlertReport {
        abstract_alert_report: AbstractAlertReport {
            abstract_report: build_abstract_report(&mdib_version),
            report_part: states
                .into_iter()
                .map(
                    |(source_mds, states)| abstract_alert_report_mod::ReportPart {
                        abstract_report_part: build_abstract_report_part(Some(source_mds)),
                        alert_state: states,
                    },
                )
                .collect(),
        },
    }))
}

async fn process_report_episodic_component(
    mdib_version: MdibVersion,
    states: HashMap<String, Vec<AbstractDeviceComponentStateOneOf>>,
) -> EpisodicReport {
    EpisodicReport::Component(Box::new(EpisodicComponentReport {
        abstract_component_report: AbstractComponentReport {
            abstract_report: build_abstract_report(&mdib_version),
            report_part: states
                .into_iter()
                .map(
                    |(source_mds, states)| abstract_component_report_mod::ReportPart {
                        abstract_report_part: build_abstract_report_part(Some(source_mds)),
                        component_state: states,
                    },
                )
                .collect(),
        },
    }))
}

async fn process_report_episodic_context(
    mdib_version: MdibVersion,
    removed_context_states: HashMap<String, Vec<AbstractContextStateOneOf>>,
    updated_context_states: HashMap<String, Vec<AbstractContextStateOneOf>>,
) -> EpisodicReport {
    EpisodicReport::Context(Box::new(EpisodicContextReport {
        abstract_context_report: AbstractContextReport {
            abstract_report: build_abstract_report(&mdib_version),
            report_part: removed_context_states
                .into_iter()
                .chain(updated_context_states.into_iter())
                .map(
                    |(source_mds, states)| abstract_context_report_mod::ReportPart {
                        abstract_report_part: build_abstract_report_part(Some(source_mds)),
                        context_state: states,
                    },
                )
                .collect(),
        },
    }))
}

async fn process_report_episodic_metric(
    mdib_version: MdibVersion,
    states: HashMap<String, Vec<AbstractMetricStateOneOf>>,
) -> EpisodicReport {
    EpisodicReport::Metric(Box::new(EpisodicMetricReport {
        abstract_metric_report: AbstractMetricReport {
            abstract_report: build_abstract_report(&mdib_version),
            report_part: states
                .into_iter()
                .map(
                    |(source_mds, states)| abstract_metric_report_mod::ReportPart {
                        abstract_report_part: build_abstract_report_part(Some(source_mds)),
                        metric_state: states,
                    },
                )
                .collect(),
        },
    }))
}

async fn process_report_episodic_operation(
    mdib_version: MdibVersion,
    states: HashMap<String, Vec<AbstractOperationStateOneOf>>,
) -> EpisodicReport {
    EpisodicReport::Operation(Box::new(EpisodicOperationalStateReport {
        abstract_operational_state_report: AbstractOperationalStateReport {
            abstract_report: build_abstract_report(&mdib_version),
            report_part: states
                .into_iter()
                .map(
                    |(source_mds, states)| abstract_operational_state_report_mod::ReportPart {
                        abstract_report_part: build_abstract_report_part(Some(source_mds)),
                        operation_state: states,
                    },
                )
                .collect(),
        },
    }))
}

async fn process_report_episodic_waveform(
    mdib_version: MdibVersion,
    states: Vec<RealTimeSampleArrayMetricState>,
) -> EpisodicReport {
    EpisodicReport::Waveform(Box::new(WaveformStream {
        abstract_report: build_abstract_report(&mdib_version),
        state: states,
    }))
}

async fn process_report_episodic(event: MdibAccessEvent) -> Vec<EpisodicReport> {
    match event {
        MdibAccessEvent::AlertStateModification {
            mdib_version,
            states,
        } => {
            vec![process_report_episodic_alert(mdib_version, states).await]
        }
        MdibAccessEvent::ComponentStateModification {
            mdib_version,
            states,
        } => {
            vec![process_report_episodic_component(mdib_version, states).await]
        }
        MdibAccessEvent::ContextStateModification {
            mdib_version,
            removed_context_states,
            updated_context_states,
        } => {
            vec![
                process_report_episodic_context(
                    mdib_version,
                    removed_context_states,
                    updated_context_states,
                )
                .await,
            ]
        }
        MdibAccessEvent::MetricStateModification {
            mdib_version,
            states,
        } => {
            vec![process_report_episodic_metric(mdib_version, states).await]
        }
        MdibAccessEvent::OperationStateModification {
            mdib_version,
            states,
        } => {
            vec![process_report_episodic_operation(mdib_version, states).await]
        }
        MdibAccessEvent::WaveformModification {
            mdib_version,
            states,
        } => {
            vec![process_report_episodic_waveform(mdib_version, states).await]
        }
        MdibAccessEvent::DescriptionModification {
            mdib_version,
            inserted,
            updated,
            deleted,
        } => {
            let mut report = DescriptionModificationReport {
                abstract_report: build_abstract_report(&mdib_version),
                report_part: vec![],
            };

            append_part(
                &mut report,
                &deleted,
                description_modification_type_mod::EnumType::Del,
            );
            append_part(
                &mut report,
                &inserted,
                description_modification_type_mod::EnumType::Crt,
            );
            append_part(
                &mut report,
                &updated,
                description_modification_type_mod::EnumType::Upt,
            );

            vec![EpisodicReport::Description(Box::new(report))]
        }
    }
}

async fn handle_episodic_event(event: MdibAccessEvent, senders: &[mpsc::Sender<EpisodicReport>]) {
    // don't do work if there are no receivers
    if senders.is_empty() {
        trace!("Not generating reports, no active receivers");
        return;
    }

    let reports = process_report_episodic(event).await;

    for report in reports {
        for sender in senders {
            debug!("Sending report {:?} to {:?}", report, sender);
            sender
                .send(report.clone())
                .await
                .map_err(|err| warn!("Error distributing report, no active receivers: {}", err))
                .ok();
        }
    }
}

impl ReportGenerator {
    pub fn new(mut mdib_receiver: mpsc::Receiver<MdibAccessEvent>) -> Self {
        let sender_vec = Arc::new(Mutex::new(vec![]));

        let sender_vec_task = sender_vec.clone();
        let receiver_task = tokio::spawn(async move {
            info!("receiver_task started");
            loop {
                let message = mdib_receiver.recv().await;
                let senders = &mut sender_vec_task.lock().await;

                // sanitize senders, remove disconnected ones
                senders.retain(|it: &mpsc::Sender<EpisodicReport>| !it.is_closed());

                match message {
                    Some(msg) => handle_episodic_event(msg, senders).await,
                    None => {
                        error!("Channel was closed");
                        break;
                    }
                }
            }
            info!("receiver_task finished");
        });

        Self {
            episodic_senders: sender_vec,
            receiver_task: Some(receiver_task),
        }
    }

    pub async fn subscribe(&mut self) -> mpsc::Receiver<EpisodicReport> {
        let (tx, rx) = mpsc::channel(1);
        self.episodic_senders.lock().await.push(tx);
        rx
    }

    pub fn stop(&mut self) {
        if let Some(it) = self.receiver_task.take() {
            it.abort()
        }
    }
}

#[cfg(test)]
mod test {
    use crate::common::reports::EpisodicReport;
    use crate::provider::report_generator::ReportGenerator;
    use biceps::common::access::mdib_access::MdibAccessEvent;
    use biceps::common::mdib_entity::{MdibEntity, StringMetric};
    use biceps::common::mdib_pair::StringMetricPair;
    use biceps::common::mdib_version::MdibVersionBuilder;
    use biceps::test_util::mdib_utils::{
        create_minimal_string_metric_descriptor, create_minimal_string_metric_state,
    };
    use protosdc_biceps::biceps::{description_modification_type_mod, DescriptionModificationType};
    use std::time::Duration;
    use tokio::sync::mpsc;
    use tokio::time::timeout;

    #[tokio::test]
    async fn test_description_modification() {
        let (tx, rx) = mpsc::channel(1);
        let mut generator = ReportGenerator::new(rx);

        let mut sub = generator.subscribe().await;

        let handle = "myhandle";

        let desc = create_minimal_string_metric_descriptor(handle);
        let state = create_minimal_string_metric_state(handle);

        let string_metric = StringMetric {
            pair: StringMetricPair {
                descriptor: desc,
                state,
            },
            parent: "whatever".to_string(),
            mds: "some_mds".to_string(),
        };

        let mdib_entity = MdibEntity {
            last_changed: MdibVersionBuilder::default().seq("whatever").create(),
            entity: Box::new(string_metric).into(),
        };

        let description_delete = MdibAccessEvent::DescriptionModification {
            mdib_version: MdibVersionBuilder::default().seq("whatever").create(),
            inserted: vec![],
            updated: vec![],
            deleted: vec![mdib_entity],
        };

        timeout(Duration::from_secs(5), async move {
            tx.send(description_delete).await.unwrap();
            match sub.recv().await {
                None => panic!("No message received"),
                Some(msg) => match msg {
                    EpisodicReport::Description(desc_msg) => {
                        assert_eq!(1, desc_msg.report_part.len());
                        let part = desc_msg.report_part.first().unwrap();

                        assert_eq!(
                            Some(DescriptionModificationType {
                                enum_type: description_modification_type_mod::EnumType::Del
                            }),
                            part.modification_type_attr
                        );
                        // no states for delete
                        assert_eq!(0, part.state.len())
                    }
                    _ => panic!("Wrong kind of message received"),
                },
            }
        })
        .await
        .unwrap();
    }
}
