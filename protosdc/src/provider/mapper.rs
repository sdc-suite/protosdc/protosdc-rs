use futures::stream::{self, StreamExt};
use futures::TryStreamExt;
use itertools::Itertools;
use thiserror::Error;

use biceps::common::access::mdib_access::MdibAccess;
use biceps::common::biceps_util::{Descriptor, MetricDescriptor, OperationDescriptor};
use biceps::common::mdib_entity::{Entity, EntityBase, MdibEntity};
use biceps::{entity_filter, entity_filter_multi};
use protosdc_biceps::biceps::{
    AbstractComplexDeviceComponentDescriptor, AbstractStateOneOf, AlertConditionDescriptorOneOf,
    AlertSystemDescriptor, ChannelDescriptor, MdDescription, MdState, Mdib, MdibVersionGroup,
    MdsDescriptor, ScoDescriptor, SystemContextDescriptor, VersionCounter, VmdDescriptor,
};
use protosdc_biceps::types::ProtoUri;

#[derive(Debug, Error)]
pub enum MappingError {
    #[error("Mapper received wrong entity type, expected {expected}, got {received:?}")]
    WrongType {
        expected: &'static str,
        received: Entity,
    },
    #[error("Found too many descriptors")]
    CardinalityError,
    #[error("Unknown mapper error occurred.")]
    Other,
    #[error("Inconsistent storage, descriptor {parent:} was missing listed child: {child:}")]
    ChildMissing { parent: String, child: String },
    #[error("Children missing for {handle:}")]
    ChildrenMissing { handle: String },
}

macro_rules! match_descriptor_type {
    ($it:ident, $ent:ident) => {
        match $it.entity {
            Entity::$ent(it) => Ok(it.pair.descriptor),
            _ => Err(MappingError::WrongType {
                expected: stringify!($ent),
                received: $it.entity,
            }),
        }
    };
}

/// Returns an Mdib instance of the current mdib.
///
/// # Arguments
///
/// * `access`: mdib access
///
/// returns: Result<Mdib, MappingError>
pub async fn map_mdib<T>(access: &T) -> Result<Mdib, MappingError>
where
    T: MdibAccess + Sync,
{
    let mdib = Mdib {
        extension_element: None,
        md_description: Some(map_md_description(access, &[]).await?),
        md_state: Some(map_md_state(access, &[]).await?),
        mdib_version_group_attr: MdibVersionGroup {
            mdib_version_attr: Some(VersionCounter {
                unsigned_long: access.mdib_version().await.version,
            }),
            sequence_id_attr: ProtoUri {
                uri: access.mdib_version().await.sequence_id,
            },
            instance_id_attr: Some(access.mdib_version().await.instance_id),
        },
    };

    Ok(mdib)
}

/// Returns an MdDescription instance of the current mdib.
///
/// # Arguments
///
/// * `access`: mdib access
/// * `filter`: a filter to limit the result according to biceps
///
/// returns: Result<MdDescription, MappingError>
pub async fn map_md_description<T>(
    access: &T,
    filter: &[String],
) -> Result<MdDescription, MappingError>
where
    T: MdibAccess + Sync,
{
    let mut mds_entities: Vec<MdibEntity> = match filter.len() {
        0 => access.root_entities().await,
        _ => {
            stream::iter(filter)
                .filter_map(|handle| async move {
                    match access.entity(handle).await {
                        None => None,
                        Some(entity) => {
                            match matches!(entity.entity, Entity::Mds { .. }) {
                                true => Some(Ok(entity)),
                                false => {
                                    // find parent
                                    let mut current = entity;
                                    while let Some(parent) = current.resolve_parent() {
                                        match access.entity(&parent).await {
                                            None => return None,
                                            Some(ent) => {
                                                if matches!(ent.entity, Entity::Mds { .. }) {
                                                    return Some(Ok(ent));
                                                } else {
                                                    current = ent
                                                }
                                            }
                                        }
                                    }
                                    // welp, didn't work out
                                    Some(Err(MappingError::Other))
                                }
                            }
                        }
                    }
                })
                .try_collect()
                .await?
        }
    };

    mds_entities = mds_entities
        .into_iter()
        .unique_by(|x| x.entity.get_abstract_descriptor().handle())
        .collect();

    let mds_descriptors: Vec<MdsDescriptor> = stream::iter(mds_entities)
        .then(|mds| async { map_mds(access, mds).await })
        .try_collect()
        .await?;

    Ok(MdDescription {
        extension_element: None,
        mds: mds_descriptors,
        description_version_attr: Some(VersionCounter {
            unsigned_long: access.md_description_version().await,
        }),
    })
}

async fn get_child_states<T>(access: &T, entity: MdibEntity) -> Vec<AbstractStateOneOf>
where
    T: MdibAccess + Sync,
{
    let mut children: Vec<AbstractStateOneOf> = vec![];

    // dfs the entities
    let mut stack: Vec<MdibEntity> = vec![entity];
    while let Some(current) = stack.pop() {
        match current.resolve_children() {
            None => {}
            Some(children) => stack.extend(
                stream::iter(children)
                    .filter_map(|child| async move { access.entity(&child).await })
                    .collect::<Vec<MdibEntity>>()
                    .await,
            ),
        }
        children.extend(current.resolve_states());
    }

    children
}

/// Returns an MdState instance of the current mdib.
///
/// # Arguments
///
/// * `access`: mdib access
/// * `_filter`: a filter to limit the result. Currently unused, as everything is returned always
///
/// returns: Result<MdState, MappingError>
pub async fn map_md_state<T>(access: &T, _filter: &[String]) -> Result<MdState, MappingError>
where
    T: MdibAccess + Sync,
{
    // this cannot be inside a stream::iter, or it will trigger a compiler bug.
    // See https://github.com/rust-lang/rust/issues/82921
    // See https://github.com/rust-lang/rust/issues/79648
    let mut states: Vec<AbstractStateOneOf> = vec![];
    for entity in access.root_entities().await {
        states.extend(get_child_states(access, entity).await)
    }

    Ok(MdState {
        extension_element: None,
        state: states,
        state_version_attr: Some(VersionCounter {
            unsigned_long: access.md_state_version().await,
        }),
    })
}

pub async fn map_mds<T>(access: &T, entity: MdibEntity) -> Result<MdsDescriptor, MappingError>
where
    T: MdibAccess + Sync,
{
    // get mds descriptor
    let descriptor = match_descriptor_type!(entity, Mds)?;
    let descriptor_handle = descriptor.handle();
    let mut clocks = access
        .children_by_type(&descriptor.handle(), entity_filter!(Clock))
        .await;
    let mut system_contexts = access
        .children_by_type(&descriptor.handle(), entity_filter!(SystemContext))
        .await;

    let mds = MdsDescriptor {
        abstract_complex_device_component_descriptor:
            map_abstract_complex_device_component_descriptor(
                access,
                &descriptor_handle,
                descriptor.abstract_complex_device_component_descriptor,
            )
            .await?,
        system_context: match system_contexts.len() {
            0 => Ok(None),
            1 => {
                let it = system_contexts
                    .pop()
                    .expect("vec of len 1 did not contain an element, help?");
                Ok(Some(map_system_context(access, it).await?))
            }
            _ => Err(MappingError::CardinalityError),
        }?,
        clock: match clocks.len() {
            0 => Ok(None),
            1 => {
                let it = clocks
                    .pop()
                    .expect("vec of len 1 did not contain an element, help?");
                Ok(Some(match_descriptor_type!(it, Clock)?))
            }
            _ => Err(MappingError::CardinalityError),
        }?,
        battery: stream::iter(
            access
                .children_by_type(&descriptor_handle, entity_filter!(Battery))
                .await,
        )
        .map(|it: MdibEntity| match_descriptor_type!(it, Battery))
        .try_collect()
        .await?,
        vmd: stream::iter(
            access
                .children_by_type(&descriptor_handle, entity_filter!(Vmd))
                .await,
        )
        .then(|it: MdibEntity| async { map_vmd(access, it).await })
        .try_collect()
        .await?,
        ..descriptor
    };

    Ok(mds)
}

pub async fn map_vmd<T>(access: &T, entity: MdibEntity) -> Result<VmdDescriptor, MappingError>
where
    T: MdibAccess + Sync,
{
    let descriptor = match_descriptor_type!(entity, Vmd)?;
    let descriptor_handle = descriptor.handle();

    Ok(VmdDescriptor {
        abstract_complex_device_component_descriptor:
            map_abstract_complex_device_component_descriptor(
                access,
                &descriptor_handle,
                descriptor
                    .abstract_complex_device_component_descriptor
                    .clone(),
            )
            .await?,
        channel: stream::iter(
            access
                .children_by_type(&descriptor.handle(), entity_filter!(Channel))
                .await,
        )
        .then(|it: MdibEntity| async { map_channel(access, it).await })
        .try_collect()
        .await?,
        ..descriptor
    })
}

pub async fn map_channel<T>(
    access: &T,
    entity: MdibEntity,
) -> Result<ChannelDescriptor, MappingError>
where
    T: MdibAccess + Sync,
{
    let children = entity
        .resolve_children()
        .ok_or_else(|| MappingError::ChildrenMissing {
            handle: entity.entity.handle(),
        })?;
    let descriptor = match_descriptor_type!(entity, Channel)?;

    Ok(ChannelDescriptor {
        metric: stream::iter(children)
            .then(|handle| async move {
                match access.entity(&handle).await {
                    None => Err(MappingError::ChildMissing {
                        parent: "unknown".to_string(),
                        child: handle.clone(),
                    }),
                    Some(child) => match child.entity {
                        Entity::StringMetric(op) => {
                            Ok(op.pair.descriptor.into_abstract_metric_descriptor_one_of())
                        }
                        Entity::EnumStringMetric(op) => {
                            Ok(op.pair.descriptor.into_abstract_metric_descriptor_one_of())
                        }
                        Entity::NumericMetric(op) => {
                            Ok(op.pair.descriptor.into_abstract_metric_descriptor_one_of())
                        }
                        Entity::RealTimeSampleArrayMetric(op) => {
                            Ok(op.pair.descriptor.into_abstract_metric_descriptor_one_of())
                        }
                        Entity::DistributionSampleArrayMetric(op) => {
                            Ok(op.pair.descriptor.into_abstract_metric_descriptor_one_of())
                        }
                        _ => Err(MappingError::WrongType {
                            expected: "Metric",
                            received: child.entity,
                        }),
                    },
                }
            })
            .try_collect()
            .await?,
        ..descriptor
    })
}

pub async fn map_system_context<T>(
    access: &T,
    entity: MdibEntity,
) -> Result<SystemContextDescriptor, MappingError>
where
    T: MdibAccess + Sync,
{
    let descriptor = match_descriptor_type!(entity, SystemContext)?;

    let mut patients = access
        .children_by_type(&descriptor.handle(), entity_filter!(PatientContext))
        .await;
    let mut locations = access
        .children_by_type(&descriptor.handle(), entity_filter!(LocationContext))
        .await;

    Ok(SystemContextDescriptor {
        patient_context: match patients.len() {
            0 => Ok(None),
            1 => {
                let it = patients
                    .pop()
                    .expect("vec of len 1 did not contain an element, help?");
                Ok(Some(match_descriptor_type!(it, PatientContext)?))
            }
            _ => Err(MappingError::CardinalityError),
        }?,
        location_context: match locations.len() {
            0 => Ok(None),
            1 => {
                let it = locations
                    .pop()
                    .expect("vec of len 1 did not contain an element, help?");
                Ok(Some(match_descriptor_type!(it, LocationContext)?))
            }
            _ => Err(MappingError::CardinalityError),
        }?,
        ensemble_context: stream::iter(
            access
                .children_by_type(&descriptor.handle(), entity_filter!(EnsembleContext))
                .await,
        )
        .map(|it: MdibEntity| match_descriptor_type!(it, EnsembleContext))
        .try_collect()
        .await?,
        operator_context: stream::iter(
            access
                .children_by_type(&descriptor.handle(), entity_filter!(OperatorContext))
                .await,
        )
        .map(|it: MdibEntity| match_descriptor_type!(it, OperatorContext))
        .try_collect()
        .await?,
        workflow_context: stream::iter(
            access
                .children_by_type(&descriptor.handle(), entity_filter!(WorkflowContext))
                .await,
        )
        .map(|it: MdibEntity| match_descriptor_type!(it, WorkflowContext))
        .try_collect()
        .await?,
        means_context: stream::iter(
            access
                .children_by_type(&descriptor.handle(), entity_filter!(MeansContext))
                .await,
        )
        .map(|it: MdibEntity| match_descriptor_type!(it, MeansContext))
        .try_collect()
        .await?,
        ..descriptor
    })
}

pub async fn map_abstract_complex_device_component_descriptor<T>(
    access: &T,
    handle: &str,
    parent_descriptor: AbstractComplexDeviceComponentDescriptor,
) -> Result<AbstractComplexDeviceComponentDescriptor, MappingError>
where
    T: MdibAccess + Sync,
{
    let mut sco_entities = access.children_by_type(handle, entity_filter!(Sco)).await;
    let mut alert_systems = access
        .children_by_type(handle, entity_filter!(AlertSystem))
        .await;

    Ok(AbstractComplexDeviceComponentDescriptor {
        alert_system: match alert_systems.len() {
            0 => Ok(None),
            1 => {
                let it = alert_systems
                    .pop()
                    .expect("vec of len 1 did not contain an element, help?");
                Ok(Some(map_alert_system(access, it).await?))
            }
            _ => Err(MappingError::CardinalityError),
        }?,
        sco: match sco_entities.len() {
            0 => Ok(None),
            1 => {
                let it = sco_entities
                    .pop()
                    .expect("vec of len 1 did not contain an element, help?");
                Ok(Some(map_sco(access, it).await?))
            }
            _ => Err(MappingError::CardinalityError),
        }?,
        ..parent_descriptor
    })
}

pub async fn map_alert_system<T>(
    access: &T,
    entity: MdibEntity,
) -> Result<AlertSystemDescriptor, MappingError>
where
    T: MdibAccess + Sync,
{
    let descriptor = match_descriptor_type!(entity, AlertSystem)?;

    Ok(AlertSystemDescriptor {
        alert_condition: stream::iter(
            access
                .children_by_type(
                    &descriptor.handle(),
                    entity_filter_multi!(AlertCondition, LimitAlertCondition),
                )
                .await,
        )
        .map(|it: MdibEntity| match it.entity {
            Entity::LimitAlertCondition(cond) => Ok(
                AlertConditionDescriptorOneOf::LimitAlertConditionDescriptor(cond.pair.descriptor),
            ),
            Entity::AlertCondition(cond) => Ok(
                AlertConditionDescriptorOneOf::AlertConditionDescriptor(cond.pair.descriptor),
            ),
            _ => Err(MappingError::WrongType {
                expected: "AlertCondition or LimitAlertCondition",
                received: it.entity,
            }),
        })
        .try_collect()
        .await?,
        alert_signal: stream::iter(
            access
                .children_by_type(&descriptor.handle(), entity_filter!(AlertSignal))
                .await,
        )
        .map(|it: MdibEntity| match_descriptor_type!(it, AlertSignal))
        .try_collect()
        .await?,
        ..descriptor
    })
}

pub async fn map_sco<T>(access: &T, entity: MdibEntity) -> Result<ScoDescriptor, MappingError>
where
    T: MdibAccess + Sync,
{
    // TODO LDe: Clone should be later, no need to clone the entire entity really.
    let clone = entity.clone();
    let descriptor = match_descriptor_type!(clone, Sco)?;

    Ok(ScoDescriptor {
        operation: stream::iter(entity.resolve_children().unwrap())
            .then(|handle| async move {
                match access.entity(&handle).await {
                    None => Err(MappingError::Other), // TODO: Proper error
                    Some(child) => match child.entity {
                        Entity::ActivateOperation(op) => Ok(op
                            .pair
                            .descriptor
                            .into_abstract_operation_descriptor_one_of()),
                        Entity::SetContextStateOperation(op) => Ok(op
                            .pair
                            .descriptor
                            .into_abstract_operation_descriptor_one_of()),
                        Entity::SetValueOperation(op) => Ok(op
                            .pair
                            .descriptor
                            .into_abstract_operation_descriptor_one_of()),
                        Entity::SetMetricStateOperation(op) => Ok(op
                            .pair
                            .descriptor
                            .into_abstract_operation_descriptor_one_of()),
                        Entity::SetAlertStateOperation(op) => Ok(op
                            .pair
                            .descriptor
                            .into_abstract_operation_descriptor_one_of()),
                        Entity::SetComponentStateOperation(op) => Ok(op
                            .pair
                            .descriptor
                            .into_abstract_operation_descriptor_one_of()),
                        Entity::SetStringOperation(op) => Ok(op
                            .pair
                            .descriptor
                            .into_abstract_operation_descriptor_one_of()),
                        _ => Err(MappingError::WrongType {
                            expected: "Operation",
                            received: child.entity,
                        }),
                    },
                }
            })
            .try_collect()
            .await?,
        ..descriptor
    })
}
