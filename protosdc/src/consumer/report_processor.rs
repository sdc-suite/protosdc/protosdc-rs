use std::cmp::Ordering;
use std::sync::atomic::AtomicBool;
use std::sync::Arc;

use async_trait::async_trait;
use log::{error, info, trace};
use tokio::sync::Mutex;

use biceps::common::access::mdib_access::MdibAccess;
use biceps::common::mdib_version::MdibVersion;
use biceps::common::preprocessing::{DescriptionPreprocessor, StatePreprocessor};
use biceps::common::storage::mdib_storage::MdibStorageImpl;
use biceps::consumer::access::remote_mdib_access::{
    InternalRemoteMdibAccess, RemoteMdibAccess, RemoteMdibAccessImpl,
};

use crate::common::reports::EpisodicReport;
use crate::consumer::report_writer;
use crate::consumer::report_writer::ReportWriterError;

#[async_trait]
pub trait ReportProcessor: Sync {
    type MDIB: RemoteMdibAccess;
    async fn process_episodic_report(
        &mut self,
        report: EpisodicReport,
    ) -> Result<(), ReportWriterError>;
    async fn start_applying_reports(&mut self, mdib: Self::MDIB) -> Result<(), ReportWriterError>;
}

pub struct ReportProcessorImpl<T>
where
    T: RemoteMdibAccess,
{
    buffered_reports: Vec<EpisodicReport>,
    // keep this in a mutex to avoid concurrent out-of-order writes from the report processor itself
    mdib: Option<Arc<Mutex<T>>>,
    buffering: AtomicBool,
}

fn get_report_mdib_version(report: &EpisodicReport) -> MdibVersion {
    match report {
        EpisodicReport::Alert(report) => report
            .abstract_alert_report
            .abstract_report
            .mdib_version_group_attr
            .clone()
            .into(),
        EpisodicReport::Component(report) => report
            .abstract_component_report
            .abstract_report
            .mdib_version_group_attr
            .clone()
            .into(),
        EpisodicReport::Context(report) => report
            .abstract_context_report
            .abstract_report
            .mdib_version_group_attr
            .clone()
            .into(),
        EpisodicReport::Metric(report) => report
            .abstract_metric_report
            .abstract_report
            .mdib_version_group_attr
            .clone()
            .into(),
        EpisodicReport::Operation(report) => report
            .abstract_operational_state_report
            .abstract_report
            .mdib_version_group_attr
            .clone()
            .into(),
        EpisodicReport::Waveform(report) => report
            .abstract_report
            .mdib_version_group_attr
            .clone()
            .into(),
        EpisodicReport::Description(report) => report
            .abstract_report
            .mdib_version_group_attr
            .clone()
            .into(),
    }
}

impl<DESC, STATE> Default
    for ReportProcessorImpl<RemoteMdibAccessImpl<MdibStorageImpl, DESC, STATE>>
where
    DESC: DescriptionPreprocessor<MdibStorageImpl> + Send + Sync,
    STATE: StatePreprocessor<MdibStorageImpl> + Send + Sync,
{
    fn default() -> Self {
        Self {
            buffered_reports: vec![],
            mdib: None,
            buffering: AtomicBool::new(true),
        }
    }
}

async fn write_report_unlocked<T>(
    mdib: &mut T,
    report: EpisodicReport,
) -> Result<(), ReportWriterError>
where
    T: InternalRemoteMdibAccess + Send + Sync,
{
    report_writer::write_report(mdib, report).await?;
    Ok(())
}

#[async_trait]
impl<DESC, STATE> ReportProcessor
    for ReportProcessorImpl<RemoteMdibAccessImpl<MdibStorageImpl, DESC, STATE>>
where
    DESC: DescriptionPreprocessor<MdibStorageImpl> + Send + Sync,
    STATE: StatePreprocessor<MdibStorageImpl> + Send + Sync,
{
    type MDIB = RemoteMdibAccessImpl<MdibStorageImpl, DESC, STATE>;
    async fn process_episodic_report(
        &mut self,
        report: EpisodicReport,
    ) -> Result<(), ReportWriterError> {
        if *self.buffering.get_mut() {
            info!("Buffering report with version {:?}", &report.mdib_version());

            self.buffered_reports.push(report);
            return Ok(());
        }

        trace!(
            "Processing report with version {:?}",
            &report.mdib_version()
        );

        // after buffering is disabled, this must be present
        let mut unlocked_mdib = self.mdib.as_mut().unwrap().lock().await;
        write_report_unlocked(&mut *unlocked_mdib, report).await
    }

    async fn start_applying_reports(&mut self, mdib: Self::MDIB) -> Result<(), ReportWriterError> {
        if !*self.buffering.get_mut() {
            error!("Already applying to mdib");
            return Ok(());
        }
        info!(
            "Applying buffered reports to mdib, buffered: {}",
            &self.buffered_reports.len()
        );

        // lock mdib, process the buffer
        self.mdib = Some(Arc::new(Mutex::new(mdib)));

        // lock mdib so we can safely stop buffering and not confuse the order of reports
        let mut unlocked_mdib = self.mdib.as_mut().unwrap().lock().await;
        let current_mdib_version = unlocked_mdib.mdib_version().await;
        // stop buffering
        *self.buffering.get_mut() = false;

        for report in self.buffered_reports.drain(..) {
            // filter outdated or irrelevant
            let mdib_version = get_report_mdib_version(&report);
            let relevant = match current_mdib_version.cmp_to(&mdib_version) {
                None => false,
                Some(order) => {
                    // same instance id and current < converted => apply
                    current_mdib_version.instance_id == mdib_version.instance_id
                        && order == Ordering::Less
                }
            };
            if !relevant {
                continue;
            }
            write_report_unlocked(&mut *unlocked_mdib, report).await?;
        }

        info!("Done applying buffered reports to mdib");
        Ok(())
    }
}
