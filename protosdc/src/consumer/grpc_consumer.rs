use std::collections::{HashMap, HashSet};
use std::convert::TryInto;
use std::str::FromStr;

use async_trait::async_trait;
use futures::stream::{self, StreamExt};
use futures::TryStreamExt;
use hyper::http;
use itertools::Itertools;
use log::{debug, error, info, warn};
use protosdc_biceps::biceps::{
    GetContextStatesResponse, GetLocalizedText, GetLocalizedTextResponse, GetMdDescriptionResponse,
    GetMdStateResponse, GetMdibResponse, GetSupportedLanguagesResponse,
};
use protosdc_mapping::MappingError;
use protosdc_proto::biceps::{
    AbstractGetMsg, GetContextStatesMsg, GetMdDescriptionMsg, GetMdStateMsg, GetMdibMsg,
    GetSupportedLanguagesMsg, HandleRefMsg,
};
use protosdc_proto::discovery::Endpoint;
use protosdc_proto::metadata::metadata_service_client::MetadataServiceClient;
use protosdc_proto::metadata::{GetMetadataRequest, GetMetadataResponse, Service};
use protosdc_proto::sdc::get_service_client::GetServiceClient;
use protosdc_proto::sdc::localization_service_client::LocalizationServiceClient;
use protosdc_proto::sdc::mdib_reporting_service_client::MdibReportingServiceClient;
use protosdc_proto::sdc::set_service_client::SetServiceClient;
use protosdc_proto::sdc::{
    GetContextStatesRequest, GetContextStatesResponse as ProtoGetContextStatesResponse,
    GetLocalizedTextRequest, GetLocalizedTextStream, GetMdDescriptionRequest,
    GetMdDescriptionResponse as ProtoGetMdDescriptionResponse, GetMdStateRequest,
    GetMdStateResponse as ProtoGetMdStateResponse, GetMdibRequest,
    GetMdibResponse as ProtoGetMdibResponse, GetSupportedLanguagesRequest,
    GetSupportedLanguagesResponse as ProtoGetSupportedLanguagesResponse,
};
use thiserror::Error;
use tonic::body::BoxBody;

use crate::addressing;
use crate::common::crypto::{CryptoConfig, CryptoError};
use crate::common::hyper_client::{
    create_client, get_dummy_uri, HyperClient, DUMMY_URI_HTTP, DUMMY_URI_HTTPS,
};
use crate::common::{action, service};

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ProviderService {
    Metadata,
    Get,
    Set,
    MdibReporting,
    Localization,
}

#[derive(Debug, Error)]
pub enum ConsumerError {
    #[error("Consumer is disconnected")]
    DisconnectedError,
    #[error("Provided endpoint {provided:?} did not match metadata service endpoint {actual:?}")]
    EndpointMismatch {
        provided: Box<Endpoint>,
        actual: Option<Box<Endpoint>>,
    },
    #[error("Endpoint {endpoint:?} provided more than one service of a type. {services:?}")]
    ServiceDuplicate {
        endpoint: Box<Endpoint>,
        services: Box<Vec<String>>,
    },

    #[error("Could not connect to present service {service:?}, encountered errors: {errors:?}")]
    ServiceConnection {
        service: ProviderService,
        errors: Box<HashMap<String, ConsumerError>>,
    },

    #[error("Endpoint {endpoint:?} did not provide a get service.")]
    NoGetService { endpoint: Box<Endpoint> },
    #[error(transparent)]
    CryptoError(#[from] CryptoError),
    #[error(transparent)]
    TonicTransportError(tonic::transport::Error),
    #[error(transparent)]
    TonicStatusError(tonic::Status),
    #[error(transparent)]
    InvalidUri(http::uri::InvalidUri),
    #[error("GetMdibResponse was not present/invalid")]
    InvalidGetMdibResponse,
    #[error("GetService response payload was not present/invalid")]
    InvalidGetServiceResponse,

    #[error("Service not present")]
    ServiceNotPresent,

    #[error("A mapping error occurred: {0:?}")]
    MappingError(#[from] MappingError),

    #[error(transparent)]
    HyperError(hyper::Error),
    #[error(transparent)]
    HyperLegacyError(hyper_util::client::legacy::Error),
    #[error(transparent)]
    HttpError(http::Error),

    #[error("Response was invalid: {0}")]
    InvalidResponse(String),
}

#[async_trait]
pub trait Consumer {
    /// Connects to the Provider at the given endpoint, must contain metadata service
    ///
    /// Calls the metadata service to determine available services and their endpoints.
    ///
    /// # Arguments
    ///
    /// * `endpoint`: to connect to
    /// * `crypto_config`: certificates to use
    ///
    /// returns: Result<(), ConsumerError>
    async fn connect(
        &mut self,
        endpoint: Endpoint,
        crypto_config: Option<CryptoConfig>,
    ) -> Result<(), ConsumerError>;

    /// Disconnects the Consumer, returns an Error if already disconnected.
    async fn disconnect(&mut self) -> Result<(), ConsumerError>;

    /// Returns the metadata service stub if connected.
    async fn get_metadata_service(&self) -> Option<MetadataServiceClient<HyperClient>>;

    /// Returns the get service stub if connected.
    async fn get_get_service(&self) -> Option<GetServiceClient<HyperClient>>;

    /// Returns the set service stub if connected and present.
    async fn get_set_service(&self) -> Option<SetServiceClient<HyperClient>>;

    /// Returns the mdib reporting service stub if connected and present.
    async fn get_mdib_reporting_service(&self) -> Option<MdibReportingServiceClient<HyperClient>>;

    /// Returns the localization service stub if connected and present.
    async fn get_localization_service(&self) -> Option<LocalizationServiceClient<HyperClient>>;

    fn is_connected(&self) -> bool;

    fn endpoint(&self) -> Option<&Endpoint>;
}

#[async_trait]
pub trait MetadataServiceConsumer: Consumer {
    async fn get_metadata(&self) -> Result<GetMetadataResponse, ConsumerError>;
}

#[async_trait]
pub trait GetServiceConsumer: Consumer {
    async fn get_mdib(&self) -> Result<GetMdibResponse, ConsumerError>;
    async fn get_md_state(&self, handles: Vec<String>)
        -> Result<GetMdStateResponse, ConsumerError>;
    async fn get_md_description(
        &self,
        handles: Vec<String>,
    ) -> Result<GetMdDescriptionResponse, ConsumerError>;
    async fn get_context_states(
        &self,
        handles: Vec<String>,
    ) -> Result<GetContextStatesResponse, ConsumerError>;
}

#[async_trait]
pub trait LocalizationServiceConsumer: Consumer {
    async fn get_localized_text(
        &self,
        request: GetLocalizedText,
    ) -> Result<GetLocalizedTextResponse, ConsumerError>;
    async fn get_supported_languages(&self)
        -> Result<GetSupportedLanguagesResponse, ConsumerError>;
}

#[derive(Debug)]
struct ConsumerImplInternal {
    metadata_service: MetadataServiceClient<HyperClient>,
    get_service: GetServiceClient<HyperClient>,
    set_service: Option<SetServiceClient<HyperClient>>,
    mdib_reporting_service: Option<MdibReportingServiceClient<HyperClient>>,
    localization_service: Option<LocalizationServiceClient<HyperClient>>,
}

#[derive(Debug, Default)]
pub struct ConsumerImpl {
    _client: Option<HyperClient>,
    internal: Option<ConsumerImplInternal>,
    endpoint: Option<Endpoint>,
}

enum ConnectResult {
    Success {
        client: Box<HyperClient>,
        uri: hyper::Uri,
    },
    Failure {
        errors: HashMap<String, ConsumerError>,
    },
}

impl ConsumerImpl {
    async fn create_client(
        address: &http::Uri,
        crypto: &Option<CryptoConfig>,
    ) -> Result<HyperClient, ConsumerError> {
        Ok(create_client(address, crypto).await?)
    }

    async fn try_post(
        http_client: &HyperClient,
        crypto_config: &Option<CryptoConfig>,
    ) -> Result<(), ConsumerError> {
        let target_uri = match crypto_config.is_some() {
            true => DUMMY_URI_HTTPS,
            false => DUMMY_URI_HTTP,
        };

        let body = hyper::Request::post(target_uri)
            .body(BoxBody::default())
            .map_err(ConsumerError::HttpError)?;
        http_client
            .request(body)
            .await
            .map_err(ConsumerError::HyperLegacyError)?;
        Ok(())
    }

    async fn try_connect_new(
        addr: &hyper::Uri,
        crypto_config: &Option<CryptoConfig>,
    ) -> Result<HyperClient, ConsumerError> {
        let client = Self::create_client(addr, crypto_config).await?;
        Self::try_post(&client, crypto_config).await?;
        info!("Connected HyperClient to {}", addr);
        Ok(client)
    }

    fn find_service_for_type<'a>(
        service: &'a [Service],
        service_type: &str,
    ) -> Option<&'a Service> {
        service
            .iter()
            .find(|service| service.r#type.iter().any(|it| it.as_str() == service_type))
    }

    async fn find_hyper_client_for_service(
        addresses: &[hyper::Uri],
        client_map: &mut HashMap<String, HyperClient>,
        crypto_config: &Option<CryptoConfig>,
        service_kind: ProviderService,
    ) -> Result<(Box<HyperClient>, hyper::Uri), ConsumerError> {
        Ok(
            match Self::find_hyper_client_for_uri(addresses, client_map, crypto_config).await {
                ConnectResult::Success { client, uri } => (client, uri),
                ConnectResult::Failure { errors } => Err(ConsumerError::ServiceConnection {
                    service: service_kind,
                    errors: Box::new(errors),
                })?,
            },
        )
    }

    async fn find_hyper_client_for_uri(
        addresses: &[hyper::Uri],
        client_map: &mut HashMap<String, HyperClient>,
        crypto_config: &Option<CryptoConfig>,
    ) -> ConnectResult {
        let result = stream::iter(addresses)
            .fold(
                (client_map, None, None, None),
                |(map, client, uri, error_map), addr| async move {
                    // short circuit if we already have a client
                    if client.is_some() {
                        return (map, client, uri, error_map);
                    }

                    // check map
                    let c: (
                        Option<HyperClient>,
                        Option<hyper::Uri>,
                        Option<HashMap<String, ConsumerError>>,
                    ) = match map.get(&addr.to_string()) {
                        Some(cl) => (Some(cl.clone()), Some(addr.clone()), error_map),
                        None => match Self::try_connect_new(addr, crypto_config).await {
                            Ok(client) => {
                                map.insert(addr.to_string(), client.clone());
                                (Some(client), Some(addr.clone()), error_map)
                            }
                            Err(err) => {
                                // add error to map
                                let mut err_map = error_map.unwrap_or_default();
                                warn!("Error connecting to {}: {:?}", &addr, err);
                                err_map.insert(addr.to_string(), err);
                                (None, None, Some(err_map))
                            }
                        },
                    };

                    (map, c.0, c.1, c.2)
                },
            )
            .await;

        if let (_, Some(c), Some(u), _) = result {
            ConnectResult::Success {
                client: Box::new(c),
                uri: u,
            }
        } else {
            ConnectResult::Failure {
                errors: result.3.unwrap_or_default(),
            }
        }
    }

    fn proto_uri_to_hyper_uri(
        data: &[protosdc_proto::common::Uri],
    ) -> Result<Vec<http::Uri>, ConsumerError> {
        data.iter()
            .map(|it| http::Uri::from_str(it.value.as_str()).map_err(ConsumerError::InvalidUri))
            .collect::<Result<Vec<http::Uri>, ConsumerError>>()
    }

    async fn get_metadata(
        metadata_client: &mut MetadataServiceClient<HyperClient>,
    ) -> Result<GetMetadataResponse, ConsumerError> {
        Ok(metadata_client
            .get_metadata(tonic::Request::new(GetMetadataRequest {
                addressing: Some(addressing::util::create_for_action(
                    action::GET_METADATA_REQUEST,
                )),
            }))
            .await
            .map_err(ConsumerError::TonicStatusError)?
            .into_inner())
    }
}

#[async_trait]
impl Consumer for ConsumerImpl {
    async fn connect(
        &mut self,
        endpoint: Endpoint,
        crypto_config: Option<CryptoConfig>,
    ) -> Result<(), ConsumerError> {
        info!("Connecting to endpoint {:?}", &endpoint);

        // use String for map as Uri is considered mutable by clippy
        let mut client_map: HashMap<String, HyperClient> = HashMap::new();
        // find an address in the channel that connects
        let endpoint_addresses = Self::proto_uri_to_hyper_uri(&endpoint.physical_address)?;
        let (metadata_http_client, metadata_uri) = Self::find_hyper_client_for_service(
            &endpoint_addresses,
            &mut client_map,
            &crypto_config,
            ProviderService::Metadata,
        )
        .await?;

        let mut metadata_client = MetadataServiceClient::with_origin(
            *metadata_http_client.clone(),
            get_dummy_uri(&metadata_uri),
        );

        let metadata = ConsumerImpl::get_metadata(&mut metadata_client).await?;

        info!("Connected to metadata service at {}", &metadata_uri);

        // ensure endpoint data is identical
        match &metadata.endpoint {
            None => {
                return Err(ConsumerError::EndpointMismatch {
                    provided: Box::new(endpoint),
                    actual: None,
                });
            }
            Some(metadata_endpoint) => {
                if metadata_endpoint != &endpoint {
                    return Err(ConsumerError::EndpointMismatch {
                        provided: Box::new(endpoint),
                        actual: metadata.endpoint.map(Box::new),
                    });
                }
            }
        }

        // ensure no service type is available twice
        let available_service_types_vec = metadata
            .service
            .iter()
            .map(|service| service.r#type.clone())
            .concat();
        let mut available_service_types_set = HashSet::new();
        available_service_types_set.extend(&available_service_types_vec);

        if available_service_types_vec.len() != available_service_types_set.len() {
            return Err(ConsumerError::ServiceDuplicate {
                endpoint: Box::new(endpoint),
                services: Box::new(available_service_types_vec),
            });
        }

        // find get service address
        let get_service_service =
            Self::find_service_for_type(&metadata.service, service::GET_SERVICE).ok_or(
                ConsumerError::NoGetService {
                    endpoint: Box::new(endpoint.clone()),
                },
            )?;

        let get_service_addresses =
            Self::proto_uri_to_hyper_uri(&get_service_service.physical_address)?;
        let (get_service_http_client, get_service_uri) = Self::find_hyper_client_for_service(
            &get_service_addresses,
            &mut client_map,
            &crypto_config,
            ProviderService::Get,
        )
        .await?;

        let get_client = GetServiceClient::with_origin(
            *get_service_http_client,
            get_dummy_uri(&get_service_uri),
        );

        let set_service_service =
            Self::find_service_for_type(&metadata.service, service::SET_SERVICE);
        let set_service_client = match set_service_service {
            None => None,
            Some(service) => {
                let set_service_address = Self::proto_uri_to_hyper_uri(&service.physical_address)?;
                let (set_service_http_client, set_service_uri) =
                    Self::find_hyper_client_for_service(
                        &set_service_address,
                        &mut client_map,
                        &crypto_config,
                        ProviderService::Set,
                    )
                    .await?;
                Some(SetServiceClient::with_origin(
                    *set_service_http_client,
                    get_dummy_uri(&set_service_uri),
                ))
            }
        };

        let mdib_reporting_service_service =
            Self::find_service_for_type(&metadata.service, service::MDIB_REPORTING_SERVICE);
        let mdib_reporting_service_client = match mdib_reporting_service_service {
            None => None,
            Some(service) => {
                let mdib_service_address = Self::proto_uri_to_hyper_uri(&service.physical_address)?;
                let (mdib_service_http_client, mdib_service_uri) =
                    Self::find_hyper_client_for_service(
                        &mdib_service_address,
                        &mut client_map,
                        &crypto_config,
                        ProviderService::MdibReporting,
                    )
                    .await?;
                Some(MdibReportingServiceClient::with_origin(
                    *mdib_service_http_client,
                    get_dummy_uri(&mdib_service_uri),
                ))
            }
        };

        let localization_service_service =
            Self::find_service_for_type(&metadata.service, service::LOCALIZATION_SERVICE);
        let localization_service_client = match localization_service_service {
            None => None,
            Some(service) => {
                let localization_service_address =
                    Self::proto_uri_to_hyper_uri(&service.physical_address)?;
                let (localization_service_http_client, localization_service_uri) =
                    Self::find_hyper_client_for_service(
                        &localization_service_address,
                        &mut client_map,
                        &crypto_config,
                        ProviderService::Localization,
                    )
                    .await?;
                Some(LocalizationServiceClient::with_origin(
                    *localization_service_http_client,
                    get_dummy_uri(&localization_service_uri),
                ))
            }
        };

        self.internal = Some(ConsumerImplInternal {
            metadata_service: metadata_client,
            get_service: get_client,
            set_service: set_service_client,
            mdib_reporting_service: mdib_reporting_service_client,
            localization_service: localization_service_client,
        });

        self.endpoint = Some(endpoint);

        debug!("Clients after connect: {:?}", client_map);

        Ok(())
    }

    async fn disconnect(&mut self) -> Result<(), ConsumerError> {
        self.internal = None;
        Ok(())
    }

    async fn get_metadata_service(&self) -> Option<MetadataServiceClient<HyperClient>> {
        self.internal
            .as_ref()
            .map(|internal| internal.metadata_service.clone())
    }

    async fn get_get_service(&self) -> Option<GetServiceClient<HyperClient>> {
        self.internal
            .as_ref()
            .map(|internal| internal.get_service.clone())
    }

    async fn get_set_service(&self) -> Option<SetServiceClient<HyperClient>> {
        match &self.internal {
            None => None,
            Some(internal) => internal.set_service.as_ref().cloned(),
        }
    }

    async fn get_mdib_reporting_service(&self) -> Option<MdibReportingServiceClient<HyperClient>> {
        match &self.internal {
            None => None,
            Some(internal) => internal.mdib_reporting_service.as_ref().cloned(),
        }
    }

    async fn get_localization_service(&self) -> Option<LocalizationServiceClient<HyperClient>> {
        match &self.internal {
            None => None,
            Some(internal) => internal.localization_service.as_ref().cloned(),
        }
    }

    fn is_connected(&self) -> bool {
        self.internal.is_some()
    }

    fn endpoint(&self) -> Option<&Endpoint> {
        match &self.endpoint {
            None => None,
            Some(ep) => Some(ep),
        }
    }
}

#[async_trait]
impl LocalizationServiceConsumer for ConsumerImpl {
    async fn get_localized_text(
        &self,
        request: GetLocalizedText,
    ) -> Result<GetLocalizedTextResponse, ConsumerError> {
        let request = tonic::Request::new(GetLocalizedTextRequest {
            addressing: Some(addressing::util::create_for_action(
                action::GET_LOCALIZED_TEXT_REQUEST,
            )),
            payload: Some(request.into()),
        });

        let response = self
            .get_localization_service()
            .await
            .ok_or(ConsumerError::ServiceNotPresent)?
            .get_localized_text(request)
            .await
            .map_err(ConsumerError::TonicStatusError)?;

        // collect all responses
        let something = response
            .into_inner()
            .map_err(ConsumerError::TonicStatusError)
            .collect::<Vec<Result<GetLocalizedTextStream, ConsumerError>>>()
            .await;

        // inverted vec of results to result of vec
        let something_else = something
            .into_iter()
            .collect::<Result<Vec<GetLocalizedTextStream>, ConsumerError>>()?;

        // merge responses
        let something_completely_different = something_else
            .into_iter()
            .filter_map(|it| it.payload)
            .reduce(|mut acc, mut e| {
                acc.text.append(&mut e.text);
                acc
            });

        Ok(something_completely_different
            .ok_or(ConsumerError::InvalidResponse(
                "Not a single response stream element was received".to_string(),
            ))?
            .try_into()?)
    }

    async fn get_supported_languages(
        &self,
    ) -> Result<GetSupportedLanguagesResponse, ConsumerError> {
        let request = tonic::Request::new(GetSupportedLanguagesRequest {
            addressing: Some(addressing::util::create_for_action(
                action::GET_SUPPORTED_LANGUAGES_REQUEST,
            )),
            payload: Some(GetSupportedLanguagesMsg {
                abstract_get: Some(AbstractGetMsg {
                    extension_element: None,
                }),
            }),
        });

        let response = self
            .get_localization_service()
            .await
            .ok_or(ConsumerError::ServiceNotPresent)?
            .get_supported_languages(request)
            .await
            .map_err(ConsumerError::TonicStatusError)?;

        let ProtoGetSupportedLanguagesResponse {
            addressing: _,
            payload,
        } = response.into_inner();
        // TODO: verify addressing
        let get_supported_languages_response_msg = payload.ok_or(
            ConsumerError::InvalidResponse("No response body".to_string()),
        )?;

        get_supported_languages_response_msg
            .try_into()
            .map_err(ConsumerError::MappingError)
    }
}

#[async_trait]
impl GetServiceConsumer for ConsumerImpl {
    async fn get_mdib(&self) -> Result<GetMdibResponse, ConsumerError> {
        let request = tonic::Request::new(GetMdibRequest {
            addressing: Some(addressing::util::create_for_action(
                action::GET_MDIB_REQUEST,
            )),
            payload: Some(GetMdibMsg {
                abstract_get: Some(AbstractGetMsg {
                    extension_element: None,
                }),
            }),
        });

        let response = self
            .get_get_service()
            .await
            .ok_or(ConsumerError::ServiceNotPresent)?
            .get_mdib(request)
            .await
            .map_err(ConsumerError::TonicStatusError)?;

        let ProtoGetMdibResponse {
            addressing: _,
            payload,
        } = response.into_inner();
        // TODO: verify addressing

        let get_mdib_response_msg = payload.ok_or(ConsumerError::InvalidGetMdibResponse)?;

        get_mdib_response_msg
            .try_into()
            .map_err(ConsumerError::MappingError)
    }

    async fn get_md_state(
        &self,
        handles: Vec<String>,
    ) -> Result<GetMdStateResponse, ConsumerError> {
        let request = tonic::Request::new(GetMdStateRequest {
            addressing: Some(addressing::util::create_for_action(
                action::GET_MDIB_REQUEST,
            )),
            payload: Some(GetMdStateMsg {
                abstract_get: Some(AbstractGetMsg {
                    extension_element: None,
                }),
                handle_ref: handles
                    .into_iter()
                    .map(|it| HandleRefMsg { string: it })
                    .collect(),
            }),
        });

        let response = self
            .get_get_service()
            .await
            .ok_or(ConsumerError::ServiceNotPresent)?
            .get_md_state(request)
            .await
            .map_err(ConsumerError::TonicStatusError)?;

        let ProtoGetMdStateResponse {
            addressing: _,
            payload,
        } = response.into_inner();
        // TODO: verify addressing
        let get_md_state_response_msg = payload.ok_or(ConsumerError::InvalidGetServiceResponse)?;

        get_md_state_response_msg
            .try_into()
            .map_err(ConsumerError::MappingError)
    }

    async fn get_md_description(
        &self,
        handles: Vec<String>,
    ) -> Result<GetMdDescriptionResponse, ConsumerError> {
        let request = tonic::Request::new(GetMdDescriptionRequest {
            addressing: Some(addressing::util::create_for_action(
                action::GET_MD_DESCRIPTION_REQUEST,
            )),
            payload: Some(GetMdDescriptionMsg {
                abstract_get: Some(AbstractGetMsg {
                    extension_element: None,
                }),
                handle_ref: handles
                    .into_iter()
                    .map(|it| HandleRefMsg { string: it })
                    .collect(),
            }),
        });

        let response = self
            .get_get_service()
            .await
            .ok_or(ConsumerError::ServiceNotPresent)?
            .get_md_description(request)
            .await
            .map_err(ConsumerError::TonicStatusError)?;

        let ProtoGetMdDescriptionResponse {
            addressing: _,
            payload,
        } = response.into_inner();
        // TODO: verify addressing
        let get_md_description_response_msg =
            payload.ok_or(ConsumerError::InvalidGetServiceResponse)?;

        get_md_description_response_msg
            .try_into()
            .map_err(ConsumerError::MappingError)
    }

    async fn get_context_states(
        &self,
        handles: Vec<String>,
    ) -> Result<GetContextStatesResponse, ConsumerError> {
        let request = tonic::Request::new(GetContextStatesRequest {
            addressing: Some(addressing::util::create_for_action(
                action::GET_CONTEXT_STATES_REQUEST,
            )),
            payload: Some(GetContextStatesMsg {
                abstract_get: Some(AbstractGetMsg {
                    extension_element: None,
                }),

                handle_ref: handles
                    .into_iter()
                    .map(|it| HandleRefMsg { string: it })
                    .collect(),
            }),
        });

        let response = self
            .get_get_service()
            .await
            .ok_or(ConsumerError::ServiceNotPresent)?
            .get_context_states(request)
            .await
            .map_err(ConsumerError::TonicStatusError)?;

        let ProtoGetContextStatesResponse {
            addressing: _,
            payload,
        } = response.into_inner();
        // TODO: verify addressing
        let get_context_states_response_msg =
            payload.ok_or(ConsumerError::InvalidGetServiceResponse)?;

        get_context_states_response_msg
            .try_into()
            .map_err(ConsumerError::MappingError)
    }
}

#[async_trait]
impl MetadataServiceConsumer for ConsumerImpl {
    async fn get_metadata(&self) -> Result<GetMetadataResponse, ConsumerError> {
        Self::get_metadata(
            &mut self
                .get_metadata_service()
                .await
                .ok_or(ConsumerError::ServiceNotPresent)?,
        )
        .await
    }
}

impl ConsumerImpl {
    //noinspection HttpUrlsUsage
    pub fn add_http_prefix(uri: &str) -> String {
        let url = url::Url::parse(uri).unwrap();
        format!("http://{}:{}", url.host_str().unwrap(), url.port().unwrap())
    }

    pub fn add_https_prefix(uri: &str) -> String {
        let url = url::Url::parse(uri).unwrap();
        format!(
            "https://{}:{}",
            url.host_str().unwrap(),
            url.port().unwrap()
        )
    }

    pub fn add_http_prefix_crypto_config(
        uri: &str,
        crypto_config: &Option<CryptoConfig>,
    ) -> String {
        match crypto_config {
            None => Self::add_http_prefix(uri),
            Some(_) => Self::add_https_prefix(uri),
        }
    }

    pub fn add_http_prefix_to_uri(
        uri: &protosdc_proto::common::Uri,
    ) -> protosdc_proto::common::Uri {
        protosdc_proto::common::Uri {
            value: Self::add_http_prefix(&uri.value),
        }
    }

    pub fn add_https_prefix_to_uri(
        uri: &protosdc_proto::common::Uri,
    ) -> protosdc_proto::common::Uri {
        protosdc_proto::common::Uri {
            value: Self::add_https_prefix(&uri.value),
        }
    }

    pub fn add_http_prefix_to_uri_crypto_config(
        uri: &protosdc_proto::common::Uri,
        crypto_config: &Option<CryptoConfig>,
    ) -> protosdc_proto::common::Uri {
        match crypto_config {
            None => Self::add_http_prefix_to_uri(uri),
            Some(_) => Self::add_https_prefix_to_uri(uri),
        }
    }

    pub fn add_http_prefix_to_uris(
        uri: &[protosdc_proto::common::Uri],
    ) -> Vec<protosdc_proto::common::Uri> {
        uri.iter().map(Self::add_http_prefix_to_uri).collect()
    }

    pub fn add_http_prefixes_to_uris(uri: &[String]) -> Vec<String> {
        uri.iter().map(|it| Self::add_http_prefix(it)).collect()
    }

    pub fn add_https_prefix_to_uris(
        uri: &[protosdc_proto::common::Uri],
    ) -> Vec<protosdc_proto::common::Uri> {
        uri.iter().map(Self::add_https_prefix_to_uri).collect()
    }

    pub fn add_https_prefixes_to_uris(uri: &[String]) -> Vec<String> {
        uri.iter().map(|it| Self::add_https_prefix(it)).collect()
    }

    pub fn add_http_prefix_to_uris_crypto_config(
        uri: &[protosdc_proto::common::Uri],
        crypto_config: &Option<CryptoConfig>,
    ) -> Vec<protosdc_proto::common::Uri> {
        match crypto_config {
            None => Self::add_http_prefix_to_uris(uri),
            Some(_) => Self::add_https_prefix_to_uris(uri),
        }
    }

    pub fn add_http_prefixes_crypto_config(
        uri: &[String],
        crypto_config: &Option<CryptoConfig>,
    ) -> Vec<String> {
        match crypto_config {
            None => Self::add_http_prefixes_to_uris(uri),
            Some(_) => Self::add_https_prefixes_to_uris(uri),
        }
    }

    pub fn add_http_prefix_to_endpoint(endpoint: Endpoint) -> Endpoint {
        Endpoint {
            physical_address: Self::add_http_prefix_to_uris(&endpoint.physical_address),
            ..endpoint.clone()
        }
    }

    pub fn add_https_prefix_to_endpoint(endpoint: Endpoint) -> Endpoint {
        Endpoint {
            physical_address: Self::add_https_prefix_to_uris(&endpoint.physical_address),
            ..endpoint.clone()
        }
    }

    pub fn add_http_prefix_to_endpoint_crypto_config(
        endpoint: Endpoint,
        crypto_config: &Option<CryptoConfig>,
    ) -> Endpoint {
        match crypto_config {
            None => Self::add_http_prefix_to_endpoint(endpoint),
            Some(_) => Self::add_https_prefix_to_endpoint(endpoint),
        }
    }
}
