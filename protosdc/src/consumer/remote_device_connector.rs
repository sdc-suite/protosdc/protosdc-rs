use std::convert::TryInto;
use std::sync::Arc;
use std::time::Duration;

use log::{debug, error, info, trace};
use thiserror::Error;
use tokio::sync::{oneshot, Mutex};

use crate::common::mdib::{modifications_from_mdib, MdibError};
use biceps::common::mdib_description_modification::MdibDescriptionModification;
use biceps::common::mdib_description_modifications::{
    MdibDescriptionModifications, ModificationError,
};
use biceps::common::mdib_version::MdibVersion;
use biceps::common::storage::mdib_storage::WriteError;
use biceps::consumer::access::remote_mdib_access::{
    InternalRemoteMdibAccess, RemoteMdibAccessImpl,
};
use common::oneshot_log_error;
use protosdc_mapping::MappingError;
use protosdc_proto::sdc::episodic_report_stream::Message;
use protosdc_proto::sdc::{
    operation_invoked_report_stream, EpisodicReportRequest, OperationInvokedReportRequest,
};
use tokio::time::timeout;

use crate::common::reports::EpisodicReport;
use crate::consumer::grpc_consumer::{Consumer, ConsumerError, ConsumerImpl, GetServiceConsumer};
use crate::consumer::remote_device::{DefaultRemoteMdibAccessImpl, SdcRemoteDevice};
use crate::consumer::report_processor::{ReportProcessor, ReportProcessorImpl};
use crate::consumer::report_writer::ReportWriterError;
use crate::consumer::sco::sco_controller::{
    ProtoSetServiceHandler, ScoController, SetServiceAccessError,
};
use crate::consumer::sco::sco_transaction::ScoTransactionImpl;

#[derive(Debug, Error)]
pub enum RemoteDeviceConnectorError {
    #[error("Consumer is already connected")]
    AlreadyConnected,

    #[error(transparent)]
    ConsumerError(ConsumerError),

    #[error(transparent)]
    TonicStatus(tonic::Status),

    #[error("A mapping error occurred: {0:?}")]
    MappingError(MappingError),

    #[error(transparent)]
    WriteError(WriteError),

    #[error(transparent)]
    MdibError(#[from] MdibError),

    #[error("ReportStream with empty report was received")]
    SubscriptionReportWithoutContent,

    #[error("GetMdibResponse invalid")]
    InvalidGetMdibResponse,

    #[error(transparent)]
    ReportWriterError(ReportWriterError),

    #[error(transparent)]
    ModificationError(ModificationError),

    #[error("SubscriptionConfirmation was not received within the timeout of {}s", .0.as_secs())]
    SubscriptionConfirmationTimeout(Duration),
}

#[derive(Debug, Error)]
pub enum SubscriptionError {
    #[error("Subscription ended")]
    Ended,

    #[error("A mapping error occurred: {0:?}")]
    MappingError(MappingError),

    #[error("ReportStream with empty report was received")]
    SubscriptionReportWithoutContent,

    #[error(transparent)]
    ReportWriterError(ReportWriterError),

    #[error(transparent)]
    SetServiceAccessError(SetServiceAccessError),

    #[error(transparent)]
    TonicStatus(tonic::Status),

    #[error("SubscriptionConfirmation was received more than once")]
    SubscriptionConfirmationDuplicate,
}

pub async fn connect(
    mut consumer: ConsumerImpl,
) -> Result<
    SdcRemoteDevice<
        ConsumerImpl,
        DefaultRemoteMdibAccessImpl,
        ScoController<ProtoSetServiceHandler>,
        ScoTransactionImpl,
    >,
    RemoteDeviceConnectorError,
> {
    if !consumer.is_connected() {
        return Err(RemoteDeviceConnectorError::ConsumerError(
            ConsumerError::DisconnectedError,
        ));
    }

    info!("Connecting using {:?}", consumer);

    // create report processor
    let report_processor = Arc::new(Mutex::new(ReportProcessorImpl::default()));

    let task_processor = report_processor.clone();
    let (is_subscribed_tx, is_subscribed_rx) = oneshot::channel::<()>();
    // subscribe
    let subscription_task = match consumer.get_mdib_reporting_service().await {
        None => None,
        Some(mut mrs) => {
            let task = Some(tokio::spawn(async move {
                let mut is_subscribed_sender = Some(is_subscribed_tx);

                info!("Subscribing to episodic reports");
                let subscription = mrs
                    .episodic_report(EpisodicReportRequest {
                        addressing: Some(crate::addressing::util::create_for_action(
                            crate::common::action::EPISODIC_REPORT_REQUEST,
                        )),
                        filter: None,
                    })
                    .await
                    .map_err(SubscriptionError::TonicStatus)?;

                let mut message_stream = subscription.into_inner();
                while let Ok(Some(report)) = message_stream.message().await {
                    trace!("Received report in stream");

                    let rep = report
                        .message
                        .ok_or(SubscriptionError::SubscriptionReportWithoutContent)
                        .map_err(|err| {
                            error!(
                                "Report writer had an error while processing report. {:?}",
                                &err
                            );
                            err
                        })?;

                    match rep {
                        Message::SubscriptionConfirmation(_) => match is_subscribed_sender.take() {
                            None => Err(SubscriptionError::SubscriptionConfirmationDuplicate)?,
                            Some(tx) => {
                                oneshot_log_error(tx, ());
                            }
                        },
                        Message::Report(report) => {
                            let converted: EpisodicReport = report.try_into().map_err(|err| {
                                error!(
                                    "Report writer had an error while processing report. {:?}",
                                    &err
                                );
                                SubscriptionError::MappingError(err)
                            })?;
                            task_processor
                                .lock()
                                .await
                                .process_episodic_report(converted)
                                .await
                                .map_err(|err| {
                                    error!(
                                        "Report writer had an error while processing report. {:?}",
                                        &err
                                    );
                                    SubscriptionError::ReportWriterError(err)
                                })?;
                        }
                    }
                }

                info!("Report processing ended");
                Err(SubscriptionError::Ended)
            }));

            // TODO: remove work around for https://github.com/hyperium/tonic/issues/515
            tokio::time::sleep(Duration::from_millis(100)).await;

            debug!("Episodic report task spawned");

            task
        }
    };

    // TODO: make configurable
    let subscription_await_timeout = Duration::from_secs(5);

    if let Some(tsk) = &subscription_task {
        // await subscriptions
        if timeout(subscription_await_timeout, is_subscribed_rx)
            .await
            .is_err()
        {
            tsk.abort();
            error!(
                "Did not receive subscription confirmation within {}s",
                subscription_await_timeout.as_secs()
            );
            Err(RemoteDeviceConnectorError::SubscriptionConfirmationTimeout(
                subscription_await_timeout,
            ))?
        }
        info!("Subscribed to episodic reports");
    }

    // apply mdib to remote mdib
    let remote_mdib = create_remote_mdib_access_impl(&mut consumer).await?;

    // apply buffered reports on mdib
    report_processor
        .lock()
        .await
        .start_applying_reports(remote_mdib.clone())
        .await
        .map_err(RemoteDeviceConnectorError::ReportWriterError)?;

    let sco_controller = match consumer.get_set_service().await {
        None => None,
        Some(access) => {
            let controller = ScoController::new(
                ProtoSetServiceHandler::new(access.clone()),
                Duration::from_secs(5),
            );

            let task_controller = controller.clone();
            let (is_subscribed_tx, is_subscribed_rx) = oneshot::channel::<()>();
            let task = Some(tokio::spawn(async move {
                let mut is_subscribed_sender = Some(is_subscribed_tx);
                info!("Subscribing to operation invoked reports");
                let subscription = access
                    .clone()
                    .operation_invoked_report(OperationInvokedReportRequest {
                        addressing: Some(crate::addressing::util::create_for_action(
                            crate::common::action::OPERATION_INVOKED_REPORT_REQUEST,
                        )),
                    })
                    .await
                    .map_err(SubscriptionError::TonicStatus)?;

                let mut message_stream = subscription.into_inner();
                while let Ok(Some(report)) = message_stream.message().await {
                    trace!("Received operation invoked report in stream");
                    let rep = report
                        .message
                        .ok_or(SubscriptionError::SubscriptionReportWithoutContent)
                        .map_err(|err| {
                            error!(
                                "SetServiceAccessError had an error while processing report. {:?}",
                                &err
                            );
                            err
                        })?;

                    match rep {
                        operation_invoked_report_stream::Message::SubscriptionConfirmation(_) => {
                            match is_subscribed_sender.take() {
                                None => Err(SubscriptionError::SubscriptionConfirmationDuplicate)?,
                                Some(tx) => {
                                    oneshot_log_error(tx, ());
                                }
                            }
                        }
                        operation_invoked_report_stream::Message::OperationInvoked(invoked) => {
                            task_controller
                                .process_operation_invoked_report_stream(invoked)
                                .await
                                .map_err(|err| {
                                    error!(
                                        "SetServiceAccessError had an error while processing report. {:?}",
                                        &err
                                    );
                                    SubscriptionError::SetServiceAccessError(err)
                            })?;
                        }
                    }

                    trace!("Done processing operation invoked report in stream");
                }

                info!("OperationInvokedReport processing ended");
                Err(SubscriptionError::Ended)
            }));

            debug!("Operation invoked report task spawned");

            // TODO: remove work around for https://github.com/hyperium/tonic/issues/515
            tokio::time::sleep(Duration::from_millis(100)).await;

            if let Some(tsk) = &task {
                // await subscriptions
                if timeout(subscription_await_timeout, is_subscribed_rx)
                    .await
                    .is_err()
                {
                    tsk.abort();
                    error!(
                        "Did not receive subscription confirmation within {}s",
                        subscription_await_timeout.as_secs()
                    );
                    Err(RemoteDeviceConnectorError::SubscriptionConfirmationTimeout(
                        subscription_await_timeout,
                    ))?
                }
                info!("Subscribed to operation invoked reports");
            }

            Some((controller, task))
        }
    };

    info!("SdcRemoteDevice connected");

    // create remote device
    match sco_controller {
        Some((controller, task)) => Ok(SdcRemoteDevice::new(
            consumer,
            remote_mdib,
            subscription_task,
            Some(controller),
            task,
        )),
        None => Ok(SdcRemoteDevice::new(
            consumer,
            remote_mdib,
            subscription_task,
            None,
            None,
        )),
    }
}

/// Creates an implementation of remote interface to the Medical Device Information Base (MDIB).
///
/// This function retrieves the MDIB information from the device via the provided
/// consumer, performs necessary transformations and creates a new instance of
/// the remote MDIB access interface.
///
/// # Arguments
///
/// * `consumer` - A reference to an object implementing the Consumer and GetServiceConsumer traits.
///
/// # Returns
///
/// * `DefaultRemoteMdibAccessImpl` - The created implementation of the remote MDIB access interface.
/// * `RemoteDeviceConnectorError` - An error encountered while retrieving and processing the MDIB.
///
/// # Errors
///
/// Errors could occur during communication with the device (the ConsumerError variant of RemoteDeviceConnectorError),
/// during the transformation of the MDIB into the modifications list (the ModificationError variant),
/// or during the write operation to the remote MDIB (the WriteError variant).
///
/// # Type Parameters
///
/// * `T` - The type of the object performing the MDIB retrieval. Must implement the Consumer and GetServiceConsumer traits.
///
async fn create_remote_mdib_access_impl<T>(
    consumer: &mut T,
) -> Result<DefaultRemoteMdibAccessImpl, RemoteDeviceConnectorError>
where
    T: Consumer + GetServiceConsumer,
{
    let get_mdib_response = consumer
        .get_mdib()
        .await
        .map_err(RemoteDeviceConnectorError::ConsumerError)?;

    //noinspection RsTypeCheck
    let mdib_version: MdibVersion = get_mdib_response
        .mdib
        .mdib_version_group_attr
        .clone()
        .into();
    let modifications: Vec<MdibDescriptionModification> =
        modifications_from_mdib(get_mdib_response.mdib).await?;

    let mut description_modifications = MdibDescriptionModifications::default();
    description_modifications
        .add_all(modifications)
        .map_err(RemoteDeviceConnectorError::ModificationError)?;

    // create remote mdib

    let mut remote_mdib = RemoteMdibAccessImpl::new(mdib_version.clone());
    remote_mdib
        .write_description(mdib_version, description_modifications)
        .await
        .map_err(RemoteDeviceConnectorError::WriteError)?;

    Ok(remote_mdib)
}
