use std::sync::Arc;
use std::time::Duration;

use async_trait::async_trait;
use log::{debug, info};
use thiserror::Error;
use tokio::sync::Mutex;
use tonic::Status;

use protosdc_biceps::biceps::{AbstractSetOneOf, AbstractSetResponseOneOf, OperationInvokedReport};
use protosdc_mapping::MappingError;
use protosdc_proto::biceps::OperationInvokedReportMsg;
use protosdc_proto::sdc::set_service_client::SetServiceClient;
use protosdc_proto::sdc::{
    ActivateRequest, SetAlertStateRequest, SetComponentStateRequest, SetContextStateRequest,
    SetMetricStateRequest, SetStringRequest, SetValueRequest,
};

use crate::addressing;
use crate::common::action;
use crate::common::hyper_client::HyperClient;
use crate::consumer::sco::operation_invocation_dispatcher::{
    OperationError, OperationInvocationDispatcher,
};
use crate::consumer::sco::sco_transaction::{ScoTransaction, ScoTransactionImpl};

#[derive(Debug, Error)]
pub enum SetServiceAccessError {
    #[error("An error occurred while mapping from proto to rust: {0:?}")]
    MappingError(MappingError),
    #[error("Message did not contain report")]
    ReportMissing,
    #[error(transparent)]
    OperationError(OperationError),
    #[error(transparent)]
    TonicStatus(Status),
    #[error("A transport error occurred: {message}")]
    TransportError { message: String },
    #[error("Sending abstract requests is not supported")]
    AbstractRequest,
    #[error("No context service available")]
    NoContextService,
}

#[async_trait]
pub trait SetServiceHandler {
    async fn invoke(
        &mut self,
        request: AbstractSetOneOf,
    ) -> Result<AbstractSetResponseOneOf, SetServiceAccessError>;
}

#[derive(Clone)]
pub struct ProtoSetServiceHandler {
    set_service: Arc<Mutex<SetServiceClient<HyperClient>>>,
}

#[async_trait]
pub trait SetServiceAccess<T>
where
    T: ScoTransaction,
{
    async fn invoke(&mut self, request: AbstractSetOneOf) -> Result<Arc<T>, SetServiceAccessError>;
}

#[derive(Clone)]
pub struct ScoController<H>
where
    H: SetServiceHandler + Clone + Send + Sync,
{
    operation_invocation_dispatcher: Arc<Mutex<OperationInvocationDispatcher>>,
    set_service: H,
}

impl<H> ScoController<H>
where
    H: SetServiceHandler + Clone + Send + Sync,
{
    pub fn new(set_service: H, transaction_timeout: Duration) -> Self {
        Self {
            operation_invocation_dispatcher: Arc::new(Mutex::new(
                OperationInvocationDispatcher::new(transaction_timeout),
            )),
            set_service,
        }
    }
}

#[async_trait]
impl<H> SetServiceAccess<ScoTransactionImpl> for ScoController<H>
where
    H: SetServiceHandler + Clone + Send + Sync,
{
    async fn invoke(
        &mut self,
        request: AbstractSetOneOf,
    ) -> Result<Arc<ScoTransactionImpl>, SetServiceAccessError> {
        debug!("Invoking operation, request {:?}", request);
        let response = self.send_request(request).await?;
        let sco_transaction = Arc::new(ScoTransactionImpl::new(response));
        self.operation_invocation_dispatcher
            .lock()
            .await
            .register_transaction(sco_transaction.clone())
            .await
            .map_err(SetServiceAccessError::OperationError)?;

        Ok(sco_transaction)
    }
}

impl<H> ScoController<H>
where
    H: SetServiceHandler + Clone + Send + Sync,
{
    pub async fn process_operation_invoked_report_stream(
        &self,
        report: OperationInvokedReportMsg,
    ) -> Result<(), SetServiceAccessError> {
        info!("Received OperationInvokedReportStream");
        self.process_operation_invoked_report(
            report
                .try_into()
                .map_err(SetServiceAccessError::MappingError)?,
        )
        .await
    }

    pub async fn process_operation_invoked_report(
        &self,
        report: OperationInvokedReport,
    ) -> Result<(), SetServiceAccessError> {
        debug!("Received OperationInvokedReport");
        self.operation_invocation_dispatcher
            .lock()
            .await
            .dispatch_report(report)
            .await
            .map_err(SetServiceAccessError::OperationError)
    }

    async fn send_request(
        &mut self,
        set_request: AbstractSetOneOf,
    ) -> Result<AbstractSetResponseOneOf, SetServiceAccessError> {
        self.set_service.invoke(set_request).await
    }
}

#[async_trait]
impl SetServiceHandler for ProtoSetServiceHandler {
    async fn invoke(
        &mut self,
        set_request: AbstractSetOneOf,
    ) -> Result<AbstractSetResponseOneOf, SetServiceAccessError> {
        let result = match set_request {
            AbstractSetOneOf::Activate(req) => AbstractSetResponseOneOf::ActivateResponse(
                self.set_service
                    .lock()
                    .await
                    .activate(ActivateRequest {
                        addressing: Some(addressing::util::create_for_action(
                            action::ACTIVATE_REQUEST,
                        )),
                        payload: Some(req.into()),
                    })
                    .await
                    .map_err(SetServiceAccessError::TonicStatus)?
                    .into_inner()
                    .payload
                    .ok_or(SetServiceAccessError::ReportMissing)?
                    .try_into()
                    .map_err(SetServiceAccessError::MappingError)?,
            ),
            AbstractSetOneOf::SetValue(req) => AbstractSetResponseOneOf::SetValueResponse(
                self.set_service
                    .lock()
                    .await
                    .set_value(SetValueRequest {
                        addressing: Some(addressing::util::create_for_action(
                            action::SET_VALUE_REQUEST,
                        )),
                        payload: Some(req.into()),
                    })
                    .await
                    .map_err(SetServiceAccessError::TonicStatus)?
                    .into_inner()
                    .payload
                    .ok_or(SetServiceAccessError::ReportMissing)?
                    .try_into()
                    .map_err(SetServiceAccessError::MappingError)?,
            ),
            AbstractSetOneOf::SetString(req) => AbstractSetResponseOneOf::SetStringResponse(
                self.set_service
                    .lock()
                    .await
                    .set_string(SetStringRequest {
                        addressing: Some(addressing::util::create_for_action(
                            action::SET_STRING_REQUEST,
                        )),
                        payload: Some(req.into()),
                    })
                    .await
                    .map_err(SetServiceAccessError::TonicStatus)?
                    .into_inner()
                    .payload
                    .ok_or(SetServiceAccessError::ReportMissing)?
                    .try_into()
                    .map_err(SetServiceAccessError::MappingError)?,
            ),
            AbstractSetOneOf::SetAlertState(req) => {
                AbstractSetResponseOneOf::SetAlertStateResponse(
                    self.set_service
                        .lock()
                        .await
                        .set_alert_state(SetAlertStateRequest {
                            addressing: Some(addressing::util::create_for_action(
                                action::SET_ALERT_STATE_REQUEST,
                            )),
                            payload: Some(req.into()),
                        })
                        .await
                        .map_err(SetServiceAccessError::TonicStatus)?
                        .into_inner()
                        .payload
                        .ok_or(SetServiceAccessError::ReportMissing)?
                        .try_into()
                        .map_err(SetServiceAccessError::MappingError)?,
                )
            }
            AbstractSetOneOf::SetMetricState(req) => {
                AbstractSetResponseOneOf::SetMetricStateResponse(
                    self.set_service
                        .lock()
                        .await
                        .set_metric_state(SetMetricStateRequest {
                            addressing: Some(addressing::util::create_for_action(
                                action::SET_METRIC_STATE_REQUEST,
                            )),
                            payload: Some(req.into()),
                        })
                        .await
                        .map_err(SetServiceAccessError::TonicStatus)?
                        .into_inner()
                        .payload
                        .ok_or(SetServiceAccessError::ReportMissing)?
                        .try_into()
                        .map_err(SetServiceAccessError::MappingError)?,
                )
            }
            AbstractSetOneOf::SetContextState(req) => {
                AbstractSetResponseOneOf::SetContextStateResponse(
                    self.set_service
                        .lock()
                        .await
                        .set_context_state(SetContextStateRequest {
                            addressing: Some(addressing::util::create_for_action(
                                action::SET_CONTEXT_STATE_REQUEST,
                            )),
                            payload: Some(req.into()),
                        })
                        .await
                        .map_err(SetServiceAccessError::TonicStatus)?
                        .into_inner()
                        .payload
                        .ok_or(SetServiceAccessError::ReportMissing)?
                        .try_into()
                        .map_err(SetServiceAccessError::MappingError)?,
                )
            }
            AbstractSetOneOf::SetComponentState(req) => {
                AbstractSetResponseOneOf::SetComponentStateResponse(
                    self.set_service
                        .lock()
                        .await
                        .set_component_state(SetComponentStateRequest {
                            addressing: Some(addressing::util::create_for_action(
                                action::SET_COMPONENT_STATE_REQUEST,
                            )),
                            payload: Some(req.into()),
                        })
                        .await
                        .map_err(SetServiceAccessError::TonicStatus)?
                        .into_inner()
                        .payload
                        .ok_or(SetServiceAccessError::ReportMissing)?
                        .try_into()
                        .map_err(SetServiceAccessError::MappingError)?,
                )
            }

            AbstractSetOneOf::AbstractSet(_) => return Err(SetServiceAccessError::AbstractRequest),
        };
        Ok(result)
    }
}

impl ProtoSetServiceHandler {
    pub(crate) fn new(set_service: SetServiceClient<HyperClient>) -> Self {
        Self {
            set_service: Arc::new(Mutex::new(set_service)),
        }
    }
}
