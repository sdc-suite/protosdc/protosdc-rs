use std::marker::PhantomData;
use tokio::task::JoinHandle;

use biceps::common::mdib_entity::MdibEntity;
use biceps::common::mdib_version::MdibVersion;
use biceps::common::storage::mdib_storage::MdibStorageImpl;
use biceps::consumer::access::remote_mdib_access::{
    DefaultRemoteDescriptorPreprocessingSegment, DefaultRemoteStatePreprocessingSegment,
    RemoteMdibAccess, RemoteMdibAccessImpl,
};
use protosdc_biceps::biceps::{
    GetContextStatesResponse, GetMdDescriptionResponse, GetMdStateResponse, GetMdibResponse,
};
use protosdc_proto::metadata::GetMetadataResponse;

use crate::consumer::grpc_consumer::{
    Consumer, ConsumerError, ConsumerImpl, GetServiceConsumer, MetadataServiceConsumer,
};
use crate::consumer::remote_device_connector::SubscriptionError;
use crate::consumer::sco::sco_controller::{
    ProtoSetServiceHandler, ScoController, SetServiceAccess,
};
use crate::consumer::sco::sco_transaction::{ScoTransaction, ScoTransactionImpl};

pub struct SdcRemoteDevice<T, U, V, W>
where
    T: Consumer,
    U: RemoteMdibAccess,
    V: SetServiceAccess<W>,
    W: ScoTransaction,
{
    consumer: T,
    remote_mdib: U,
    // TODO: Subscription task watchdog to monitor subscription(s) health
    subscription_task: Option<JoinHandle<Result<(), SubscriptionError>>>,
    set_service_access: Option<V>,
    set_service_subscription_task: Option<JoinHandle<Result<(), SubscriptionError>>>,

    phantom: PhantomData<W>,
}

pub type DefaultRemoteMdibAccessImpl = RemoteMdibAccessImpl<
    MdibStorageImpl,
    DefaultRemoteDescriptorPreprocessingSegment<MdibStorageImpl>,
    DefaultRemoteStatePreprocessingSegment<MdibStorageImpl>,
>;

impl
    SdcRemoteDevice<
        ConsumerImpl,
        DefaultRemoteMdibAccessImpl,
        ScoController<ProtoSetServiceHandler>,
        ScoTransactionImpl,
    >
{
    /// Creates a new remote sdc device using a given endpoint.
    pub fn new(
        consumer: ConsumerImpl,
        mdib: DefaultRemoteMdibAccessImpl,
        subscription_task: Option<JoinHandle<Result<(), SubscriptionError>>>,
        set_service_access: Option<ScoController<ProtoSetServiceHandler>>,
        set_service_subscription_task: Option<JoinHandle<Result<(), SubscriptionError>>>,
    ) -> Self {
        Self {
            consumer,
            remote_mdib: mdib,
            subscription_task,
            set_service_access,
            set_service_subscription_task,

            phantom: PhantomData,
        }
    }
}

impl<T, U, V, W> SdcRemoteDevice<T, U, V, W>
where
    T: Consumer,
    U: RemoteMdibAccess + Clone,
    V: SetServiceAccess<W> + Clone,
    W: ScoTransaction,
{
    pub async fn get_mdib_access(&self) -> U {
        self.remote_mdib.clone()
    }

    pub async fn set_service(&self) -> Option<V> {
        self.set_service_access.clone()
    }

    pub async fn mdib_version(&self) -> MdibVersion {
        self.remote_mdib.mdib_version().await
    }

    pub async fn mdib_entity(&self, handle: &str) -> Option<MdibEntity> {
        self.remote_mdib.entity(handle).await
    }

    pub fn consumer_mut(&mut self) -> &mut T {
        &mut self.consumer
    }

    pub fn consumer(&self) -> &T {
        &self.consumer
    }

    pub async fn disconnect(&mut self) -> Result<(), ConsumerError> {
        if let Some(task) = &self.subscription_task {
            task.abort();
        }
        if let Some(task) = &self.set_service_subscription_task {
            task.abort();
        }
        self.consumer.disconnect().await
    }
}

impl<T, U, V, W> SdcRemoteDevice<T, U, V, W>
where
    T: GetServiceConsumer,
    U: RemoteMdibAccess,
    V: SetServiceAccess<W>,
    W: ScoTransaction,
{
    pub async fn get_mdib(&self) -> Result<GetMdibResponse, ConsumerError> {
        self.consumer.get_mdib().await
    }

    pub async fn get_md_state(
        &self,
        handles: Vec<String>,
    ) -> Result<GetMdStateResponse, ConsumerError> {
        self.consumer.get_md_state(handles).await
    }

    pub async fn get_md_description(
        &self,
        handles: Vec<String>,
    ) -> Result<GetMdDescriptionResponse, ConsumerError> {
        self.consumer.get_md_description(handles).await
    }

    pub async fn get_context_states(
        &self,
        handles: Vec<String>,
    ) -> Result<GetContextStatesResponse, ConsumerError> {
        self.consumer.get_context_states(handles).await
    }
}

impl<T, U, V, W> SdcRemoteDevice<T, U, V, W>
where
    T: MetadataServiceConsumer,
    U: RemoteMdibAccess,
    V: SetServiceAccess<W>,
    W: ScoTransaction,
{
    pub async fn get_metadata(&self) -> Result<GetMetadataResponse, ConsumerError> {
        self.consumer.get_metadata().await
    }
}
