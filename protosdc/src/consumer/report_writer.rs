use std::collections::{HashMap, HashSet};

use thiserror::Error;

use biceps::common::biceps_util::{
    into_context_state_one_of, is_context_descriptor, Descriptor, State,
};
use biceps::common::mdib_description_modification::MdibDescriptionModification;
use biceps::common::mdib_description_modifications::{
    MdibDescriptionModifications, ModificationError,
};
use biceps::common::mdib_pair::PairError;
use biceps::common::mdib_state_modifications::MdibStateModifications;
use biceps::common::mdib_version::MdibVersion;
use biceps::common::storage::mdib_storage::{WriteDescriptionResult, WriteError, WriteStateResult};
use biceps::consumer::access::remote_mdib_access::InternalRemoteMdibAccess;
use protosdc_biceps::biceps::description_modification_type_mod::EnumType;
use protosdc_biceps::biceps::{
    description_modification_type_mod, AbstractAlertStateOneOf, AbstractContextStateOneOf,
    AbstractDeviceComponentStateOneOf, AbstractMetricStateOneOf, AbstractOperationStateOneOf,
    AbstractStateOneOf, DescriptionModificationReport, DescriptionModificationType,
    EpisodicAlertReport, EpisodicComponentReport, EpisodicContextReport, EpisodicMetricReport,
    EpisodicOperationalStateReport, RealTimeSampleArrayMetricState, WaveformStream,
};
use protosdc_mapping::MappingError;

use crate::common::reports::EpisodicReport;

#[derive(Debug, Error)]
pub enum ReportWriterError {
    #[error(transparent)]
    WriteError(WriteError),

    #[error(transparent)]
    ModificationError(ModificationError),

    #[error("DescriptionModificationReport contained state(s) without descriptor: {handles:?}")]
    StateWithoutDescriptor { handles: Vec<String> },

    #[error("DescriptionModificationReport contained single state(s) for multi-state descriptor: {handle:?}")]
    MultiStateDescriptorWithSingleStateType { handle: String },

    #[error("DescriptionModificationReport ReportPart contained empty modification type")]
    ReportPartWithEmptyModificationType,

    #[error("DescriptionModificationReport ReportPart contained empty parent descriptor")]
    ReportPartWithEmptyParentDescriptor,

    #[error("DescriptionModificationReport ReportPart had single-state descriptor ({handle:?}) with incorrect number of states ({num:?})")]
    CardinalityError { handle: String, num: usize },

    #[error("A mapping error occurred: {0:?}")]
    MappingError(MappingError),

    #[error("Unknown report type")]
    UnknownReportType,

    #[error(transparent)]
    PairError(#[from] PairError),
}

pub async fn write_report<MDIB>(
    mdib: &mut MDIB,
    report: EpisodicReport,
) -> Result<(), ReportWriterError>
where
    MDIB: InternalRemoteMdibAccess,
{
    match report {
        EpisodicReport::Alert(report) => {
            let (version, modifications) = modification_from_report_alert(*report);
            write_states(mdib, modifications, version).await?;
        }
        EpisodicReport::Component(report) => {
            let (version, modifications) = modification_from_report_component(*report);
            write_states(mdib, modifications, version).await?;
        }
        EpisodicReport::Context(report) => {
            let (version, modifications) = modification_from_report_context(*report);
            write_states(mdib, modifications, version).await?;
        }
        EpisodicReport::Metric(report) => {
            let (version, modifications) = modification_from_report_metric(*report);
            write_states(mdib, modifications, version).await?;
        }
        EpisodicReport::Operation(report) => {
            let (version, modifications) = modification_from_report_operation(*report);
            write_states(mdib, modifications, version).await?;
        }
        EpisodicReport::Waveform(report) => {
            let (version, modifications) = modification_from_report_waveform(*report);
            write_states(mdib, modifications, version).await?;
        }
        EpisodicReport::Description(report) => {
            let (version, modifications) = modification_from_report_description(*report)?;
            write_description(mdib, modifications, version).await?;
        }
    }

    Ok(())
}

async fn write_states<MDIB>(
    mdib: &mut MDIB,
    state_modifications: MdibStateModifications,
    mdib_version: MdibVersion,
) -> Result<WriteStateResult, ReportWriterError>
where
    MDIB: InternalRemoteMdibAccess,
{
    mdib.write_states(mdib_version, state_modifications)
        .await
        .map_err(ReportWriterError::WriteError)
}

async fn write_description<MDIB>(
    mdib: &mut MDIB,
    description_modifications: MdibDescriptionModifications,
    mdib_version: MdibVersion,
) -> Result<WriteDescriptionResult, ReportWriterError>
where
    MDIB: InternalRemoteMdibAccess,
{
    mdib.write_description(mdib_version, description_modifications)
        .await
        .map_err(ReportWriterError::WriteError)
}

fn modification_from_report_alert(
    report: EpisodicAlertReport,
) -> (MdibVersion, MdibStateModifications) {
    let mdib_version = report
        .abstract_alert_report
        .abstract_report
        .mdib_version_group_attr;
    let states: Vec<AbstractAlertStateOneOf> = report
        .abstract_alert_report
        .report_part
        .into_iter()
        .flat_map(|it| it.alert_state.into_iter())
        .collect();
    (
        mdib_version.into(),
        MdibStateModifications::Alert {
            alert_states: states,
        },
    )
}

fn modification_from_report_component(
    report: EpisodicComponentReport,
) -> (MdibVersion, MdibStateModifications) {
    let mdib_version = report
        .abstract_component_report
        .abstract_report
        .mdib_version_group_attr;
    let states: Vec<AbstractDeviceComponentStateOneOf> = report
        .abstract_component_report
        .report_part
        .into_iter()
        .flat_map(|it| it.component_state.into_iter())
        .collect();
    (
        mdib_version.into(),
        MdibStateModifications::Component {
            component_states: states,
        },
    )
}

fn modification_from_report_context(
    report: EpisodicContextReport,
) -> (MdibVersion, MdibStateModifications) {
    let mdib_version = report
        .abstract_context_report
        .abstract_report
        .mdib_version_group_attr;
    let states: Vec<AbstractContextStateOneOf> = report
        .abstract_context_report
        .report_part
        .into_iter()
        .flat_map(|it| it.context_state.into_iter())
        .collect();
    (
        mdib_version.into(),
        MdibStateModifications::Context {
            context_states: states,
        },
    )
}

fn modification_from_report_metric(
    report: EpisodicMetricReport,
) -> (MdibVersion, MdibStateModifications) {
    let mdib_version = report
        .abstract_metric_report
        .abstract_report
        .mdib_version_group_attr;
    let states: Vec<AbstractMetricStateOneOf> = report
        .abstract_metric_report
        .report_part
        .into_iter()
        .flat_map(|it| it.metric_state.into_iter())
        .collect();
    (
        mdib_version.into(),
        MdibStateModifications::Metric {
            metric_states: states,
        },
    )
}

fn modification_from_report_operation(
    report: EpisodicOperationalStateReport,
) -> (MdibVersion, MdibStateModifications) {
    let mdib_version = report
        .abstract_operational_state_report
        .abstract_report
        .mdib_version_group_attr;
    let states: Vec<AbstractOperationStateOneOf> = report
        .abstract_operational_state_report
        .report_part
        .into_iter()
        .flat_map(|it| it.operation_state.into_iter())
        .collect();
    (
        mdib_version.into(),
        MdibStateModifications::Operation {
            operation_states: states,
        },
    )
}

fn modification_from_report_waveform(
    report: WaveformStream,
) -> (MdibVersion, MdibStateModifications) {
    let mdib_version = report.abstract_report.mdib_version_group_attr;
    let states: Vec<RealTimeSampleArrayMetricState> = report.state;
    (
        mdib_version.into(),
        MdibStateModifications::Waveform {
            waveform_states: states,
        },
    )
}

fn modification_from_report_description(
    report: DescriptionModificationReport,
) -> Result<(MdibVersion, MdibDescriptionModifications), ReportWriterError> {
    let mdib_version = report.abstract_report.mdib_version_group_attr;

    let modifications = report.report_part.into_iter().map(|part| {

        let modification_type = part.modification_type_attr
            .unwrap_or(DescriptionModificationType { enum_type: description_modification_type_mod::EnumType::Upt });

        let parent_descriptor = part.parent_descriptor_attr.clone();

        let mut state_map: HashMap<String, Vec<AbstractStateOneOf>> = part.state.into_iter()
            .fold(HashMap::new(), |mut map, value| {
                let descriptor_handle = value.descriptor_handle();
                let to_insert = match map.remove(&descriptor_handle) {
                    None => vec![value],
                    Some(mut v) => {
                        v.push(value);
                        v
                    }
                };
                map.insert(descriptor_handle, to_insert);
                map
            });

        let descriptor_handles: HashSet<String> = part.descriptor.iter().map(|it| it.handle()).collect();

        let unknown_descriptors: Vec<String> = state_map.iter()
            .filter(|(handle, _state)| !descriptor_handles.contains(handle as &str))
            .map(|(handle, _state)| handle.clone())
            .collect();

        if !unknown_descriptors.is_empty() {
            return Err(ReportWriterError::StateWithoutDescriptor { handles: unknown_descriptors })
        }

        part.descriptor.into_iter()
            .map(|descriptor| {
            let states = state_map.remove(&*descriptor.handle());

            match is_context_descriptor(&descriptor) {
                true => {
                    let mapped_states: Vec<AbstractStateOneOf> = match states {
                        Some(s) => {
                            s.into_iter()
                                .map(|state| {
                                    let context_state = into_context_state_one_of(state)
                                        .map_err(|state| ReportWriterError::MultiStateDescriptorWithSingleStateType { handle: state.descriptor_handle() });

                                    match context_state {
                                        Ok(c) => Ok(c.into_abstract_state_one_of()),
                                        Err(err) => Err(err),
                                    }
                                })
                                .collect::<Result<Vec<AbstractStateOneOf>, ReportWriterError>>()
                        },
                        None => Ok(vec![]),
                    }?;

                    match modification_type.enum_type {
                        EnumType::Crt => Ok(MdibDescriptionModification::Insert {
                            pair: (descriptor, mapped_states).try_into()?,
                            parent: Some(parent_descriptor.as_ref().ok_or(ReportWriterError::ReportPartWithEmptyParentDescriptor)?.string.clone())
                        }),
                        EnumType::Upt => Ok(MdibDescriptionModification::Update {
                            pair: (descriptor, mapped_states).try_into()?,
                        }),
                        EnumType::Del => Ok(MdibDescriptionModification::Delete {
                            handle: descriptor.handle()
                        })
                    }
                }
                false => {

                    match modification_type.enum_type {
                        EnumType::Crt => {
                            let mut single_state_vec = states.ok_or(ReportWriterError::CardinalityError { handle: descriptor.handle(), num: 0 })?;
                            if single_state_vec.len() != 1 {
                                return Err(ReportWriterError::CardinalityError { handle: descriptor.handle(), num: single_state_vec.len() })
                            }

                            Ok(MdibDescriptionModification::Insert {
                                pair: (descriptor, single_state_vec.pop().unwrap()).try_into()?,

                                // TODO: if mds, allow none
                                parent: Some(parent_descriptor.as_ref().ok_or(ReportWriterError::ReportPartWithEmptyParentDescriptor)?.string.clone())
                            })
                        }
                        EnumType::Upt => {
                            let mut single_state_vec = states.ok_or(ReportWriterError::CardinalityError { handle: descriptor.handle(), num: 0 })?;
                            if single_state_vec.len() != 1 {
                                return Err(ReportWriterError::CardinalityError { handle: descriptor.handle(), num: single_state_vec.len() })
                            }

                            Ok(MdibDescriptionModification::Update {
                                pair: (descriptor, single_state_vec.pop().unwrap()).try_into()?,
                            })
                        }
                        EnumType::Del => {
                            Ok(MdibDescriptionModification::Delete {
                                handle: descriptor.handle()
                            })
                        }
                    }


                }
            }

        }).collect::<Result<Vec<MdibDescriptionModification>, ReportWriterError>>()

        // modifications
    }).collect::<Result<Vec<Vec<MdibDescriptionModification>>, _>>()?;

    let modifications_flattened = modifications.into_iter().flatten().collect();

    let mut mdib_description_modifications = MdibDescriptionModifications::default();
    mdib_description_modifications
        .add_all(modifications_flattened)
        .map_err(ReportWriterError::ModificationError)?;

    Ok((mdib_version.into(), mdib_description_modifications))
}
