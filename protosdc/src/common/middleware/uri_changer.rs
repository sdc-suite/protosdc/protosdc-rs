use hyper::http::Request;
use std::task::{Context, Poll};
use tower_service::Service;

#[derive(Clone, Debug)]
pub struct UriChanger<T> {
    inner: T,
    uri: hyper::Uri,
}

impl<T> UriChanger<T> {
    pub(crate) fn new(inner: T, uri: hyper::Uri) -> Self {
        Self { inner, uri }
    }
}

impl<T, ReqBody> Service<Request<ReqBody>> for UriChanger<T>
where
    T: Service<Request<ReqBody>>,
{
    type Response = T::Response;
    type Error = T::Error;
    type Future = T::Future;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx)
    }

    fn call(&mut self, mut req: Request<ReqBody>) -> Self::Future {
        *req.uri_mut() = self.uri.clone();
        self.inner.call(req)
    }
}

impl<T> Service<hyper::Uri> for UriChanger<T>
where
    T: Service<hyper::Uri>,
{
    type Response = T::Response;
    type Error = T::Error;
    type Future = T::Future;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx)
    }

    fn call(&mut self, _: hyper::Uri) -> Self::Future {
        self.inner.call(self.uri.clone())
    }
}
