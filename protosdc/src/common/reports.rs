use std::convert::TryFrom;

use protosdc_biceps::biceps::{
    DescriptionModificationReport, EpisodicAlertReport, EpisodicComponentReport,
    EpisodicContextReport, EpisodicMetricReport, EpisodicOperationalStateReport, MdibVersionGroup,
    WaveformStream,
};
use protosdc_mapping::MappingError;

use protosdc_proto::sdc;
use protosdc_proto::sdc::episodic_report::Type;

#[derive(Debug, Clone)]
pub enum EpisodicReport {
    Alert(Box<EpisodicAlertReport>),
    Component(Box<EpisodicComponentReport>),
    Context(Box<EpisodicContextReport>),
    Metric(Box<EpisodicMetricReport>),
    Operation(Box<EpisodicOperationalStateReport>),
    Waveform(Box<WaveformStream>),

    Description(Box<DescriptionModificationReport>),
}

impl TryFrom<sdc::EpisodicReport> for EpisodicReport {
    type Error = MappingError;

    fn try_from(value: sdc::EpisodicReport) -> Result<Self, Self::Error> {
        match value
            .r#type
            .ok_or(MappingError::FromProtoMandatoryMissing {
                field_name: "message".to_string(),
            })? {
            Type::Alert(report) => Ok(EpisodicReport::Alert(Box::new(report.try_into()?))),
            Type::Component(report) => Ok(EpisodicReport::Component(Box::new(report.try_into()?))),
            Type::Metric(report) => Ok(EpisodicReport::Metric(Box::new(report.try_into()?))),
            Type::OperationalState(report) => {
                Ok(EpisodicReport::Operation(Box::new(report.try_into()?)))
            }
            Type::Context(report) => Ok(EpisodicReport::Context(Box::new(report.try_into()?))),
            Type::Waveform(report) => Ok(EpisodicReport::Waveform(Box::new(report.try_into()?))),
            Type::Description(report) => {
                Ok(EpisodicReport::Description(Box::new(report.try_into()?)))
            }
        }
    }
}

impl From<EpisodicReport> for sdc::EpisodicReport {
    fn from(value: EpisodicReport) -> Self {
        Self {
            r#type: Some(match value {
                EpisodicReport::Alert(rep) => Type::Alert((*rep).into()),
                EpisodicReport::Component(rep) => Type::Component((*rep).into()),
                EpisodicReport::Context(rep) => Type::Context((*rep).into()),
                EpisodicReport::Metric(rep) => Type::Metric((*rep).into()),
                EpisodicReport::Operation(rep) => Type::OperationalState((*rep).into()),
                EpisodicReport::Waveform(rep) => Type::Waveform((*rep).into()),
                EpisodicReport::Description(rep) => Type::Description((*rep).into()),
            }),
        }
    }
}

impl EpisodicReport {
    pub fn mdib_version(&self) -> &MdibVersionGroup {
        match self {
            EpisodicReport::Alert(report) => {
                &report
                    .abstract_alert_report
                    .abstract_report
                    .mdib_version_group_attr
            }
            EpisodicReport::Component(report) => {
                &report
                    .abstract_component_report
                    .abstract_report
                    .mdib_version_group_attr
            }
            EpisodicReport::Context(report) => {
                &report
                    .abstract_context_report
                    .abstract_report
                    .mdib_version_group_attr
            }
            EpisodicReport::Metric(report) => {
                &report
                    .abstract_metric_report
                    .abstract_report
                    .mdib_version_group_attr
            }
            EpisodicReport::Operation(report) => {
                &report
                    .abstract_operational_state_report
                    .abstract_report
                    .mdib_version_group_attr
            }
            EpisodicReport::Waveform(report) => &report.abstract_report.mdib_version_group_attr,
            EpisodicReport::Description(report) => &report.abstract_report.mdib_version_group_attr,
        }
    }
}
