use const_format::concatcp;

const SERVICE_PREFIX: &str = "org.somda.protosdc.service.";

pub const METADATA_SERVICE: &str = concatcp!(SERVICE_PREFIX, ".metadata.Metadata");
pub const GET_SERVICE: &str = concatcp!(SERVICE_PREFIX, "Get");
pub const SET_SERVICE: &str = concatcp!(SERVICE_PREFIX, "Set");
pub const MDIB_REPORTING_SERVICE: &str = concatcp!(SERVICE_PREFIX, "MdibReporting");
pub const LOCALIZATION_SERVICE: &str = concatcp!(SERVICE_PREFIX, "Localization");
