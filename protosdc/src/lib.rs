pub mod common {
    pub mod action;
    pub mod crypto;
    pub mod mdib;
    pub mod middleware {
        pub mod uri_changer;
    }
    pub mod hyper_client;
    pub mod reports;
    pub mod service;
    pub mod uuid;
}

pub mod addressing {
    pub mod util;
}

pub mod discovery {
    pub mod common {
        pub mod error;
        pub(crate) mod util;
    }
    pub mod consumer {
        pub mod discovery_consumer;
    }
    pub mod provider {
        pub mod discovery_provider;
    }
}

pub mod consumer {
    pub mod grpc_consumer;
    pub mod remote_device;
    pub mod remote_device_connector;
    pub mod report_processor;
    pub mod report_writer;
    pub mod sco {
        pub(crate) mod operation_invocation_dispatcher;
        pub mod sco_controller;
        pub mod sco_transaction;
        pub(crate) mod sco_util;
    }
}

pub mod provider {
    pub mod common;
    pub mod device;
    pub mod localization_storage;
    pub mod mapper;
    pub mod metadata;
    pub mod report_generator;
    pub mod sco;
    pub mod sco_util;

    pub mod plugins;
    pub mod services;
}
