use uuid::Uuid;

use protosdc_proto::addressing::Addressing;

/// Creates an addressing element for a given action with a random message id.
///
/// # Arguments
///
/// * `action`: action to create addressing for
///
/// returns: Addressing
///
/// # Examples
///
/// ```
/// use protosdc::addressing::util::create_for_action;
/// use protosdc::common::action;
/// let addressing = create_for_action(action::GET_METADATA_REQUEST);
/// ```
pub fn create_for_action(action: &str) -> Addressing {
    Addressing {
        action: action.to_string(),
        message_id: Uuid::new_v4().urn().to_string(),
        relates_id: None,
    }
}

/// Creates an addressing element for a given action with a random message id and a relates_id set.
///
/// # Arguments
///
/// * `request`: addressing element to generate a reply for
/// * `action`: action to create addressing for
///
/// returns: Addressing
pub fn create_reply_to(request: &Addressing, action: &str) -> Addressing {
    Addressing {
        action: action.to_string(),
        message_id: Uuid::new_v4().urn().to_string(),
        relates_id: Some(request.message_id.clone()),
    }
}

/// Sets the `message_id` of an `Addressing` element to a random uuid.
///
/// # Arguments
///
/// * `request`: to set message id in
pub fn set_random_message_id(request: &mut Addressing) {
    request.message_id = Uuid::new_v4().urn().to_string();
}
