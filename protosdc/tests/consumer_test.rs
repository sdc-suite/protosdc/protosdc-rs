use std::net::{IpAddr, SocketAddr};
use std::time::{Duration, Instant};

use anyhow::Result;
use futures::StreamExt;
use log::{debug, info};
use tokio_stream::wrappers::ReceiverStream;

use biceps::common::access::mdib_access::{MdibAccess, ObservableMdib};
use biceps::common::biceps_util::MetricState;
use biceps::common::mdib_entity::Entity;
use biceps::common::mdib_state_modifications::MdibStateModifications;
use biceps::provider::access::local_mdib_access::LocalMdibAccess;
use biceps::test_util::handles;
use biceps::test_util::mdib::mdib_modification_presets;
use common::Service;
use network::platform::test_util::determine_adapter_address_for_multicast;
use network::udp_binding::IPV4_MULTICAST_ADDRESS;
use protosdc::common::uuid::generate_provider_epr_uuid;
use protosdc::consumer::grpc_consumer::{Consumer, ConsumerImpl};
use protosdc::consumer::remote_device_connector;
use protosdc::provider::device::{SdcDevice, ServerConfig};
use protosdc::provider::metadata::DeviceMetadata;
use protosdc::provider::plugins::ScopesPlugin;
use protosdc::provider::sco::DummyOperationInvocationReceiver;
use protosdc_biceps::biceps::AbstractStateOneOf;

#[cfg(test)]
pub mod test_util;

#[tokio::test]
async fn test_remote_device_connector_connection() -> Result<()> {
    remote_device_connector_connection(1000).await
}

async fn remote_device_connector_connection(num_messages: usize) -> Result<()> {
    common::test_util::init_logging();

    // find address which does ipv4 multicast
    let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
    let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

    let server_config = ServerConfig {
        crypto_config: None,
        server_address: SocketAddr::new(adapter_address, 0),
    };
    let mdib_data = mdib_modification_presets();
    let sequence = "prefix:seq";

    let dummy_op_inv_rec = DummyOperationInvocationReceiver {};

    let mut device = SdcDevice::new(
        sequence.to_string(),
        server_config,
        dummy_op_inv_rec,
        generate_provider_epr_uuid(),
        Some(DeviceMetadata::default()),
        None,
        None,
        ScopesPlugin::default(),
    )
    .await?;
    device.start().await?;
    let device_endpoint = device.endpoint().await;
    device.local_mdib.write_description(mdib_data).await?;

    let task_mdib = device.local_mdib.clone();

    let mut consumer = ConsumerImpl::default();
    consumer.connect(device_endpoint, None).await?;

    let remote_device = remote_device_connector::connect(consumer).await?;
    {
        let mdib_version = remote_device.mdib_version().await;
        info!("Remote Device Mdib Version: {:?}", mdib_version);
    }

    let remote_device_sub = remote_device.get_mdib_access().await.subscribe().await;
    let sub_collection_task = tokio::spawn(async move {
        let actual_messages = ReceiverStream::new(remote_device_sub)
            .take(num_messages)
            .fold(0, |x, _| async move { x + 1 })
            .await;
        info!(
            "Received {} messages in sub_collection_task",
            actual_messages
        )
    });

    let start = Instant::now();

    // start updating before the connection is done
    let update_task = tokio::spawn(async move {
        let mut locked = task_mdib;
        // update METRIC_0 every second
        for i in 0..num_messages {
            debug!("Updating METRIC_0 in provider to {}", i);
            let metric = locked
                .entity(handles::METRIC_0)
                .await
                .expect("METRIC_0 missing");

            let metric_state = metric.resolve_single_state().expect("state missing");
            let modifications = match metric_state {
                AbstractStateOneOf::NumericMetricState(mut state) => {
                    state.active_averaging_period_attr = Some(Duration::from_secs(i as u64).into());
                    MdibStateModifications::Metric {
                        metric_states: vec![state.into_abstract_metric_state_one_of()],
                    }
                }
                _ => panic!("Wrong state type"),
            };

            locked
                .write_states(modifications)
                .await
                .expect("Write failed");
        }
    });

    update_task.await.unwrap();
    sub_collection_task.await.unwrap();

    let task_done = start.elapsed();
    info!(
        "Sending {} messages took {}ms end-to-end",
        num_messages,
        task_done.as_millis()
    );

    {
        let mdib_version = remote_device.mdib_version().await;
        info!("Remote Device Mdib Version: {:?}", mdib_version);
    }

    // assert metric is equal on both sides
    let device_entity = { device.local_mdib.entity(handles::METRIC_0).await.unwrap() };
    let remote_entity = { remote_device.mdib_entity(handles::METRIC_0).await.unwrap() };

    let device_state = match device_entity.entity {
        Entity::NumericMetric(met) => met.pair.state,
        _ => panic!("wrong type"),
    };

    let remote_state = match remote_entity.entity {
        Entity::NumericMetric(met) => met.pair.state,
        _ => panic!("wrong type"),
    };

    assert_eq!(
        device_state.active_averaging_period_attr,
        remote_state.active_averaging_period_attr
    );

    Ok(())
}
