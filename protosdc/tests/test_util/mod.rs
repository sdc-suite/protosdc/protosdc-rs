#[cfg(test)]
use std::net::{IpAddr, SocketAddr};
use std::str::FromStr;

use anyhow::Result;
use log::debug;

use biceps::provider::access::local_mdib_access::LocalMdibAccess;
use biceps::test_util::mdib::mdib_modification_presets;
use common::crypto::CryptoConfig;
use network::udp_binding::UdpBindingImpl;
use protosdc::common::uuid::generate_provider_epr_uuid;
use protosdc::consumer::grpc_consumer::{Consumer, ConsumerImpl};
use protosdc::consumer::remote_device::{DefaultRemoteMdibAccessImpl, SdcRemoteDevice};
use protosdc::consumer::remote_device_connector;
use protosdc::consumer::sco::sco_controller::{ProtoSetServiceHandler, ScoController};
use protosdc::consumer::sco::sco_transaction::ScoTransactionImpl;
use protosdc::discovery::provider::discovery_provider::DiscoveryProviderImpl;
use protosdc::provider::device::{DefaultLocalMdibAccessImpl, SdcDevice, ServerConfig};
use protosdc::provider::localization_storage::LocalizationStorageImpl;
use protosdc::provider::metadata::DeviceMetadata;
use protosdc::provider::plugins::ScopesPlugin;
use protosdc::provider::sco::{DummyOperationInvocationReceiver, OperationInvocationReceiver};
use protosdc_proto::discovery::Endpoint;

pub async fn create_device_generic<T>(
    receiver: T,
    adapter_address: IpAddr,
    crypto_config: Option<CryptoConfig>,
    localization: Option<LocalizationStorageImpl>,
) -> Result<
    SdcDevice<
        DiscoveryProviderImpl<UdpBindingImpl>,
        T,
        DefaultLocalMdibAccessImpl,
        LocalizationStorageImpl,
        ScopesPlugin,
    >,
>
where
    T: 'static + OperationInvocationReceiver<DefaultLocalMdibAccessImpl> + Sync + Send,
{
    let server_config = ServerConfig {
        crypto_config,
        server_address: SocketAddr::from_str(format!("{}:0", adapter_address).as_str())?,
    };

    let mdib_data = mdib_modification_presets();
    let sequence = "prefix:seq";

    let mut device = SdcDevice::new(
        sequence.to_string(),
        server_config,
        receiver,
        generate_provider_epr_uuid(),
        Some(DeviceMetadata::default()),
        None,
        localization,
        ScopesPlugin::default(),
    )
    .await?;
    device.local_mdib.write_description(mdib_data).await?;

    Ok(device)
}

pub async fn create_device(
    adapter_address: IpAddr,
    crypto_config: Option<CryptoConfig>,
    localization: Option<LocalizationStorageImpl>,
) -> Result<
    SdcDevice<
        DiscoveryProviderImpl<UdpBindingImpl>,
        DummyOperationInvocationReceiver,
        DefaultLocalMdibAccessImpl,
        LocalizationStorageImpl,
        ScopesPlugin,
    >,
> {
    create_device_generic(
        DummyOperationInvocationReceiver {},
        adapter_address,
        crypto_config,
        localization,
    )
    .await
}

pub async fn connect_consumer(
    endpoint: Endpoint,
    crypto_config: Option<CryptoConfig>,
) -> Result<
    SdcRemoteDevice<
        ConsumerImpl,
        DefaultRemoteMdibAccessImpl,
        ScoController<ProtoSetServiceHandler>,
        ScoTransactionImpl,
    >,
> {
    debug!("Connecting consumer");
    let mut consumer = ConsumerImpl::default();
    consumer.connect(endpoint, crypto_config).await?;

    let remote_device = remote_device_connector::connect(consumer).await?;
    let mdib_version = remote_device.mdib_version().await;
    debug!("Remote Device Mdib Version: {:?}", mdib_version);

    Ok(remote_device)
}
