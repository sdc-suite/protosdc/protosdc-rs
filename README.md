<p align="center">
  <img width="200px" src="./media/logo.png" alt="protosdc-rs">
</p>

# protoSDC-rs

protoSDC-rs is a Rust implementation of the IEEE 11073 Service-Oriented Device Connectivity (SDC) standard. It supports both gRPC and protobuf, as well as ISO/IEEE 11073-20702 Medical Device Profile for Web Services (MDPWS).

For more information on protoSDC, please visit the [official website](https://protosdc.org). For more information on the IEEE 11073 standards, please refer to the [Wikipedia page](https://en.wikipedia.org/wiki/IEEE_11073_service-oriented_device_connectivity).

## Building the Project

To build the project, it is recommended to have the `protoc` executable in your `PATH`. If it's not present, [prost-build](https://github.com/tokio-rs/prost) will attempt to build it. However, this process can be complex on Windows and is best avoided.

### Windows

Download `protoc` from the [official Github repository](https://github.com/protocolbuffers/protobuf/releases) and [add it to your `PATH`](https://stackoverflow.com/questions/44272416/how-to-add-a-folder-to-path-environment-variable-in-windows-10-with-screensho).

### Linux

Install `protobuf` using your distribution's package manager. For example, on Arch Linux:

```bash
pacman -S protobuf
```

### macOS

Install `protobuf` using Homebrew:

```bash
brew install protobuf
```

## Getting Started

To start using protoSDC-rs, refer to the examples provided in the `sdc/examples` directory.

## Project Overview

- **biceps**: Implements the Medical Device Information Base (MDIB) functionality.
- **common**: Provides common functionality for the protoSDC-rs project.
- **common_test**: Offers commonly used functionality for testing purposes.
- **dpws**: Implements a transport compatible with the ISO/IEEE 11073-20702 standard.
- **network**: Handles the UDP networking functionality for the protoSDC-rs project.
- **protosdc**: Implements the [protoSDC](https://protosdc.org) standard.
- **sdc**: Provides a transport-agnostic way to utilize SDC over both MDPWS (via the `dpws` crate) and `protosdc`. This is the recommended way to use protoSDC-rs.

## License

This project is licensed under the [Mozilla Public License Version 2.0](LICENSE).

## Disclaimer

This project operates independently and is not affiliated with or endorsed by any manufacturer of medical devices. It is a collaborative effort developed by a group of volunteers.
