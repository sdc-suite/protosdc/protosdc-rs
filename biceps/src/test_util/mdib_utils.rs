use protosdc_biceps::biceps::abstract_device_component_descriptor_mod::ProductionSpecification;
use protosdc_biceps::biceps::mds_descriptor_mod::MetaData;
use protosdc_biceps::biceps::{
    alert_activation_mod, alert_condition_kind_mod, alert_condition_priority_mod,
    alert_signal_manifestation_mod, metric_availability_mod, metric_category_mod,
    operating_mode_mod, AbstractAlertDescriptor, AbstractAlertState,
    AbstractComplexDeviceComponentDescriptor, AbstractComplexDeviceComponentState,
    AbstractContextDescriptor, AbstractContextState, AbstractDescriptor,
    AbstractDeviceComponentDescriptor, AbstractDeviceComponentState, AbstractMetricDescriptor,
    AbstractMetricState, AbstractMultiState, AbstractOperationDescriptor, AbstractOperationState,
    AbstractSetStateOperationDescriptor, AbstractState, ActivateOperationDescriptor,
    ActivateOperationState, AlertActivation, AlertConditionDescriptor, AlertConditionKind,
    AlertConditionPriority, AlertConditionState, AlertSignalDescriptor, AlertSignalManifestation,
    AlertSignalState, AlertSystemDescriptor, AlertSystemState, ApprovedJurisdictions,
    BatteryDescriptor, BatteryState, CalibrationInfo, ChannelDescriptor, ChannelState,
    ClockDescriptor, ClockState, CodeIdentifier, CodedValue, ComponentActivation, Handle,
    HandleRef, LocationContextDescriptor, LocationContextState, MdsDescriptor, MdsState,
    MeansContextDescriptor, MeansContextState, MetricAvailability, MetricCategory,
    NumericMetricDescriptor, NumericMetricState, OperatingMode, PatientContextDescriptor,
    PatientContextState, PhysicalConnectorInfo, RealTimeSampleArrayMetricDescriptor,
    RealTimeSampleArrayMetricState, ReferencedVersion, SafetyClassification, ScoDescriptor,
    ScoState, SetAlertStateOperationDescriptor, SetAlertStateOperationState,
    SetComponentStateOperationDescriptor, SetComponentStateOperationState,
    SetContextStateOperationDescriptor, SetContextStateOperationState,
    SetMetricStateOperationDescriptor, SetMetricStateOperationState, SetStringOperationDescriptor,
    SetStringOperationState, SetValueOperationDescriptor, SetValueOperationState,
    StringMetricDescriptor, StringMetricState, SystemContextDescriptor, SystemContextState,
    VersionCounter, VmdDescriptor, VmdState,
};

use protosdc_biceps::types::{ProtoDecimal, ProtoDuration};

const DEFAULT_CODE: &str = "MinimalCodedValue";

// generic creators
pub fn create_version_counter(version: Option<u64>) -> Option<VersionCounter> {
    version.map(|v| VersionCounter { unsigned_long: v })
}

pub fn create_referenced_version(version: Option<u64>) -> Option<ReferencedVersion> {
    version.map(|v| ReferencedVersion {
        version_counter: VersionCounter { unsigned_long: v },
    })
}

pub fn create_minimal_coded_value(code: &str) -> CodedValue {
    CodedValue {
        extension_element: None,
        coding_system_name: vec![],
        concept_description: vec![],
        translation: vec![],
        code_attr: CodeIdentifier {
            string: code.to_string(),
        },
        coding_system_attr: None,
        coding_system_version_attr: None,
        symbolic_code_name_attr: None,
    }
}

// full abstract creators (excluding extensions)
pub fn create_abstract_descriptor(
    type_p: Option<CodedValue>,
    handle: &str,
    version: Option<u64>,
    safety_classification: Option<SafetyClassification>,
) -> AbstractDescriptor {
    AbstractDescriptor {
        extension_element: None,
        r#type: type_p,
        handle_attr: Handle {
            string: handle.to_string(),
        },
        descriptor_version_attr: create_version_counter(version),
        safety_classification_attr: safety_classification,
    }
}

pub fn create_abstract_device_component_descriptor(
    abstract_descriptor: AbstractDescriptor,
    production_specification: Vec<ProductionSpecification>,
) -> AbstractDeviceComponentDescriptor {
    AbstractDeviceComponentDescriptor {
        abstract_descriptor,
        production_specification,
    }
}

pub fn create_abstract_complex_device_component_descriptor(
    abstract_device_component_descriptor: AbstractDeviceComponentDescriptor,
    alert_system: Option<AlertSystemDescriptor>,
    sco: Option<ScoDescriptor>,
) -> AbstractComplexDeviceComponentDescriptor {
    AbstractComplexDeviceComponentDescriptor {
        abstract_device_component_descriptor,
        alert_system,
        sco,
    }
}

pub fn create_abstract_alert_descriptor(
    abstract_descriptor: AbstractDescriptor,
) -> AbstractAlertDescriptor {
    AbstractAlertDescriptor {
        abstract_descriptor,
    }
}

// minimal abstract descriptors
pub fn create_minimal_abstract_descriptor(handle: &str) -> AbstractDescriptor {
    create_abstract_descriptor(None, handle, None, None)
}

pub fn create_minimal_abstract_device_component_descriptor(
    handle: &str,
) -> AbstractDeviceComponentDescriptor {
    create_abstract_device_component_descriptor(create_minimal_abstract_descriptor(handle), vec![])
}

pub fn create_minimal_abstract_complex_device_component_descriptor(
    handle: &str,
) -> AbstractComplexDeviceComponentDescriptor {
    create_abstract_complex_device_component_descriptor(
        create_minimal_abstract_device_component_descriptor(handle),
        None,
        None,
    )
}

pub fn create_minimal_abstract_metric_descriptor(handle: &str) -> AbstractMetricDescriptor {
    AbstractMetricDescriptor {
        abstract_descriptor: create_minimal_abstract_descriptor(handle),
        unit: create_minimal_coded_value(DEFAULT_CODE),
        body_site: vec![],
        relation: vec![],
        metric_category_attr: MetricCategory {
            enum_type: metric_category_mod::EnumType::Unspec,
        },
        derivation_method_attr: None,
        metric_availability_attr: MetricAvailability {
            enum_type: metric_availability_mod::EnumType::Intr,
        },
        max_measurement_time_attr: None,
        max_delay_time_attr: None,
        determination_period_attr: None,
        life_time_period_attr: None,
        activation_duration_attr: None,
    }
}

pub fn create_minimal_abstract_alert_descriptor(handle: &str) -> AbstractAlertDescriptor {
    create_abstract_alert_descriptor(create_minimal_abstract_descriptor(handle))
}

pub fn create_minimal_abstract_context_descriptor(handle: &str) -> AbstractContextDescriptor {
    AbstractContextDescriptor {
        abstract_descriptor: create_minimal_abstract_descriptor(handle),
    }
}

pub fn create_minimal_abstract_operation_descriptor(
    handle: &str,
    op_target: &str,
) -> AbstractOperationDescriptor {
    AbstractOperationDescriptor {
        abstract_descriptor: create_minimal_abstract_descriptor(handle),
        operation_target_attr: HandleRef {
            string: op_target.to_string(),
        },
        max_time_to_finish_attr: None,
        invocation_effective_timeout_attr: None,
        retriggerable_attr: None,
        access_level_attr: None,
    }
}

pub fn create_minimal_abstract_set_state_operation_descriptor(
    handle: &str,
    op_target: &str,
) -> AbstractSetStateOperationDescriptor {
    AbstractSetStateOperationDescriptor {
        abstract_operation_descriptor: create_minimal_abstract_operation_descriptor(
            handle, op_target,
        ),
        modifiable_data: vec![],
    }
}

// component descriptors
pub fn create_mds_descriptor(
    abstract_complex_device_component_descriptor: AbstractComplexDeviceComponentDescriptor,
    meta_data: Option<MetaData>,
    system_context: Option<SystemContextDescriptor>,
    clock: Option<ClockDescriptor>,
    battery: Vec<BatteryDescriptor>,
    approved_jurisdiction: Option<ApprovedJurisdictions>,
    vmd: Vec<VmdDescriptor>,
) -> MdsDescriptor {
    MdsDescriptor {
        abstract_complex_device_component_descriptor,
        meta_data,
        system_context,
        clock,
        battery,
        approved_jurisdictions: approved_jurisdiction,
        vmd,
    }
}

// minimal component descriptors
pub fn create_minimal_mds_descriptor(handle: &str) -> MdsDescriptor {
    create_mds_descriptor(
        create_minimal_abstract_complex_device_component_descriptor(handle),
        None,
        None,
        None,
        vec![],
        None,
        vec![],
    )
}

pub fn create_minimal_system_context_descriptor(handle: &str) -> SystemContextDescriptor {
    SystemContextDescriptor {
        abstract_device_component_descriptor: create_minimal_abstract_device_component_descriptor(
            handle,
        ),
        patient_context: None,
        location_context: None,
        ensemble_context: vec![],
        operator_context: vec![],
        workflow_context: vec![],
        means_context: vec![],
    }
}

pub fn create_minimal_vmd_descriptor(handle: &str) -> VmdDescriptor {
    VmdDescriptor {
        abstract_complex_device_component_descriptor:
            create_minimal_abstract_complex_device_component_descriptor(handle),
        approved_jurisdictions: None,
        channel: vec![],
    }
}

pub fn create_minimal_channel_descriptor(handle: &str) -> ChannelDescriptor {
    ChannelDescriptor {
        abstract_device_component_descriptor: create_minimal_abstract_device_component_descriptor(
            handle,
        ),
        metric: vec![],
    }
}

pub fn create_minimal_sco_descriptor(handle: &str) -> ScoDescriptor {
    ScoDescriptor {
        abstract_device_component_descriptor: create_minimal_abstract_device_component_descriptor(
            handle,
        ),
        operation: vec![],
    }
}

pub fn create_minimal_clock_descriptor(handle: &str) -> ClockDescriptor {
    ClockDescriptor {
        abstract_device_component_descriptor: create_minimal_abstract_device_component_descriptor(
            handle,
        ),
        time_protocol: vec![],
        resolution_attr: None,
    }
}

pub fn create_minimal_battery_descriptor(handle: &str) -> BatteryDescriptor {
    BatteryDescriptor {
        abstract_device_component_descriptor: create_minimal_abstract_device_component_descriptor(
            handle,
        ),
        capacity_full_charge: None,
        capacity_specified: None,
        voltage_specified: None,
    }
}

// minimal context descriptors
pub fn create_minimal_patient_context_descriptor(handle: &str) -> PatientContextDescriptor {
    PatientContextDescriptor {
        abstract_context_descriptor: create_minimal_abstract_context_descriptor(handle),
    }
}

pub fn create_minimal_location_context_descriptor(handle: &str) -> LocationContextDescriptor {
    LocationContextDescriptor {
        abstract_context_descriptor: create_minimal_abstract_context_descriptor(handle),
    }
}

pub fn create_minimal_means_context_descriptor(handle: &str) -> MeansContextDescriptor {
    MeansContextDescriptor {
        abstract_context_descriptor: create_minimal_abstract_context_descriptor(handle),
    }
}

// minimal metric descriptors
pub fn create_minimal_string_metric_descriptor(handle: &str) -> StringMetricDescriptor {
    StringMetricDescriptor {
        abstract_metric_descriptor: create_minimal_abstract_metric_descriptor(handle),
    }
}

pub fn create_minimal_numeric_metric_descriptor(handle: &str) -> NumericMetricDescriptor {
    NumericMetricDescriptor {
        abstract_metric_descriptor: create_minimal_abstract_metric_descriptor(handle),
        technical_range: vec![],
        resolution_attr: ProtoDecimal::default(),
        averaging_period_attr: None,
    }
}

pub fn create_minimal_real_time_sample_array_metric_descriptor(
    handle: &str,
) -> RealTimeSampleArrayMetricDescriptor {
    RealTimeSampleArrayMetricDescriptor {
        abstract_metric_descriptor: create_minimal_abstract_metric_descriptor(handle),
        technical_range: vec![],
        resolution_attr: ProtoDecimal::default(),
        sample_period_attr: ProtoDuration::default(),
    }
}

// alert descriptors
pub fn create_minimal_alert_system_descriptor(handle: &str) -> AlertSystemDescriptor {
    AlertSystemDescriptor {
        abstract_alert_descriptor: create_minimal_abstract_alert_descriptor(handle),
        alert_condition: vec![],
        alert_signal: vec![],
        max_physiological_parallel_alarms_attr: None,
        max_technical_parallel_alarms_attr: None,
        self_check_period_attr: None,
    }
}

pub fn create_minimal_alert_condition_descriptor(handle: &str) -> AlertConditionDescriptor {
    AlertConditionDescriptor {
        abstract_alert_descriptor: create_minimal_abstract_alert_descriptor(handle),
        source: vec![],
        cause_info: vec![],
        kind_attr: AlertConditionKind {
            enum_type: alert_condition_kind_mod::EnumType::Phy,
        },
        priority_attr: AlertConditionPriority {
            enum_type: alert_condition_priority_mod::EnumType::Lo,
        },
        default_condition_generation_delay_attr: None,
        can_escalate_attr: None,
        can_deescalate_attr: None,
    }
}

pub fn create_minimal_alert_signal_descriptor(handle: &str) -> AlertSignalDescriptor {
    AlertSignalDescriptor {
        abstract_alert_descriptor: create_minimal_abstract_alert_descriptor(handle),
        condition_signaled_attr: None,
        manifestation_attr: AlertSignalManifestation {
            enum_type: alert_signal_manifestation_mod::EnumType::Aud,
        },
        latching_attr: false,
        default_signal_generation_delay_attr: None,
        min_signal_generation_delay_attr: None,
        max_signal_generation_delay_attr: None,
        signal_delegation_supported_attr: None,
        acknowledgement_supported_attr: None,
        acknowledge_timeout_attr: None,
    }
}

// operation descriptors
pub fn create_minimal_activate_operation_descriptor(
    handle: &str,
    op_target: &str,
) -> ActivateOperationDescriptor {
    ActivateOperationDescriptor {
        abstract_set_state_operation_descriptor:
            create_minimal_abstract_set_state_operation_descriptor(handle, op_target),
        argument: vec![],
    }
}

pub fn create_minimal_set_alert_state_operation_descriptor(
    handle: &str,
    op_target: &str,
) -> SetAlertStateOperationDescriptor {
    SetAlertStateOperationDescriptor {
        abstract_set_state_operation_descriptor:
            create_minimal_abstract_set_state_operation_descriptor(handle, op_target),
    }
}

pub fn create_minimal_set_component_state_operation_descriptor(
    handle: &str,
    op_target: &str,
) -> SetComponentStateOperationDescriptor {
    SetComponentStateOperationDescriptor {
        abstract_set_state_operation_descriptor:
            create_minimal_abstract_set_state_operation_descriptor(handle, op_target),
    }
}

pub fn create_minimal_set_context_state_operation_descriptor(
    handle: &str,
    op_target: &str,
) -> SetContextStateOperationDescriptor {
    SetContextStateOperationDescriptor {
        abstract_set_state_operation_descriptor:
            create_minimal_abstract_set_state_operation_descriptor(handle, op_target),
    }
}

pub fn create_minimal_set_metric_state_operation_descriptor(
    handle: &str,
    op_target: &str,
) -> SetMetricStateOperationDescriptor {
    SetMetricStateOperationDescriptor {
        abstract_set_state_operation_descriptor:
            create_minimal_abstract_set_state_operation_descriptor(handle, op_target),
    }
}

pub fn create_minimal_set_string_operation_descriptor(
    handle: &str,
    op_target: &str,
) -> SetStringOperationDescriptor {
    SetStringOperationDescriptor {
        abstract_operation_descriptor: create_minimal_abstract_operation_descriptor(
            handle, op_target,
        ),
        max_length_attr: None,
    }
}

pub fn create_minimal_set_value_operation_descriptor(
    handle: &str,
    op_target: &str,
) -> SetValueOperationDescriptor {
    SetValueOperationDescriptor {
        abstract_operation_descriptor: create_minimal_abstract_operation_descriptor(
            handle, op_target,
        ),
    }
}

// full abstract states (excluding extensions)
pub fn create_abstract_state(
    version: Option<u64>,
    handle: &str,
    descriptor_version: Option<u64>,
) -> AbstractState {
    AbstractState {
        extension_element: None,
        state_version_attr: create_version_counter(version),
        descriptor_handle_attr: HandleRef {
            string: handle.to_string(),
        },
        descriptor_version_attr: create_referenced_version(descriptor_version),
    }
}

pub fn create_abstract_metric_state(
    abstract_state: AbstractState,
    body_site: Vec<CodedValue>,
    physical_connector: Option<PhysicalConnectorInfo>,
    activation_state: Option<ComponentActivation>,
    active_determination_period: Option<std::time::Duration>,
    life_time_period: Option<std::time::Duration>,
) -> AbstractMetricState {
    AbstractMetricState {
        abstract_state,
        body_site,
        physical_connector,
        activation_state_attr: activation_state,
        active_determination_period_attr: active_determination_period.map(|it| it.into()),
        life_time_period_attr: life_time_period.map(|it| it.into()),
    }
}

pub fn create_abstract_device_component_state(
    abstract_state: AbstractState,
    calibration_info: Option<CalibrationInfo>,
    next_calibration: Option<CalibrationInfo>,
    physical_connector: Option<PhysicalConnectorInfo>,
    activation_state: Option<ComponentActivation>,
    operating_hours: Option<u32>,
    operating_cycles: Option<i32>,
) -> AbstractDeviceComponentState {
    AbstractDeviceComponentState {
        abstract_state,
        calibration_info,
        next_calibration,
        physical_connector,
        activation_state_attr: activation_state,
        operating_hours_attr: operating_hours,
        operating_cycles_attr: operating_cycles,
    }
}

pub fn create_abstract_complex_device_component_state(
    abstract_device_component_state: AbstractDeviceComponentState,
) -> AbstractComplexDeviceComponentState {
    AbstractComplexDeviceComponentState {
        abstract_device_component_state,
    }
}

pub fn create_abstract_alert_state(
    abstract_state: AbstractState,
    activation_state: AlertActivation,
) -> AbstractAlertState {
    AbstractAlertState {
        abstract_state,
        activation_state_attr: activation_state,
    }
}

// minimal abstract states
pub fn create_minimal_abstract_state(handle: &str) -> AbstractState {
    create_abstract_state(None, handle, None)
}

pub fn create_minimal_abstract_metric_state(handle: &str) -> AbstractMetricState {
    create_abstract_metric_state(
        create_minimal_abstract_state(handle),
        vec![],
        None,
        None,
        None,
        None,
    )
}

pub fn create_minimal_abstract_device_component_state(
    handle: &str,
) -> AbstractDeviceComponentState {
    create_abstract_device_component_state(
        create_minimal_abstract_state(handle),
        None,
        None,
        None,
        None,
        None,
        None,
    )
}

pub fn create_minimal_abstract_complex_device_component_state(
    handle: &str,
) -> AbstractComplexDeviceComponentState {
    create_abstract_complex_device_component_state(create_minimal_abstract_device_component_state(
        handle,
    ))
}

pub fn create_minimal_abstract_alert_state(handle: &str) -> AbstractAlertState {
    create_abstract_alert_state(
        create_minimal_abstract_state(handle),
        AlertActivation {
            enum_type: alert_activation_mod::EnumType::On,
        },
    )
}

pub fn create_minimal_abstract_multi_state(
    descriptor_handle: &str,
    state_handle: &str,
) -> AbstractMultiState {
    AbstractMultiState {
        abstract_state: create_minimal_abstract_state(descriptor_handle),
        category: None,
        handle_attr: Handle {
            string: state_handle.to_string(),
        },
    }
}

pub fn create_minimal_abstract_context_state(
    descriptor_handle: &str,
    state_handle: &str,
) -> AbstractContextState {
    AbstractContextState {
        abstract_multi_state: create_minimal_abstract_multi_state(descriptor_handle, state_handle),
        validator: vec![],
        identification: vec![],
        context_association_attr: None,
        binding_mdib_version_attr: None,
        unbinding_mdib_version_attr: None,
        binding_start_time_attr: None,
        binding_end_time_attr: None,
    }
}

pub fn create_minimal_abstract_operation_state(descriptor_handle: &str) -> AbstractOperationState {
    AbstractOperationState {
        abstract_state: create_minimal_abstract_state(descriptor_handle),
        operating_mode_attr: OperatingMode {
            enum_type: operating_mode_mod::EnumType::Dis,
        },
    }
}

// component states
pub fn create_minimal_mds_state(handle: &str) -> MdsState {
    MdsState {
        abstract_complex_device_component_state:
            create_minimal_abstract_complex_device_component_state(handle),
        operating_jurisdiction: None,
        lang_attr: None,
        operating_mode_attr: None,
    }
}

pub fn create_minimal_system_context_state(handle: &str) -> SystemContextState {
    SystemContextState {
        abstract_device_component_state: create_minimal_abstract_device_component_state(handle),
    }
}

pub fn create_minimal_vmd_state(handle: &str) -> VmdState {
    VmdState {
        abstract_complex_device_component_state:
            create_minimal_abstract_complex_device_component_state(handle),
        operating_jurisdiction: None,
    }
}

pub fn create_minimal_channel_state(handle: &str) -> ChannelState {
    ChannelState {
        abstract_device_component_state: create_minimal_abstract_device_component_state(handle),
    }
}

pub fn create_minimal_sco_state(handle: &str) -> ScoState {
    ScoState {
        abstract_device_component_state: create_minimal_abstract_device_component_state(handle),
        operation_group: vec![],
        invocation_requested_attr: None,
        invocation_required_attr: None,
    }
}

pub fn create_minimal_clock_state(handle: &str) -> ClockState {
    ClockState {
        abstract_device_component_state: create_minimal_abstract_device_component_state(handle),
        active_sync_protocol: None,
        reference_source: vec![],
        date_and_time_attr: None,
        remote_sync_attr: false,
        accuracy_attr: None,
        last_set_attr: None,
        time_zone_attr: None,
        critical_use_attr: None,
    }
}

pub fn create_minimal_battery_state(handle: &str) -> BatteryState {
    BatteryState {
        abstract_device_component_state: create_minimal_abstract_device_component_state(handle),
        capacity_remaining: None,
        voltage: None,
        current: None,
        temperature: None,
        remaining_battery_time: None,
        charge_status_attr: None,
        charge_cycles_attr: None,
    }
}

// minimal context states
pub fn create_minimal_patient_context_state(
    descriptor_handle: &str,
    state_handle: &str,
) -> PatientContextState {
    PatientContextState {
        abstract_context_state: create_minimal_abstract_context_state(
            descriptor_handle,
            state_handle,
        ),
        core_data: None,
    }
}

pub fn create_minimal_location_context_state(
    descriptor_handle: &str,
    state_handle: &str,
) -> LocationContextState {
    LocationContextState {
        abstract_context_state: create_minimal_abstract_context_state(
            descriptor_handle,
            state_handle,
        ),
        location_detail: None,
    }
}

pub fn create_minimal_means_context_state(
    descriptor_handle: &str,
    state_handle: &str,
) -> MeansContextState {
    MeansContextState {
        abstract_context_state: create_minimal_abstract_context_state(
            descriptor_handle,
            state_handle,
        ),
    }
}

// metric states
pub fn create_minimal_string_metric_state(handle: &str) -> StringMetricState {
    StringMetricState {
        abstract_metric_state: create_minimal_abstract_metric_state(handle),
        metric_value: None,
    }
}

pub fn create_minimal_numeric_metric_state(handle: &str) -> NumericMetricState {
    NumericMetricState {
        abstract_metric_state: create_minimal_abstract_metric_state(handle),
        metric_value: None,
        physiological_range: vec![],
        active_averaging_period_attr: None,
    }
}

pub fn create_minimal_real_time_sample_array_metric_state(
    handle: &str,
) -> RealTimeSampleArrayMetricState {
    RealTimeSampleArrayMetricState {
        abstract_metric_state: create_minimal_abstract_metric_state(handle),
        metric_value: None,
        physiological_range: vec![],
    }
}

// alert states
pub fn create_minimal_alert_system_state(handle: &str) -> AlertSystemState {
    AlertSystemState {
        abstract_alert_state: create_minimal_abstract_alert_state(handle),
        system_signal_activation: vec![],
        last_self_check_attr: None,
        self_check_count_attr: None,
        present_physiological_alarm_conditions_attr: None,
        present_technical_alarm_conditions_attr: None,
    }
}

pub fn create_minimal_alert_condition_state(handle: &str) -> AlertConditionState {
    AlertConditionState {
        abstract_alert_state: create_minimal_abstract_alert_state(handle),
        actual_condition_generation_delay_attr: None,
        actual_priority_attr: None,
        rank_attr: None,
        presence_attr: None,
        determination_time_attr: None,
    }
}

pub fn create_minimal_alert_signal_state(handle: &str) -> AlertSignalState {
    AlertSignalState {
        abstract_alert_state: create_minimal_abstract_alert_state(handle),
        actual_signal_generation_delay_attr: None,
        presence_attr: None,
        location_attr: None,
        slot_attr: None,
    }
}

// operation states
pub fn create_minimal_activate_operation_state(handle: &str) -> ActivateOperationState {
    ActivateOperationState {
        abstract_operation_state: create_minimal_abstract_operation_state(handle),
    }
}

pub fn create_minimal_set_alert_state_operation_state(handle: &str) -> SetAlertStateOperationState {
    SetAlertStateOperationState {
        abstract_operation_state: create_minimal_abstract_operation_state(handle),
    }
}

pub fn create_minimal_set_component_state_operation_state(
    handle: &str,
) -> SetComponentStateOperationState {
    SetComponentStateOperationState {
        abstract_operation_state: create_minimal_abstract_operation_state(handle),
    }
}

pub fn create_minimal_set_context_state_operation_state(
    handle: &str,
) -> SetContextStateOperationState {
    SetContextStateOperationState {
        abstract_operation_state: create_minimal_abstract_operation_state(handle),
    }
}

pub fn create_minimal_set_metric_state_operation_state(
    handle: &str,
) -> SetMetricStateOperationState {
    SetMetricStateOperationState {
        abstract_operation_state: create_minimal_abstract_operation_state(handle),
    }
}

pub fn create_minimal_set_string_operation_state(handle: &str) -> SetStringOperationState {
    SetStringOperationState {
        abstract_operation_state: create_minimal_abstract_operation_state(handle),
        allowed_values: None,
    }
}

pub fn create_minimal_set_value_operation_state(handle: &str) -> SetValueOperationState {
    SetValueOperationState {
        abstract_operation_state: create_minimal_abstract_operation_state(handle),
        allowed_range: vec![],
    }
}
