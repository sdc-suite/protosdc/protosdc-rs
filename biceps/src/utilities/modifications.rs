use crate::common::biceps_util::{Descriptor, State};
use crate::common::mdib_description_modification::MdibDescriptionModification;
use crate::common::mdib_pair::{Pair, PairError};
use itertools::Itertools;
use protosdc_biceps::biceps::{
    AbstractDescriptorOneOf, AbstractStateOneOf, AlertSystemDescriptor, ChannelDescriptor, Mdib,
    MdsDescriptor, ScoDescriptor, SystemContextDescriptor, VmdDescriptor,
};
use std::collections::HashMap;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum MdibError {
    #[error("No MdState element in provided mdib")]
    NoMdState,
    #[error("No MdDescription element in provided mdib")]
    NoMdDescription,
    #[error(transparent)]
    PairError(#[from] PairError),
}

type StateMap = HashMap<String, Vec<AbstractStateOneOf>>;

/// Separates an mdib into a vec of description modifications to apply to an mdib.
///
/// # Arguments
///
/// * `mdib`: to separate into modifications
///
/// returns: Result<Vec<MdibDescriptionModification>, MdibError>
pub async fn modifications_from_mdib(
    mdib: Mdib,
) -> Result<Vec<MdibDescriptionModification>, MdibError> {
    let state_map: StateMap = mdib
        .md_state
        .ok_or(MdibError::NoMdState)?
        .state
        .into_iter()
        .fold(HashMap::new(), |mut map, value| {
            let descriptor_handle = value.descriptor_handle();
            let to_insert = match map.remove(&descriptor_handle) {
                None => vec![value],
                Some(mut v) => {
                    v.push(value);
                    v
                }
            };
            map.insert(descriptor_handle, to_insert);
            map
        });

    let modifications: Vec<MdibDescriptionModification> = mdib
        .md_description
        .ok_or(MdibError::NoMdDescription)?
        .mds
        .into_iter()
        // collect all descriptors first
        .try_fold(
            (state_map, vec![]),
            |(mut state_map, mut modifications), mds| {
                match collect_mds(mds, &mut state_map) {
                    Ok(coll) => {
                        modifications.extend(coll);
                        Ok((state_map, modifications))
                    }
                    Err(err) => Err(err),
                }
                // modifications.extend(?);
            },
        )?
        .1;

    Ok(modifications)
}

fn collect_mds(
    mut mds: MdsDescriptor,
    state_map: &mut StateMap,
) -> Result<Vec<MdibDescriptionModification>, PairError> {
    let mut descriptors: Vec<MdibDescriptionModification> = vec![];
    let mds_handle = mds.handle();

    if let Some(alert_system) = mds
        .abstract_complex_device_component_descriptor
        .alert_system
        .take()
    {
        descriptors.extend(collect_alert_system(alert_system, state_map, &mds_handle)?);
    }
    if let Some(sco) = mds.abstract_complex_device_component_descriptor.sco.take() {
        descriptors.extend(collect_sco(sco, state_map, &mds_handle)?);
    }

    if let Some(system_context) = mds.system_context.take() {
        descriptors.extend(collect_system_context(
            system_context,
            state_map,
            &mds_handle,
        )?);
    }
    if let Some(clock) = mds.clock.take() {
        descriptors.push(build_insert(
            clock.into_abstract_descriptor_one_of(),
            state_map,
            &mds_handle,
        )?);
    }
    descriptors.extend(
        mds.battery
            .drain(..)
            .map(|it| build_insert(it.into_abstract_descriptor_one_of(), state_map, &mds_handle))
            .collect::<Result<Vec<MdibDescriptionModification>, PairError>>()?,
    );
    descriptors.extend(
        mds.vmd
            .drain(..)
            .map(|it| collect_vmd(it, state_map, &mds_handle))
            .flatten_ok()
            .collect::<Result<Vec<MdibDescriptionModification>, PairError>>()?,
    );

    descriptors.insert(
        0,
        build_insert_mds(mds.into_abstract_descriptor_one_of(), state_map)?,
    );
    Ok(descriptors)
}

fn collect_alert_system(
    mut alert_system: AlertSystemDescriptor,
    state_map: &mut StateMap,
    parent: &str,
) -> Result<Vec<MdibDescriptionModification>, PairError> {
    let mut descriptors = vec![];
    let alert_system_handle = alert_system.handle();

    descriptors.extend(
        alert_system
            .alert_condition
            .drain(..)
            .map(|it| {
                build_insert(
                    it.into_abstract_descriptor_one_of(),
                    state_map,
                    &alert_system_handle,
                )
            })
            .collect::<Result<Vec<MdibDescriptionModification>, PairError>>()?,
    );
    descriptors.extend(
        alert_system
            .alert_signal
            .drain(..)
            .map(|it| {
                build_insert(
                    it.into_abstract_descriptor_one_of(),
                    state_map,
                    &alert_system_handle,
                )
            })
            .collect::<Result<Vec<MdibDescriptionModification>, PairError>>()?,
    );

    descriptors.insert(
        0,
        build_insert(
            alert_system.into_abstract_descriptor_one_of(),
            state_map,
            parent,
        )?,
    );
    Ok(descriptors)
}

fn collect_sco(
    mut sco: ScoDescriptor,
    state_map: &mut StateMap,
    parent: &str,
) -> Result<Vec<MdibDescriptionModification>, PairError> {
    let mut descriptors = vec![];
    let sco_handle = sco.handle();

    descriptors.extend(
        sco.operation
            .drain(..)
            .map(|it| build_insert(it.into_abstract_descriptor_one_of(), state_map, &sco_handle))
            .collect::<Result<Vec<MdibDescriptionModification>, PairError>>()?,
    );

    descriptors.insert(
        0,
        build_insert(sco.into_abstract_descriptor_one_of(), state_map, parent)?,
    );
    Ok(descriptors)
}

fn collect_system_context(
    mut system_context: SystemContextDescriptor,
    state_map: &mut StateMap,
    parent: &str,
) -> Result<Vec<MdibDescriptionModification>, PairError> {
    let mut descriptors: Vec<MdibDescriptionModification> = vec![];
    let system_context_handle = system_context.handle();

    if let Some(location_context) = system_context.location_context.take() {
        descriptors.push(build_insert(
            location_context.into_abstract_descriptor_one_of(),
            state_map,
            &system_context_handle,
        )?);
    }
    if let Some(patient_context) = system_context.patient_context.take() {
        descriptors.push(build_insert(
            patient_context.into_abstract_descriptor_one_of(),
            state_map,
            &system_context_handle,
        )?);
    }
    descriptors.extend(
        system_context
            .ensemble_context
            .drain(..)
            .map(|it| {
                build_insert(
                    it.into_abstract_descriptor_one_of(),
                    state_map,
                    &system_context_handle,
                )
            })
            .collect::<Result<Vec<MdibDescriptionModification>, PairError>>()?,
    );
    descriptors.extend(
        system_context
            .workflow_context
            .drain(..)
            .map(|it| {
                build_insert(
                    it.into_abstract_descriptor_one_of(),
                    state_map,
                    &system_context_handle,
                )
            })
            .collect::<Result<Vec<MdibDescriptionModification>, PairError>>()?,
    );
    descriptors.extend(
        system_context
            .operator_context
            .drain(..)
            .map(|it| {
                build_insert(
                    it.into_abstract_descriptor_one_of(),
                    state_map,
                    &system_context_handle,
                )
            })
            .collect::<Result<Vec<MdibDescriptionModification>, PairError>>()?,
    );
    descriptors.extend(
        system_context
            .means_context
            .drain(..)
            .map(|it| {
                build_insert(
                    it.into_abstract_descriptor_one_of(),
                    state_map,
                    &system_context_handle,
                )
            })
            .collect::<Result<Vec<MdibDescriptionModification>, PairError>>()?,
    );

    descriptors.insert(
        0,
        build_insert(
            system_context.into_abstract_descriptor_one_of(),
            state_map,
            parent,
        )?,
    );
    Ok(descriptors)
}

fn collect_vmd(
    mut vmd: VmdDescriptor,
    state_map: &mut StateMap,
    parent: &str,
) -> Result<Vec<MdibDescriptionModification>, PairError> {
    let mut descriptors: Vec<MdibDescriptionModification> = vec![];
    let vmd_handle = vmd.handle();

    if let Some(alert_system) = vmd
        .abstract_complex_device_component_descriptor
        .alert_system
        .take()
    {
        descriptors.extend(collect_alert_system(alert_system, state_map, &vmd_handle)?);
    }
    if let Some(sco) = vmd.abstract_complex_device_component_descriptor.sco.take() {
        descriptors.extend(collect_sco(sco, state_map, &vmd_handle)?);
    }
    descriptors.extend(
        vmd.channel
            .drain(..)
            .map(|it| collect_channel(it, state_map, &vmd_handle))
            .flatten_ok()
            .collect::<Result<Vec<MdibDescriptionModification>, PairError>>()?,
    );

    descriptors.insert(
        0,
        build_insert(vmd.into_abstract_descriptor_one_of(), state_map, parent)?,
    );
    Ok(descriptors)
}

fn collect_channel(
    mut channel: ChannelDescriptor,
    state_map: &mut StateMap,
    parent: &str,
) -> Result<Vec<MdibDescriptionModification>, PairError> {
    let mut descriptors = vec![];
    let channel_handle = channel.handle();

    descriptors.extend(
        channel
            .metric
            .drain(..)
            .map(|it| {
                build_insert(
                    it.into_abstract_descriptor_one_of(),
                    state_map,
                    &channel_handle,
                )
            })
            .collect::<Result<Vec<MdibDescriptionModification>, PairError>>()?,
    );

    descriptors.insert(
        0,
        build_insert(channel.into_abstract_descriptor_one_of(), state_map, parent)?,
    );
    Ok(descriptors)
}

fn build_insert(
    descriptor: AbstractDescriptorOneOf,
    state_map: &mut StateMap,
    parent: &str,
) -> Result<MdibDescriptionModification, PairError> {
    // cardinality errors are handled by preprocessing chain
    let states = state_map.remove(&descriptor.handle()).unwrap_or_default();

    let pair: Pair = (descriptor, states).try_into()?;

    Ok(MdibDescriptionModification::Insert {
        pair,
        parent: Some(parent.to_string()),
    })
}

fn build_insert_mds(
    descriptor: AbstractDescriptorOneOf,
    state_map: &mut StateMap,
) -> Result<MdibDescriptionModification, PairError> {
    // cardinality errors are handled by preprocessing chain
    let states = state_map.remove(&descriptor.handle()).unwrap_or_default();

    let pair: Pair = (descriptor, states).try_into()?;

    Ok(MdibDescriptionModification::Insert { pair, parent: None })
}
