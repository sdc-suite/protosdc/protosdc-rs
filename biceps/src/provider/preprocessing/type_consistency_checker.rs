use std::marker::PhantomData;

use log::debug;

use crate::common::biceps_util::Descriptor;
use crate::common::mdib_description_modification::MdibDescriptionModification;
use crate::common::mdib_pair::MdibPair;
use crate::common::mdib_tree_validator;
use crate::common::preprocessing::{DescriptionPreprocessor, PreprocessingError};
use crate::common::storage::mdib_storage::MdibStorage;

/// Preprocessing segment that ensures correctness of child-parent type relationships.
#[derive(Debug)]
pub struct TypeConsistencyChecker<T: MdibStorage> {
    phantom: PhantomData<T>,
}

impl<T: MdibStorage> Default for TypeConsistencyChecker<T> {
    fn default() -> Self {
        TypeConsistencyChecker {
            phantom: Default::default(),
        }
    }
}

impl<T: MdibStorage> DescriptionPreprocessor<T> for TypeConsistencyChecker<T> {
    fn process(
        &mut self,
        mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        let errors: Vec<PreprocessingError> = modifications
            .iter()
            .filter_map(|it| match it {
                MdibDescriptionModification::Insert { pair, parent, .. } => Some((pair, parent)),
                _ => None,
            })
            .filter_map(|(pair, parent_opt)| parent_opt.as_ref().map(|parent| (pair, parent)))
            .filter_map(|(pair, parent)| {
                debug!(
                    "Finding parent descriptor {} for {}, ",
                    &pair.handle(),
                    parent
                );
                let parent_desc_search = match mdib_storage.entity(parent) {
                    Some(p) => Ok(p.entity.get_abstract_descriptor()),
                    None => {
                        // find in modifications
                        match modifications
                            .iter()
                            .filter_map(|it| match it {
                                MdibDescriptionModification::Insert { pair, .. } => Some(pair),
                                _ => None,
                            })
                            .find(|descriptor| &descriptor.handle() == parent)
                        {
                            Some(pair) => {
                                let (descriptor, _) = pair.clone().into();
                                Ok(descriptor.into_abstract_descriptor_one_of())
                            }
                            None => Err(PreprocessingError::Unknown {
                                message: format!("mdib is inconsistent, {} missing", parent),
                            }),
                        }
                    }
                };

                debug!("Found parent descriptor {:?}", &parent_desc_search);

                match parent_desc_search {
                    Err(err) => Some(err),
                    Ok(parent) => match mdib_tree_validator::is_valid_parent(&parent, pair) {
                        true => None,
                        false => Some(PreprocessingError::TypeConsistencyError {
                            handles: vec![pair.handle()],
                        }),
                    },
                }
            })
            .collect();

        if !errors.is_empty() {
            let error_strings = errors
                .iter()
                .map(|it| it.to_string())
                .collect::<Vec<String>>();
            let error_string = error_strings.join(" - ");
            return Err(PreprocessingError::Unknown {
                message: error_string,
            });
        }

        Ok(modifications)
    }
}

#[cfg(test)]
mod test {
    use anyhow::Result;
    use env_logger;
    use log::debug;

    use crate::common::mdib_description_modification::MdibDescriptionModification;
    use crate::common::mdib_description_modifications::MdibDescriptionModifications;
    use crate::common::preprocessing::DescriptionPreprocessor;
    use crate::common::storage::mdib_storage::{MdibStorageConstructor, MdibStorageImpl};
    use crate::provider::preprocessing::type_consistency_checker::TypeConsistencyChecker;
    use crate::test_util::mdib::{base_storage, mdib_modification_presets};
    use crate::test_util::{handles, mdib_utils};

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[tokio::test]
    async fn test_consistent_types() -> Result<()> {
        init();
        let storage = MdibStorageImpl::new("wat:yolo".to_string(), false);
        let modifications = mdib_modification_presets().into_vec();

        let mut checker = TypeConsistencyChecker::default();

        let result = checker.process(&storage, modifications);

        debug!("{:?}", &result);

        assert!(result.is_ok());
        Ok(())
    }

    #[tokio::test]
    async fn test_valid_pair_mds_system_context() {
        init();
        let storage = base_storage();
        let mut checker = TypeConsistencyChecker::default();

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: (
                    mdib_utils::create_minimal_system_context_descriptor(handles::SYSTEM_CONTEXT_1),
                    mdib_utils::create_minimal_system_context_state(handles::SYSTEM_CONTEXT_1),
                )
                    .into(),
                parent: Some(handles::MDS_1.to_string()),
            })
            .expect("Could not add modification");

        // should work
        let result = checker.process(&storage, modifications.into_vec());

        assert!(result.is_ok())
    }

    #[tokio::test]
    async fn test_invalid_pair_mds_channel() {
        init();
        let storage = base_storage();
        let mut checker = TypeConsistencyChecker::default();

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: (
                    mdib_utils::create_minimal_channel_descriptor(handles::CHANNEL_3),
                    mdib_utils::create_minimal_channel_state(handles::CHANNEL_3),
                )
                    .into(),
                parent: Some(handles::MDS_0.to_string()),
            })
            .expect("Could not add modification");

        // should break
        let result = checker.process(&storage, modifications.into_vec());

        assert!(result.is_err())
    }
}
