use std::marker::PhantomData;

use log::{debug, error};

use crate::common::mdib_description_modification::MdibDescriptionModification;
use crate::common::mdib_pair::{MdibPair, Pair};
use crate::common::mdib_tree_validator;
use crate::common::preprocessing::{DescriptionPreprocessor, PreprocessingError};
use crate::common::storage::mdib_storage::MdibStorage;
use crate::entity_filter_by_pair;

/// Preprocessing segment that verifies correctness of child cardinality.
///
/// In BICEPS descriptors of a certain type can appear one or many times as a child of another
/// descriptor. This checker guarantees that no child is inserted twice if the maximum allowed
/// number of children is one.
#[derive(Debug)]
pub struct CardinalityChecker<T: MdibStorage> {
    phantom: PhantomData<T>,
}

impl<T: MdibStorage> Default for CardinalityChecker<T> {
    fn default() -> Self {
        CardinalityChecker {
            phantom: Default::default(),
        }
    }
}

impl<T: MdibStorage> DescriptionPreprocessor<T> for CardinalityChecker<T> {
    fn before_first_modification(
        &mut self,
        _: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        Ok(modifications)
    }

    fn after_last_modification(
        &mut self,
        _: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        Ok(modifications)
    }

    fn process(
        &mut self,
        mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        let error: Vec<(String, String)> = modifications
            .iter()
            .filter_map(|current_modification| {
                match current_modification {
                    MdibDescriptionModification::Insert { parent, pair, .. } => {
                        Some((parent, pair))
                    }
                    _ => None, // Only insert is affected
                }
            })
            .filter_map(|(parent_opt, pair)| parent_opt.as_ref().map(|p| (p, pair)))
            .filter_map(|(parent, pair)| {
                debug!("validating parent {} child {}", parent, &pair.handle());

                if is_same_type_in_modification(modifications.as_slice(), pair, parent)
                    && !mdib_tree_validator::is_many_allowed(pair)
                {
                    error!("{}, {:?}", parent, pair);
                    return Some((parent, pair));
                }

                match mdib_storage.entity(parent) {
                    None => None,
                    Some(_) => {
                        let valid = mdib_storage
                            .children_by_type(parent, entity_filter_by_pair!(pair))
                            .is_empty()
                            || mdib_tree_validator::is_many_allowed(pair);
                        if !valid {
                            Some((parent, pair))
                        } else {
                            None
                        }
                    }
                }
            })
            .fold(vec![], |mut v, a| {
                v.push((a.0.clone(), a.1.handle()));
                v
            });

        if !error.is_empty() {
            return Err(PreprocessingError::CardinalityError { conflicts: error });
        }
        Ok(modifications)
    }
}

fn is_same_type_in_modification(
    modifications: &[MdibDescriptionModification],
    pair: &Pair,
    parent: &str,
) -> bool {
    modifications
        .iter()
        // get pairs from modifications
        .filter_map(|it| match it {
            MdibDescriptionModification::Insert { pair, parent, .. } => Some((pair, parent)),
            _ => None,
        })
        // remove modification that contains the same handle to not check against itself
        .filter(|(mod_pair, _)| mod_pair.handle() != pair.handle())
        // check for parent equality
        .filter(|(_, mod_parent)| match mod_parent {
            None => false,
            Some(mp) => parent == mp,
        })
        // if parents are equal, check for type equality
        .any(|(mod_pair, _)| pair_variant_eq(mod_pair, pair))
}

fn pair_variant_eq(a: &Pair, b: &Pair) -> bool {
    match (a, b) {
        (Pair::SingleStatePair(a1), Pair::SingleStatePair(b1)) => variant_eq(a1, b1),
        (Pair::MultiStatePair(a1), Pair::MultiStatePair(b1)) => variant_eq(a1, b1),
        _ => false,
    }
}

fn variant_eq<T>(a: &T, b: &T) -> bool {
    std::mem::discriminant(a) == std::mem::discriminant(b)
}

#[cfg(test)]
mod test {
    use env_logger;

    use crate::common::mdib_description_modification::MdibDescriptionModification;
    use crate::common::mdib_description_modifications::MdibDescriptionModifications;
    use crate::common::preprocessing::{DescriptionPreprocessor, PreprocessingError};
    use crate::common::storage::mdib_storage::{MdibStorageConstructor, MdibStorageImpl};
    use crate::provider::preprocessing::cardinality_checker::CardinalityChecker;
    use crate::test_util::mdib::{base_storage, mdib_modification_presets};
    use crate::test_util::{handles, mdib_utils};

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[tokio::test]
    async fn test_mds_system_context_duplicate_in_storage() {
        init();
        let storage = base_storage();
        let mut checker = CardinalityChecker::default();

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: (
                    mdib_utils::create_minimal_system_context_descriptor(handles::SYSTEM_CONTEXT_1),
                    mdib_utils::create_minimal_system_context_state(handles::SYSTEM_CONTEXT_1),
                )
                    .into(),
                parent: Some(handles::MDS_0.to_string()),
            })
            .expect("Could not add modification");

        // should break
        let result = checker.process(&storage, modifications.into_vec());

        assert!(result.is_err());
        assert_eq!(
            PreprocessingError::CardinalityError {
                conflicts: vec![(
                    handles::MDS_0.to_string(),
                    handles::SYSTEM_CONTEXT_1.to_string()
                ),]
            },
            result.err().unwrap()
        );
    }

    #[tokio::test]
    async fn test_mds_system_context_duplicate_in_modifications() {
        init();
        let storage = base_storage();
        let mut checker = CardinalityChecker::default();

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: (
                    mdib_utils::create_minimal_system_context_descriptor(handles::SYSTEM_CONTEXT_1),
                    mdib_utils::create_minimal_system_context_state(handles::SYSTEM_CONTEXT_1),
                )
                    .into(),
                parent: Some(handles::MDS_1.to_string()),
            })
            .expect("Could not add modification");

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: (
                    mdib_utils::create_minimal_system_context_descriptor(handles::SYSTEM_CONTEXT_2),
                    mdib_utils::create_minimal_system_context_state(handles::SYSTEM_CONTEXT_2),
                )
                    .into(),
                parent: Some(handles::MDS_1.to_string()),
            })
            .expect("Could not add modification");

        // should break
        let result = checker.process(&storage, modifications.into_vec());

        assert!(result.is_err());

        assert_eq!(
            PreprocessingError::CardinalityError {
                conflicts: vec![
                    (
                        handles::MDS_1.to_string(),
                        handles::SYSTEM_CONTEXT_1.to_string()
                    ),
                    (
                        handles::MDS_1.to_string(),
                        handles::SYSTEM_CONTEXT_2.to_string()
                    )
                ]
            },
            result.err().unwrap()
        );
    }

    #[tokio::test]
    async fn test_valid_vmd_channel() {
        init();
        let storage = base_storage();
        let mut checker = CardinalityChecker::default();

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: (
                    mdib_utils::create_minimal_channel_descriptor(handles::CHANNEL_3),
                    mdib_utils::create_minimal_channel_state(handles::CHANNEL_3),
                )
                    .into(),
                parent: Some(handles::VMD_0.to_string()),
            })
            .expect("Could not add modification");

        // should work
        let result = checker.process(&storage, modifications.into_vec());

        assert!(result.is_ok())
    }

    #[tokio::test]
    async fn test_valid_set() {
        init();
        let empty_storage = MdibStorageImpl::new("wat:yolo".to_string(), false);
        let base_modifications = mdib_modification_presets();
        let mut checker = CardinalityChecker::default();

        // should work
        let result = checker.process(&empty_storage, base_modifications.into_vec());

        assert!(result.is_ok())
    }
}
