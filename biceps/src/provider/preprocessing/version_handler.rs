use std::collections::{HashMap, HashSet};
use std::marker::PhantomData;
use std::sync::{Arc, Mutex};

use log::{debug, error};

use protosdc_biceps::biceps::{
    AbstractContextStateOneOf, AbstractDescriptorOneOf, AbstractStateOneOf,
};

use crate::common::biceps_util;
use crate::common::biceps_util::{
    CopyableDescriptor, CopyableState, Descriptor, MultiState, State,
};
use crate::common::mdib_description_modification::MdibDescriptionModification;
use crate::common::mdib_entity::{Entity, MdibEntity, MdibEntityStateVersion, MdibEntityVersion};
use crate::common::mdib_pair::{
    MdibPair, MdibPairStateVersion, MultiStatePair, Pair, SingleStatePair,
};
use crate::common::mdib_state_modifications::MdibStateModifications;
use crate::common::preprocessing::{
    DescriptionPreprocessor, PreprocessingError, StatePreprocessor,
};
use crate::common::storage::mdib_storage::MdibStorage;

#[derive(Debug, Default)]
pub struct VersionHandlerInterior {
    versions_of_deleted_artifacts: HashMap<String, VersionPair>,

    // temporary and deleted for each update
    updated_parents: HashSet<String>,
    updated_versions: HashMap<String, VersionPair>,
    deleted: HashSet<String>,
}

/// Preprocessing segment that manages BICEPS versioning.
///
/// The segment takes care of incrementing descriptor and state versions.
/// If an error occurs during processing, the MDIB storage is very likely to be inconsistent,
/// i.e. there is no transactional behavior that triggers a rollback in case of an error.
#[derive(Debug)]
pub struct VersionHandler<T: MdibStorage> {
    phantom: PhantomData<T>,
    version_handler_interior: Arc<Mutex<VersionHandlerInterior>>,
}

impl<T: MdibStorage> VersionHandler<T> {
    pub fn new(version_handler_interior: Arc<Mutex<VersionHandlerInterior>>) -> VersionHandler<T> {
        VersionHandler {
            phantom: Default::default(),
            version_handler_interior,
        }
    }

    fn get_version_of_deleted_artifacts(&self, handle: &str) -> Option<VersionPair> {
        self.version_handler_interior
            .lock()
            .unwrap()
            .versions_of_deleted_artifacts
            .get(handle)
            .cloned()
    }

    fn set_version_of_deleted_artifacts(&mut self, handle: String, version_pair: VersionPair) {
        self.version_handler_interior
            .lock()
            .unwrap()
            .versions_of_deleted_artifacts
            .insert(handle, version_pair);
    }

    fn get_updated_versions(&self, handle: &str) -> Option<VersionPair> {
        self.version_handler_interior
            .lock()
            .unwrap()
            .updated_versions
            .get(handle)
            .cloned()
    }

    fn set_updated_versions(&mut self, handle: String, version_pair: VersionPair) {
        self.version_handler_interior
            .lock()
            .unwrap()
            .updated_versions
            .insert(handle, version_pair);
    }

    fn clear_updated_versions(&self) {
        self.version_handler_interior
            .lock()
            .unwrap()
            .updated_versions
            .clear();
    }

    fn get_deleted_handles(&self) -> HashSet<String> {
        self.version_handler_interior
            .lock()
            .unwrap()
            .deleted
            .clone()
    }

    fn add_deleted_handles(&self, handles: Vec<String>) {
        self.version_handler_interior
            .lock()
            .unwrap()
            .deleted
            .extend(handles);
    }

    fn clear_deleted_handles(&self) {
        self.version_handler_interior
            .lock()
            .unwrap()
            .deleted
            .clear();
    }

    fn get_updated_parents(&self, handle: &str) -> bool {
        self.version_handler_interior
            .lock()
            .unwrap()
            .updated_parents
            .contains(handle)
    }

    fn add_updated_parents(&self, handle: String) {
        self.version_handler_interior
            .lock()
            .unwrap()
            .updated_parents
            .insert(handle);
    }

    fn clear_updated_parents(&self) {
        self.version_handler_interior
            .lock()
            .unwrap()
            .updated_parents
            .clear();
    }

    fn get_version_descriptor(&self, storage: &T, handle: &str) -> Option<Version> {
        match storage.entity(handle) {
            Some(entity) => Some(Version::from_entity(&entity)),
            None => self
                .get_version_of_deleted_artifacts(handle)
                .map(|ver| Version {
                    number: ver.descriptor_version,
                }),
        }
    }

    fn get_saved_version_descriptor(&self, handle: &str) -> Option<Version> {
        self.get_updated_versions(handle)
            .map(|saved_version| Version {
                number: saved_version.descriptor_version,
            })
    }

    fn get_version_context_state(&self, storage: &T, handle: &str) -> Option<Version> {
        match storage.context_state(handle) {
            Some(ctx) => Some(Version::from_context_state(&ctx)),
            None => self
                .get_version_of_deleted_artifacts(handle)
                .map(|ver| Version {
                    number: ver.state_version,
                }),
        }
    }

    fn get_saved_version_state(&self, handle: &str) -> Option<Version> {
        self.get_updated_versions(handle)
            .map(|saved_version| Version {
                number: saved_version.state_version,
            })
    }

    fn get_version_pair(&self, storage: &T, handle: &str) -> Option<VersionPair> {
        match storage.entity_version(handle) {
            Some(entity) => Some(VersionPair::from_entity_version(&entity)),
            None => self.get_version_of_deleted_artifacts(handle),
        }
    }

    fn get_version_pair_context(&self, storage: &T, handle: &str) -> Option<VersionPair> {
        match storage.context_state(handle) {
            Some(entity) => Some(VersionPair::from_context(&entity)),
            None => self.get_version_of_deleted_artifacts(handle),
        }
    }

    fn get_saved_version_pair(&self, handle: &str) -> Option<VersionPair> {
        self.get_updated_versions(handle)
    }

    fn set_updated(&mut self, pair: &Pair) {
        self.add_updated_parents(pair.handle());

        let versions = pair.version();

        match versions.state_version {
            MdibPairStateVersion::SingleStateVersion(state_version) => {
                self.set_updated_versions(
                    pair.handle(),
                    VersionPair {
                        descriptor_version: Some(versions.descriptor_version),
                        state_version: Some(state_version),
                    },
                );
            }
            MdibPairStateVersion::MultiStateVersion(state_versions) => {
                self.set_updated_versions(
                    pair.handle(),
                    VersionPair {
                        descriptor_version: Some(versions.descriptor_version),
                        state_version: None,
                    },
                );

                // insert all states, expensive.
                state_versions
                    .into_iter()
                    .for_each(|(state_handle, version)| {
                        self.set_updated_versions(
                            state_handle,
                            VersionPair {
                                descriptor_version: Some(pair.version().descriptor_version),
                                state_version: Some(version),
                            },
                        );
                    });
            }
        }
    }

    fn process_insert(
        &mut self,
        storage: &T,
        pair: Pair,
        parent: Option<String>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        let update_result: Pair = match pair {
            Pair::MultiStatePair(multi_state_pair) => self
                .process_insert_multi_state(storage, multi_state_pair)?
                .into(),
            Pair::SingleStatePair(single_state_pair) => self
                .process_insert_single_state(storage, single_state_pair)?
                .into(),
        };

        self.set_updated(&update_result);
        let update_optional = match &parent {
            None => Ok(vec![]),
            Some(parent_handle) => {
                match storage.entity(parent_handle) {
                    Some(entity) => match self.process_update(storage, entity.entity.into()) {
                        Ok(update) => Ok(update),
                        Err(err) => return Err(err),
                    },
                    None => {
                        // parent must be inserted as well then
                        if !self.get_updated_parents(parent_handle) {
                            error!(
                                "Parent {} for newly inserted descriptor {} missing",
                                parent_handle,
                                &update_result.handle()
                            );
                            return Err(PreprocessingError::ParentEntityMissing {
                                parent: parent_handle.clone(),
                                child: update_result.handle(),
                            });
                        }
                        Ok(vec![])
                    }
                }
            }
        };

        let new_insert = MdibDescriptionModification::Insert {
            pair: update_result,
            parent,
        };

        match update_optional {
            Ok(mut update) => {
                update.insert(0, new_insert); // prepend insert to update
                Ok(update)
            }
            Err(err) => Err(err),
        }
    }

    fn process_update_already_updated_multi_state(
        &self,
        storage: &T,
        pair: Pair,
    ) -> Result<Pair, PreprocessingError> {
        let new_version = self
            .get_saved_version_descriptor(&pair.handle())
            .expect("Do not call this when no updated version is present");

        let (descriptor, states) = pair.into();

        // update descriptor
        let updated_descriptor =
            descriptor.replace_in_copy(new_version.number.expect("stored version cannot be empty"));

        let updated_states: Result<Vec<AbstractStateOneOf>, PreprocessingError> = states
            .into_iter()
            .map(|state| self.update_context_state(storage, updated_descriptor.version(), state))
            .collect();

        match updated_states {
            Ok(states) => Ok((updated_descriptor, states).try_into().unwrap()),
            Err(err) => Err(err),
        }
    }

    fn process_update_already_updated_single_state(
        &self,
        pair: Pair,
    ) -> Result<Pair, PreprocessingError> {
        let (descriptor, mut states) = pair.into();

        if states.len() != 1 {
            return Err(PreprocessingError::SingleStateDescriptorCardinality {
                parent: descriptor.handle(),
                children: states.len(),
            });
        }

        let new_version = self
            .get_saved_version_pair(&descriptor.handle())
            .expect("Do not call this when no updated version is present");

        // update descriptor
        let updated_descriptor = descriptor
            .into_abstract_descriptor_one_of()
            .replace_in_copy(
                new_version
                    .descriptor_version
                    .expect("stored version cannot be empty"),
            );

        let updated_state = states.pop().unwrap().replace(
            updated_descriptor.version(),
            new_version
                .state_version
                .expect("stored version cannot be empty"),
        );

        Ok((updated_descriptor, updated_state).try_into().unwrap())
    }

    fn process_multi_state_update(
        &self,
        storage: &T,
        pair: MultiStatePair,
    ) -> Result<MultiStatePair, PreprocessingError> {
        let new_descriptor_version = match self.get_version_descriptor(storage, &pair.handle()) {
            None => {
                error!(
                    "Could not determine the previous version for {}",
                    pair.handle()
                );
                return Err(PreprocessingError::EntityUnknown {
                    handle: pair.handle(),
                });
            }
            Some(version) => version,
        };

        let (descriptor, states) = pair.into();

        // update descriptor
        let updated_descriptor = descriptor
            .into_abstract_descriptor_one_of()
            .replace_in_copy(
                new_descriptor_version.inc().number.unwrap(), // unwrap must be safe after inc
            );

        // collect already present states
        let mut storage_states: HashMap<String, AbstractContextStateOneOf> = storage
            .context_states()
            .into_iter()
            .filter(|it| it.descriptor_handle() == updated_descriptor.handle())
            .map(|it| (it.state_handle(), it))
            .collect();

        let replace_versions = |state: AbstractContextStateOneOf| {
            let state_handle = state.state_handle();
            state.replace(
                updated_descriptor.version(),
                self.get_version_context_state(storage, &state_handle)
                    .unwrap_or_default()
                    .inc()
                    .number
                    .unwrap(), // unwrap must be safe after inc
            )
        };

        // increment versions for all states from change set
        let mut updated_states: Vec<AbstractStateOneOf> = states.into_iter().map(|state| {
            match biceps_util::into_context_state_one_of(state) {
                Err(broken_state) => {
                    error!("Expected context state in process_multi_state_update, was state for {}", broken_state.descriptor_handle());
                    Err(PreprocessingError::TypeConsistencyError { handles: vec![broken_state.descriptor_handle()]})
                },
                Ok(context_state) => {
                    storage_states.remove(&context_state.state_handle());
                    Ok(replace_versions(context_state).into_abstract_state_one_of())
                }
            }
        }).collect::<Result<Vec<AbstractStateOneOf>, PreprocessingError>>()?;

        // update states not in change set
        storage_states
            .into_values()
            .map(|state| replace_versions(state).into_abstract_state_one_of())
            .for_each(|state| updated_states.push(state));

        Ok((updated_descriptor, updated_states)
            .try_into()
            .expect("Only fails if input is invalid pair"))
    }

    fn process_single_state_update(
        &self,
        storage: &T,
        pair: SingleStatePair,
    ) -> Result<SingleStatePair, PreprocessingError> {
        let version_pair = match self.get_version_pair(storage, &pair.handle()) {
            None => {
                error!(
                    "Could not determine the previous version for {}",
                    pair.handle()
                );
                return Err(PreprocessingError::EntityUnknown {
                    handle: pair.handle(),
                });
            }
            Some(version) => version,
        };

        let (descriptor, state): (AbstractDescriptorOneOf, AbstractStateOneOf) = pair.into();

        // update descriptor
        let updated_descriptor = descriptor.replace_in_copy(version_pair.inc_desc());

        let updated_state = state.replace(updated_descriptor.version(), version_pair.inc_state());

        Ok((updated_descriptor, updated_state)
            .try_into()
            .expect("Only fails if input pair was invalid"))
    }

    fn process_update(
        &mut self,
        storage: &T,
        pair: Pair,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        // we've already established new versions for at least this descriptor, handle them and throw them back.
        if self.get_updated_versions(&pair.handle()).is_some() {
            debug!("process_update: updated versions already includes {}, rewriting versions and returning", &pair.handle());
            let updated = match biceps_util::is_context_pair(&pair) {
                true => self.process_update_already_updated_multi_state(storage, pair),
                false => self.process_update_already_updated_single_state(pair),
            }?;
            self.set_updated(&updated);
            return Ok(vec![MdibDescriptionModification::Update { pair: updated }]);
        }

        let updated: Pair = match pair {
            Pair::MultiStatePair(multi_state_pair) => self
                .process_multi_state_update(storage, multi_state_pair)?
                .into(),
            Pair::SingleStatePair(single_state_pair) => self
                .process_single_state_update(storage, single_state_pair)?
                .into(),
        };

        self.set_updated(&updated);

        Ok(vec![MdibDescriptionModification::Update { pair: updated }])
    }

    fn process_delete(
        &mut self,
        storage: &T,
        handle: String,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        let entity_to_delete = storage
            .entity(&handle)
            .expect("Delete received not present handle");

        self.set_version_of_deleted_artifacts(
            entity_to_delete.entity.get_abstract_descriptor().handle(),
            VersionPair::from_entity(&entity_to_delete),
        );

        entity_to_delete
            .resolve_context_states()
            .iter()
            .for_each(|state| {
                self.set_version_of_deleted_artifacts(
                    state.state_handle(),
                    VersionPair::from_context(state),
                );
            });

        // if mds, no incrementing parents needed
        if matches!(entity_to_delete.entity, Entity::Mds { .. }) {
            return Ok(vec![MdibDescriptionModification::Delete { handle }]);
        }

        let parent_handle = entity_to_delete
            .resolve_parent()
            .expect("Parent handle missing");

        // check if parent is getting deleted as well, don't add an update then
        if self.get_deleted_handles().contains(&parent_handle) {
            return Ok(vec![MdibDescriptionModification::Delete { handle }]);
        }

        let parent_entity = storage
            .entity(&parent_handle)
            .unwrap_or_else(|| panic!("Storage inconsistency, parent {} missing", parent_handle));

        let update = self.process_update(storage, parent_entity.entity.into());

        match update {
            Ok(mut upd) => {
                upd.insert(0, MdibDescriptionModification::Delete { handle });
                Ok(upd)
            }
            Err(err) => Err(err),
        }
    }

    fn update_context_state(
        &self,
        storage: &T,
        descriptor_version: u64,
        state: AbstractStateOneOf,
    ) -> Result<AbstractStateOneOf, PreprocessingError> {
        match biceps_util::into_context_state_one_of(state) {
            Err(broken_state) => {
                error!(
                    "Expected context state in update_context_state, was state for {}",
                    broken_state.descriptor_handle()
                );
                Err(PreprocessingError::TypeConsistencyError {
                    handles: vec![broken_state.descriptor_handle()],
                })
            }
            Ok(ctx) => {
                // use either an unmodified saved version, or get the one from storage and bump it up
                let state_version = match self.get_saved_version_state(&ctx.state_handle()) {
                    None => self
                        .get_version_context_state(storage, &ctx.state_handle())
                        .unwrap_or_default()
                        .inc(),
                    Some(ver) => ver,
                };
                Ok(ctx
                    .into_abstract_state_one_of()
                    .replace(descriptor_version, state_version.number.unwrap()))
            }
        }
    }

    fn process_insert_multi_state(
        &mut self,
        storage: &T,
        pair: MultiStatePair,
    ) -> Result<MultiStatePair, PreprocessingError> {
        let descriptor_version = self
            .get_version_descriptor(storage, &pair.handle())
            .unwrap_or_default()
            .inc();

        let (descriptor, states) = pair.into();

        let updated_descriptor = descriptor.replace_in_copy(descriptor_version.number.unwrap());

        let updated_states: Result<Vec<AbstractStateOneOf>, PreprocessingError> = states
            .into_iter()
            .map(|state| self.update_context_state(storage, updated_descriptor.version(), state))
            .collect();

        match updated_states {
            Ok(states) => Ok((updated_descriptor, states)
                .try_into()
                .expect("Only fails if input pair was invalid")),
            Err(err) => Err(err),
        }
    }

    fn process_insert_single_state(
        &mut self,
        storage: &T,
        pair: SingleStatePair,
    ) -> Result<SingleStatePair, PreprocessingError> {
        let (descriptor, state): (AbstractDescriptorOneOf, AbstractStateOneOf) = pair.into();

        let (descriptor_version, state_version) =
            match self.get_saved_version_pair(&descriptor.handle()) {
                None => {
                    let ver = self
                        .get_version_pair(storage, &descriptor.handle())
                        .unwrap_or_default();
                    (ver.inc_desc(), ver.inc_state())
                }
                Some(ver) => (ver.descriptor_version.unwrap(), ver.state_version.unwrap()),
            };

        let updated_descriptor = descriptor.replace_in_copy(descriptor_version);
        let updated_state = state.replace(descriptor_version, state_version);

        Ok((updated_descriptor, updated_state)
            .try_into()
            .expect("Only fails if input pair was invalid"))
    }

    fn process_state_update_single_state<S: State + CopyableState>(
        &self,
        storage: &T,
        state: S,
    ) -> Result<S, PreprocessingError> {
        let version_pair = match self.get_saved_version_pair(&state.descriptor_handle()) {
            None => match self.get_version_pair(storage, &state.descriptor_handle()) {
                None => {
                    error!(
                        "Could not determine the previous or saved version for state {}",
                        state.descriptor_handle()
                    );
                    return Err(PreprocessingError::EntityUnknown {
                        handle: state.descriptor_handle(),
                    });
                }
                Some(ver) => ver.get_inc_state(), // increment state version
            },
            Some(ver) => {
                debug!("Found saved version for {}", &state.descriptor_handle());
                ver
            }
        };
        Ok(state.replace(
            version_pair.get_descriptor_version(),
            version_pair.get_state_version(),
        ))
    }

    fn process_state_update_context_state<S: State + MultiState + CopyableState>(
        &self,
        storage: &T,
        state: S,
    ) -> Result<S, PreprocessingError> {
        let version_pair = match self.get_saved_version_pair(&state.state_handle()) {
            None => match self.get_version_pair_context(storage, &state.state_handle()) {
                None => {
                    // if neither a storage version, deleted entity version or saved version is present, this state is new
                    let descriptor_version = self
                        .get_version_pair(storage, &state.descriptor_handle())
                        .ok_or(PreprocessingError::EntityUnknown {
                            handle: state.descriptor_handle(),
                        })?;
                    debug!("Encountered unknown context state {}, must be new, assigning empty state version and descriptor version {:?}", &state.state_handle(), descriptor_version.descriptor_version);
                    VersionPair {
                        descriptor_version: descriptor_version.descriptor_version,
                        state_version: None,
                    }
                }
                Some(ver) => ver.get_inc_state(), // increment state version
            },
            Some(ver) => ver,
        };

        Ok(state.replace(
            version_pair.get_descriptor_version(),
            version_pair.get_state_version(),
        ))
    }
}

impl<T: MdibStorage> DescriptionPreprocessor<T> for VersionHandler<T> {
    fn before_first_modification(
        &mut self,
        _mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        self.clear_updated_parents();
        self.clear_updated_versions();
        self.clear_deleted_handles();
        let deletions: Vec<String> = modifications
            .iter()
            .filter_map(|it| match it {
                MdibDescriptionModification::Delete { handle } => Some(handle.clone()),
                _ => None,
            })
            .collect();
        self.add_deleted_handles(deletions);
        Ok(modifications)
    }

    fn process(
        &mut self,
        mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        let new_modifications: Result<Vec<Vec<MdibDescriptionModification>>, PreprocessingError> =
            modifications
                .into_iter()
                .map(|modification| match modification {
                    MdibDescriptionModification::Insert { pair, parent } => {
                        self.process_insert(mdib_storage, pair, parent)
                    }
                    MdibDescriptionModification::Update { pair } => {
                        self.process_update(mdib_storage, pair)
                    }
                    MdibDescriptionModification::Delete { handle } => {
                        self.process_delete(mdib_storage, handle)
                    }
                })
                .collect();

        match new_modifications {
            Ok(vecs) => Ok(vecs.into_iter().flatten().collect()),
            Err(err) => Err(err),
        }
    }
}

macro_rules! process_state_type {
    ($self:ident , $struct_name:ident, $struct_field_name:ident, $states:ident, $mdib_storage:ident) => {{
        let updated_states = $states
            .into_iter()
            .map(|state| $self.process_state_update_single_state($mdib_storage, state))
            .collect();

        match updated_states {
            Ok(states) => Ok(MdibStateModifications::$struct_name {
                $struct_field_name: states,
            }),
            Err(err) => return Err(err),
        }
    }};
}

impl<T: MdibStorage> StatePreprocessor<T> for VersionHandler<T> {
    fn before_first_modification(
        &mut self,
        _mdib_storage: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError> {
        self.clear_updated_parents();
        self.clear_updated_versions();
        Ok(modifications)
    }

    fn process(
        &mut self,
        mdib_storage: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError> {
        match modifications {
            MdibStateModifications::Alert { alert_states } => {
                process_state_type!(self, Alert, alert_states, alert_states, mdib_storage)
            }
            MdibStateModifications::Component { component_states } => {
                process_state_type!(
                    self,
                    Component,
                    component_states,
                    component_states,
                    mdib_storage
                )
            }
            MdibStateModifications::Metric { metric_states } => {
                process_state_type!(self, Metric, metric_states, metric_states, mdib_storage)
            }
            MdibStateModifications::Operation { operation_states } => {
                process_state_type!(
                    self,
                    Operation,
                    operation_states,
                    operation_states,
                    mdib_storage
                )
            }
            MdibStateModifications::Waveform { waveform_states } => {
                process_state_type!(
                    self,
                    Waveform,
                    waveform_states,
                    waveform_states,
                    mdib_storage
                )
            }
            MdibStateModifications::Context { context_states } => {
                let updated_states = context_states
                    .into_iter()
                    .map(|state| self.process_state_update_context_state(mdib_storage, state))
                    .collect();

                match updated_states {
                    Ok(states) => Ok(MdibStateModifications::Context {
                        context_states: states,
                    }),
                    Err(err) => Err(err),
                }
            }
        }
    }
}

#[derive(Debug, PartialEq, Clone, Default)]
struct VersionPair {
    descriptor_version: Option<u64>,
    state_version: Option<u64>,
}

#[derive(Debug, PartialEq, Clone, Default)]
struct Version {
    number: Option<u64>,
}

impl VersionPair {
    fn from_entity(entity: &MdibEntity) -> VersionPair {
        VersionPair {
            descriptor_version: Some(entity.entity.get_abstract_descriptor_ref().version()),
            state_version: entity
                .entity
                .get_abstract_state_ref()
                .map(|state| state.version()),
        }
    }

    fn from_entity_version(entity: &MdibEntityVersion) -> VersionPair {
        VersionPair {
            descriptor_version: Some(entity.descriptor_version),
            state_version: match entity.state_version {
                MdibEntityStateVersion::SingleStateVersion(single) => Some(single),
                MdibEntityStateVersion::MultiStateVersion(_) => None,
            },
        }
    }

    fn from_context(context: &AbstractContextStateOneOf) -> VersionPair {
        VersionPair {
            descriptor_version: Some(context.descriptor_version()),
            state_version: Some(context.version()),
        }
    }

    fn get_descriptor_version(&self) -> u64 {
        self.descriptor_version.unwrap_or(0)
    }

    fn get_state_version(&self) -> u64 {
        self.state_version.unwrap_or(0)
    }

    fn inc_desc(&self) -> u64 {
        match self.descriptor_version {
            None => 0,
            Some(i) => i + 1,
        }
    }

    fn inc_state(&self) -> u64 {
        match self.state_version {
            None => 0,
            Some(i) => i + 1,
        }
    }

    fn get_inc_state(&self) -> VersionPair {
        VersionPair {
            descriptor_version: self.descriptor_version,
            state_version: Some(self.inc_state()),
        }
    }
}

impl Version {
    fn from_entity(entity: &MdibEntity) -> Version {
        Version {
            number: Some(entity.entity.get_abstract_descriptor().version()),
        }
    }

    fn from_context_state(context: &AbstractContextStateOneOf) -> Version {
        Version {
            number: Some(context.version()),
        }
    }

    fn inc(&self) -> Version {
        Version {
            number: match self.number {
                None => Some(0),
                Some(i) => Some(i + 1),
            },
        }
    }
}

#[cfg(test)]
mod test {
    use std::collections::HashMap;
    use std::sync::{Arc, Mutex};
    use std::vec;

    use env_logger;

    use protosdc_biceps::biceps::{
        safety_classification_mod, AbstractContextStateOneOf, AbstractDescriptorOneOf,
        AbstractDeviceComponentStateOneOf, AbstractStateOneOf, ChannelDescriptor,
        InstanceIdentifier, NumericMetricDescriptor, OperatingJurisdiction, PatientContextState,
        ReferencedVersion, SafetyClassification, VmdDescriptor,
    };

    use crate::common::biceps_util;
    use crate::common::biceps_util::{
        ContextState, CopyableDescriptor, CopyableState, Descriptor, MultiState, State,
    };
    use crate::common::mdib_description_modification::MdibDescriptionModification;
    use crate::common::mdib_description_modifications::MdibDescriptionModifications;
    use crate::common::mdib_entity::{Entity, EntityBase};
    use crate::common::mdib_pair::{
        MdibPair, MultiStatePair, NumericMetricPair, Pair, SingleStatePair,
    };
    use crate::common::mdib_state_modifications::MdibStateModifications;
    use crate::common::preprocessing::{DescriptionPreprocessor, StatePreprocessor};
    use crate::common::storage::mdib_storage::MdibStorage;
    use crate::provider::preprocessing::version_handler::{VersionHandler, VersionHandlerInterior};
    use crate::test_util::mdib::base_storage;
    use crate::test_util::{handles, mdib_utils};

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    fn new_handler<T: MdibStorage>() -> VersionHandler<T> {
        let interior = VersionHandlerInterior::default();
        VersionHandler::new(Arc::new(Mutex::new(interior)))
    }

    /// Inserts a single state descriptor and checks that the parent is updated.
    #[tokio::test]
    async fn test_descriptor_versioning_single_state_insert_causes_update() {
        init();
        let storage = base_storage();
        let mut checker = new_handler();

        let mut modifications = vec![MdibDescriptionModification::Insert {
            pair: Pair::from((
                mdib_utils::create_minimal_channel_descriptor(handles::CHANNEL_3),
                mdib_utils::create_minimal_channel_state(handles::CHANNEL_3),
            )),
            parent: Some(handles::VMD_0.to_string()),
        }];

        let parent_entity = storage.entity(handles::VMD_0).expect("broke");

        modifications = DescriptionPreprocessor::before_first_modification(
            &mut checker,
            &storage,
            modifications,
        )
        .expect("broke");
        assert_eq!(1, modifications.len());

        modifications =
            DescriptionPreprocessor::process(&mut checker, &storage, modifications).expect("broke");

        // update added
        assert_eq!(2, modifications.len());
        assert!(matches!(
            modifications.get(1).unwrap(),
            MdibDescriptionModification::Update { .. }
        ));

        // make sure update bumps parent version
        if let MdibDescriptionModification::Update { pair } = modifications.remove(1) {
            assert_eq!(
                parent_entity.entity.get_abstract_descriptor().version() + 1,
                pair.version().descriptor_version
            );
            let (descriptor, states): (AbstractDescriptorOneOf, Vec<AbstractStateOneOf>) =
                pair.into();
            let state = states.first().expect("broke");

            assert_eq!(
                parent_entity
                    .resolve_single_state()
                    .expect("broke")
                    .version()
                    + 1,
                state.version()
            );
            assert_eq!(descriptor.version(), state.descriptor_version());
        } else {
            panic!();
        }

        // make sure update is in cache and removed after the before_first_modification call
        assert!(
            checker
                .version_handler_interior
                .lock()
                .expect("Lock")
                .updated_versions
                .contains_key(handles::VMD_0),
            "Cache didn't contain handle"
        );
        DescriptionPreprocessor::before_first_modification(&mut checker, &storage, modifications)
            .expect("broke");
        assert!(
            !checker
                .version_handler_interior
                .lock()
                .expect("Lock")
                .updated_versions
                .contains_key(handles::VMD_0),
            "Cache still contained handle"
        );
    }

    /// Inserts a multi state descriptor with no states and checks that the parent is updated.
    #[tokio::test]
    async fn test_descriptor_versioning_multi_state_without_states_insert_causes_update() {
        init();
        let storage = base_storage();
        let mut checker = new_handler();

        let mut modifications = vec![MdibDescriptionModification::Insert {
            pair: Pair::from((
                mdib_utils::create_minimal_means_context_descriptor(handles::CONTEXT_DESCRIPTOR_5),
                vec![],
            )),
            parent: Some(handles::SYSTEM_CONTEXT_0.to_string()),
        }];

        let parent_entity = storage.entity(handles::SYSTEM_CONTEXT_0).expect("broke");

        modifications = DescriptionPreprocessor::before_first_modification(
            &mut checker,
            &storage,
            modifications,
        )
        .expect("broke");
        assert_eq!(1, modifications.len());

        modifications =
            DescriptionPreprocessor::process(&mut checker, &storage, modifications).expect("broke");

        // update added
        assert_eq!(2, modifications.len());
        assert!(matches!(
            modifications.get(1).unwrap(),
            MdibDescriptionModification::Update { .. }
        ));

        // make sure update bumps parent version
        if let MdibDescriptionModification::Update { pair } = modifications.remove(1) {
            assert_eq!(
                parent_entity.entity.get_abstract_descriptor().version() + 1,
                pair.version().descriptor_version
            );
            let (descriptor, states): (AbstractDescriptorOneOf, Vec<AbstractStateOneOf>) =
                pair.into();

            let state = states.first().expect("broke");

            assert_eq!(
                parent_entity
                    .resolve_single_state()
                    .expect("broke")
                    .version()
                    + 1,
                state.version()
            );
            assert_eq!(descriptor.version(), state.descriptor_version());
        } else {
            panic!();
        }
    }

    /// Inserts a multi state descriptor with states and checks that the parent is updated.
    #[tokio::test]
    async fn test_descriptor_versioning_multi_state_insert_causes_update() {
        init();
        let storage = base_storage();
        let mut checker = new_handler();

        let mut modifications = vec![MdibDescriptionModification::Insert {
            pair: Pair::from((
                mdib_utils::create_minimal_means_context_descriptor(handles::CONTEXT_DESCRIPTOR_5),
                mdib_utils::create_minimal_means_context_state(
                    handles::CONTEXT_DESCRIPTOR_5,
                    handles::CONTEXT_5,
                ),
            )),
            parent: Some(handles::SYSTEM_CONTEXT_0.to_string()),
        }];

        let parent_entity = storage.entity(handles::SYSTEM_CONTEXT_0).expect("broke");

        modifications = DescriptionPreprocessor::before_first_modification(
            &mut checker,
            &storage,
            modifications,
        )
        .expect("broke");
        assert_eq!(1, modifications.len());

        modifications =
            DescriptionPreprocessor::process(&mut checker, &storage, modifications).expect("broke");

        // update added
        assert_eq!(2, modifications.len());
        assert!(matches!(
            modifications.get(1).unwrap(),
            MdibDescriptionModification::Update { .. }
        ));

        // make sure update bumps parent version
        if let MdibDescriptionModification::Update { pair } = modifications.remove(1) {
            assert_eq!(
                parent_entity.entity.get_abstract_descriptor().version() + 1,
                pair.version().descriptor_version
            );
            let (descriptor, states): (AbstractDescriptorOneOf, Vec<AbstractStateOneOf>) =
                pair.into();

            let state = states.first().expect("broke");

            assert_eq!(
                parent_entity
                    .resolve_single_state()
                    .expect("broke")
                    .version()
                    + 1,
                state.version()
            );
            assert_eq!(descriptor.version(), state.descriptor_version());
        } else {
            panic!();
        }
    }

    /// Inserts a single state descriptor checks that the parent is updated, but also the manually
    /// added update later on.
    #[tokio::test]
    async fn test_descriptor_versioning_single_state_insert_updates_later_update_versions() {
        init();
        let storage = base_storage();
        let mut checker = new_handler();

        let mut modifications = vec![MdibDescriptionModification::Insert {
            pair: Pair::from((
                mdib_utils::create_minimal_channel_descriptor(handles::CHANNEL_3),
                mdib_utils::create_minimal_channel_state(handles::CHANNEL_3),
            )),
            parent: Some(handles::VMD_0.to_string()),
        }];

        let parent_entity = storage.entity(handles::VMD_0).expect("broke");

        let (mut updated_descriptor, state) = match parent_entity.clone().entity {
            Entity::Vmd(vmd) => (vmd.pair.descriptor, vmd.pair.state),
            _ => panic!(),
        };

        let med_c = Some(SafetyClassification {
            enum_type: safety_classification_mod::EnumType::MedC,
        });

        updated_descriptor
            .abstract_complex_device_component_descriptor
            .abstract_device_component_descriptor
            .abstract_descriptor
            .safety_classification_attr
            .clone_from(&med_c);

        modifications.push(MdibDescriptionModification::Update {
            pair: Pair::from((updated_descriptor, state)),
        });

        modifications = DescriptionPreprocessor::before_first_modification(
            &mut checker,
            &storage,
            modifications,
        )
        .expect("broke");
        assert_eq!(2, modifications.len());

        modifications =
            DescriptionPreprocessor::process(&mut checker, &storage, modifications).expect("broke");

        // update added
        assert_eq!(3, modifications.len());
        assert!(matches!(
            modifications.get(1).unwrap(),
            MdibDescriptionModification::Update { .. }
        ));
        assert!(matches!(
            modifications.get(2).unwrap(),
            MdibDescriptionModification::Update { .. }
        ));

        // make sure update bumps parent version but does not add safety classification
        if let MdibDescriptionModification::Update { pair } = modifications.remove(1) {
            assert_eq!(
                parent_entity.entity.get_abstract_descriptor().version() + 1,
                pair.version().descriptor_version
            );

            let (descriptor, states): (AbstractDescriptorOneOf, Vec<AbstractStateOneOf>) =
                pair.into();

            let state = states.first().expect("broke");

            assert_eq!(
                parent_entity
                    .resolve_single_state()
                    .expect("broke")
                    .version()
                    + 1,
                state.version()
            );
            assert_eq!(descriptor.version(), state.descriptor_version());

            match descriptor {
                AbstractDescriptorOneOf::VmdDescriptor(vmd) => {
                    assert_eq!(
                        None,
                        vmd.abstract_complex_device_component_descriptor
                            .abstract_device_component_descriptor
                            .abstract_descriptor
                            .safety_classification_attr
                    )
                }
                _ => panic!("Unexpected descriptor type"),
            };
        } else {
            panic!();
        }

        // make sure manual update bumps parent version and sets safety classification
        if let MdibDescriptionModification::Update { pair } = modifications.remove(1) {
            assert_eq!(
                parent_entity.entity.get_abstract_descriptor().version() + 1,
                pair.version().descriptor_version
            );
            let (descriptor, states): (AbstractDescriptorOneOf, Vec<AbstractStateOneOf>) =
                pair.into();

            let state = states.first().expect("broke");

            assert_eq!(
                parent_entity
                    .resolve_single_state()
                    .expect("broke")
                    .version()
                    + 1,
                state.version()
            );
            assert_eq!(descriptor.version(), state.descriptor_version());

            match descriptor {
                AbstractDescriptorOneOf::VmdDescriptor(vmd) => {
                    assert_eq!(
                        med_c,
                        vmd.abstract_complex_device_component_descriptor
                            .abstract_device_component_descriptor
                            .abstract_descriptor
                            .safety_classification_attr
                    )
                }
                _ => panic!("Unexpected descriptor type"),
            };
        } else {
            panic!();
        }
    }

    /// Updates a multi state descriptor and adds a new state as a side effect, ensures versions
    /// are correctly updated, i.e. only the new states is not incremented.
    #[tokio::test]
    async fn test_descriptor_versioning_update_insert_new_multi_state_state() {
        init();
        let storage = base_storage();
        let mut checker = new_handler();

        let mut modifications = vec![];

        // get current patient context entity
        let patient_entity = storage
            .entity(handles::CONTEXT_DESCRIPTOR_0)
            .expect("Patient missing");

        let (patient_descriptor, patient_states) = match patient_entity.entity {
            Entity::PatientContext(patient) => (patient.pair.descriptor, patient.pair.states),
            _ => panic!(),
        };

        let mut new_contexts: Vec<PatientContextState> = patient_states.clone();
        new_contexts.push(mdib_utils::create_minimal_patient_context_state(
            handles::CONTEXT_DESCRIPTOR_0,
            handles::CONTEXT_5,
        ));

        let old_contexts_map: HashMap<String, PatientContextState> = patient_states
            .into_iter()
            .map(|it| (it.state_handle(), it))
            .collect();

        modifications.push(MdibDescriptionModification::Update {
            pair: Pair::from((patient_descriptor.clone(), new_contexts)),
        });

        modifications = DescriptionPreprocessor::before_first_modification(
            &mut checker,
            &storage,
            modifications,
        )
        .expect("broke");
        assert_eq!(1, modifications.len());

        modifications =
            DescriptionPreprocessor::process(&mut checker, &storage, modifications).expect("broke");

        // nothing added
        assert_eq!(1, modifications.len());

        // make sure update bumps parent version
        if let MdibDescriptionModification::Update { pair } = modifications.remove(0) {
            assert_eq!(
                patient_descriptor.version() + 1,
                pair.version().descriptor_version
            );

            let (descriptor, states): (AbstractDescriptorOneOf, Vec<AbstractStateOneOf>) =
                pair.into();

            assert_eq!(old_contexts_map.len() + 1, states.len());

            states
                .into_iter()
                .map(|state| biceps_util::into_context_state_one_of(state).unwrap())
                .for_each(|state| {
                    if state.state_handle() != handles::CONTEXT_5 {
                        let old_state = old_contexts_map.get(&state.state_handle()).unwrap();

                        // old context state should be bumped
                        assert_eq!(old_state.version() + 1, state.version());
                        assert_eq!(descriptor.version(), state.descriptor_version());
                    } else {
                        // new context state should be 0
                        assert_eq!(0, state.version());
                        assert_eq!(descriptor.version(), state.descriptor_version());
                    }
                });
        } else {
            panic!();
        }
    }

    /// Updates multiple single states with incorrect versions, checks for correctness of versions
    /// of all updates after processing.
    #[tokio::test]
    async fn test_descriptor_versioning_updates() {
        init();
        let storage = base_storage();
        let mut checker = new_handler();

        let vmd_entity = storage.entity(handles::VMD_0).unwrap();
        let channel_entity = storage.entity(handles::CHANNEL_0).unwrap();
        let metric_entity = storage.entity(handles::METRIC_0).unwrap();

        let vmd_entity_ent = match vmd_entity.entity {
            Entity::Vmd(vmd) => *vmd,
            _ => panic!("Whatevs"),
        };

        let channel_entity_ent = match channel_entity.entity {
            Entity::Channel(ch) => *ch,
            _ => panic!("Whatevs"),
        };

        let metric_entity_ent = match metric_entity.entity {
            Entity::NumericMetric(m) => *m,
            _ => panic!("Whatevs"),
        };

        // update a bunch of state versions and check for correct versions later on
        let mut modifications = vec![
            MdibDescriptionModification::Update {
                pair: Pair::from((
                    vmd_entity_ent.pair.descriptor.replace_in_copy(4),
                    vmd_entity_ent.pair.state.replace_in_copy(6, 22),
                )),
            },
            MdibDescriptionModification::Update {
                pair: Pair::from((
                    channel_entity_ent.pair.descriptor.replace_in_copy(12),
                    channel_entity_ent.pair.state.clone(),
                )),
            },
            MdibDescriptionModification::Update {
                pair: Pair::from((
                    metric_entity_ent.pair.descriptor.replace_in_copy(5),
                    metric_entity_ent.pair.state.clone(),
                )),
            },
        ];

        modifications = DescriptionPreprocessor::before_first_modification(
            &mut checker,
            &storage,
            modifications,
        )
        .expect("broke");
        assert_eq!(3, modifications.len());

        modifications =
            DescriptionPreprocessor::process(&mut checker, &storage, modifications).expect("broke");
        // nothing added
        assert_eq!(3, modifications.len());

        // check vmd version
        if let MdibDescriptionModification::Update { pair } = modifications.remove(0) {
            let (descriptor, states) = pair.into();

            assert_eq!(
                vmd_entity_ent.pair.descriptor.version() + 1,
                descriptor.version()
            );
            assert_eq!(
                vmd_entity_ent.pair.descriptor.version() + 1,
                states.first().unwrap().descriptor_version()
            );
            assert_eq!(
                vmd_entity_ent.pair.state.version() + 1,
                states.first().unwrap().version()
            );

            assert!(matches!(
                descriptor,
                AbstractDescriptorOneOf::VmdDescriptor(VmdDescriptor { .. })
            ))
        } else {
            panic!()
        }

        // check channel version
        if let MdibDescriptionModification::Update { pair } = modifications.remove(0) {
            let (descriptor, states) = pair.into();

            assert_eq!(
                channel_entity_ent.pair.descriptor.version() + 1,
                descriptor.version()
            );
            assert_eq!(
                channel_entity_ent.pair.descriptor.version() + 1,
                states.first().unwrap().descriptor_version()
            );
            assert_eq!(
                channel_entity_ent.pair.state.version() + 1,
                states.first().unwrap().version()
            );

            assert!(matches!(
                descriptor,
                AbstractDescriptorOneOf::ChannelDescriptor(ChannelDescriptor { .. })
            ))
        } else {
            panic!()
        }

        // check metric version
        if let MdibDescriptionModification::Update { pair } = modifications.remove(0) {
            let (descriptor, states) = pair.into();
            assert_eq!(
                metric_entity_ent.pair.descriptor.version() + 1,
                descriptor.version()
            );

            assert_eq!(
                metric_entity_ent.pair.descriptor.version() + 1,
                states.first().unwrap().descriptor_version()
            );
            assert_eq!(
                metric_entity_ent.pair.state.version() + 1,
                states.first().unwrap().version()
            );

            assert!(matches!(
                descriptor,
                AbstractDescriptorOneOf::NumericMetricDescriptor(NumericMetricDescriptor { .. })
            ))
        } else {
            panic!()
        }
    }

    /// Deletes a metric descriptor, checks for correct versioning of the parent.
    #[tokio::test]
    async fn test_descriptor_delete_causes_update() {
        init();
        let storage = base_storage();
        let mut checker = new_handler();

        let channel_entity = storage.entity(handles::CHANNEL_0).unwrap();
        let metric_entity = storage.entity(handles::METRIC_0).unwrap();

        // delete metric
        {
            let mut modifications = vec![MdibDescriptionModification::Delete {
                handle: metric_entity.entity.handle(),
            }];

            modifications = DescriptionPreprocessor::before_first_modification(
                &mut checker,
                &storage,
                modifications,
            )
            .expect("broke");
            assert_eq!(1, modifications.len());

            modifications = DescriptionPreprocessor::process(&mut checker, &storage, modifications)
                .expect("broke");
            // parent update added
            assert_eq!(2, modifications.len());

            // check delete
            if let MdibDescriptionModification::Delete { handle } =
                modifications.first().expect("broke")
            {
                assert_eq!(&metric_entity.entity.handle(), handle);
            } else {
                panic!()
            }

            // check channel version
            if let MdibDescriptionModification::Update { pair } = modifications.remove(1) {
                let (descriptor, states) = pair.into();

                assert_eq!(
                    channel_entity.entity.get_abstract_descriptor().version() + 1,
                    descriptor.version()
                );
                assert_eq!(
                    channel_entity.entity.get_abstract_descriptor().version() + 1,
                    states.first().unwrap().descriptor_version()
                );
                assert_eq!(
                    channel_entity.resolve_single_state().unwrap().version() + 1,
                    states.first().unwrap().version()
                );

                assert!(matches!(
                    descriptor,
                    AbstractDescriptorOneOf::ChannelDescriptor(ChannelDescriptor { .. })
                ))
            } else {
                panic!()
            }
        }
    }

    /// Deletes a channel and its metric, ensuring that deleting the metric does not cause a
    /// channel update.
    #[tokio::test]
    async fn test_descriptor_delete_subtree_causes_no_additional_updates() {
        init();
        let storage = base_storage();
        let mut checker = new_handler();

        let channel_entity = storage.entity(handles::CHANNEL_0).unwrap();
        let metric_entity = storage.entity(handles::METRIC_0).unwrap();
        let vmd_entity = storage.entity(handles::VMD_0).unwrap();

        // delete metric
        {
            let mut modifications = vec![
                MdibDescriptionModification::Delete {
                    handle: metric_entity.entity.handle(),
                },
                MdibDescriptionModification::Delete {
                    handle: channel_entity.entity.handle(),
                },
            ];

            modifications = DescriptionPreprocessor::before_first_modification(
                &mut checker,
                &storage,
                modifications,
            )
            .expect("broke");
            assert_eq!(2, modifications.len());

            modifications = DescriptionPreprocessor::process(&mut checker, &storage, modifications)
                .expect("broke");
            // parent update added
            assert_eq!(3, modifications.len());

            // check delete
            if let MdibDescriptionModification::Delete { handle } =
                modifications.first().expect("broke")
            {
                assert_eq!(&metric_entity.entity.handle(), handle);
            } else {
                panic!()
            }

            if let MdibDescriptionModification::Delete { handle } =
                modifications.get(1).expect("broke")
            {
                assert_eq!(&channel_entity.entity.handle(), handle);
            } else {
                panic!()
            }

            // check vmd version
            if let MdibDescriptionModification::Update { pair } = modifications.remove(2) {
                let (descriptor, states) = pair.into();

                assert_eq!(
                    vmd_entity.entity.get_abstract_descriptor().version() + 1,
                    descriptor.version()
                );
                assert_eq!(
                    vmd_entity.entity.get_abstract_descriptor().version() + 1,
                    states.first().unwrap().descriptor_version()
                );
                assert_eq!(
                    vmd_entity.resolve_single_state().unwrap().version() + 1,
                    states.first().unwrap().version()
                );

                assert!(matches!(
                    descriptor,
                    AbstractDescriptorOneOf::VmdDescriptor(VmdDescriptor { .. })
                ))
            } else {
                panic!()
            }
        }
    }

    async fn delete_one<S: MdibStorage>(
        handle: &str,
        _parent_handle: &str,
        storage: &mut S,
        checker: &mut VersionHandler<S>,
    ) -> anyhow::Result<()> {
        let mut modifications = vec![MdibDescriptionModification::Delete {
            handle: handle.to_string(),
        }];

        modifications =
            DescriptionPreprocessor::before_first_modification(checker, storage, modifications)?;
        assert_eq!(1, modifications.len());
        modifications = DescriptionPreprocessor::process(checker, storage, modifications)?;
        // parent update added
        assert_eq!(2, modifications.len());
        modifications =
            DescriptionPreprocessor::after_last_modification(checker, storage, modifications)?;
        assert_eq!(2, modifications.len());

        let mut storage_modifications = MdibDescriptionModifications::default();
        storage_modifications.add_all(modifications)?;
        storage.apply_description(
            storage.mdib_version().inc(),
            Some(storage.md_description_version() + 1),
            Some(storage.md_state_version() + 1),
            storage_modifications,
        )?;

        Ok(())
    }

    async fn insert_metric<S: MdibStorage>(
        handle: &str,
        parent_handle: &str,
        expected_descriptor_version: u64,
        storage: &mut S,
        checker: &mut VersionHandler<S>,
    ) -> anyhow::Result<()> {
        let mut modifications = vec![MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(NumericMetricPair::from((
                mdib_utils::create_minimal_numeric_metric_descriptor(handle),
                mdib_utils::create_minimal_numeric_metric_state(handle),
            )))),
            parent: Some(handles::CHANNEL_0.to_string()),
        }];

        modifications =
            DescriptionPreprocessor::before_first_modification(checker, storage, modifications)?;
        assert_eq!(1, modifications.len());
        modifications = DescriptionPreprocessor::process(checker, storage, modifications)?;
        // parent update added
        assert_eq!(2, modifications.len());
        modifications =
            DescriptionPreprocessor::after_last_modification(checker, storage, modifications)?;
        assert_eq!(2, modifications.len());
        modifications =
            DescriptionPreprocessor::after_last_modification(checker, storage, modifications)?;
        assert_eq!(2, modifications.len());

        // check insert
        if let MdibDescriptionModification::Insert { pair, parent } =
            modifications.first().expect("broke")
        {
            assert_eq!(parent_handle, parent.as_ref().expect("No parent").as_str());
            assert_eq!(
                expected_descriptor_version,
                pair.version().descriptor_version
            );
        } else {
            panic!()
        }

        let mut storage_modifications = MdibDescriptionModifications::default();
        storage_modifications.add_all(modifications)?;
        storage.apply_description(
            storage.mdib_version().inc(),
            Some(storage.md_description_version() + 1),
            Some(storage.md_state_version() + 1),
            storage_modifications,
        )?;

        Ok(())
    }

    /// Deletes and reinserts a descriptor, ensuring the reinserted descriptor has an incremented version.
    #[tokio::test]
    async fn test_delete_and_reinsert_increments_descriptor_version() -> anyhow::Result<()> {
        init();
        let mut storage = base_storage();
        let mut checker = new_handler();

        let channel_entity = storage.entity(handles::CHANNEL_0).unwrap();
        let metric_entity = storage.entity(handles::METRIC_0).unwrap();

        // delete metric
        {
            delete_one(
                &metric_entity.entity.handle(),
                &channel_entity.entity.handle(),
                &mut storage,
                &mut checker,
            )
            .await?;
        }
        // reinsert metric
        {
            insert_metric(
                &metric_entity.entity.handle(),
                &channel_entity.entity.handle(),
                1,
                &mut storage,
                &mut checker,
            )
            .await?;
        }
        // delete again
        {
            delete_one(
                &metric_entity.entity.handle(),
                &channel_entity.entity.handle(),
                &mut storage,
                &mut checker,
            )
            .await?;
        }
        // reinsert again
        {
            insert_metric(
                &metric_entity.entity.handle(),
                &channel_entity.entity.handle(),
                2,
                &mut storage,
                &mut checker,
            )
            .await?;
        }

        Ok(())
    }

    #[tokio::test]
    async fn test_state_versioning_component() {
        init();
        let storage = base_storage();
        let mut checker = new_handler();

        let vmd_entity = storage.entity(handles::VMD_0).unwrap();
        let clock_entity = storage.entity(handles::CLOCK_0).unwrap();

        let vmd_code = Some(mdib_utils::create_minimal_coded_value("yolocode"));
        let clock_bad_version = 500;

        let mut vmd_state = match vmd_entity.entity {
            Entity::Vmd(ref vmd) => vmd.clone().pair.state,
            _ => panic!(),
        };

        let clock_state = match clock_entity.entity {
            Entity::Clock(ref clock) => clock.clone().pair.state,
            _ => panic!(),
        }
        .replace_in_copy(clock_bad_version, clock_bad_version);

        // change vmd state a little
        vmd_state.operating_jurisdiction = Some(OperatingJurisdiction {
            instance_identifier: InstanceIdentifier {
                extension_element: None,
                r#type: vmd_code.clone(),
                identifier_name: vec![],
                root_attr: None,
                extension_attr: None,
            },
        });

        let state_modifications = MdibStateModifications::Component {
            component_states: vec![
                AbstractDeviceComponentStateOneOf::VmdState(vmd_state),
                AbstractDeviceComponentStateOneOf::ClockState(clock_state),
            ],
        };

        let mut modifications = StatePreprocessor::before_first_modification(
            &mut checker,
            &storage,
            state_modifications,
        )
        .expect("broke");
        assert_eq!(2, modifications.len());

        modifications =
            StatePreprocessor::process(&mut checker, &storage, modifications).expect("broke");
        // nothing added
        assert_eq!(2, modifications.len());

        let modifications_vec = modifications.states();

        if let AbstractStateOneOf::VmdState(state) = modifications_vec.first().unwrap() {
            // check version correct
            assert_eq!(
                vmd_entity.entity.get_abstract_descriptor().version(),
                state.descriptor_version()
            );
            assert_eq!(
                vmd_entity.resolve_single_state().unwrap().version() + 1,
                state.version()
            );
            // assert update present
            assert_eq!(
                vmd_code,
                state
                    .operating_jurisdiction
                    .as_ref()
                    .unwrap()
                    .instance_identifier
                    .r#type
            );
        } else {
            panic!()
        };

        if let AbstractStateOneOf::ClockState(state) = modifications_vec.get(1).unwrap() {
            // check version correct
            assert_eq!(
                clock_entity.entity.get_abstract_descriptor().version(),
                state.descriptor_version()
            );
            assert_eq!(
                clock_entity.resolve_single_state().unwrap().version() + 1,
                state.version()
            );

            assert_ne!(clock_bad_version, state.descriptor_version());
            assert_ne!(clock_bad_version + 1, state.descriptor_version());

            assert_ne!(clock_bad_version, state.version());
            assert_ne!(clock_bad_version + 1, state.version());
        } else {
            panic!()
        };
    }

    #[tokio::test]
    async fn test_state_versioning_component_multiple() {
        init();
        let storage = base_storage();
        let mut checker = new_handler();

        let vmd_entity = storage.entity(handles::VMD_0).unwrap();

        let vmd_code = Some(mdib_utils::create_minimal_coded_value("yolocode"));
        let bad_version = 500;

        let mut vmd_state = match vmd_entity.entity {
            Entity::Vmd(ref vmd) => vmd.clone().pair.state,
            _ => panic!(),
        }
        .replace_in_copy(bad_version, bad_version);

        // change vmd state a little
        vmd_state.operating_jurisdiction = Some(OperatingJurisdiction {
            instance_identifier: InstanceIdentifier {
                extension_element: None,
                r#type: vmd_code.clone(),
                identifier_name: vec![],
                root_attr: None,
                extension_attr: None,
            },
        });

        let state_modifications = MdibStateModifications::Component {
            component_states: vec![
                AbstractDeviceComponentStateOneOf::VmdState(vmd_state.clone()),
                AbstractDeviceComponentStateOneOf::VmdState(vmd_state),
            ],
        };

        let mut modifications = StatePreprocessor::before_first_modification(
            &mut checker,
            &storage,
            state_modifications,
        )
        .expect("broke");
        assert_eq!(2, modifications.len());

        modifications =
            StatePreprocessor::process(&mut checker, &storage, modifications).expect("broke");
        // nothing added
        assert_eq!(2, modifications.len());

        let modifications_vec = modifications.states();

        let mut counter = 0;

        for i in 0..=1 {
            if let AbstractStateOneOf::VmdState(state) = modifications_vec.get(i).unwrap() {
                // check version correct
                assert_eq!(
                    vmd_entity.entity.get_abstract_descriptor().version(),
                    state.descriptor_version()
                );
                assert_eq!(
                    vmd_entity.resolve_single_state().unwrap().version() + 1,
                    state.version()
                );
                // assert update present
                assert_eq!(
                    vmd_code,
                    state
                        .operating_jurisdiction
                        .as_ref()
                        .unwrap()
                        .instance_identifier
                        .r#type
                );
            } else {
                panic!()
            };
            counter += 1
        }

        assert_eq!(2, counter);
    }

    #[tokio::test]
    async fn test_state_versioning_context() {
        init();
        let storage = base_storage();
        let mut checker = new_handler();

        let patient_context = storage.entity(handles::CONTEXT_DESCRIPTOR_0).unwrap();

        let binding_version = Some(ReferencedVersion {
            version_counter: mdib_utils::create_version_counter(Some(5)).unwrap(),
        });
        let bad_version = 650;

        let mut first_state = match patient_context.resolve_context_states().remove(0) {
            AbstractContextStateOneOf::PatientContextState(pat) => pat,
            _ => panic!(),
        };
        first_state
            .abstract_context_state
            .binding_mdib_version_attr
            .clone_from(&binding_version);
        first_state = first_state.replace_in_copy(bad_version, bad_version);

        let new_state = mdib_utils::create_minimal_patient_context_state(
            handles::CONTEXT_DESCRIPTOR_0,
            handles::CONTEXT_5,
        )
        .replace_in_copy(bad_version, bad_version);

        let state_modifications = MdibStateModifications::Context {
            context_states: vec![
                first_state.into_abstract_context_state_one_of(),
                new_state.into_abstract_context_state_one_of(),
            ],
        };

        let mut modifications = StatePreprocessor::before_first_modification(
            &mut checker,
            &storage,
            state_modifications,
        )
        .expect("broke");
        assert_eq!(2, modifications.len());

        modifications =
            StatePreprocessor::process(&mut checker, &storage, modifications).expect("broke");
        // nothing added
        assert_eq!(2, modifications.len());

        let modifications_vec = modifications.states();

        if let AbstractStateOneOf::PatientContextState(state) = modifications_vec.first().unwrap() {
            assert_eq!(handles::CONTEXT_0, state.state_handle());
            // check version correct
            assert_eq!(1, state.version());
            assert_eq!(
                patient_context.entity.get_abstract_descriptor().version(),
                state.descriptor_version()
            );
            // assert update present
            assert_eq!(
                binding_version,
                state.abstract_context_state.binding_mdib_version_attr
            );
        } else {
            panic!()
        };

        if let AbstractStateOneOf::PatientContextState(state) = modifications_vec.get(1).unwrap() {
            assert_eq!(handles::CONTEXT_5, state.state_handle());
            // check version correct
            assert_eq!(0, state.version());
            assert_eq!(
                patient_context.entity.get_abstract_descriptor().version(),
                state.descriptor_version()
            );
        } else {
            panic!()
        };
    }

    #[tokio::test]
    async fn test_state_versioning_context_multiple() {
        init();
        let storage = base_storage();
        let mut checker = new_handler();

        let patient_context = storage.entity(handles::CONTEXT_DESCRIPTOR_0).unwrap();

        let binding_version = Some(ReferencedVersion {
            version_counter: mdib_utils::create_version_counter(Some(5)).unwrap(),
        });
        let bad_version = 650;

        let mut updated_state = match patient_context.resolve_context_states().remove(0) {
            AbstractContextStateOneOf::PatientContextState(pat) => pat,
            _ => panic!(),
        };
        updated_state
            .abstract_context_state
            .binding_mdib_version_attr
            .clone_from(&binding_version);
        updated_state = updated_state.replace_in_copy(bad_version, bad_version);

        let state_modifications = MdibStateModifications::Context {
            context_states: vec![
                updated_state.clone().into_abstract_context_state_one_of(),
                updated_state.into_abstract_context_state_one_of(),
            ],
        };

        let mut modifications = StatePreprocessor::before_first_modification(
            &mut checker,
            &storage,
            state_modifications,
        )
        .expect("broke");
        assert_eq!(2, modifications.len());

        modifications =
            StatePreprocessor::process(&mut checker, &storage, modifications).expect("broke");
        // nothing added
        assert_eq!(2, modifications.len());

        let modifications_vec = modifications.states();

        for i in 0..=1 {
            if let AbstractStateOneOf::PatientContextState(state) =
                modifications_vec.get(i).unwrap()
            {
                assert_eq!(handles::CONTEXT_0, state.state_handle());
                // check version correct
                assert_eq!(1, state.version());
                assert_eq!(
                    patient_context.entity.get_abstract_descriptor().version(),
                    state.descriptor_version()
                );
                // assert update present
                assert_eq!(
                    binding_version,
                    state.abstract_context_state.binding_mdib_version_attr
                );
            } else {
                panic!()
            };
        }
    }

    /// Inserts a single state descriptor and checks that the version is saved, then checks that starting a state update flushes saved versions.
    #[tokio::test]
    async fn test_state_update_before_flushes_saved_versions() {
        init();
        let storage = base_storage();
        let mut checker = new_handler();

        let mut modifications = vec![MdibDescriptionModification::Insert {
            pair: Pair::from((
                mdib_utils::create_minimal_channel_descriptor(handles::CHANNEL_3),
                mdib_utils::create_minimal_channel_state(handles::CHANNEL_3),
            )),
            parent: Some(handles::VMD_0.to_string()),
        }];

        let parent_entity = storage.entity(handles::VMD_0).expect("broke");

        modifications = DescriptionPreprocessor::before_first_modification(
            &mut checker,
            &storage,
            modifications,
        )
        .expect("broke");
        assert_eq!(1, modifications.len());

        modifications =
            DescriptionPreprocessor::process(&mut checker, &storage, modifications).expect("broke");

        // update added
        assert_eq!(2, modifications.len());
        assert!(matches!(
            modifications.get(1).unwrap(),
            MdibDescriptionModification::Update { .. }
        ));

        // make sure update bumps parent version
        if let MdibDescriptionModification::Update { pair } = modifications.remove(1) {
            assert_eq!(
                parent_entity.entity.get_abstract_descriptor().version() + 1,
                pair.version().descriptor_version
            );

            let (descriptor, states) = pair.into();

            let state = states.first().expect("broke");

            assert_eq!(
                parent_entity
                    .resolve_single_state()
                    .expect("broke")
                    .version()
                    + 1,
                state.version()
            );
            assert_eq!(descriptor.version(), state.descriptor_version());
        } else {
            panic!();
        }

        // make sure update is in cache and removed after the before_first_modification call
        assert!(
            checker
                .version_handler_interior
                .lock()
                .expect("Lock")
                .updated_versions
                .contains_key(handles::VMD_0),
            "Cache didn't contain handle"
        );
        let _state_modifications = StatePreprocessor::before_first_modification(
            &mut checker,
            &storage,
            MdibStateModifications::Alert {
                alert_states: vec![],
            },
        )
        .expect("broke");
        assert!(
            !checker
                .version_handler_interior
                .lock()
                .expect("Lock")
                .updated_versions
                .contains_key(handles::VMD_0),
            "Cache still contained handle"
        );
    }

    /// Inserts a multi state descriptor and checks that the version of the descriptor is set correctly, not to a default 0.
    #[tokio::test]
    async fn test_inserted_context_state_uses_correct_descriptor_version() -> anyhow::Result<()> {
        init();
        let mut storage = base_storage();
        let mut checker = new_handler();

        // update patient context descriptor to version 1
        let mut modifications = vec![MdibDescriptionModification::Update {
            pair: Pair::from((
                mdib_utils::create_minimal_patient_context_descriptor(
                    handles::CONTEXT_DESCRIPTOR_0,
                ),
                vec![mdib_utils::create_minimal_patient_context_state(
                    handles::CONTEXT_DESCRIPTOR_0,
                    handles::CONTEXT_0,
                )],
            )),
        }];

        modifications = DescriptionPreprocessor::before_first_modification(
            &mut checker,
            &storage,
            modifications,
        )?;
        assert_eq!(1, modifications.len());

        modifications = DescriptionPreprocessor::process(&mut checker, &storage, modifications)?;
        assert_eq!(1, modifications.len());

        assert!(match modifications.first().unwrap() {
            MdibDescriptionModification::Update {
                pair: Pair::MultiStatePair(MultiStatePair::PatientContext(patient)),
            } => {
                assert_eq!(
                    1,
                    patient
                        .descriptor
                        .abstract_context_descriptor
                        .abstract_descriptor
                        .descriptor_version_attr
                        .as_ref()
                        .unwrap()
                        .unsigned_long
                );
                // ensure that CONTEXT_1 was added
                assert_eq!(2, patient.states.len());
                assert!(patient
                    .states
                    .iter()
                    .any(|state| state.state_handle() == handles::CONTEXT_1));
                patient.states.iter().for_each(|state| {
                    assert_eq!(
                        1,
                        state
                            .abstract_context_state
                            .abstract_multi_state
                            .abstract_state
                            .descriptor_version_attr
                            .as_ref()
                            .unwrap()
                            .version_counter
                            .unsigned_long
                    )
                });
                true
            }
            _ => false,
        });

        modifications = DescriptionPreprocessor::after_last_modification(
            &mut checker,
            &storage,
            modifications,
        )?;

        let mut storage_modifications = MdibDescriptionModifications::default();
        storage_modifications.add_all(modifications)?;
        storage
            .apply_description(
                storage.mdib_version().inc(),
                Some(storage.md_description_version() + 1),
                Some(storage.md_state_version() + 1),
                storage_modifications,
            )
            .expect("write failed");

        // ensure that descriptor versions are 1 for descriptor and all states
        let storage_patient = storage.entity(handles::CONTEXT_DESCRIPTOR_0).unwrap();
        assert_eq!(
            1,
            storage_patient
                .entity
                .get_abstract_descriptor_ref()
                .descriptor_version_attr
                .as_ref()
                .unwrap()
                .unsigned_long
        );
        assert_eq!(
            1,
            storage
                .context_state(handles::CONTEXT_0)
                .unwrap()
                .descriptor_version()
        );
        assert_eq!(
            1,
            storage
                .context_state(handles::CONTEXT_1)
                .unwrap()
                .descriptor_version()
        );

        // add a new context state through a state update
        let mut state_modification = MdibStateModifications::Context {
            context_states: vec![mdib_utils::create_minimal_patient_context_state(
                handles::CONTEXT_DESCRIPTOR_0,
                handles::CONTEXT_5,
            )
            .into_abstract_context_state_one_of()],
        };

        state_modification = StatePreprocessor::before_first_modification(
            &mut checker,
            &storage,
            state_modification,
        )?;
        state_modification =
            StatePreprocessor::process(&mut checker, &storage, state_modification)?;
        state_modification =
            StatePreprocessor::after_last_modification(&mut checker, &storage, state_modification)?;

        assert_eq!(
            1,
            state_modification
                .states()
                .first()
                .unwrap()
                .descriptor_version()
        );

        Ok(())
    }
}
