use std::marker::PhantomData;

use crate::common::mdib_description_modification::MdibDescriptionModification;
use crate::common::mdib_pair::MdibPair;
use crate::common::preprocessing::{DescriptionPreprocessor, PreprocessingError};
use crate::common::storage::mdib_storage::MdibStorage;

/// Preprocessing segment which checks for handle collisions with already present descriptors.
#[derive(Debug)]
pub struct DuplicateDescriptorHandler<T: MdibStorage> {
    phantom: PhantomData<T>,
}

impl<T: MdibStorage> Default for DuplicateDescriptorHandler<T> {
    fn default() -> Self {
        DuplicateDescriptorHandler {
            phantom: Default::default(),
        }
    }
}

impl<T: MdibStorage> DescriptionPreprocessor<T> for DuplicateDescriptorHandler<T> {
    fn process(
        &mut self,
        mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        let duplicates: Vec<String> = modifications
            .iter()
            .filter_map(|it| match it {
                MdibDescriptionModification::Insert { pair, .. } => Some(pair),
                _ => None,
            })
            .filter_map(|it| mdib_storage.entity(&it.handle()).map(|_| it.handle()))
            .collect();

        if !duplicates.is_empty() {
            return Err(PreprocessingError::DuplicateDescriptorHandle {
                handles: duplicates,
            });
        }
        Ok(modifications)
    }
}

#[cfg(test)]
mod test {
    use env_logger;

    use crate::common::mdib_description_modification::MdibDescriptionModification;
    use crate::common::mdib_description_modifications::MdibDescriptionModifications;
    use crate::common::preprocessing::{DescriptionPreprocessor, PreprocessingError};
    use crate::provider::preprocessing::duplicate_descriptor_handler::DuplicateDescriptorHandler;
    use crate::test_util::mdib::base_storage;
    use crate::test_util::{handles, mdib_utils};

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[tokio::test]
    async fn test_valid() {
        init();
        let storage = base_storage();
        let mut checker = DuplicateDescriptorHandler::default();

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: (
                    mdib_utils::create_minimal_system_context_descriptor(handles::SYSTEM_CONTEXT_1),
                    mdib_utils::create_minimal_system_context_state(handles::SYSTEM_CONTEXT_1),
                )
                    .into(),
                parent: Some(handles::MDS_1.to_string()),
            })
            .expect("Could not add modification");

        // should work
        let result = checker.process(&storage, modifications.into_vec());

        assert!(result.is_ok())
    }

    #[tokio::test]
    async fn test_invalid_duplicate_vmd0_handle() {
        init();
        let storage = base_storage();
        let mut checker = DuplicateDescriptorHandler::default();

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: (
                    mdib_utils::create_minimal_string_metric_descriptor(handles::VMD_0),
                    mdib_utils::create_minimal_string_metric_state(handles::VMD_0),
                )
                    .into(),
                parent: Some(handles::CHANNEL_0.to_string()),
            })
            .expect("Could not add modification");

        // should break
        let result = checker.process(&storage, modifications.into_vec());

        assert!(result.is_err());
        assert_eq!(
            PreprocessingError::DuplicateDescriptorHandle {
                handles: vec![handles::VMD_0.to_string()]
            },
            result.err().unwrap()
        )
    }
}
