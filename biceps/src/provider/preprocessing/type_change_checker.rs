use crate::common::biceps_util::MultiState;
use crate::common::mdib_description_modification::MdibDescriptionModification;
use crate::common::mdib_pair::{MdibPair, MultiStatePair, Pair, SingleStatePair};
use crate::common::mdib_state_modifications::MdibStateModifications;
use crate::common::preprocessing::{
    DescriptionPreprocessor, PreprocessingError, StatePreprocessor,
};
use crate::common::storage::mdib_storage::MdibStorage;
use itertools::Itertools;
use log::trace;
use protosdc_biceps::biceps::{
    AbstractContextStateOneOf, EnsembleContextState, LocationContextState, MeansContextState,
    OperatorContextState, PatientContextState, WorkflowContextState,
};
use std::any::{Any, TypeId};
use std::collections::HashMap;
use std::marker::PhantomData;
use std::sync::{Arc, Mutex};

#[derive(Debug, Default)]
pub struct TypeChangeCheckerInterior {
    handles: HashMap<String, TypeId>,
}

/// Preprocessing segment which checks that descriptor handles are only ever used for the same data type.
#[derive(Debug)]
pub struct TypeChangeChecker<T: MdibStorage> {
    phantom: PhantomData<T>,
    interior: Arc<Mutex<TypeChangeCheckerInterior>>,
}

fn descriptor_type_id(value: &Pair) -> TypeId {
    match value {
        Pair::SingleStatePair(ssp) => match ssp {
            SingleStatePair::StringMetric(x) => x.descriptor.type_id(),
            SingleStatePair::EnumStringMetric(x) => x.descriptor.type_id(),
            SingleStatePair::NumericMetric(x) => x.descriptor.type_id(),
            SingleStatePair::RealTimeSampleArrayMetric(x) => x.descriptor.type_id(),
            SingleStatePair::DistributionSampleArrayMetric(x) => x.descriptor.type_id(),
            SingleStatePair::AlertCondition(x) => x.descriptor.type_id(),
            SingleStatePair::LimitAlertCondition(x) => x.descriptor.type_id(),
            SingleStatePair::AlertSignal(x) => x.descriptor.type_id(),
            SingleStatePair::AlertSystem(x) => x.descriptor.type_id(),
            SingleStatePair::Mds(x) => x.descriptor.type_id(),
            SingleStatePair::Vmd(x) => x.descriptor.type_id(),
            SingleStatePair::Channel(x) => x.descriptor.type_id(),
            SingleStatePair::Sco(x) => x.descriptor.type_id(),
            SingleStatePair::SystemContext(x) => x.descriptor.type_id(),
            SingleStatePair::Battery(x) => x.descriptor.type_id(),
            SingleStatePair::Clock(x) => x.descriptor.type_id(),
            SingleStatePair::SetValueOperation(x) => x.descriptor.type_id(),
            SingleStatePair::SetStringOperation(x) => x.descriptor.type_id(),
            SingleStatePair::SetAlertStateOperation(x) => x.descriptor.type_id(),
            SingleStatePair::SetMetricStateOperation(x) => x.descriptor.type_id(),
            SingleStatePair::SetComponentStateOperation(x) => x.descriptor.type_id(),
            SingleStatePair::SetContextStateOperation(x) => x.descriptor.type_id(),
            SingleStatePair::ActivateOperation(x) => x.descriptor.type_id(),
        },
        Pair::MultiStatePair(msp) => match msp {
            MultiStatePair::PatientContext(x) => x.descriptor.type_id(),
            MultiStatePair::LocationContext(x) => x.descriptor.type_id(),
            MultiStatePair::EnsembleContext(x) => x.descriptor.type_id(),
            MultiStatePair::WorkflowContext(x) => x.descriptor.type_id(),
            MultiStatePair::MeansContext(x) => x.descriptor.type_id(),
            MultiStatePair::OperatorContext(x) => x.descriptor.type_id(),
        },
    }
}

fn multi_state_type_id(value: &MultiStatePair) -> TypeId {
    match value {
        MultiStatePair::PatientContext(_) => TypeId::of::<PatientContextState>(),
        MultiStatePair::LocationContext(_) => TypeId::of::<LocationContextState>(),
        MultiStatePair::EnsembleContext(_) => TypeId::of::<EnsembleContextState>(),
        MultiStatePair::WorkflowContext(_) => TypeId::of::<WorkflowContextState>(),
        MultiStatePair::MeansContext(_) => TypeId::of::<MeansContextState>(),
        MultiStatePair::OperatorContext(_) => TypeId::of::<OperatorContextState>(),
    }
}

fn multi_state_handles(value: &MultiStatePair) -> Vec<String> {
    match value {
        MultiStatePair::PatientContext(x) => {
            x.states.iter().map(|it| it.state_handle()).collect_vec()
        }
        MultiStatePair::LocationContext(x) => {
            x.states.iter().map(|it| it.state_handle()).collect_vec()
        }
        MultiStatePair::EnsembleContext(x) => {
            x.states.iter().map(|it| it.state_handle()).collect_vec()
        }
        MultiStatePair::WorkflowContext(x) => {
            x.states.iter().map(|it| it.state_handle()).collect_vec()
        }
        MultiStatePair::MeansContext(x) => {
            x.states.iter().map(|it| it.state_handle()).collect_vec()
        }
        MultiStatePair::OperatorContext(x) => {
            x.states.iter().map(|it| it.state_handle()).collect_vec()
        }
    }
}

fn abstract_context_state_type_id(value: &AbstractContextStateOneOf) -> TypeId {
    match value {
        AbstractContextStateOneOf::AbstractContextState(x) => x.type_id(),
        AbstractContextStateOneOf::EnsembleContextState(x) => x.type_id(),
        AbstractContextStateOneOf::LocationContextState(x) => x.type_id(),
        AbstractContextStateOneOf::MeansContextState(x) => x.type_id(),
        AbstractContextStateOneOf::OperatorContextState(x) => x.type_id(),
        AbstractContextStateOneOf::PatientContextState(x) => x.type_id(),
        AbstractContextStateOneOf::WorkflowContextState(x) => x.type_id(),
    }
}

impl<T: MdibStorage> TypeChangeChecker<T> {
    pub fn new(interior: Arc<Mutex<TypeChangeCheckerInterior>>) -> Self {
        Self {
            phantom: PhantomData,
            interior,
        }
    }
}

impl<T: MdibStorage> DescriptionPreprocessor<T> for TypeChangeChecker<T> {
    fn process(
        &mut self,
        _mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        let mut handles = self.interior.lock().unwrap();
        let collision_handles = modifications
            .iter()
            .filter_map(|it| match it {
                MdibDescriptionModification::Insert { pair, .. } => Some(pair),
                MdibDescriptionModification::Update { pair } => Some(pair),
                _ => None,
            })
            .flat_map(|pair| {
                let descriptor_type_id = descriptor_type_id(pair);
                let descriptor_handle = pair.handle();

                trace!(
                    "handle {} new descriptor type id {:?}",
                    descriptor_handle,
                    descriptor_type_id
                );

                // descriptor type
                let mut res = match handles.handles.get(&descriptor_handle) {
                    Some(present) => {
                        if present != &descriptor_type_id {
                            vec![pair.handle()]
                        } else {
                            vec![]
                        }
                    }
                    None => vec![],
                };
                if res.is_empty() {
                    handles.handles.insert(pair.handle(), descriptor_type_id);
                }

                match pair {
                    Pair::SingleStatePair(_) => {}
                    Pair::MultiStatePair(msp) => {
                        let multi_state_type_id = multi_state_type_id(msp);
                        let multi_state_handles = multi_state_handles(msp);

                        trace!(
                            "handles {:?}, multi_state_type_id {:?}",
                            multi_state_handles,
                            multi_state_type_id
                        );

                        let mut multi_state_res = multi_state_handles
                            .into_iter()
                            .filter_map(|it| {
                                let r = match handles.handles.get(&it) {
                                    Some(present) => {
                                        if present != &multi_state_type_id {
                                            Some(it.clone())
                                        } else {
                                            None
                                        }
                                    }
                                    None => None,
                                };
                                if r.is_none() {
                                    handles.handles.insert(it, multi_state_type_id);
                                }
                                r
                            })
                            .collect_vec();

                        res.append(&mut multi_state_res);
                    }
                }
                res
            })
            .collect_vec();

        if !collision_handles.is_empty() {
            return Err(PreprocessingError::TypeChangeError {
                handles: collision_handles,
            });
        }

        Ok(modifications)
    }
}

impl<T: MdibStorage> StatePreprocessor<T> for TypeChangeChecker<T> {
    fn process(
        &mut self,
        _mdib_storage: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError> {
        let mut handles = self.interior.lock().unwrap();
        match &modifications {
            MdibStateModifications::Context { context_states } => {
                let errors = context_states
                    .iter()
                    .filter_map(|state| {
                        let multi_state_type_id = abstract_context_state_type_id(state);
                        let multi_state_handle = state.state_handle();

                        trace!(
                            "handle: {}, new multi state type id {:?}",
                            multi_state_handle,
                            multi_state_type_id
                        );

                        let r = match handles.handles.get(&multi_state_handle) {
                            Some(present) => {
                                if present != &multi_state_type_id {
                                    Some(multi_state_handle.clone())
                                } else {
                                    None
                                }
                            }
                            None => None,
                        };
                        if r.is_none() {
                            handles
                                .handles
                                .insert(multi_state_handle, multi_state_type_id);
                        }
                        r
                    })
                    .collect_vec();

                if !errors.is_empty() {
                    return Err(PreprocessingError::TypeChangeError { handles: errors });
                }
                Ok(modifications)
            }
            _ => Ok(modifications),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::common::biceps_util::ContextState;
    use crate::common::mdib_description_modification::MdibDescriptionModification;
    use crate::common::mdib_description_modifications::MdibDescriptionModifications;
    use crate::common::mdib_state_modifications::MdibStateModifications;
    use crate::common::preprocessing::{
        DescriptionPreprocessor, PreprocessingError, StatePreprocessor,
    };
    use crate::common::storage::mdib_storage::{
        MdibStorage, MdibStorageConstructor, MdibStorageImpl,
    };
    use crate::provider::preprocessing::type_change_checker::{
        TypeChangeChecker, TypeChangeCheckerInterior,
    };
    use crate::test_util::{handles, mdib_utils};
    use std::sync::{Arc, Mutex};

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn test_consistent_types_descriptor() -> anyhow::Result<()> {
        init();
        let storage = MdibStorageImpl::new("wat:yolo".to_string(), false);

        let interior = Arc::new(Mutex::new(TypeChangeCheckerInterior::default()));
        let mut checker = TypeChangeChecker::new(interior);

        {
            let mut modifications = MdibDescriptionModifications::default();
            modifications
                .add(MdibDescriptionModification::Insert {
                    pair: (
                        mdib_utils::create_minimal_mds_descriptor(handles::MDS_0),
                        mdib_utils::create_minimal_mds_state(handles::MDS_0),
                    )
                        .into(),
                    parent: None,
                })
                .expect("Could not add modification");

            apply_description(modifications, &storage, &mut checker)?;
        }

        {
            let mut modifications = MdibDescriptionModifications::default();
            modifications
                .add(MdibDescriptionModification::Insert {
                    pair: (
                        mdib_utils::create_minimal_mds_descriptor(handles::MDS_0),
                        mdib_utils::create_minimal_mds_state(handles::MDS_0),
                    )
                        .into(),
                    parent: None,
                })
                .expect("Could not add modification");

            apply_description(modifications, &storage, &mut checker)?;
        }

        for _ in 1..=2 {
            {
                let mut modifications = MdibDescriptionModifications::default();
                modifications
                    .add(MdibDescriptionModification::Insert {
                        pair: (
                            mdib_utils::create_minimal_vmd_descriptor(handles::MDS_0),
                            mdib_utils::create_minimal_vmd_state(handles::MDS_0),
                        )
                            .into(),
                        parent: Some(handles::MDS_1.to_string()),
                    })
                    .expect("Could not add modification");

                apply_description_check_error(
                    modifications,
                    &storage,
                    &mut checker,
                    vec![handles::MDS_0.to_string()],
                )
            }
        }

        for _ in 1..=2 {
            {
                let mut modifications = MdibDescriptionModifications::default();
                modifications
                    .add(MdibDescriptionModification::Insert {
                        pair: (
                            mdib_utils::create_minimal_patient_context_descriptor(handles::MDS_0),
                            mdib_utils::create_minimal_patient_context_state(
                                handles::MDS_0,
                                handles::CONTEXT_5,
                            ),
                        )
                            .into(),
                        parent: Some(handles::SYSTEM_CONTEXT_1.to_string()),
                    })
                    .expect("Could not add modification");

                apply_description_check_error(
                    modifications,
                    &storage,
                    &mut checker,
                    vec![handles::MDS_0.to_string()],
                )
            }
        }
        Ok(())
    }

    #[test]
    fn test_consistent_types_states() -> anyhow::Result<()> {
        init();
        let storage = MdibStorageImpl::new("wat:yolo".to_string(), false);

        let interior = Arc::new(Mutex::new(TypeChangeCheckerInterior::default()));
        let mut checker = TypeChangeChecker::new(interior);

        {
            let mut modifications = MdibDescriptionModifications::default();
            modifications
                .add(MdibDescriptionModification::Insert {
                    pair: (
                        mdib_utils::create_minimal_patient_context_descriptor(
                            handles::CONTEXT_DESCRIPTOR_0,
                        ),
                        mdib_utils::create_minimal_patient_context_state(
                            handles::CONTEXT_DESCRIPTOR_0,
                            handles::CONTEXT_0,
                        ),
                    )
                        .into(),
                    parent: Some(handles::SYSTEM_CONTEXT_1.to_string()),
                })
                .expect("Could not add modification");

            apply_description(modifications, &storage, &mut checker)?;
        }

        for _ in 1..=2 {
            {
                let collision_handle = handles::CONTEXT_DESCRIPTOR_0;
                let mut modifications = MdibDescriptionModifications::default();
                modifications
                    .add(MdibDescriptionModification::Insert {
                        pair: (
                            mdib_utils::create_minimal_location_context_descriptor(
                                handles::CONTEXT_DESCRIPTOR_1,
                            ),
                            mdib_utils::create_minimal_location_context_state(
                                handles::CONTEXT_DESCRIPTOR_1,
                                collision_handle,
                            ),
                        )
                            .into(),
                        parent: Some(handles::SYSTEM_CONTEXT_1.to_string()),
                    })
                    .expect("Could not add modification");

                apply_description_check_error(
                    modifications,
                    &storage,
                    &mut checker,
                    vec![collision_handle.to_string()],
                )
            }
        }

        for _ in 1..=2 {
            {
                let collision_handle = handles::CONTEXT_DESCRIPTOR_0;
                let modification = MdibStateModifications::Context {
                    context_states: vec![mdib_utils::create_minimal_location_context_state(
                        handles::CONTEXT_DESCRIPTOR_1,
                        collision_handle,
                    )
                    .into_abstract_context_state_one_of()],
                };

                apply_state_check_error(
                    modification,
                    &storage,
                    &mut checker,
                    vec![collision_handle.to_string()],
                )
            }
        }

        Ok(())
    }

    fn apply_description<M: MdibStorage>(
        modifications: MdibDescriptionModifications,
        storage: &M,
        checker: &mut TypeChangeChecker<M>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        DescriptionPreprocessor::process(checker, storage, modifications.into_vec())
    }

    fn apply_state<M: MdibStorage>(
        modifications: MdibStateModifications,
        storage: &M,
        checker: &mut TypeChangeChecker<M>,
    ) -> Result<MdibStateModifications, PreprocessingError> {
        StatePreprocessor::process(checker, storage, modifications)
    }

    fn apply_description_check_error<M: MdibStorage>(
        modifications: MdibDescriptionModifications,
        storage: &M,
        checker: &mut TypeChangeChecker<M>,
        expected_handles: Vec<String>,
    ) {
        match apply_description(modifications, storage, checker) {
            Ok(_) => panic!("Should not be ok"),
            Err(err) => match err {
                PreprocessingError::TypeChangeError { handles } => {
                    assert_eq!(handles, expected_handles)
                }
                _ => panic!("Wrong error type"),
            },
        }
    }

    fn apply_state_check_error<M: MdibStorage>(
        modifications: MdibStateModifications,
        storage: &M,
        checker: &mut TypeChangeChecker<M>,
        expected_handles: Vec<String>,
    ) {
        match apply_state(modifications, storage, checker) {
            Ok(_) => panic!("Should not be ok"),
            Err(err) => match err {
                PreprocessingError::TypeChangeError { handles } => {
                    assert_eq!(handles, expected_handles)
                }
                _ => panic!("Wrong error type"),
            },
        }
    }
}
