use ambassador::{delegatable_trait, delegate_to_remote_methods, Delegate};
use derive_more::From;
use paste::paste;
use std::ops::Deref;

use protosdc_biceps::biceps::{
    AbstractDescriptorOneOf, AbstractStateOneOf, ActivateOperationDescriptor,
    ActivateOperationState, AlertConditionDescriptor, AlertConditionState, AlertSignalDescriptor,
    AlertSignalState, AlertSystemDescriptor, AlertSystemState, BatteryDescriptor, BatteryState,
    ChannelDescriptor, ChannelState, ClockDescriptor, ClockState,
    DistributionSampleArrayMetricDescriptor, DistributionSampleArrayMetricState,
    EnsembleContextDescriptor, EnsembleContextState, EnumStringMetricDescriptor,
    EnumStringMetricState, LimitAlertConditionDescriptor, LimitAlertConditionState,
    LocationContextDescriptor, LocationContextState, MdsDescriptor, MdsState,
    MeansContextDescriptor, MeansContextState, NumericMetricDescriptor, NumericMetricState,
    OperatorContextDescriptor, OperatorContextState, PatientContextDescriptor, PatientContextState,
    RealTimeSampleArrayMetricDescriptor, RealTimeSampleArrayMetricState, ScoDescriptor, ScoState,
    SetAlertStateOperationDescriptor, SetAlertStateOperationState,
    SetComponentStateOperationDescriptor, SetComponentStateOperationState,
    SetContextStateOperationDescriptor, SetContextStateOperationState,
    SetMetricStateOperationDescriptor, SetMetricStateOperationState, SetStringOperationDescriptor,
    SetStringOperationState, SetValueOperationDescriptor, SetValueOperationState,
    StringMetricDescriptor, StringMetricState, SystemContextDescriptor, SystemContextState,
    VmdDescriptor, VmdState, WorkflowContextDescriptor, WorkflowContextState,
};
use thiserror::Error;

use crate::common::biceps_util::{Descriptor, MultiState, State};

pub type MdibPairTuple = (AbstractDescriptorOneOf, Vec<AbstractStateOneOf>);

pub type MdibSingleStateTuple = (AbstractDescriptorOneOf, AbstractStateOneOf);

// TODO: Revisit all of these macros one RFC3086 is merged, so we can generate nested macros with variable arguments
//  See https://github.com/rust-lang/rust/issues/35853 for the issue of variable arguments
//  see https://github.com/rust-lang/rust/issues/83527 for the RFC3086 tracking issue
//  see stackoverflow for nested usage concepts
//  https://stackoverflow.com/questions/71822468/dynamically-creating-parameters-in-nested-rust-macros
//  generate_try_from_match_multi_state_pair does make use of the workaround, but readability kinda suffers
//  although to be fair, this kind of currying always has readability issues

/// Macro that invokes a macro with all MDIB entity types, first single state pairs, then
/// separated by "; and" multi state pairs
macro_rules! all_pairs {
    ($macro_id:ident) => {
        $macro_id! {
            StringMetric,
            EnumStringMetric,
            NumericMetric,
            RealTimeSampleArrayMetric,
            DistributionSampleArrayMetric,
            AlertCondition,
            LimitAlertCondition,
            AlertSignal,
            AlertSystem,
            Mds,
            Vmd,
            Channel,
            Sco,
            SystemContext,
            Battery,
            Clock,
            SetValueOperation,
            SetStringOperation,
            SetAlertStateOperation,
            SetMetricStateOperation,
            SetComponentStateOperation,
            SetContextStateOperation,
            ActivateOperation; and
            PatientContext,
            LocationContext,
            EnsembleContext,
            WorkflowContext,
            MeansContext,
            OperatorContext
        }
    };
}

/// Macro that invokes a macro with all MDIB single state entity types
macro_rules! single_states {
    ($macro_id:ident) => {
        $macro_id! {
            StringMetric,
            EnumStringMetric,
            NumericMetric,
            RealTimeSampleArrayMetric,
            DistributionSampleArrayMetric,
            AlertCondition,
            LimitAlertCondition,
            AlertSignal,
            AlertSystem,
            Mds,
            Vmd,
            Channel,
            Sco,
            SystemContext,
            Battery,
            Clock,
            SetValueOperation,
            SetStringOperation,
            SetAlertStateOperation,
            SetMetricStateOperation,
            SetComponentStateOperation,
            SetContextStateOperation,
            ActivateOperation
        }
    };
}

/// Macro that invokes a macro with all MDIB multi state entity types
macro_rules! multi_states {
    ($macro_id:ident) => {
        $macro_id! {
            PatientContext,
            LocationContext,
            EnsembleContext,
            WorkflowContext,
            MeansContext,
            OperatorContext
        }
    };
}

// export macros for other uses in crate
pub(crate) use {all_pairs, multi_states, single_states};

#[delegatable_trait]
pub trait MdibPair {
    /// Returns the descriptor handle of the entity
    fn handle(&self) -> String;

    /// Returns the version(s) of the entity, see [MdibPairVersion]
    fn version(&self) -> MdibPairVersion;
}

/// Enum to represent versions for the two different variants of states, single state and
/// multi-state.
pub enum MdibPairStateVersion {
    /// The version for a single state entity.
    SingleStateVersion(u64),
    /// A vec of multi-state handles and their respective state version.
    MultiStateVersion(Vec<(String, u64)>),
}

/// Represents the version of an [MdibPair].
///
/// The descriptor version is always a single entry, while the state version depends on whether
/// the entity represents a single or a multi state. See [MdibPairStateVersion].
pub struct MdibPairVersion {
    pub descriptor_version: u64,
    pub state_version: MdibPairStateVersion,
}

#[derive(Error, Debug)]
pub enum PairError {
    #[error("Descriptor and state(s) type do not match")]
    TypeMismatch,
    #[error("Descriptor {handle} requires exactly one state")]
    TooManyStates { handle: String },
}

macro_rules! generate_from_single_state {
    ($struct_name:ident, $descriptor_type:ident, $state_type: ident) => {
        impl From<($descriptor_type, $state_type)> for $struct_name {
            fn from(arg: ($descriptor_type, $state_type)) -> Self {
                Self {
                    descriptor: arg.0,
                    state: arg.1,
                }
            }
        }

        impl TryFrom<(AbstractDescriptorOneOf, AbstractStateOneOf)> for $struct_name {
            type Error = PairError;

            fn try_from(
                value: (AbstractDescriptorOneOf, AbstractStateOneOf),
            ) -> Result<Self, Self::Error> {
                match value {
                    (
                        AbstractDescriptorOneOf::$descriptor_type(descriptor),
                        AbstractStateOneOf::$state_type(state),
                    ) => Ok(Self { descriptor, state }),
                    _ => Err(PairError::TypeMismatch),
                }
            }
        }
    };
}

macro_rules! generate_from_multi_state {
    ($struct_name:ident, $descriptor_type:ident, $state_type: ident) => {
        impl From<($descriptor_type, $state_type)> for $struct_name {
            fn from(arg: ($descriptor_type, $state_type)) -> Self {
                Self {
                    descriptor: arg.0,
                    states: vec![arg.1],
                }
            }
        }

        impl From<($descriptor_type, Vec<$state_type>)> for $struct_name {
            fn from(arg: ($descriptor_type, Vec<$state_type>)) -> Self {
                Self {
                    descriptor: arg.0,
                    states: arg.1,
                }
            }
        }

        impl TryFrom<(AbstractDescriptorOneOf, AbstractStateOneOf)> for $struct_name {
            type Error = PairError;

            fn try_from(
                value: (AbstractDescriptorOneOf, AbstractStateOneOf),
            ) -> Result<Self, Self::Error> {
                match value {
                    (
                        AbstractDescriptorOneOf::$descriptor_type(descriptor),
                        AbstractStateOneOf::$state_type(state),
                    ) => Ok(Self {
                        descriptor,
                        states: vec![state],
                    }),
                    _ => Err(PairError::TypeMismatch),
                }
            }
        }
    };
}

macro_rules! generate_mdib_pair_single_state {
    ($struct_name:ident) => {
        impl MdibPair for $struct_name {
            fn handle(&self) -> String {
                self.descriptor.handle()
            }

            fn version(&self) -> MdibPairVersion {
                MdibPairVersion {
                    descriptor_version: self.descriptor.version(),
                    state_version: MdibPairStateVersion::SingleStateVersion(self.state.version()),
                }
            }
        }

        impl From<$struct_name> for MdibPairTuple {
            fn from(value: $struct_name) -> Self {
                (
                    value.descriptor.into_abstract_descriptor_one_of(),
                    vec![value.state.into_abstract_state_one_of()],
                )
            }
        }

        impl From<$struct_name> for MdibSingleStateTuple {
            fn from(value: $struct_name) -> Self {
                (
                    value.descriptor.into_abstract_descriptor_one_of(),
                    value.state.into_abstract_state_one_of(),
                )
            }
        }
    };
}

macro_rules! generate_mdib_pair_multi_state {
    ($struct_name:ident) => {
        impl MdibPair for $struct_name {
            fn handle(&self) -> String {
                self.descriptor.handle()
            }

            fn version(&self) -> MdibPairVersion {
                MdibPairVersion {
                    descriptor_version: self.descriptor.version(),
                    state_version: MdibPairStateVersion::MultiStateVersion(
                        self.states
                            .iter()
                            .map(|state| (state.state_handle(), state.version()))
                            .collect(),
                    ),
                }
            }
        }

        impl From<$struct_name> for MdibPairTuple {
            fn from(value: $struct_name) -> Self {
                (
                    value.descriptor.into_abstract_descriptor_one_of(),
                    value
                        .states
                        .into_iter()
                        .map(|it| it.into_abstract_state_one_of())
                        .collect(),
                )
            }
        }
    };
}

macro_rules! generate_pair_single_state {
    ($struct_name:ident, $descriptor_type:ident, $state_type: ident) => {
        paste! {
            #[allow(dead_code)]
            #[derive(Clone, Debug)]
            pub struct [<$struct_name Pair>] {
                pub descriptor: $descriptor_type,
                pub state: $state_type,
            }

            generate_mdib_pair_single_state!([<$struct_name Pair>]);
            generate_from_single_state!([<$struct_name Pair>], $descriptor_type, $state_type);
        }
    };
}

macro_rules! generate_pair_multi_state {
    ($struct_name:ident, $descriptor_type:ident, $state_type: ident) => {
        paste! {
            #[allow(dead_code)]
            #[derive(Clone, Debug)]
            pub struct [<$struct_name Pair>] {
                pub descriptor: $descriptor_type,
                pub states: Vec<$state_type>,
            }

            generate_mdib_pair_multi_state!([<$struct_name Pair>]);
            generate_from_multi_state!([<$struct_name Pair>], $descriptor_type, $state_type);
        }
    };
}

macro_rules! generate_pairs_single_state {
    { $($field:ident),* } => {
        $(
             paste! {
                generate_pair_single_state!($field, [<$field Descriptor>], [<$field State>]);
            }
        )*
    }
}

macro_rules! generate_pairs_multi_state {
    { $($field:ident),* } => {
        $(
             paste! {
                generate_pair_multi_state!($field, [<$field Descriptor>], [<$field State>]);
            }
        )*
    }
}

generate_pairs_single_state!(
    // metrics
    StringMetric,
    EnumStringMetric,
    NumericMetric,
    RealTimeSampleArrayMetric,
    DistributionSampleArrayMetric,
    // alerts
    AlertCondition,
    LimitAlertCondition,
    AlertSignal,
    AlertSystem,
    // components
    Mds,
    Vmd,
    Channel,
    Sco,
    SystemContext,
    Battery,
    Clock,
    // operations
    SetValueOperation,
    SetStringOperation,
    SetAlertStateOperation,
    SetMetricStateOperation,
    SetComponentStateOperation,
    SetContextStateOperation,
    ActivateOperation
);

// contexts
generate_pairs_multi_state!(
    PatientContext,
    LocationContext,
    EnsembleContext,
    WorkflowContext,
    MeansContext,
    OperatorContext
);

impl From<Pair> for MdibPairTuple {
    fn from(value: Pair) -> Self {
        match value {
            Pair::SingleStatePair(x) => x.into(),
            Pair::MultiStatePair(x) => x.into(),
        }
    }
}

#[delegate_to_remote_methods]
#[delegate(MdibPair, target_ref = "deref")]
impl<M: ?Sized + MdibPair> MdibPair for Box<M> {
    fn deref(&self) -> &M;
}

/// Represents a descriptor-state pair.
#[derive(Delegate, From, Clone, Debug)]
#[delegate(MdibPair)]
#[allow(dead_code)]
pub enum Pair {
    SingleStatePair(SingleStatePair),
    MultiStatePair(MultiStatePair),
}

// generates the enum for all single state pair variants
macro_rules! generate_boxed_pairs_single {
    ($($name:ident),*) => {
        paste! {
            /// Represents a descriptor-state pair with a single state.
            #[derive(Delegate, From, Clone, Debug)]
            #[delegate(MdibPair)]
            #[allow(dead_code)]
            pub enum SingleStatePair {
                $(
                    #[from(types([<$name Pair>]))]
                    $name(Box<[<$name Pair>]>),
                )*
            }
        }
    }
}

single_states!(generate_boxed_pairs_single);

// generates the enum for all multi state pair variants
macro_rules! generate_boxed_pairs_multi {
    ($($name:ident),*) => {
        paste! {
            /// Represents a descriptor-state pair with n states.
            #[derive(Delegate, From, Clone, Debug)]
            #[delegate(MdibPair)]
            #[allow(dead_code)]
            pub enum MultiStatePair {
                $(
                    #[from(types([<$name Pair>]))]
                    $name(Box<[<$name Pair>]>),
                )*
            }
        }
    }
}

multi_states!(generate_boxed_pairs_multi);

impl TryFrom<Pair> for SingleStatePair {
    type Error = PairError;

    fn try_from(value: Pair) -> Result<Self, Self::Error> {
        match value {
            Pair::SingleStatePair(x) => Ok(x),
            Pair::MultiStatePair(_) => Err(Self::Error::TypeMismatch),
        }
    }
}

impl TryFrom<Pair> for MultiStatePair {
    type Error = PairError;

    fn try_from(value: Pair) -> Result<Self, Self::Error> {
        match value {
            Pair::SingleStatePair(_) => Err(Self::Error::TypeMismatch),
            Pair::MultiStatePair(x) => Ok(x),
        }
    }
}

/// generates implementations for SingleStatePair Into [MdibPairTuple] and [MdibSingleStateTuple]
macro_rules! generate_into_mdib_pair_tuple_single_state {
    ($($name:ident),*) => {
        paste! {
            impl From<SingleStatePair> for MdibPairTuple {
                fn from(value: SingleStatePair) -> Self {
                    match value {
                    $(
                        SingleStatePair::$name(x) => (*x).into(),
                    )*
                    }
                }
            }

            impl From<SingleStatePair> for MdibSingleStateTuple {
                fn from(value: SingleStatePair) -> Self {
                    match value {
                    $(
                        SingleStatePair::$name(x) => (*x).into(),
                    )*
                    }
                }

            }
        }
    }
}

single_states!(generate_into_mdib_pair_tuple_single_state);

impl From<MultiStatePair> for MdibPairTuple {
    fn from(value: MultiStatePair) -> Self {
        match value {
            MultiStatePair::PatientContext(x) => (*x).into(),
            MultiStatePair::LocationContext(x) => (*x).into(),
            MultiStatePair::EnsembleContext(x) => (*x).into(),
            MultiStatePair::WorkflowContext(x) => (*x).into(),
            MultiStatePair::MeansContext(x) => (*x).into(),
            MultiStatePair::OperatorContext(x) => (*x).into(),
        }
    }
}

macro_rules! generate_try_from_match_single_combined {
    { $($single_field:ident),*; and $($multi_field:ident),* } => {
        fn try_from(value: MdibSingleStateTuple) -> Result<Self, Self::Error> {
            paste!{
                match value {
                    $(
                        (AbstractDescriptorOneOf::[<$single_field Descriptor>](descriptor), AbstractStateOneOf::[<$single_field State>](state)) => Ok(Pair::SingleStatePair(SingleStatePair::$single_field(Box::new((descriptor, state).into())))),
                    )*
                    $(
                        (AbstractDescriptorOneOf::[<$multi_field Descriptor>](descriptor), AbstractStateOneOf::[<$multi_field State>](state)) => Ok(Pair::MultiStatePair(MultiStatePair::$multi_field(Box::new((descriptor, state).into())))),
                    )*
                    _ => Err(PairError::TypeMismatch)
                }
            }
        }
    }
}

macro_rules! generate_try_from_match_single_tuple_single_state {
    { $($field:ident),* } => {
        fn try_from(value: MdibSingleStateTuple) -> Result<Self, Self::Error> {
            paste!{
                match value {
                    $(
                        (AbstractDescriptorOneOf::[<$field Descriptor>](descriptor), AbstractStateOneOf::[<$field State>](state)) => Ok(SingleStatePair::$field(Box::new((descriptor, state).into()))),
                    )*
                    _ => Err(PairError::TypeMismatch)
                }
            }
        }
    }
}

macro_rules! generate_try_from_match_single_tuple_multi_state {
    { $($field:ident),* } => {
        fn try_from(value: MdibSingleStateTuple) -> Result<Self, Self::Error> {
            paste!{
                match value {
                    $(
                        (AbstractDescriptorOneOf::[<$field Descriptor>](descriptor), AbstractStateOneOf::[<$field State>](state)) => Ok(MultiStatePair::$field(Box::new((descriptor, state).into()))),
                    )*
                    _ => Err(PairError::TypeMismatch)
                }
            }
        }
    }
}

macro_rules! generate_try_from_match_pair {
    { $($single_field:ident),*; and $($multi_field:ident),* } => {
        fn try_from(value: MdibPairTuple) -> Result<Self, Self::Error> {
            paste! {
                match value {
                    $(
                        (AbstractDescriptorOneOf::[<$single_field Descriptor>](descriptor), mut states) => {
                            if states.len() != 1 {
                                Err(PairError::TooManyStates { handle: descriptor.handle()})
                            } else {
                                let state = states.pop().unwrap();
                                match state {
                                    AbstractStateOneOf::[<$single_field State>](state) => Ok(Pair::SingleStatePair(SingleStatePair::$single_field(Box::new((descriptor, state).into())))),
                                    _ => Err(PairError::TypeMismatch)
                                }
                            }
                        },
                    )*

                    $(
                        (AbstractDescriptorOneOf::[<$multi_field Descriptor>](descriptor), states) => {
                            let converted: Vec<[<$multi_field State>]> = states.into_iter().map(|it| match it {
                                AbstractStateOneOf::[<$multi_field State>](state) => Ok(state),
                                _ => Err(PairError::TypeMismatch)
                            }).collect::<Result<Vec<_>, PairError>>()?;
                            Ok(Pair::MultiStatePair(MultiStatePair::$multi_field(Box::new((descriptor, converted).into()))))
                        },
                    )*
                    _ => Err(PairError::TypeMismatch)
                }
            }
        }
    }
}

macro_rules! generate_try_from_match_single_state_pair {
    { $($single_field:ident),* } => {
        fn try_from(value: MdibPairTuple) -> Result<Self, Self::Error> {
            paste!{
                match value {
                    $(
                        (AbstractDescriptorOneOf::[<$single_field Descriptor>](descriptor), mut states) => {
                            if states.len() != 1 {
                                Err(PairError::TooManyStates { handle: descriptor.handle()})
                            } else {
                                let state = states.pop().unwrap();
                                match state {
                                    AbstractStateOneOf::[<$single_field State>](state) => Ok(SingleStatePair::$single_field(Box::new((descriptor, state).into()))),
                                    _ => Err(PairError::TypeMismatch)
                                }
                            }
                        },
                    )*

                    _ => Err(PairError::TypeMismatch)
                }
            }
        }
    }
}

macro_rules! generate_try_from_match_multi_state_pair {
    { $d:tt $value:ident, $macro_call:ident, $inner_name:ident } => {
        macro_rules! $inner_name {
            { $d($d multi_field:ident),* } => {
                paste! {
                    match $value {
                        $d(
                            (AbstractDescriptorOneOf::[<$d multi_field Descriptor>](descriptor), states) => {
                                let converted: Vec<[<$d multi_field State>]> = states.into_iter().map(|it| match it {
                                    AbstractStateOneOf::[<$d multi_field State>](state) => Ok(state),
                                    _ => Err(PairError::TypeMismatch)
                                }).collect::<Result<Vec<_>, PairError>>()?;
                                Ok(MultiStatePair::$d multi_field(Box::new((descriptor, converted).into())))
                            },
                        )*
                        _ => Err(PairError::TypeMismatch)
                    }
                }
            }
        }
        $macro_call!{ $inner_name }
    }
}

impl TryFrom<MdibPairTuple> for Pair {
    type Error = PairError;

    all_pairs!(generate_try_from_match_pair);
}

impl TryFrom<MdibPairTuple> for SingleStatePair {
    type Error = PairError;

    single_states!(generate_try_from_match_single_state_pair);
}

impl TryFrom<MdibPairTuple> for MultiStatePair {
    type Error = PairError;

    fn try_from(value: MdibPairTuple) -> Result<Self, Self::Error> {
        generate_try_from_match_multi_state_pair! { $ value, multi_states, inner_macro }
    }
}

impl TryFrom<MdibSingleStateTuple> for Pair {
    type Error = PairError;

    all_pairs!(generate_try_from_match_single_combined);
}

impl TryFrom<MdibSingleStateTuple> for MultiStatePair {
    type Error = PairError;

    multi_states!(generate_try_from_match_single_tuple_multi_state);
}

impl TryFrom<MdibSingleStateTuple> for SingleStatePair {
    type Error = PairError;

    single_states!(generate_try_from_match_single_tuple_single_state);
}

macro_rules! generate_from_single_state_pair_pairs {
    { $($source:ident),* } => {
        paste! {
            $(
                impl From<([<$source Descriptor>], [<$source State>])> for Pair {
                    fn from(value: ([<$source Descriptor>], [<$source State>])) -> Self {
                        Self::SingleStatePair(SingleStatePair::$source(Box::new([<$source Pair>] { descriptor: value.0, state: value.1 })))
                    }
                }
            )*
        }
    }
}

macro_rules! generate_from_multi_state_pair_pairs {
    { $($source:ident),* } => {
        paste! {
            $(
                impl From<([<$source Descriptor>], [<$source State>])> for Pair {
                    fn from(value: ([<$source Descriptor>], [<$source State>])) -> Self {
                        Self::MultiStatePair(MultiStatePair::$source(Box::new([<$source Pair>] { descriptor: value.0, states: vec![value.1] })))
                    }
                }

                impl From<([<$source Descriptor>], Vec<[<$source State>]>)> for Pair {
                    fn from(value: ([<$source Descriptor>], Vec<[<$source State>]>)) -> Self {
                        Self::MultiStatePair(MultiStatePair::$source(Box::new([<$source Pair>] { descriptor: value.0, states: value.1 })))
                    }
                }
            )*
        }
    }
}

single_states!(generate_from_single_state_pair_pairs);
multi_states!(generate_from_multi_state_pair_pairs);
