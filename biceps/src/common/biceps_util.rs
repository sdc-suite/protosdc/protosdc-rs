use ambassador::delegatable_trait;

use crate::common::mdib_pair::Pair;
use protosdc_biceps::biceps::{
    AbstractAlertDescriptor, AbstractAlertState, AbstractAlertStateOneOf,
    AbstractComplexDeviceComponentDescriptor, AbstractComplexDeviceComponentState,
    AbstractComplexDeviceComponentStateOneOf, AbstractContextDescriptor,
    AbstractContextDescriptorOneOf, AbstractContextState, AbstractContextStateOneOf,
    AbstractDescriptor, AbstractDescriptorOneOf, AbstractDeviceComponentDescriptor,
    AbstractDeviceComponentState, AbstractDeviceComponentStateOneOf, AbstractMetricDescriptor,
    AbstractMetricDescriptorOneOf, AbstractMetricState, AbstractMetricStateOneOf,
    AbstractMultiState, AbstractOperationDescriptor, AbstractOperationDescriptorOneOf,
    AbstractOperationState, AbstractOperationStateOneOf, AbstractSetOneOf,
    AbstractSetResponseOneOf, AbstractSetStateOperationDescriptor, AbstractState,
    AbstractStateOneOf, Activate, ActivateOperationDescriptor, ActivateOperationState,
    ActivateResponse, AlertConditionDescriptor, AlertConditionDescriptorOneOf, AlertConditionState,
    AlertSignalDescriptor, AlertSignalState, AlertSystemDescriptor, AlertSystemState,
    BatteryDescriptor, BatteryState, ChannelDescriptor, ChannelState, ClockDescriptor, ClockState,
    ContextAssociation, DistributionSampleArrayMetricDescriptor,
    DistributionSampleArrayMetricState, EnsembleContextDescriptor, EnsembleContextState,
    EnumStringMetricDescriptor, EnumStringMetricState, LimitAlertConditionDescriptor,
    LimitAlertConditionState, LocationContextDescriptor, LocationContextState, MdsDescriptor,
    MdsState, MeansContextDescriptor, MeansContextState, NumericMetricDescriptor,
    NumericMetricState, OperatorContextDescriptor, OperatorContextState, PatientContextDescriptor,
    PatientContextState, RealTimeSampleArrayMetricDescriptor, RealTimeSampleArrayMetricState,
    ReferencedVersion, ScoDescriptor, ScoState, SetAlertState, SetAlertStateOperationDescriptor,
    SetAlertStateOperationState, SetAlertStateResponse, SetComponentState,
    SetComponentStateOperationDescriptor, SetComponentStateOperationState,
    SetComponentStateResponse, SetContextState, SetContextStateOperationDescriptor,
    SetContextStateOperationState, SetContextStateResponse, SetMetricState,
    SetMetricStateOperationDescriptor, SetMetricStateOperationState, SetMetricStateResponse,
    SetString, SetStringOperationDescriptor, SetStringOperationState, SetStringResponse, SetValue,
    SetValueOperationDescriptor, SetValueOperationState, SetValueResponse, StringMetricDescriptor,
    StringMetricState, SystemContextDescriptor, SystemContextState, VersionCounter, VmdDescriptor,
    VmdState, WorkflowContextDescriptor, WorkflowContextState,
};

/// Descriptor which can be copied and its version updated.
#[delegatable_trait]
pub trait CopyableDescriptor {
    /// Creates a copy of [Self] in which the descriptor version was updated.
    fn replace_in_copy(&self, version: u64) -> Self;
}

/// Descriptor which can be copied and its versions updated.
#[delegatable_trait]
pub trait CopyableState {
    /// Creates a copy of [Self] in which the descriptor and state versions are updated.
    fn replace_in_copy(&self, descriptor_version: u64, state_version: u64) -> Self;

    /// Consumes [Self] and updates the descriptor and state version.
    fn replace(self, descriptor_version: u64, state_version: u64) -> Self;
}

/// Basic descriptor functionality.
#[delegatable_trait]
pub trait Descriptor {
    /// Returns the handle of the descriptor
    fn handle(&self) -> String;
    /// Returns the version of the descriptor.
    fn version(&self) -> u64;

    fn into_abstract_descriptor_one_of(self) -> AbstractDescriptorOneOf;
}

/// Basic state functionality.
#[delegatable_trait]
pub trait State {
    /// Returns the handle of the descriptor
    fn descriptor_handle(&self) -> String;
    /// Returns the version of the state.
    fn version(&self) -> u64;
    /// Returns the version of the descriptor for the state.
    fn descriptor_version(&self) -> u64;

    fn into_abstract_state_one_of(self) -> AbstractStateOneOf;
}

/// Basic multi-state functionality.
#[delegatable_trait]
pub trait MultiState {
    /// Returns the handle of the multi-state.
    fn state_handle(&self) -> String;
}

/// Basic context-state functionality.
#[delegatable_trait]
pub trait ContextState: MultiState {
    /// Returns the context association of the state.
    ///
    /// None if no association is set.
    fn context_association(&self) -> Option<ContextAssociation>;
    fn into_abstract_context_state_one_of(self) -> AbstractContextStateOneOf;
}

/// Basic operation descriptor functionality.
#[delegatable_trait]
pub trait OperationDescriptor {
    fn into_abstract_operation_descriptor_one_of(self) -> AbstractOperationDescriptorOneOf;
}

/// Basic metric descriptor functionality.
#[delegatable_trait]
pub trait MetricDescriptor {
    fn into_abstract_metric_descriptor_one_of(self) -> AbstractMetricDescriptorOneOf;
}

/// Basic metric state functionality.
#[delegatable_trait]
pub trait MetricState {
    fn into_abstract_metric_state_one_of(self) -> AbstractMetricStateOneOf;
}

/// Basic component state functionality.
#[delegatable_trait]
pub trait ComponentState {
    fn into_abstract_component_state_one_of(self) -> AbstractDeviceComponentStateOneOf;
}

/// Basic operation state functionality.
#[delegatable_trait]
pub trait OperationState {
    fn into_abstract_operation_state_one_of(self) -> AbstractOperationStateOneOf;
}

/// Basic alert state functionality.
#[delegatable_trait]
pub trait AlertState {
    fn into_abstract_alert_state_one_of(self) -> AbstractAlertStateOneOf;
}

/// Basic alert state functionality.
#[delegatable_trait]
pub trait SetRequest {
    fn into_abstract_set_one_of(self) -> AbstractSetOneOf;
}

/// Basic alert state functionality.
#[delegatable_trait]
pub trait SetResponse {
    fn into_abstract_set_response_one_of(self) -> AbstractSetResponseOneOf;
}

/// Generates an implementation of the [Descriptor] trait.
/// # Arguments
///
/// * `struct_name`: name of the struct to generate the trait for
/// * `field_name`: path to access the extended base descriptor type,
///                 e.g. [AbstractMetricDescriptor] for metrics
macro_rules! generate_descriptor {
    ($struct_name:ident; and $field_name:ident) => {
        impl Descriptor for $struct_name {
            fn handle(&self) -> String {
                self.$field_name.handle()
            }

            fn version(&self) -> u64 {
                self.$field_name.version()
            }

            fn into_abstract_descriptor_one_of(self) -> AbstractDescriptorOneOf {
                AbstractDescriptorOneOf::$struct_name(self)
            }
        }
    };
}

/// Generates an implementation of the [OperationDescriptor] trait.
macro_rules! generate_operation_descriptor {
    ($struct_name:ident) => {
        impl OperationDescriptor for $struct_name {
            fn into_abstract_operation_descriptor_one_of(self) -> AbstractOperationDescriptorOneOf {
                AbstractOperationDescriptorOneOf::$struct_name(self)
            }
        }
    };
}

/// Generates an implementation of the [MetricDescriptor] trait.
macro_rules! generate_metric_descriptor {
    ($struct_name:ident) => {
        impl MetricDescriptor for $struct_name {
            fn into_abstract_metric_descriptor_one_of(self) -> AbstractMetricDescriptorOneOf {
                AbstractMetricDescriptorOneOf::$struct_name(self)
            }
        }
    };
}

/// Generates an implementation of the [State] trait.
/// # Arguments
///
/// * `struct_name`: name of the struct to generate the trait for
/// * `field_name`: path to access the extended base state type,
///                 e.g. [AbstractMetricState] for metrics
macro_rules! generate_state {
    ($struct_name:ident; and $field_name:ident) => {
        impl State for $struct_name {
            fn descriptor_handle(&self) -> String {
                self.$field_name.descriptor_handle()
            }

            fn version(&self) -> u64 {
                self.$field_name.version()
            }

            fn descriptor_version(&self) -> u64 {
                self.$field_name.descriptor_version()
            }

            fn into_abstract_state_one_of(self) -> AbstractStateOneOf {
                AbstractStateOneOf::$struct_name(self)
            }
        }
    };
}

/// Generates an implementation of the [MultiState] trait.
///
/// # Arguments
///
/// * `struct_name`: name of the struct to generate the trait for
/// * `field_name`: path to access the extended base state type,
///                 e.g. [AbstractMetricState] for metrics
macro_rules! generate_multi_state {
    ($struct_name:ident; and $field_name:ident) => {
        impl MultiState for $struct_name {
            fn state_handle(&self) -> String {
                self.$field_name.state_handle()
            }
        }
    };
}

/// Generates an implementation of the [ContextState] trait.
macro_rules! generate_context_state {
    ($struct_name:ident; and $field_name:ident) => {
        impl ContextState for $struct_name {
            fn context_association(&self) -> Option<ContextAssociation> {
                self.$field_name.context_association()
            }

            fn into_abstract_context_state_one_of(self) -> AbstractContextStateOneOf {
                AbstractContextStateOneOf::$struct_name(self)
            }
        }
    };
}

/// Generates an implementation of the [MetricState] trait.
macro_rules! generate_metric_state {
    ($struct_name:ident) => {
        impl MetricState for $struct_name {
            fn into_abstract_metric_state_one_of(self) -> AbstractMetricStateOneOf {
                AbstractMetricStateOneOf::$struct_name(self)
            }
        }
    };
}

/// Generates an implementation of the [AlertState] trait.
macro_rules! generate_alert_state {
    ($struct_name:ident) => {
        impl AlertState for $struct_name {
            fn into_abstract_alert_state_one_of(self) -> AbstractAlertStateOneOf {
                AbstractAlertStateOneOf::$struct_name(self)
            }
        }
    };
}

/// Generates an implementation of the [ComponentState] trait.
macro_rules! generate_component_state {
    ($struct_name:ident) => {
        impl ComponentState for $struct_name {
            fn into_abstract_component_state_one_of(self) -> AbstractDeviceComponentStateOneOf {
                AbstractDeviceComponentStateOneOf::$struct_name(self)
            }
        }
    };
}

/// Generates an implementation of the [OperationState] trait.
macro_rules! generate_operation_state {
    ($struct_name:ident) => {
        impl OperationState for $struct_name {
            fn into_abstract_operation_state_one_of(self) -> AbstractOperationStateOneOf {
                AbstractOperationStateOneOf::$struct_name(self)
            }
        }
    };
}

/// Generates trait implementations for [AbstractSetOneOf] types.
macro_rules! generate_abstract_set {
    ($struct_name:ident) => {
        impl SetRequest for $struct_name {
            fn into_abstract_set_one_of(self) -> AbstractSetOneOf {
                AbstractSetOneOf::$struct_name(self)
            }
        }
    };
}

/// Generates trait implementations for [AbstractSetResponseOneOf] types.
macro_rules! generate_abstract_set_response {
    ($struct_name:ident) => {
        impl SetResponse for $struct_name {
            fn into_abstract_set_response_one_of(self) -> AbstractSetResponseOneOf {
                AbstractSetResponseOneOf::$struct_name(self)
            }
        }
    };
}

/// Generates an implementation of the [CopyableDescriptor] trait.
///
/// # Arguments
///
/// * `struct_name`: name of the struct to generate the trait for
/// * `field_path`: path to access the [AbstractDescriptor] field
macro_rules! generate_copyable_descriptor {
    ($struct_name:ident, $($field_path:ident ).+) => {
impl CopyableDescriptor for $struct_name {
    fn replace_in_copy(&self, version: u64) -> Self {
        let mut desc = self.clone();
        desc.$($field_path).+ = desc.$($field_path).+.replace_in_copy(version);
        desc
    }
}
    }
}

/// Generates an implementation of the [CopyableState] trait.
///
/// # Arguments
///
/// * `struct_name`: name of the struct to generate the trait for
/// * `field_path`: path to access the [AbstractState] field
macro_rules! generate_copyable_state {
    ($struct_name:ident, $($field_path:ident ).+) => {
impl CopyableState for $struct_name {
    fn replace_in_copy(&self, descriptor_version: u64, state_version: u64) -> Self {
        let mut state = self.clone();
        state.$($field_path).+ = state.$($field_path).+.replace_in_copy(descriptor_version, state_version);
        state
    }

    fn replace(self, descriptor_version: u64, state_version: u64) -> Self {
        let mut state = Self { ..self };
        state.$($field_path).+ = state.$($field_path).+.replace(descriptor_version, state_version);
        state
    }
}
    }
}

// generate all the trait implementations applicable

// TODO: I'd like to iterate this somehow
// metrics
generate_state!(StringMetricState; and abstract_metric_state);
generate_copyable_state!(StringMetricState, abstract_metric_state.abstract_state);
generate_metric_state!(StringMetricState);
generate_descriptor!(StringMetricDescriptor; and abstract_metric_descriptor);
generate_copyable_descriptor!(
    StringMetricDescriptor,
    abstract_metric_descriptor.abstract_descriptor
);
generate_metric_descriptor!(StringMetricDescriptor);

generate_state!(EnumStringMetricState; and string_metric_state);
generate_copyable_state!(
    EnumStringMetricState,
    string_metric_state.abstract_metric_state.abstract_state
);
generate_metric_state!(EnumStringMetricState);
generate_descriptor!(EnumStringMetricDescriptor; and string_metric_descriptor);
generate_copyable_descriptor!(
    EnumStringMetricDescriptor,
    string_metric_descriptor
        .abstract_metric_descriptor
        .abstract_descriptor
);
generate_metric_descriptor!(EnumStringMetricDescriptor);

generate_state!(NumericMetricState; and abstract_metric_state);
generate_copyable_state!(NumericMetricState, abstract_metric_state.abstract_state);
generate_metric_state!(NumericMetricState);
generate_descriptor!(NumericMetricDescriptor; and abstract_metric_descriptor);
generate_copyable_descriptor!(
    NumericMetricDescriptor,
    abstract_metric_descriptor.abstract_descriptor
);
generate_metric_descriptor!(NumericMetricDescriptor);

generate_state!(RealTimeSampleArrayMetricState; and abstract_metric_state);
generate_copyable_state!(
    RealTimeSampleArrayMetricState,
    abstract_metric_state.abstract_state
);
generate_metric_state!(RealTimeSampleArrayMetricState);
generate_descriptor!(RealTimeSampleArrayMetricDescriptor; and abstract_metric_descriptor);
generate_copyable_descriptor!(
    RealTimeSampleArrayMetricDescriptor,
    abstract_metric_descriptor.abstract_descriptor
);
generate_metric_descriptor!(RealTimeSampleArrayMetricDescriptor);

generate_state!(DistributionSampleArrayMetricState; and abstract_metric_state);
generate_copyable_state!(
    DistributionSampleArrayMetricState,
    abstract_metric_state.abstract_state
);
generate_metric_state!(DistributionSampleArrayMetricState);
generate_descriptor!(DistributionSampleArrayMetricDescriptor; and abstract_metric_descriptor);
generate_copyable_descriptor!(
    DistributionSampleArrayMetricDescriptor,
    abstract_metric_descriptor.abstract_descriptor
);
generate_metric_descriptor!(DistributionSampleArrayMetricDescriptor);

generate_state!(AbstractMetricState; and abstract_state);
generate_copyable_state!(AbstractMetricState, abstract_state);
generate_metric_state!(AbstractMetricState);
generate_descriptor!(AbstractMetricDescriptor; and abstract_descriptor);
generate_copyable_descriptor!(AbstractMetricDescriptor, abstract_descriptor);
generate_metric_descriptor!(AbstractMetricDescriptor);

// alerts
generate_state!(AlertSystemState; and abstract_alert_state);
generate_alert_state!(AlertSystemState);
generate_copyable_state!(AlertSystemState, abstract_alert_state.abstract_state);
generate_descriptor!(AlertSystemDescriptor; and abstract_alert_descriptor);
generate_copyable_descriptor!(
    AlertSystemDescriptor,
    abstract_alert_descriptor.abstract_descriptor
);

generate_state!(AlertConditionState; and abstract_alert_state);
generate_alert_state!(AlertConditionState);
generate_copyable_state!(AlertConditionState, abstract_alert_state.abstract_state);
generate_descriptor!(AlertConditionDescriptor; and abstract_alert_descriptor);
generate_copyable_descriptor!(
    AlertConditionDescriptor,
    abstract_alert_descriptor.abstract_descriptor
);

generate_state!(LimitAlertConditionState; and alert_condition_state);
generate_alert_state!(LimitAlertConditionState);
generate_copyable_state!(
    LimitAlertConditionState,
    alert_condition_state.abstract_alert_state.abstract_state
);
generate_descriptor!(LimitAlertConditionDescriptor; and alert_condition_descriptor);
generate_copyable_descriptor!(
    LimitAlertConditionDescriptor,
    alert_condition_descriptor
        .abstract_alert_descriptor
        .abstract_descriptor
);

generate_state!(AlertSignalState; and abstract_alert_state);
generate_alert_state!(AlertSignalState);
generate_copyable_state!(AlertSignalState, abstract_alert_state.abstract_state);
generate_descriptor!(AlertSignalDescriptor; and abstract_alert_descriptor);
generate_copyable_descriptor!(
    AlertSignalDescriptor,
    abstract_alert_descriptor.abstract_descriptor
);

generate_state!(AbstractAlertState; and abstract_state);
generate_alert_state!(AbstractAlertState);
generate_copyable_state!(AbstractAlertState, abstract_state);
generate_descriptor!(AbstractAlertDescriptor; and abstract_descriptor);
generate_copyable_descriptor!(AbstractAlertDescriptor, abstract_descriptor);

// components
generate_state!(MdsState; and abstract_complex_device_component_state);
generate_component_state!(MdsState);
generate_copyable_state!(
    MdsState,
    abstract_complex_device_component_state
        .abstract_device_component_state
        .abstract_state
);
generate_descriptor!(MdsDescriptor; and abstract_complex_device_component_descriptor);
generate_copyable_descriptor!(
    MdsDescriptor,
    abstract_complex_device_component_descriptor
        .abstract_device_component_descriptor
        .abstract_descriptor
);

generate_state!(VmdState; and abstract_complex_device_component_state);
generate_component_state!(VmdState);
generate_copyable_state!(
    VmdState,
    abstract_complex_device_component_state
        .abstract_device_component_state
        .abstract_state
);
generate_descriptor!(VmdDescriptor; and abstract_complex_device_component_descriptor);
generate_copyable_descriptor!(
    VmdDescriptor,
    abstract_complex_device_component_descriptor
        .abstract_device_component_descriptor
        .abstract_descriptor
);

generate_state!(ChannelState; and abstract_device_component_state);
generate_component_state!(ChannelState);
generate_copyable_state!(ChannelState, abstract_device_component_state.abstract_state);
generate_descriptor!(ChannelDescriptor; and abstract_device_component_descriptor);
generate_copyable_descriptor!(
    ChannelDescriptor,
    abstract_device_component_descriptor.abstract_descriptor
);

generate_state!(ScoState; and abstract_device_component_state);
generate_component_state!(ScoState);
generate_copyable_state!(ScoState, abstract_device_component_state.abstract_state);
generate_descriptor!(ScoDescriptor; and abstract_device_component_descriptor);
generate_copyable_descriptor!(
    ScoDescriptor,
    abstract_device_component_descriptor.abstract_descriptor
);

generate_state!(BatteryState; and abstract_device_component_state);
generate_component_state!(BatteryState);
generate_copyable_state!(BatteryState, abstract_device_component_state.abstract_state);
generate_descriptor!(BatteryDescriptor; and abstract_device_component_descriptor);
generate_copyable_descriptor!(
    BatteryDescriptor,
    abstract_device_component_descriptor.abstract_descriptor
);

generate_state!(ClockState; and abstract_device_component_state);
generate_component_state!(ClockState);
generate_copyable_state!(ClockState, abstract_device_component_state.abstract_state);
generate_descriptor!(ClockDescriptor; and abstract_device_component_descriptor);
generate_copyable_descriptor!(
    ClockDescriptor,
    abstract_device_component_descriptor.abstract_descriptor
);

generate_state!(SystemContextState; and abstract_device_component_state);
generate_component_state!(SystemContextState);
generate_copyable_state!(
    SystemContextState,
    abstract_device_component_state.abstract_state
);
generate_descriptor!(SystemContextDescriptor; and abstract_device_component_descriptor);
generate_copyable_descriptor!(
    SystemContextDescriptor,
    abstract_device_component_descriptor.abstract_descriptor
);

generate_state!(AbstractComplexDeviceComponentState; and abstract_device_component_state);
generate_component_state!(AbstractComplexDeviceComponentState);
generate_copyable_state!(
    AbstractComplexDeviceComponentState,
    abstract_device_component_state.abstract_state
);
generate_descriptor!(AbstractComplexDeviceComponentDescriptor; and abstract_device_component_descriptor);
generate_copyable_descriptor!(
    AbstractComplexDeviceComponentDescriptor,
    abstract_device_component_descriptor.abstract_descriptor
);

generate_state!(AbstractDeviceComponentState; and abstract_state);
generate_component_state!(AbstractDeviceComponentState);
generate_copyable_state!(AbstractDeviceComponentState, abstract_state);
generate_descriptor!(AbstractDeviceComponentDescriptor; and abstract_descriptor);
generate_copyable_descriptor!(AbstractDeviceComponentDescriptor, abstract_descriptor);

// operations
generate_state!(SetValueOperationState; and abstract_operation_state);
generate_operation_state!(SetValueOperationState);
generate_copyable_state!(
    SetValueOperationState,
    abstract_operation_state.abstract_state
);
generate_descriptor!(SetValueOperationDescriptor; and abstract_operation_descriptor);
generate_copyable_descriptor!(
    SetValueOperationDescriptor,
    abstract_operation_descriptor.abstract_descriptor
);
generate_operation_descriptor!(SetValueOperationDescriptor);

generate_state!(SetStringOperationState; and abstract_operation_state);
generate_operation_state!(SetStringOperationState);
generate_copyable_state!(
    SetStringOperationState,
    abstract_operation_state.abstract_state
);
generate_descriptor!(SetStringOperationDescriptor; and abstract_operation_descriptor);
generate_copyable_descriptor!(
    SetStringOperationDescriptor,
    abstract_operation_descriptor.abstract_descriptor
);
generate_operation_descriptor!(SetStringOperationDescriptor);

generate_state!(SetAlertStateOperationState; and abstract_operation_state);
generate_operation_state!(SetAlertStateOperationState);
generate_copyable_state!(
    SetAlertStateOperationState,
    abstract_operation_state.abstract_state
);
generate_descriptor!(SetAlertStateOperationDescriptor; and abstract_set_state_operation_descriptor);
generate_copyable_descriptor!(
    SetAlertStateOperationDescriptor,
    abstract_set_state_operation_descriptor
        .abstract_operation_descriptor
        .abstract_descriptor
);
generate_operation_descriptor!(SetAlertStateOperationDescriptor);

generate_state!(SetMetricStateOperationState; and abstract_operation_state);
generate_operation_state!(SetMetricStateOperationState);
generate_copyable_state!(
    SetMetricStateOperationState,
    abstract_operation_state.abstract_state
);
generate_descriptor!(SetMetricStateOperationDescriptor; and abstract_set_state_operation_descriptor);
generate_copyable_descriptor!(
    SetMetricStateOperationDescriptor,
    abstract_set_state_operation_descriptor
        .abstract_operation_descriptor
        .abstract_descriptor
);
generate_operation_descriptor!(SetMetricStateOperationDescriptor);

generate_state!(SetComponentStateOperationState; and abstract_operation_state);
generate_operation_state!(SetComponentStateOperationState);
generate_copyable_state!(
    SetComponentStateOperationState,
    abstract_operation_state.abstract_state
);
generate_descriptor!(SetComponentStateOperationDescriptor; and abstract_set_state_operation_descriptor);
generate_copyable_descriptor!(
    SetComponentStateOperationDescriptor,
    abstract_set_state_operation_descriptor
        .abstract_operation_descriptor
        .abstract_descriptor
);
generate_operation_descriptor!(SetComponentStateOperationDescriptor);

generate_state!(SetContextStateOperationState; and abstract_operation_state);
generate_operation_state!(SetContextStateOperationState);
generate_copyable_state!(
    SetContextStateOperationState,
    abstract_operation_state.abstract_state
);
generate_descriptor!(SetContextStateOperationDescriptor; and abstract_set_state_operation_descriptor);
generate_copyable_descriptor!(
    SetContextStateOperationDescriptor,
    abstract_set_state_operation_descriptor
        .abstract_operation_descriptor
        .abstract_descriptor
);
generate_operation_descriptor!(SetContextStateOperationDescriptor);

generate_state!(ActivateOperationState; and abstract_operation_state);
generate_operation_state!(ActivateOperationState);
generate_copyable_state!(
    ActivateOperationState,
    abstract_operation_state.abstract_state
);
generate_descriptor!(ActivateOperationDescriptor; and abstract_set_state_operation_descriptor);
generate_copyable_descriptor!(
    ActivateOperationDescriptor,
    abstract_set_state_operation_descriptor
        .abstract_operation_descriptor
        .abstract_descriptor
);
generate_operation_descriptor!(ActivateOperationDescriptor);

generate_operation_descriptor!(AbstractOperationDescriptor);

//generate_state!(AbstractSetStateOperationState; and abstract_operation_state);
generate_descriptor!(AbstractSetStateOperationDescriptor; and abstract_operation_descriptor);
generate_copyable_descriptor!(
    AbstractSetStateOperationDescriptor,
    abstract_operation_descriptor.abstract_descriptor
);

generate_state!(AbstractOperationState; and abstract_state);
generate_copyable_state!(AbstractOperationState, abstract_state);
generate_descriptor!(AbstractOperationDescriptor; and abstract_descriptor);
generate_copyable_descriptor!(AbstractOperationDescriptor, abstract_descriptor);

// contexts
generate_state!(PatientContextState; and abstract_context_state);
generate_multi_state!(PatientContextState; and abstract_context_state);
generate_context_state!(PatientContextState; and abstract_context_state);
generate_copyable_state!(
    PatientContextState,
    abstract_context_state.abstract_multi_state.abstract_state
);
generate_descriptor!(PatientContextDescriptor; and abstract_context_descriptor);
generate_copyable_descriptor!(
    PatientContextDescriptor,
    abstract_context_descriptor.abstract_descriptor
);

generate_state!(LocationContextState; and abstract_context_state);
generate_multi_state!(LocationContextState; and abstract_context_state);
generate_context_state!(LocationContextState; and abstract_context_state);
generate_copyable_state!(
    LocationContextState,
    abstract_context_state.abstract_multi_state.abstract_state
);
generate_descriptor!(LocationContextDescriptor; and abstract_context_descriptor);
generate_copyable_descriptor!(
    LocationContextDescriptor,
    abstract_context_descriptor.abstract_descriptor
);

generate_state!(EnsembleContextState; and abstract_context_state);
generate_multi_state!(EnsembleContextState; and abstract_context_state);
generate_context_state!(EnsembleContextState; and abstract_context_state);
generate_copyable_state!(
    EnsembleContextState,
    abstract_context_state.abstract_multi_state.abstract_state
);
generate_descriptor!(EnsembleContextDescriptor; and abstract_context_descriptor);
generate_copyable_descriptor!(
    EnsembleContextDescriptor,
    abstract_context_descriptor.abstract_descriptor
);

generate_state!(WorkflowContextState; and abstract_context_state);
generate_multi_state!(WorkflowContextState; and abstract_context_state);
generate_context_state!(WorkflowContextState; and abstract_context_state);
generate_copyable_state!(
    WorkflowContextState,
    abstract_context_state.abstract_multi_state.abstract_state
);
generate_descriptor!(WorkflowContextDescriptor; and abstract_context_descriptor);
generate_copyable_descriptor!(
    WorkflowContextDescriptor,
    abstract_context_descriptor.abstract_descriptor
);

generate_state!(MeansContextState; and abstract_context_state);
generate_multi_state!(MeansContextState; and abstract_context_state);
generate_context_state!(MeansContextState; and abstract_context_state);
generate_copyable_state!(
    MeansContextState,
    abstract_context_state.abstract_multi_state.abstract_state
);
generate_descriptor!(MeansContextDescriptor; and abstract_context_descriptor);
generate_copyable_descriptor!(
    MeansContextDescriptor,
    abstract_context_descriptor.abstract_descriptor
);

generate_state!(OperatorContextState; and abstract_context_state);
generate_multi_state!(OperatorContextState; and abstract_context_state);
generate_context_state!(OperatorContextState; and abstract_context_state);
generate_copyable_state!(
    OperatorContextState,
    abstract_context_state.abstract_multi_state.abstract_state
);
generate_descriptor!(OperatorContextDescriptor; and abstract_context_descriptor);
generate_copyable_descriptor!(
    OperatorContextDescriptor,
    abstract_context_descriptor.abstract_descriptor
);

generate_state!(AbstractContextState; and abstract_multi_state);
generate_multi_state!(AbstractContextState; and abstract_multi_state);
generate_copyable_state!(AbstractContextState, abstract_multi_state.abstract_state);
generate_descriptor!(AbstractContextDescriptor; and abstract_descriptor);
generate_copyable_descriptor!(AbstractContextDescriptor, abstract_descriptor);

generate_state!(AbstractMultiState; and abstract_state);
generate_copyable_state!(AbstractMultiState, abstract_state);

generate_abstract_set!(Activate);
generate_abstract_set_response!(ActivateResponse);
generate_abstract_set!(SetAlertState);
generate_abstract_set_response!(SetAlertStateResponse);
generate_abstract_set!(SetComponentState);
generate_abstract_set_response!(SetComponentStateResponse);
generate_abstract_set!(SetContextState);
generate_abstract_set_response!(SetContextStateResponse);
generate_abstract_set!(SetMetricState);
generate_abstract_set_response!(SetMetricStateResponse);
generate_abstract_set!(SetString);
generate_abstract_set_response!(SetStringResponse);
generate_abstract_set!(SetValue);
generate_abstract_set_response!(SetValueResponse);

// manual implementations of the traits for base types, i.e. [AbstractDescriptor], [AbstractState]
// and a bunch of the OneOf containers.

impl MultiState for AbstractMultiState {
    fn state_handle(&self) -> String {
        self.handle_attr.string.clone()
    }
}

impl ContextState for AbstractContextState {
    fn context_association(&self) -> Option<ContextAssociation> {
        self.context_association_attr.clone()
    }

    fn into_abstract_context_state_one_of(self) -> AbstractContextStateOneOf {
        AbstractContextStateOneOf::AbstractContextState(self)
    }
}

impl MultiState for AbstractContextStateOneOf {
    fn state_handle(&self) -> String {
        match self {
            AbstractContextStateOneOf::AbstractContextState(state) => state.state_handle(),
            AbstractContextStateOneOf::OperatorContextState(state) => state.state_handle(),
            AbstractContextStateOneOf::EnsembleContextState(state) => state.state_handle(),
            AbstractContextStateOneOf::WorkflowContextState(state) => state.state_handle(),
            AbstractContextStateOneOf::PatientContextState(state) => state.state_handle(),
            AbstractContextStateOneOf::LocationContextState(state) => state.state_handle(),
            AbstractContextStateOneOf::MeansContextState(state) => state.state_handle(),
        }
    }
}

impl ContextState for AbstractContextStateOneOf {
    fn context_association(&self) -> Option<ContextAssociation> {
        match self {
            AbstractContextStateOneOf::AbstractContextState(state) => state.context_association(),
            AbstractContextStateOneOf::OperatorContextState(state) => state.context_association(),
            AbstractContextStateOneOf::EnsembleContextState(state) => state.context_association(),
            AbstractContextStateOneOf::WorkflowContextState(state) => state.context_association(),
            AbstractContextStateOneOf::PatientContextState(state) => state.context_association(),
            AbstractContextStateOneOf::LocationContextState(state) => state.context_association(),
            AbstractContextStateOneOf::MeansContextState(state) => state.context_association(),
        }
    }

    fn into_abstract_context_state_one_of(self) -> AbstractContextStateOneOf {
        self
    }
}

impl State for AbstractState {
    fn descriptor_handle(&self) -> String {
        self.descriptor_handle_attr.string.clone()
    }

    fn version(&self) -> u64 {
        match &self.state_version_attr {
            None => crate::common::constants::IMPLIED_VERSION,
            Some(ver) => ver.unsigned_long,
        }
    }

    fn descriptor_version(&self) -> u64 {
        match &self.descriptor_version_attr {
            None => crate::common::constants::IMPLIED_VERSION,
            Some(ver) => ver.version_counter.unsigned_long,
        }
    }

    fn into_abstract_state_one_of(self) -> AbstractStateOneOf {
        AbstractStateOneOf::AbstractState(self)
    }
}

impl Descriptor for AbstractDescriptor {
    fn handle(&self) -> String {
        self.handle_attr.string.to_string()
    }

    fn version(&self) -> u64 {
        match &self.descriptor_version_attr {
            None => crate::common::constants::IMPLIED_VERSION,
            Some(ver) => ver.unsigned_long,
        }
    }

    fn into_abstract_descriptor_one_of(self) -> AbstractDescriptorOneOf {
        AbstractDescriptorOneOf::AbstractDescriptor(self)
    }
}

impl Descriptor for AbstractDescriptorOneOf {
    fn handle(&self) -> String {
        match self {
            AbstractDescriptorOneOf::AbstractDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::AbstractAlertDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::AlertConditionDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::LimitAlertConditionDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::AlertSignalDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::AlertSystemDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::AbstractDeviceComponentDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::ClockDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::BatteryDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::ChannelDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::ScoDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::SystemContextDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::AbstractComplexDeviceComponentDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::VmdDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::MdsDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::AbstractOperationDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::SetStringOperationDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::AbstractSetStateOperationDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::SetComponentStateOperationDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::SetAlertStateOperationDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::SetMetricStateOperationDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::SetContextStateOperationDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::ActivateOperationDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::SetValueOperationDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::AbstractMetricDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::NumericMetricDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::DistributionSampleArrayMetricDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::RealTimeSampleArrayMetricDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::StringMetricDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::EnumStringMetricDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::AbstractContextDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::MeansContextDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::WorkflowContextDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::PatientContextDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::OperatorContextDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::EnsembleContextDescriptor(x) => x.handle(),
            AbstractDescriptorOneOf::LocationContextDescriptor(x) => x.handle(),
        }
    }

    fn version(&self) -> u64 {
        match self {
            AbstractDescriptorOneOf::AbstractDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::AbstractAlertDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::AlertConditionDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::LimitAlertConditionDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::AlertSignalDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::AlertSystemDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::AbstractDeviceComponentDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::ClockDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::BatteryDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::ChannelDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::ScoDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::SystemContextDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::AbstractComplexDeviceComponentDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::VmdDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::MdsDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::AbstractOperationDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::SetStringOperationDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::AbstractSetStateOperationDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::SetComponentStateOperationDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::SetAlertStateOperationDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::SetMetricStateOperationDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::SetContextStateOperationDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::ActivateOperationDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::SetValueOperationDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::AbstractMetricDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::NumericMetricDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::DistributionSampleArrayMetricDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::RealTimeSampleArrayMetricDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::StringMetricDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::EnumStringMetricDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::AbstractContextDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::MeansContextDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::WorkflowContextDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::PatientContextDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::OperatorContextDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::EnsembleContextDescriptor(x) => x.version(),
            AbstractDescriptorOneOf::LocationContextDescriptor(x) => x.version(),
        }
    }

    fn into_abstract_descriptor_one_of(self) -> AbstractDescriptorOneOf {
        self
    }
}

impl Descriptor for AbstractContextDescriptorOneOf {
    fn handle(&self) -> String {
        match self {
            AbstractContextDescriptorOneOf::AbstractContextDescriptor(x) => x.handle(),
            AbstractContextDescriptorOneOf::MeansContextDescriptor(x) => x.handle(),
            AbstractContextDescriptorOneOf::WorkflowContextDescriptor(x) => x.handle(),
            AbstractContextDescriptorOneOf::PatientContextDescriptor(x) => x.handle(),
            AbstractContextDescriptorOneOf::OperatorContextDescriptor(x) => x.handle(),
            AbstractContextDescriptorOneOf::EnsembleContextDescriptor(x) => x.handle(),
            AbstractContextDescriptorOneOf::LocationContextDescriptor(x) => x.handle(),
        }
    }

    fn version(&self) -> u64 {
        match self {
            AbstractContextDescriptorOneOf::AbstractContextDescriptor(x) => x.version(),
            AbstractContextDescriptorOneOf::MeansContextDescriptor(x) => x.version(),
            AbstractContextDescriptorOneOf::WorkflowContextDescriptor(x) => x.version(),
            AbstractContextDescriptorOneOf::PatientContextDescriptor(x) => x.version(),
            AbstractContextDescriptorOneOf::OperatorContextDescriptor(x) => x.version(),
            AbstractContextDescriptorOneOf::EnsembleContextDescriptor(x) => x.version(),
            AbstractContextDescriptorOneOf::LocationContextDescriptor(x) => x.version(),
        }
    }

    fn into_abstract_descriptor_one_of(self) -> AbstractDescriptorOneOf {
        match self {
            AbstractContextDescriptorOneOf::AbstractContextDescriptor(desc) => {
                desc.into_abstract_descriptor_one_of()
            }
            AbstractContextDescriptorOneOf::MeansContextDescriptor(desc) => {
                desc.into_abstract_descriptor_one_of()
            }
            AbstractContextDescriptorOneOf::WorkflowContextDescriptor(desc) => {
                desc.into_abstract_descriptor_one_of()
            }
            AbstractContextDescriptorOneOf::PatientContextDescriptor(desc) => {
                desc.into_abstract_descriptor_one_of()
            }
            AbstractContextDescriptorOneOf::OperatorContextDescriptor(desc) => {
                desc.into_abstract_descriptor_one_of()
            }
            AbstractContextDescriptorOneOf::EnsembleContextDescriptor(desc) => {
                desc.into_abstract_descriptor_one_of()
            }
            AbstractContextDescriptorOneOf::LocationContextDescriptor(desc) => {
                desc.into_abstract_descriptor_one_of()
            }
        }
    }
}

impl Descriptor for AbstractOperationDescriptorOneOf {
    fn handle(&self) -> String {
        match self {
            AbstractOperationDescriptorOneOf::AbstractOperationDescriptor(x) => x.handle(),
            AbstractOperationDescriptorOneOf::SetStringOperationDescriptor(x) => x.handle(),
            AbstractOperationDescriptorOneOf::AbstractSetStateOperationDescriptor(x) => x.handle(),
            AbstractOperationDescriptorOneOf::SetComponentStateOperationDescriptor(x) => x.handle(),
            AbstractOperationDescriptorOneOf::SetAlertStateOperationDescriptor(x) => x.handle(),
            AbstractOperationDescriptorOneOf::SetMetricStateOperationDescriptor(x) => x.handle(),
            AbstractOperationDescriptorOneOf::SetContextStateOperationDescriptor(x) => x.handle(),
            AbstractOperationDescriptorOneOf::ActivateOperationDescriptor(x) => x.handle(),
            AbstractOperationDescriptorOneOf::SetValueOperationDescriptor(x) => x.handle(),
        }
    }

    fn version(&self) -> u64 {
        match self {
            AbstractOperationDescriptorOneOf::AbstractOperationDescriptor(x) => x.version(),
            AbstractOperationDescriptorOneOf::SetStringOperationDescriptor(x) => x.version(),
            AbstractOperationDescriptorOneOf::AbstractSetStateOperationDescriptor(x) => x.version(),
            AbstractOperationDescriptorOneOf::SetComponentStateOperationDescriptor(x) => {
                x.version()
            }
            AbstractOperationDescriptorOneOf::SetAlertStateOperationDescriptor(x) => x.version(),
            AbstractOperationDescriptorOneOf::SetMetricStateOperationDescriptor(x) => x.version(),
            AbstractOperationDescriptorOneOf::SetContextStateOperationDescriptor(x) => x.version(),
            AbstractOperationDescriptorOneOf::ActivateOperationDescriptor(x) => x.version(),
            AbstractOperationDescriptorOneOf::SetValueOperationDescriptor(x) => x.version(),
        }
    }

    fn into_abstract_descriptor_one_of(self) -> AbstractDescriptorOneOf {
        match self {
            AbstractOperationDescriptorOneOf::AbstractOperationDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AbstractOperationDescriptorOneOf::SetStringOperationDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AbstractOperationDescriptorOneOf::AbstractSetStateOperationDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AbstractOperationDescriptorOneOf::SetComponentStateOperationDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AbstractOperationDescriptorOneOf::SetAlertStateOperationDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AbstractOperationDescriptorOneOf::SetMetricStateOperationDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AbstractOperationDescriptorOneOf::SetContextStateOperationDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AbstractOperationDescriptorOneOf::ActivateOperationDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AbstractOperationDescriptorOneOf::SetValueOperationDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
        }
    }
}

impl Descriptor for AlertConditionDescriptorOneOf {
    fn handle(&self) -> String {
        match self {
            AlertConditionDescriptorOneOf::AlertConditionDescriptor(x) => x.handle(),
            AlertConditionDescriptorOneOf::LimitAlertConditionDescriptor(x) => x.handle(),
        }
    }

    fn version(&self) -> u64 {
        match self {
            AlertConditionDescriptorOneOf::AlertConditionDescriptor(x) => x.version(),
            AlertConditionDescriptorOneOf::LimitAlertConditionDescriptor(x) => x.version(),
        }
    }

    fn into_abstract_descriptor_one_of(self) -> AbstractDescriptorOneOf {
        match self {
            AlertConditionDescriptorOneOf::AlertConditionDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AlertConditionDescriptorOneOf::LimitAlertConditionDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
        }
    }
}

impl Descriptor for AbstractMetricDescriptorOneOf {
    fn handle(&self) -> String {
        match self {
            AbstractMetricDescriptorOneOf::AbstractMetricDescriptor(x) => x.handle(),
            AbstractMetricDescriptorOneOf::NumericMetricDescriptor(x) => x.handle(),
            AbstractMetricDescriptorOneOf::DistributionSampleArrayMetricDescriptor(x) => x.handle(),
            AbstractMetricDescriptorOneOf::RealTimeSampleArrayMetricDescriptor(x) => x.handle(),
            AbstractMetricDescriptorOneOf::StringMetricDescriptor(x) => x.handle(),
            AbstractMetricDescriptorOneOf::EnumStringMetricDescriptor(x) => x.handle(),
        }
    }

    fn version(&self) -> u64 {
        match self {
            AbstractMetricDescriptorOneOf::AbstractMetricDescriptor(x) => x.version(),
            AbstractMetricDescriptorOneOf::NumericMetricDescriptor(x) => x.version(),
            AbstractMetricDescriptorOneOf::DistributionSampleArrayMetricDescriptor(x) => {
                x.version()
            }
            AbstractMetricDescriptorOneOf::RealTimeSampleArrayMetricDescriptor(x) => x.version(),
            AbstractMetricDescriptorOneOf::StringMetricDescriptor(x) => x.version(),
            AbstractMetricDescriptorOneOf::EnumStringMetricDescriptor(x) => x.version(),
        }
    }

    fn into_abstract_descriptor_one_of(self) -> AbstractDescriptorOneOf {
        match self {
            AbstractMetricDescriptorOneOf::AbstractMetricDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AbstractMetricDescriptorOneOf::NumericMetricDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AbstractMetricDescriptorOneOf::DistributionSampleArrayMetricDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AbstractMetricDescriptorOneOf::RealTimeSampleArrayMetricDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AbstractMetricDescriptorOneOf::StringMetricDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
            AbstractMetricDescriptorOneOf::EnumStringMetricDescriptor(x) => {
                x.into_abstract_descriptor_one_of()
            }
        }
    }
}

impl State for AbstractStateOneOf {
    fn descriptor_handle(&self) -> String {
        match self {
            AbstractStateOneOf::AbstractState(x) => x.descriptor_handle(),
            AbstractStateOneOf::AbstractOperationState(x) => x.descriptor_handle(),
            AbstractStateOneOf::SetContextStateOperationState(x) => x.descriptor_handle(),
            AbstractStateOneOf::SetValueOperationState(x) => x.descriptor_handle(),
            AbstractStateOneOf::SetComponentStateOperationState(x) => x.descriptor_handle(),
            AbstractStateOneOf::SetMetricStateOperationState(x) => x.descriptor_handle(),
            AbstractStateOneOf::SetAlertStateOperationState(x) => x.descriptor_handle(),
            AbstractStateOneOf::SetStringOperationState(x) => x.descriptor_handle(),
            AbstractStateOneOf::ActivateOperationState(x) => x.descriptor_handle(),
            AbstractStateOneOf::AbstractAlertState(x) => x.descriptor_handle(),
            AbstractStateOneOf::AlertConditionState(x) => x.descriptor_handle(),
            AbstractStateOneOf::LimitAlertConditionState(x) => x.descriptor_handle(),
            AbstractStateOneOf::AlertSystemState(x) => x.descriptor_handle(),
            AbstractStateOneOf::AlertSignalState(x) => x.descriptor_handle(),
            AbstractStateOneOf::AbstractMultiState(x) => x.descriptor_handle(),
            AbstractStateOneOf::AbstractContextState(x) => x.descriptor_handle(),
            AbstractStateOneOf::OperatorContextState(x) => x.descriptor_handle(),
            AbstractStateOneOf::EnsembleContextState(x) => x.descriptor_handle(),
            AbstractStateOneOf::WorkflowContextState(x) => x.descriptor_handle(),
            AbstractStateOneOf::PatientContextState(x) => x.descriptor_handle(),
            AbstractStateOneOf::LocationContextState(x) => x.descriptor_handle(),
            AbstractStateOneOf::MeansContextState(x) => x.descriptor_handle(),
            AbstractStateOneOf::AbstractMetricState(x) => x.descriptor_handle(),
            AbstractStateOneOf::RealTimeSampleArrayMetricState(x) => x.descriptor_handle(),
            AbstractStateOneOf::NumericMetricState(x) => x.descriptor_handle(),
            AbstractStateOneOf::DistributionSampleArrayMetricState(x) => x.descriptor_handle(),
            AbstractStateOneOf::StringMetricState(x) => x.descriptor_handle(),
            AbstractStateOneOf::EnumStringMetricState(x) => x.descriptor_handle(),
            AbstractStateOneOf::AbstractDeviceComponentState(x) => x.descriptor_handle(),
            AbstractStateOneOf::ClockState(x) => x.descriptor_handle(),
            AbstractStateOneOf::ChannelState(x) => x.descriptor_handle(),
            AbstractStateOneOf::SystemContextState(x) => x.descriptor_handle(),
            AbstractStateOneOf::AbstractComplexDeviceComponentState(x) => x.descriptor_handle(),
            AbstractStateOneOf::VmdState(x) => x.descriptor_handle(),
            AbstractStateOneOf::MdsState(x) => x.descriptor_handle(),
            AbstractStateOneOf::BatteryState(x) => x.descriptor_handle(),
            AbstractStateOneOf::ScoState(x) => x.descriptor_handle(),
        }
    }

    fn version(&self) -> u64 {
        match self {
            AbstractStateOneOf::AbstractState(x) => x.version(),
            AbstractStateOneOf::AbstractOperationState(x) => x.version(),
            AbstractStateOneOf::SetContextStateOperationState(x) => x.version(),
            AbstractStateOneOf::SetValueOperationState(x) => x.version(),
            AbstractStateOneOf::SetComponentStateOperationState(x) => x.version(),
            AbstractStateOneOf::SetMetricStateOperationState(x) => x.version(),
            AbstractStateOneOf::SetAlertStateOperationState(x) => x.version(),
            AbstractStateOneOf::SetStringOperationState(x) => x.version(),
            AbstractStateOneOf::ActivateOperationState(x) => x.version(),
            AbstractStateOneOf::AbstractAlertState(x) => x.version(),
            AbstractStateOneOf::AlertConditionState(x) => x.version(),
            AbstractStateOneOf::LimitAlertConditionState(x) => x.version(),
            AbstractStateOneOf::AlertSystemState(x) => x.version(),
            AbstractStateOneOf::AlertSignalState(x) => x.version(),
            AbstractStateOneOf::AbstractMultiState(x) => x.version(),
            AbstractStateOneOf::AbstractContextState(x) => x.version(),
            AbstractStateOneOf::OperatorContextState(x) => x.version(),
            AbstractStateOneOf::EnsembleContextState(x) => x.version(),
            AbstractStateOneOf::WorkflowContextState(x) => x.version(),
            AbstractStateOneOf::PatientContextState(x) => x.version(),
            AbstractStateOneOf::LocationContextState(x) => x.version(),
            AbstractStateOneOf::MeansContextState(x) => x.version(),
            AbstractStateOneOf::AbstractMetricState(x) => x.version(),
            AbstractStateOneOf::RealTimeSampleArrayMetricState(x) => x.version(),
            AbstractStateOneOf::NumericMetricState(x) => x.version(),
            AbstractStateOneOf::DistributionSampleArrayMetricState(x) => x.version(),
            AbstractStateOneOf::StringMetricState(x) => x.version(),
            AbstractStateOneOf::EnumStringMetricState(x) => x.version(),
            AbstractStateOneOf::AbstractDeviceComponentState(x) => x.version(),
            AbstractStateOneOf::ClockState(x) => x.version(),
            AbstractStateOneOf::ChannelState(x) => x.version(),
            AbstractStateOneOf::SystemContextState(x) => x.version(),
            AbstractStateOneOf::AbstractComplexDeviceComponentState(x) => x.version(),
            AbstractStateOneOf::VmdState(x) => x.version(),
            AbstractStateOneOf::MdsState(x) => x.version(),
            AbstractStateOneOf::BatteryState(x) => x.version(),
            AbstractStateOneOf::ScoState(x) => x.version(),
        }
    }

    fn descriptor_version(&self) -> u64 {
        match self {
            AbstractStateOneOf::AbstractState(x) => x.descriptor_version(),
            AbstractStateOneOf::AbstractOperationState(x) => x.descriptor_version(),
            AbstractStateOneOf::SetContextStateOperationState(x) => x.descriptor_version(),
            AbstractStateOneOf::SetValueOperationState(x) => x.descriptor_version(),
            AbstractStateOneOf::SetComponentStateOperationState(x) => x.descriptor_version(),
            AbstractStateOneOf::SetMetricStateOperationState(x) => x.descriptor_version(),
            AbstractStateOneOf::SetAlertStateOperationState(x) => x.descriptor_version(),
            AbstractStateOneOf::SetStringOperationState(x) => x.descriptor_version(),
            AbstractStateOneOf::ActivateOperationState(x) => x.descriptor_version(),
            AbstractStateOneOf::AbstractAlertState(x) => x.descriptor_version(),
            AbstractStateOneOf::AlertConditionState(x) => x.descriptor_version(),
            AbstractStateOneOf::LimitAlertConditionState(x) => x.descriptor_version(),
            AbstractStateOneOf::AlertSystemState(x) => x.descriptor_version(),
            AbstractStateOneOf::AlertSignalState(x) => x.descriptor_version(),
            AbstractStateOneOf::AbstractMultiState(x) => x.descriptor_version(),
            AbstractStateOneOf::AbstractContextState(x) => x.descriptor_version(),
            AbstractStateOneOf::OperatorContextState(x) => x.descriptor_version(),
            AbstractStateOneOf::EnsembleContextState(x) => x.descriptor_version(),
            AbstractStateOneOf::WorkflowContextState(x) => x.descriptor_version(),
            AbstractStateOneOf::PatientContextState(x) => x.descriptor_version(),
            AbstractStateOneOf::LocationContextState(x) => x.descriptor_version(),
            AbstractStateOneOf::MeansContextState(x) => x.descriptor_version(),
            AbstractStateOneOf::AbstractMetricState(x) => x.descriptor_version(),
            AbstractStateOneOf::RealTimeSampleArrayMetricState(x) => x.descriptor_version(),
            AbstractStateOneOf::NumericMetricState(x) => x.descriptor_version(),
            AbstractStateOneOf::DistributionSampleArrayMetricState(x) => x.descriptor_version(),
            AbstractStateOneOf::StringMetricState(x) => x.descriptor_version(),
            AbstractStateOneOf::EnumStringMetricState(x) => x.descriptor_version(),
            AbstractStateOneOf::AbstractDeviceComponentState(x) => x.descriptor_version(),
            AbstractStateOneOf::ClockState(x) => x.descriptor_version(),
            AbstractStateOneOf::ChannelState(x) => x.descriptor_version(),
            AbstractStateOneOf::SystemContextState(x) => x.descriptor_version(),
            AbstractStateOneOf::AbstractComplexDeviceComponentState(x) => x.descriptor_version(),
            AbstractStateOneOf::VmdState(x) => x.descriptor_version(),
            AbstractStateOneOf::MdsState(x) => x.descriptor_version(),
            AbstractStateOneOf::BatteryState(x) => x.descriptor_version(),
            AbstractStateOneOf::ScoState(x) => x.descriptor_version(),
        }
    }

    fn into_abstract_state_one_of(self) -> AbstractStateOneOf {
        self
    }
}

impl State for AbstractAlertStateOneOf {
    fn descriptor_handle(&self) -> String {
        match self {
            AbstractAlertStateOneOf::AbstractAlertState(state) => state.descriptor_handle(),
            AbstractAlertStateOneOf::AlertConditionState(state) => state.descriptor_handle(),
            AbstractAlertStateOneOf::LimitAlertConditionState(state) => state.descriptor_handle(),
            AbstractAlertStateOneOf::AlertSystemState(state) => state.descriptor_handle(),
            AbstractAlertStateOneOf::AlertSignalState(state) => state.descriptor_handle(),
        }
    }

    fn version(&self) -> u64 {
        match self {
            AbstractAlertStateOneOf::AbstractAlertState(state) => state.version(),
            AbstractAlertStateOneOf::AlertConditionState(state) => state.version(),
            AbstractAlertStateOneOf::LimitAlertConditionState(state) => state.version(),
            AbstractAlertStateOneOf::AlertSystemState(state) => state.version(),
            AbstractAlertStateOneOf::AlertSignalState(state) => state.version(),
        }
    }

    fn descriptor_version(&self) -> u64 {
        match self {
            AbstractAlertStateOneOf::AbstractAlertState(state) => state.descriptor_version(),
            AbstractAlertStateOneOf::AlertConditionState(state) => state.descriptor_version(),
            AbstractAlertStateOneOf::LimitAlertConditionState(state) => state.descriptor_version(),
            AbstractAlertStateOneOf::AlertSystemState(state) => state.descriptor_version(),
            AbstractAlertStateOneOf::AlertSignalState(state) => state.descriptor_version(),
        }
    }

    fn into_abstract_state_one_of(self) -> AbstractStateOneOf {
        match self {
            AbstractAlertStateOneOf::AbstractAlertState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractAlertStateOneOf::AlertConditionState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractAlertStateOneOf::LimitAlertConditionState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractAlertStateOneOf::AlertSystemState(state) => state.into_abstract_state_one_of(),
            AbstractAlertStateOneOf::AlertSignalState(state) => state.into_abstract_state_one_of(),
        }
    }
}

impl State for AbstractMetricStateOneOf {
    fn descriptor_handle(&self) -> String {
        match self {
            AbstractMetricStateOneOf::AbstractMetricState(state) => state.descriptor_handle(),
            AbstractMetricStateOneOf::RealTimeSampleArrayMetricState(state) => {
                state.descriptor_handle()
            }
            AbstractMetricStateOneOf::NumericMetricState(state) => state.descriptor_handle(),
            AbstractMetricStateOneOf::DistributionSampleArrayMetricState(state) => {
                state.descriptor_handle()
            }
            AbstractMetricStateOneOf::StringMetricState(state) => state.descriptor_handle(),
            AbstractMetricStateOneOf::EnumStringMetricState(state) => state.descriptor_handle(),
        }
    }

    fn version(&self) -> u64 {
        match self {
            AbstractMetricStateOneOf::AbstractMetricState(state) => state.version(),
            AbstractMetricStateOneOf::RealTimeSampleArrayMetricState(state) => state.version(),
            AbstractMetricStateOneOf::NumericMetricState(state) => state.version(),
            AbstractMetricStateOneOf::DistributionSampleArrayMetricState(state) => state.version(),
            AbstractMetricStateOneOf::StringMetricState(state) => state.version(),
            AbstractMetricStateOneOf::EnumStringMetricState(state) => state.version(),
        }
    }

    fn descriptor_version(&self) -> u64 {
        match self {
            AbstractMetricStateOneOf::AbstractMetricState(state) => state.descriptor_version(),
            AbstractMetricStateOneOf::RealTimeSampleArrayMetricState(state) => {
                state.descriptor_version()
            }
            AbstractMetricStateOneOf::NumericMetricState(state) => state.descriptor_version(),
            AbstractMetricStateOneOf::DistributionSampleArrayMetricState(state) => {
                state.descriptor_version()
            }
            AbstractMetricStateOneOf::StringMetricState(state) => state.descriptor_version(),
            AbstractMetricStateOneOf::EnumStringMetricState(state) => state.descriptor_version(),
        }
    }

    fn into_abstract_state_one_of(self) -> AbstractStateOneOf {
        match self {
            AbstractMetricStateOneOf::AbstractMetricState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractMetricStateOneOf::RealTimeSampleArrayMetricState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractMetricStateOneOf::NumericMetricState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractMetricStateOneOf::DistributionSampleArrayMetricState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractMetricStateOneOf::StringMetricState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractMetricStateOneOf::EnumStringMetricState(state) => {
                state.into_abstract_state_one_of()
            }
        }
    }
}

impl State for AbstractContextStateOneOf {
    fn descriptor_handle(&self) -> String {
        match self {
            AbstractContextStateOneOf::AbstractContextState(state) => state.descriptor_handle(),
            AbstractContextStateOneOf::OperatorContextState(state) => state.descriptor_handle(),
            AbstractContextStateOneOf::EnsembleContextState(state) => state.descriptor_handle(),
            AbstractContextStateOneOf::WorkflowContextState(state) => state.descriptor_handle(),
            AbstractContextStateOneOf::PatientContextState(state) => state.descriptor_handle(),
            AbstractContextStateOneOf::LocationContextState(state) => state.descriptor_handle(),
            AbstractContextStateOneOf::MeansContextState(state) => state.descriptor_handle(),
        }
    }

    fn version(&self) -> u64 {
        match self {
            AbstractContextStateOneOf::AbstractContextState(state) => state.version(),
            AbstractContextStateOneOf::OperatorContextState(state) => state.version(),
            AbstractContextStateOneOf::EnsembleContextState(state) => state.version(),
            AbstractContextStateOneOf::WorkflowContextState(state) => state.version(),
            AbstractContextStateOneOf::PatientContextState(state) => state.version(),
            AbstractContextStateOneOf::LocationContextState(state) => state.version(),
            AbstractContextStateOneOf::MeansContextState(state) => state.version(),
        }
    }

    fn descriptor_version(&self) -> u64 {
        match self {
            AbstractContextStateOneOf::AbstractContextState(state) => state.descriptor_version(),
            AbstractContextStateOneOf::OperatorContextState(state) => state.descriptor_version(),
            AbstractContextStateOneOf::EnsembleContextState(state) => state.descriptor_version(),
            AbstractContextStateOneOf::WorkflowContextState(state) => state.descriptor_version(),
            AbstractContextStateOneOf::PatientContextState(state) => state.descriptor_version(),
            AbstractContextStateOneOf::LocationContextState(state) => state.descriptor_version(),
            AbstractContextStateOneOf::MeansContextState(state) => state.descriptor_version(),
        }
    }

    fn into_abstract_state_one_of(self) -> AbstractStateOneOf {
        match self {
            AbstractContextStateOneOf::AbstractContextState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractContextStateOneOf::OperatorContextState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractContextStateOneOf::EnsembleContextState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractContextStateOneOf::WorkflowContextState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractContextStateOneOf::PatientContextState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractContextStateOneOf::LocationContextState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractContextStateOneOf::MeansContextState(state) => {
                state.into_abstract_state_one_of()
            }
        }
    }
}

impl State for AbstractComplexDeviceComponentStateOneOf {
    fn descriptor_handle(&self) -> String {
        match self {
            AbstractComplexDeviceComponentStateOneOf::AbstractComplexDeviceComponentState(
                state,
            ) => state.descriptor_handle(),
            AbstractComplexDeviceComponentStateOneOf::VmdState(state) => state.descriptor_handle(),
            AbstractComplexDeviceComponentStateOneOf::MdsState(state) => state.descriptor_handle(),
        }
    }

    fn version(&self) -> u64 {
        match self {
            AbstractComplexDeviceComponentStateOneOf::AbstractComplexDeviceComponentState(
                state,
            ) => state.version(),
            AbstractComplexDeviceComponentStateOneOf::VmdState(state) => state.version(),
            AbstractComplexDeviceComponentStateOneOf::MdsState(state) => state.version(),
        }
    }

    fn descriptor_version(&self) -> u64 {
        match self {
            AbstractComplexDeviceComponentStateOneOf::AbstractComplexDeviceComponentState(
                state,
            ) => state.descriptor_version(),
            AbstractComplexDeviceComponentStateOneOf::VmdState(state) => state.descriptor_version(),
            AbstractComplexDeviceComponentStateOneOf::MdsState(state) => state.descriptor_version(),
        }
    }

    fn into_abstract_state_one_of(self) -> AbstractStateOneOf {
        match self {
            AbstractComplexDeviceComponentStateOneOf::AbstractComplexDeviceComponentState(
                state,
            ) => state.into_abstract_state_one_of(),
            AbstractComplexDeviceComponentStateOneOf::VmdState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractComplexDeviceComponentStateOneOf::MdsState(state) => {
                state.into_abstract_state_one_of()
            }
        }
    }
}

impl State for AbstractDeviceComponentStateOneOf {
    fn descriptor_handle(&self) -> String {
        match self {
            AbstractDeviceComponentStateOneOf::AbstractDeviceComponentState(state) => {
                state.descriptor_handle()
            }
            AbstractDeviceComponentStateOneOf::ClockState(state) => state.descriptor_handle(),
            AbstractDeviceComponentStateOneOf::ChannelState(state) => state.descriptor_handle(),
            AbstractDeviceComponentStateOneOf::SystemContextState(state) => {
                state.descriptor_handle()
            }
            AbstractDeviceComponentStateOneOf::AbstractComplexDeviceComponentState(state) => {
                state.descriptor_handle()
            }
            AbstractDeviceComponentStateOneOf::VmdState(state) => state.descriptor_handle(),
            AbstractDeviceComponentStateOneOf::MdsState(state) => state.descriptor_handle(),
            AbstractDeviceComponentStateOneOf::BatteryState(state) => state.descriptor_handle(),
            AbstractDeviceComponentStateOneOf::ScoState(state) => state.descriptor_handle(),
        }
    }

    fn version(&self) -> u64 {
        match self {
            AbstractDeviceComponentStateOneOf::AbstractDeviceComponentState(state) => {
                state.version()
            }
            AbstractDeviceComponentStateOneOf::ClockState(state) => state.version(),
            AbstractDeviceComponentStateOneOf::ChannelState(state) => state.version(),
            AbstractDeviceComponentStateOneOf::SystemContextState(state) => state.version(),
            AbstractDeviceComponentStateOneOf::AbstractComplexDeviceComponentState(state) => {
                state.version()
            }
            AbstractDeviceComponentStateOneOf::VmdState(state) => state.version(),
            AbstractDeviceComponentStateOneOf::MdsState(state) => state.version(),
            AbstractDeviceComponentStateOneOf::BatteryState(state) => state.version(),
            AbstractDeviceComponentStateOneOf::ScoState(state) => state.version(),
        }
    }

    fn descriptor_version(&self) -> u64 {
        match self {
            AbstractDeviceComponentStateOneOf::AbstractDeviceComponentState(state) => {
                state.descriptor_version()
            }
            AbstractDeviceComponentStateOneOf::ClockState(state) => state.descriptor_version(),
            AbstractDeviceComponentStateOneOf::ChannelState(state) => state.descriptor_version(),
            AbstractDeviceComponentStateOneOf::SystemContextState(state) => {
                state.descriptor_version()
            }
            AbstractDeviceComponentStateOneOf::AbstractComplexDeviceComponentState(state) => {
                state.descriptor_version()
            }
            AbstractDeviceComponentStateOneOf::VmdState(state) => state.descriptor_version(),
            AbstractDeviceComponentStateOneOf::MdsState(state) => state.descriptor_version(),
            AbstractDeviceComponentStateOneOf::BatteryState(state) => state.descriptor_version(),
            AbstractDeviceComponentStateOneOf::ScoState(state) => state.descriptor_version(),
        }
    }

    fn into_abstract_state_one_of(self) -> AbstractStateOneOf {
        match self {
            AbstractDeviceComponentStateOneOf::AbstractDeviceComponentState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractDeviceComponentStateOneOf::ClockState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractDeviceComponentStateOneOf::ChannelState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractDeviceComponentStateOneOf::SystemContextState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractDeviceComponentStateOneOf::AbstractComplexDeviceComponentState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractDeviceComponentStateOneOf::VmdState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractDeviceComponentStateOneOf::MdsState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractDeviceComponentStateOneOf::BatteryState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractDeviceComponentStateOneOf::ScoState(state) => {
                state.into_abstract_state_one_of()
            }
        }
    }
}

impl State for AbstractOperationStateOneOf {
    fn descriptor_handle(&self) -> String {
        match self {
            AbstractOperationStateOneOf::AbstractOperationState(state) => state.descriptor_handle(),
            AbstractOperationStateOneOf::SetContextStateOperationState(state) => {
                state.descriptor_handle()
            }
            AbstractOperationStateOneOf::SetValueOperationState(state) => state.descriptor_handle(),
            AbstractOperationStateOneOf::SetComponentStateOperationState(state) => {
                state.descriptor_handle()
            }
            AbstractOperationStateOneOf::SetMetricStateOperationState(state) => {
                state.descriptor_handle()
            }
            AbstractOperationStateOneOf::SetAlertStateOperationState(state) => {
                state.descriptor_handle()
            }
            AbstractOperationStateOneOf::SetStringOperationState(state) => {
                state.descriptor_handle()
            }
            AbstractOperationStateOneOf::ActivateOperationState(state) => state.descriptor_handle(),
        }
    }

    fn version(&self) -> u64 {
        match self {
            AbstractOperationStateOneOf::AbstractOperationState(state) => state.version(),
            AbstractOperationStateOneOf::SetContextStateOperationState(state) => state.version(),
            AbstractOperationStateOneOf::SetValueOperationState(state) => state.version(),
            AbstractOperationStateOneOf::SetComponentStateOperationState(state) => state.version(),
            AbstractOperationStateOneOf::SetMetricStateOperationState(state) => state.version(),
            AbstractOperationStateOneOf::SetAlertStateOperationState(state) => state.version(),
            AbstractOperationStateOneOf::SetStringOperationState(state) => state.version(),
            AbstractOperationStateOneOf::ActivateOperationState(state) => state.version(),
        }
    }

    fn descriptor_version(&self) -> u64 {
        match self {
            AbstractOperationStateOneOf::AbstractOperationState(state) => {
                state.descriptor_version()
            }
            AbstractOperationStateOneOf::SetContextStateOperationState(state) => {
                state.descriptor_version()
            }
            AbstractOperationStateOneOf::SetValueOperationState(state) => {
                state.descriptor_version()
            }
            AbstractOperationStateOneOf::SetComponentStateOperationState(state) => {
                state.descriptor_version()
            }
            AbstractOperationStateOneOf::SetMetricStateOperationState(state) => {
                state.descriptor_version()
            }
            AbstractOperationStateOneOf::SetAlertStateOperationState(state) => {
                state.descriptor_version()
            }
            AbstractOperationStateOneOf::SetStringOperationState(state) => {
                state.descriptor_version()
            }
            AbstractOperationStateOneOf::ActivateOperationState(state) => {
                state.descriptor_version()
            }
        }
    }

    fn into_abstract_state_one_of(self) -> AbstractStateOneOf {
        match self {
            AbstractOperationStateOneOf::AbstractOperationState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractOperationStateOneOf::SetContextStateOperationState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractOperationStateOneOf::SetValueOperationState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractOperationStateOneOf::SetComponentStateOperationState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractOperationStateOneOf::SetMetricStateOperationState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractOperationStateOneOf::SetAlertStateOperationState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractOperationStateOneOf::SetStringOperationState(state) => {
                state.into_abstract_state_one_of()
            }
            AbstractOperationStateOneOf::ActivateOperationState(state) => {
                state.into_abstract_state_one_of()
            }
        }
    }
}

impl CopyableDescriptor for AbstractDescriptorOneOf {
    fn replace_in_copy(&self, version: u64) -> Self {
        match self {
            AbstractDescriptorOneOf::AbstractDescriptor(desc) => {
                Self::AbstractDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::AbstractAlertDescriptor(desc) => {
                Self::AbstractAlertDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::AlertConditionDescriptor(desc) => {
                Self::AlertConditionDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::LimitAlertConditionDescriptor(desc) => {
                Self::LimitAlertConditionDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::AlertSignalDescriptor(desc) => {
                Self::AlertSignalDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::AlertSystemDescriptor(desc) => {
                Self::AlertSystemDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::AbstractDeviceComponentDescriptor(desc) => {
                Self::AbstractDeviceComponentDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::ClockDescriptor(desc) => {
                Self::ClockDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::BatteryDescriptor(desc) => {
                Self::BatteryDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::ChannelDescriptor(desc) => {
                Self::ChannelDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::ScoDescriptor(desc) => {
                Self::ScoDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::SystemContextDescriptor(desc) => {
                Self::SystemContextDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::AbstractComplexDeviceComponentDescriptor(desc) => {
                Self::AbstractComplexDeviceComponentDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::VmdDescriptor(desc) => {
                Self::VmdDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::MdsDescriptor(desc) => {
                Self::MdsDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::AbstractOperationDescriptor(desc) => {
                Self::AbstractOperationDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::SetStringOperationDescriptor(desc) => {
                Self::SetStringOperationDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::AbstractSetStateOperationDescriptor(desc) => {
                Self::AbstractSetStateOperationDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::SetComponentStateOperationDescriptor(desc) => {
                Self::SetComponentStateOperationDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::SetAlertStateOperationDescriptor(desc) => {
                Self::SetAlertStateOperationDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::SetMetricStateOperationDescriptor(desc) => {
                Self::SetMetricStateOperationDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::SetContextStateOperationDescriptor(desc) => {
                Self::SetContextStateOperationDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::ActivateOperationDescriptor(desc) => {
                Self::ActivateOperationDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::SetValueOperationDescriptor(desc) => {
                Self::SetValueOperationDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::AbstractMetricDescriptor(desc) => {
                Self::AbstractMetricDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::NumericMetricDescriptor(desc) => {
                Self::NumericMetricDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::DistributionSampleArrayMetricDescriptor(desc) => {
                Self::DistributionSampleArrayMetricDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::RealTimeSampleArrayMetricDescriptor(desc) => {
                Self::RealTimeSampleArrayMetricDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::StringMetricDescriptor(desc) => {
                Self::StringMetricDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::EnumStringMetricDescriptor(desc) => {
                Self::EnumStringMetricDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::AbstractContextDescriptor(desc) => {
                Self::AbstractContextDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::MeansContextDescriptor(desc) => {
                Self::MeansContextDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::WorkflowContextDescriptor(desc) => {
                Self::WorkflowContextDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::PatientContextDescriptor(desc) => {
                Self::PatientContextDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::OperatorContextDescriptor(desc) => {
                Self::OperatorContextDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::EnsembleContextDescriptor(desc) => {
                Self::EnsembleContextDescriptor(desc.replace_in_copy(version))
            }
            AbstractDescriptorOneOf::LocationContextDescriptor(desc) => {
                Self::LocationContextDescriptor(desc.replace_in_copy(version))
            }
        }
    }
}

impl CopyableState for AbstractStateOneOf {
    fn replace_in_copy(&self, descriptor_version: u64, state_version: u64) -> Self {
        match self {
            AbstractStateOneOf::AbstractState(state) => {
                Self::AbstractState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::AbstractOperationState(state) => Self::AbstractOperationState(
                state.replace_in_copy(descriptor_version, state_version),
            ),
            AbstractStateOneOf::SetContextStateOperationState(state) => {
                Self::SetContextStateOperationState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractStateOneOf::SetValueOperationState(state) => Self::SetValueOperationState(
                state.replace_in_copy(descriptor_version, state_version),
            ),
            AbstractStateOneOf::SetComponentStateOperationState(state) => {
                Self::SetComponentStateOperationState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractStateOneOf::SetMetricStateOperationState(state) => {
                Self::SetMetricStateOperationState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractStateOneOf::SetAlertStateOperationState(state) => {
                Self::SetAlertStateOperationState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractStateOneOf::SetStringOperationState(state) => Self::SetStringOperationState(
                state.replace_in_copy(descriptor_version, state_version),
            ),
            AbstractStateOneOf::ActivateOperationState(state) => Self::ActivateOperationState(
                state.replace_in_copy(descriptor_version, state_version),
            ),
            AbstractStateOneOf::AbstractAlertState(state) => {
                Self::AbstractAlertState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::AlertConditionState(state) => {
                Self::AlertConditionState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::LimitAlertConditionState(state) => Self::LimitAlertConditionState(
                state.replace_in_copy(descriptor_version, state_version),
            ),
            AbstractStateOneOf::AlertSystemState(state) => {
                Self::AlertSystemState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::AlertSignalState(state) => {
                Self::AlertSignalState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::AbstractMultiState(state) => {
                Self::AbstractMultiState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::AbstractContextState(state) => {
                Self::AbstractContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::OperatorContextState(state) => {
                Self::OperatorContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::EnsembleContextState(state) => {
                Self::EnsembleContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::WorkflowContextState(state) => {
                Self::WorkflowContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::PatientContextState(state) => {
                Self::PatientContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::LocationContextState(state) => {
                Self::LocationContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::MeansContextState(state) => {
                Self::MeansContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::AbstractMetricState(state) => {
                Self::AbstractMetricState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::RealTimeSampleArrayMetricState(state) => {
                Self::RealTimeSampleArrayMetricState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractStateOneOf::NumericMetricState(state) => {
                Self::NumericMetricState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::DistributionSampleArrayMetricState(state) => {
                Self::DistributionSampleArrayMetricState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractStateOneOf::StringMetricState(state) => {
                Self::StringMetricState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::EnumStringMetricState(state) => Self::EnumStringMetricState(
                state.replace_in_copy(descriptor_version, state_version),
            ),
            AbstractStateOneOf::AbstractDeviceComponentState(state) => {
                Self::AbstractDeviceComponentState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractStateOneOf::ClockState(state) => {
                Self::ClockState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::ChannelState(state) => {
                Self::ChannelState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::SystemContextState(state) => {
                Self::SystemContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::AbstractComplexDeviceComponentState(state) => {
                Self::AbstractComplexDeviceComponentState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractStateOneOf::VmdState(state) => {
                Self::VmdState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::MdsState(state) => {
                Self::MdsState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::BatteryState(state) => {
                Self::BatteryState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractStateOneOf::ScoState(state) => {
                Self::ScoState(state.replace_in_copy(descriptor_version, state_version))
            }
        }
    }

    fn replace(self, descriptor_version: u64, state_version: u64) -> Self {
        match self {
            AbstractStateOneOf::AbstractState(state) => {
                Self::AbstractState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::AbstractOperationState(state) => {
                Self::AbstractOperationState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::SetContextStateOperationState(state) => {
                Self::SetContextStateOperationState(
                    state.replace(descriptor_version, state_version),
                )
            }
            AbstractStateOneOf::SetValueOperationState(state) => {
                Self::SetValueOperationState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::SetComponentStateOperationState(state) => {
                Self::SetComponentStateOperationState(
                    state.replace(descriptor_version, state_version),
                )
            }
            AbstractStateOneOf::SetMetricStateOperationState(state) => {
                Self::SetMetricStateOperationState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::SetAlertStateOperationState(state) => {
                Self::SetAlertStateOperationState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::SetStringOperationState(state) => {
                Self::SetStringOperationState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::ActivateOperationState(state) => {
                Self::ActivateOperationState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::AbstractAlertState(state) => {
                Self::AbstractAlertState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::AlertConditionState(state) => {
                Self::AlertConditionState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::LimitAlertConditionState(state) => {
                Self::LimitAlertConditionState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::AlertSystemState(state) => {
                Self::AlertSystemState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::AlertSignalState(state) => {
                Self::AlertSignalState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::AbstractMultiState(state) => {
                Self::AbstractMultiState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::AbstractContextState(state) => {
                Self::AbstractContextState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::OperatorContextState(state) => {
                Self::OperatorContextState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::EnsembleContextState(state) => {
                Self::EnsembleContextState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::WorkflowContextState(state) => {
                Self::WorkflowContextState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::PatientContextState(state) => {
                Self::PatientContextState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::LocationContextState(state) => {
                Self::LocationContextState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::MeansContextState(state) => {
                Self::MeansContextState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::AbstractMetricState(state) => {
                Self::AbstractMetricState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::RealTimeSampleArrayMetricState(state) => {
                Self::RealTimeSampleArrayMetricState(
                    state.replace(descriptor_version, state_version),
                )
            }
            AbstractStateOneOf::NumericMetricState(state) => {
                Self::NumericMetricState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::DistributionSampleArrayMetricState(state) => {
                Self::DistributionSampleArrayMetricState(
                    state.replace(descriptor_version, state_version),
                )
            }
            AbstractStateOneOf::StringMetricState(state) => {
                Self::StringMetricState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::EnumStringMetricState(state) => {
                Self::EnumStringMetricState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::AbstractDeviceComponentState(state) => {
                Self::AbstractDeviceComponentState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::ClockState(state) => {
                Self::ClockState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::ChannelState(state) => {
                Self::ChannelState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::SystemContextState(state) => {
                Self::SystemContextState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::AbstractComplexDeviceComponentState(state) => {
                Self::AbstractComplexDeviceComponentState(
                    state.replace(descriptor_version, state_version),
                )
            }
            AbstractStateOneOf::VmdState(state) => {
                Self::VmdState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::MdsState(state) => {
                Self::MdsState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::BatteryState(state) => {
                Self::BatteryState(state.replace(descriptor_version, state_version))
            }
            AbstractStateOneOf::ScoState(state) => {
                Self::ScoState(state.replace(descriptor_version, state_version))
            }
        }
    }
}

impl CopyableState for AbstractContextStateOneOf {
    fn replace_in_copy(&self, descriptor_version: u64, state_version: u64) -> Self {
        match self {
            AbstractContextStateOneOf::AbstractContextState(state) => {
                Self::AbstractContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractContextStateOneOf::OperatorContextState(state) => {
                Self::OperatorContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractContextStateOneOf::EnsembleContextState(state) => {
                Self::EnsembleContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractContextStateOneOf::WorkflowContextState(state) => {
                Self::WorkflowContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractContextStateOneOf::PatientContextState(state) => {
                Self::PatientContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractContextStateOneOf::LocationContextState(state) => {
                Self::LocationContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractContextStateOneOf::MeansContextState(state) => {
                Self::MeansContextState(state.replace_in_copy(descriptor_version, state_version))
            }
        }
    }

    fn replace(self, descriptor_version: u64, state_version: u64) -> Self {
        match self {
            AbstractContextStateOneOf::AbstractContextState(state) => {
                Self::AbstractContextState(state.replace(descriptor_version, state_version))
            }
            AbstractContextStateOneOf::OperatorContextState(state) => {
                Self::OperatorContextState(state.replace(descriptor_version, state_version))
            }
            AbstractContextStateOneOf::EnsembleContextState(state) => {
                Self::EnsembleContextState(state.replace(descriptor_version, state_version))
            }
            AbstractContextStateOneOf::WorkflowContextState(state) => {
                Self::WorkflowContextState(state.replace(descriptor_version, state_version))
            }
            AbstractContextStateOneOf::PatientContextState(state) => {
                Self::PatientContextState(state.replace(descriptor_version, state_version))
            }
            AbstractContextStateOneOf::LocationContextState(state) => {
                Self::LocationContextState(state.replace(descriptor_version, state_version))
            }
            AbstractContextStateOneOf::MeansContextState(state) => {
                Self::MeansContextState(state.replace(descriptor_version, state_version))
            }
        }
    }
}

impl CopyableState for AbstractAlertStateOneOf {
    fn replace_in_copy(&self, descriptor_version: u64, state_version: u64) -> Self {
        match self {
            AbstractAlertStateOneOf::AbstractAlertState(state) => {
                Self::AbstractAlertState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractAlertStateOneOf::AlertConditionState(state) => {
                Self::AlertConditionState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractAlertStateOneOf::LimitAlertConditionState(state) => {
                Self::LimitAlertConditionState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractAlertStateOneOf::AlertSystemState(state) => {
                Self::AlertSystemState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractAlertStateOneOf::AlertSignalState(state) => {
                Self::AlertSignalState(state.replace_in_copy(descriptor_version, state_version))
            }
        }
    }

    fn replace(self, descriptor_version: u64, state_version: u64) -> Self {
        match self {
            AbstractAlertStateOneOf::AbstractAlertState(state) => {
                Self::AbstractAlertState(state.replace(descriptor_version, state_version))
            }
            AbstractAlertStateOneOf::AlertConditionState(state) => {
                Self::AlertConditionState(state.replace(descriptor_version, state_version))
            }
            AbstractAlertStateOneOf::LimitAlertConditionState(state) => {
                Self::LimitAlertConditionState(state.replace(descriptor_version, state_version))
            }
            AbstractAlertStateOneOf::AlertSystemState(state) => {
                Self::AlertSystemState(state.replace(descriptor_version, state_version))
            }
            AbstractAlertStateOneOf::AlertSignalState(state) => {
                Self::AlertSignalState(state.replace(descriptor_version, state_version))
            }
        }
    }
}

impl CopyableState for AbstractDeviceComponentStateOneOf {
    fn replace_in_copy(&self, descriptor_version: u64, state_version: u64) -> Self {
        match self {
            AbstractDeviceComponentStateOneOf::AbstractDeviceComponentState(state) => {
                Self::AbstractDeviceComponentState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractDeviceComponentStateOneOf::ClockState(state) => {
                Self::ClockState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractDeviceComponentStateOneOf::ChannelState(state) => {
                Self::ChannelState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractDeviceComponentStateOneOf::SystemContextState(state) => {
                Self::SystemContextState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractDeviceComponentStateOneOf::AbstractComplexDeviceComponentState(state) => {
                Self::AbstractComplexDeviceComponentState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractDeviceComponentStateOneOf::VmdState(state) => {
                Self::VmdState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractDeviceComponentStateOneOf::MdsState(state) => {
                Self::MdsState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractDeviceComponentStateOneOf::BatteryState(state) => {
                Self::BatteryState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractDeviceComponentStateOneOf::ScoState(state) => {
                Self::ScoState(state.replace_in_copy(descriptor_version, state_version))
            }
        }
    }

    fn replace(self, descriptor_version: u64, state_version: u64) -> Self {
        match self {
            AbstractDeviceComponentStateOneOf::AbstractDeviceComponentState(state) => {
                Self::AbstractDeviceComponentState(state.replace(descriptor_version, state_version))
            }
            AbstractDeviceComponentStateOneOf::ClockState(state) => {
                Self::ClockState(state.replace(descriptor_version, state_version))
            }
            AbstractDeviceComponentStateOneOf::ChannelState(state) => {
                Self::ChannelState(state.replace(descriptor_version, state_version))
            }
            AbstractDeviceComponentStateOneOf::SystemContextState(state) => {
                Self::SystemContextState(state.replace(descriptor_version, state_version))
            }
            AbstractDeviceComponentStateOneOf::AbstractComplexDeviceComponentState(state) => {
                Self::AbstractComplexDeviceComponentState(
                    state.replace(descriptor_version, state_version),
                )
            }
            AbstractDeviceComponentStateOneOf::VmdState(state) => {
                Self::VmdState(state.replace(descriptor_version, state_version))
            }
            AbstractDeviceComponentStateOneOf::MdsState(state) => {
                Self::MdsState(state.replace(descriptor_version, state_version))
            }
            AbstractDeviceComponentStateOneOf::BatteryState(state) => {
                Self::BatteryState(state.replace(descriptor_version, state_version))
            }
            AbstractDeviceComponentStateOneOf::ScoState(state) => {
                Self::ScoState(state.replace(descriptor_version, state_version))
            }
        }
    }
}

impl CopyableState for AbstractMetricStateOneOf {
    fn replace_in_copy(&self, descriptor_version: u64, state_version: u64) -> Self {
        match self {
            AbstractMetricStateOneOf::AbstractMetricState(state) => {
                Self::AbstractMetricState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractMetricStateOneOf::RealTimeSampleArrayMetricState(state) => {
                Self::RealTimeSampleArrayMetricState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractMetricStateOneOf::NumericMetricState(state) => {
                Self::NumericMetricState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractMetricStateOneOf::DistributionSampleArrayMetricState(state) => {
                Self::DistributionSampleArrayMetricState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractMetricStateOneOf::StringMetricState(state) => {
                Self::StringMetricState(state.replace_in_copy(descriptor_version, state_version))
            }
            AbstractMetricStateOneOf::EnumStringMetricState(state) => Self::EnumStringMetricState(
                state.replace_in_copy(descriptor_version, state_version),
            ),
        }
    }

    fn replace(self, descriptor_version: u64, state_version: u64) -> Self {
        match self {
            AbstractMetricStateOneOf::AbstractMetricState(state) => {
                Self::AbstractMetricState(state.replace(descriptor_version, state_version))
            }
            AbstractMetricStateOneOf::RealTimeSampleArrayMetricState(state) => {
                Self::RealTimeSampleArrayMetricState(
                    state.replace(descriptor_version, state_version),
                )
            }
            AbstractMetricStateOneOf::NumericMetricState(state) => {
                Self::NumericMetricState(state.replace(descriptor_version, state_version))
            }
            AbstractMetricStateOneOf::DistributionSampleArrayMetricState(state) => {
                Self::DistributionSampleArrayMetricState(
                    state.replace(descriptor_version, state_version),
                )
            }
            AbstractMetricStateOneOf::StringMetricState(state) => {
                Self::StringMetricState(state.replace(descriptor_version, state_version))
            }
            AbstractMetricStateOneOf::EnumStringMetricState(state) => {
                Self::EnumStringMetricState(state.replace(descriptor_version, state_version))
            }
        }
    }
}

impl CopyableState for AbstractOperationStateOneOf {
    fn replace_in_copy(&self, descriptor_version: u64, state_version: u64) -> Self {
        match self {
            AbstractOperationStateOneOf::AbstractOperationState(state) => {
                Self::AbstractOperationState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractOperationStateOneOf::SetContextStateOperationState(state) => {
                Self::SetContextStateOperationState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractOperationStateOneOf::SetValueOperationState(state) => {
                Self::SetValueOperationState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractOperationStateOneOf::SetComponentStateOperationState(state) => {
                Self::SetComponentStateOperationState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractOperationStateOneOf::SetMetricStateOperationState(state) => {
                Self::SetMetricStateOperationState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractOperationStateOneOf::SetAlertStateOperationState(state) => {
                Self::SetAlertStateOperationState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractOperationStateOneOf::SetStringOperationState(state) => {
                Self::SetStringOperationState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
            AbstractOperationStateOneOf::ActivateOperationState(state) => {
                Self::ActivateOperationState(
                    state.replace_in_copy(descriptor_version, state_version),
                )
            }
        }
    }

    fn replace(self, descriptor_version: u64, state_version: u64) -> Self {
        match self {
            AbstractOperationStateOneOf::AbstractOperationState(state) => {
                Self::AbstractOperationState(state.replace(descriptor_version, state_version))
            }
            AbstractOperationStateOneOf::SetContextStateOperationState(state) => {
                Self::SetContextStateOperationState(
                    state.replace(descriptor_version, state_version),
                )
            }
            AbstractOperationStateOneOf::SetValueOperationState(state) => {
                Self::SetValueOperationState(state.replace(descriptor_version, state_version))
            }
            AbstractOperationStateOneOf::SetComponentStateOperationState(state) => {
                Self::SetComponentStateOperationState(
                    state.replace(descriptor_version, state_version),
                )
            }
            AbstractOperationStateOneOf::SetMetricStateOperationState(state) => {
                Self::SetMetricStateOperationState(state.replace(descriptor_version, state_version))
            }
            AbstractOperationStateOneOf::SetAlertStateOperationState(state) => {
                Self::SetAlertStateOperationState(state.replace(descriptor_version, state_version))
            }
            AbstractOperationStateOneOf::SetStringOperationState(state) => {
                Self::SetStringOperationState(state.replace(descriptor_version, state_version))
            }
            AbstractOperationStateOneOf::ActivateOperationState(state) => {
                Self::ActivateOperationState(state.replace(descriptor_version, state_version))
            }
        }
    }
}

pub fn is_context_descriptor(descriptor: &AbstractDescriptorOneOf) -> bool {
    matches!(
        descriptor,
        AbstractDescriptorOneOf::MeansContextDescriptor(_)
            | AbstractDescriptorOneOf::WorkflowContextDescriptor(_)
            | AbstractDescriptorOneOf::PatientContextDescriptor(_)
            | AbstractDescriptorOneOf::OperatorContextDescriptor(_)
            | AbstractDescriptorOneOf::EnsembleContextDescriptor(_)
            | AbstractDescriptorOneOf::LocationContextDescriptor(_)
    )
}

pub fn is_context_pair(descriptor: &Pair) -> bool {
    matches!(descriptor, Pair::MultiStatePair { .. })
}

pub fn into_context_descriptor_one_of(
    descriptor: AbstractDescriptorOneOf,
) -> Result<Box<AbstractContextDescriptorOneOf>, Box<AbstractDescriptorOneOf>> {
    match descriptor {
        AbstractDescriptorOneOf::MeansContextDescriptor(desc) => Ok(Box::new(
            AbstractContextDescriptorOneOf::MeansContextDescriptor(desc),
        )),
        AbstractDescriptorOneOf::WorkflowContextDescriptor(desc) => Ok(Box::new(
            AbstractContextDescriptorOneOf::WorkflowContextDescriptor(desc),
        )),
        AbstractDescriptorOneOf::PatientContextDescriptor(desc) => Ok(Box::new(
            AbstractContextDescriptorOneOf::PatientContextDescriptor(desc),
        )),
        AbstractDescriptorOneOf::OperatorContextDescriptor(desc) => Ok(Box::new(
            AbstractContextDescriptorOneOf::OperatorContextDescriptor(desc),
        )),
        AbstractDescriptorOneOf::EnsembleContextDescriptor(desc) => Ok(Box::new(
            AbstractContextDescriptorOneOf::EnsembleContextDescriptor(desc),
        )),
        AbstractDescriptorOneOf::LocationContextDescriptor(desc) => Ok(Box::new(
            AbstractContextDescriptorOneOf::LocationContextDescriptor(desc),
        )),
        _ => Err(Box::new(descriptor)),
    }
}

pub fn into_context_state_one_of(
    state: AbstractStateOneOf,
) -> Result<AbstractContextStateOneOf, AbstractStateOneOf> {
    match state {
        AbstractStateOneOf::AbstractContextState(state) => {
            Ok(AbstractContextStateOneOf::AbstractContextState(state))
        }
        AbstractStateOneOf::OperatorContextState(state) => {
            Ok(AbstractContextStateOneOf::OperatorContextState(state))
        }
        AbstractStateOneOf::EnsembleContextState(state) => {
            Ok(AbstractContextStateOneOf::EnsembleContextState(state))
        }
        AbstractStateOneOf::WorkflowContextState(state) => {
            Ok(AbstractContextStateOneOf::WorkflowContextState(state))
        }
        AbstractStateOneOf::PatientContextState(state) => {
            Ok(AbstractContextStateOneOf::PatientContextState(state))
        }
        AbstractStateOneOf::LocationContextState(state) => {
            Ok(AbstractContextStateOneOf::LocationContextState(state))
        }
        AbstractStateOneOf::MeansContextState(state) => {
            Ok(AbstractContextStateOneOf::MeansContextState(state))
        }
        _ => Err(state),
    }
}

pub fn get_context_association(
    state: &AbstractStateOneOf,
) -> Option<(&Option<ContextAssociation>, String)> {
    match state {
        AbstractStateOneOf::AbstractContextState(state) => {
            Some((&state.context_association_attr, state.state_handle()))
        }
        AbstractStateOneOf::OperatorContextState(state) => Some((
            &state.abstract_context_state.context_association_attr,
            state.state_handle(),
        )),
        AbstractStateOneOf::EnsembleContextState(state) => Some((
            &state.abstract_context_state.context_association_attr,
            state.state_handle(),
        )),
        AbstractStateOneOf::WorkflowContextState(state) => Some((
            &state.abstract_context_state.context_association_attr,
            state.state_handle(),
        )),
        AbstractStateOneOf::PatientContextState(state) => Some((
            &state.abstract_context_state.context_association_attr,
            state.state_handle(),
        )),
        AbstractStateOneOf::LocationContextState(state) => Some((
            &state.abstract_context_state.context_association_attr,
            state.state_handle(),
        )),
        AbstractStateOneOf::MeansContextState(state) => Some((
            &state.abstract_context_state.context_association_attr,
            state.state_handle(),
        )),
        _ => None,
    }
}

impl CopyableDescriptor for AbstractDescriptor {
    fn replace_in_copy(&self, version: u64) -> Self {
        AbstractDescriptor {
            descriptor_version_attr: Some(VersionCounter {
                unsigned_long: version,
            }),
            ..self.clone()
        }
    }
}

impl CopyableState for AbstractState {
    fn replace_in_copy(&self, descriptor_version: u64, state_version: u64) -> Self {
        AbstractState {
            descriptor_version_attr: Some(ReferencedVersion {
                version_counter: VersionCounter {
                    unsigned_long: descriptor_version,
                },
            }),
            state_version_attr: Some(VersionCounter {
                unsigned_long: state_version,
            }),
            ..self.clone()
        }
    }

    fn replace(self, descriptor_version: u64, state_version: u64) -> Self {
        AbstractState {
            descriptor_version_attr: Some(ReferencedVersion {
                version_counter: VersionCounter {
                    unsigned_long: descriptor_version,
                },
            }),
            state_version_attr: Some(VersionCounter {
                unsigned_long: state_version,
            }),
            ..self
        }
    }
}
