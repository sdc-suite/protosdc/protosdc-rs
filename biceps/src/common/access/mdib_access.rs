use async_trait::async_trait;
use std::collections::HashMap;

use protosdc_biceps::biceps::{
    AbstractAlertStateOneOf, AbstractContextStateOneOf, AbstractDeviceComponentStateOneOf,
    AbstractMetricStateOneOf, AbstractOperationStateOneOf, RealTimeSampleArrayMetricState,
};
use tokio::sync::mpsc;

use crate::common::mdib_entity::MdibEntity;
use crate::common::mdib_version::MdibVersion;

/// Subscribe access to mdib change events.
#[async_trait]
pub trait ObservableMdib {
    /// Subscribes to the [MdibAccessEvent] stream.
    ///
    /// Returns a `Receiver` that can be used to receive [MdibAccessEvent] asynchronously. To
    /// unsubscribe, simply drop the receiver.
    async fn subscribe(&self) -> mpsc::Receiver<MdibAccessEvent>;

    /// Unsubscribes all subscribers of the mdib changes.
    async fn unsubscribe_all(&self);
}

/// Read access to an MDIB storage.
#[async_trait]
pub trait MdibAccess {
    /// The latest known MDIB version.
    ///
    /// The latest known MDIB version which in case of remote access may not necessarily reflect the MDIB version
    /// of the whole MDIB (e.g. if only a subset of all available reports is subscribed). In case of local access
    /// the returned version corresponds to the latest state.
    async fn mdib_version(&self) -> MdibVersion;

    /// The latest known medical device description version.
    ///
    /// The latest known medical device description version which in case of remote access
    /// may be outdated. In case of local access the returned version corresponds to the latest
    /// description.
    async fn md_description_version(&self) -> u64;

    /// The latest known medical device state version.
    ///
    /// The latest known medial device state version which in case of remote access may be outdated.
    /// In case of local access the returned version corresponds to the latest state.
    async fn md_state_version(&self) -> u64;

    /// Returns an entity for the given handle, if available.
    ///
    /// # Arguments
    ///
    /// * `handle`: the descriptor handle to retrieve the entity for
    async fn entity(&self, handle: &str) -> Option<MdibEntity>;

    /// Returns all available root entities (i.e. Mds).
    async fn root_entities(&self) -> Vec<MdibEntity>;

    /// Returns a context state for the given handle, if available.
    ///
    /// # Arguments
    ///
    /// * `handle`: the state handle of the context state to retrieve
    async fn context_state(&self, handle: &str) -> Option<AbstractContextStateOneOf>;

    /// Returns all available context states.
    async fn context_states(&self) -> Vec<AbstractContextStateOneOf>;

    /// Finds all entities matching the given type filter.
    ///
    /// # Arguments
    ///
    /// * `filter`: filter to execute for search, see [`crate::entity_filter`]
    async fn entities_by_type<F>(&self, filter: F) -> Vec<MdibEntity>
    where
        F: 'static + Fn(&MdibEntity) -> bool + Send + Sync;

    /// Finds all child entities for a handle matching the given type filter.
    ///
    /// # Arguments
    ///
    /// * `handle`: handle of the parent descriptor
    /// * `filter`: filter to execute for search, see [`crate::entity_filter`]
    async fn children_by_type<F>(&self, handle: &str, filter: F) -> Vec<MdibEntity>
    where
        F: 'static + Fn(&MdibEntity) -> bool + Send + Sync;
}

/// An enum of description and state change events communicated by the mdib.
#[derive(Debug, Clone)]
pub enum MdibAccessEvent {
    /// Modification of alert states
    ///
    /// * `mdib_version`: the mdib version in which the change occurred
    /// * `states`: the new alert states applied in that version and their source mds as key
    AlertStateModification {
        mdib_version: MdibVersion,
        states: HashMap<String, Vec<AbstractAlertStateOneOf>>,
    },

    /// Modification of component states
    ///
    /// * `mdib_version`: the mdib version in which the change occurred
    /// * `states`: the new component states applied in that version and their source mds as key
    ComponentStateModification {
        mdib_version: MdibVersion,
        states: HashMap<String, Vec<AbstractDeviceComponentStateOneOf>>,
    },

    /// Modification of context states
    ///
    /// * `mdib_version`: the mdib version in which the change occurred\
    /// * `removed_context_states`: context states that were removed from the mdib
    /// * `updated_context_states`: the new context states applied in that version and their source mds as key
    ContextStateModification {
        mdib_version: MdibVersion,
        removed_context_states: HashMap<String, Vec<AbstractContextStateOneOf>>,
        updated_context_states: HashMap<String, Vec<AbstractContextStateOneOf>>,
    },

    /// Modification of metric states
    ///
    /// * `mdib_version`: the mdib version in which the change occurred
    /// * `states`: the new metric states applied in that version and their source mds as key
    MetricStateModification {
        mdib_version: MdibVersion,
        states: HashMap<String, Vec<AbstractMetricStateOneOf>>,
    },

    /// Modification of operation states
    ///
    /// * `mdib_version`: the mdib version in which the change occurred
    /// * `states`: the new operation states applied in that version and their source mds as key
    OperationStateModification {
        mdib_version: MdibVersion,
        states: HashMap<String, Vec<AbstractOperationStateOneOf>>,
    },

    /// Modification of realtime sample array states
    ///
    /// * `mdib_version`: the mdib version in which the change occurred
    /// * `states`: the new realtime sample array states applied in that version
    WaveformModification {
        mdib_version: MdibVersion,
        states: Vec<RealTimeSampleArrayMetricState>,
    },

    /// Modification of the description and the respective states.
    ///
    /// * `mdib_version`: the mdib version in which the change occurred
    /// * `inserted`: the newly inserted entities
    /// * `updated`: the updated entities
    /// * `deleted`: the deleted entities
    DescriptionModification {
        mdib_version: MdibVersion,
        inserted: Vec<MdibEntity>,
        updated: Vec<MdibEntity>,
        deleted: Vec<MdibEntity>,
    },
}

impl MdibAccessEvent {
    pub fn mdib_version(&self) -> &MdibVersion {
        match self {
            MdibAccessEvent::AlertStateModification { mdib_version, .. } => mdib_version,
            MdibAccessEvent::ComponentStateModification { mdib_version, .. } => mdib_version,
            MdibAccessEvent::ContextStateModification { mdib_version, .. } => mdib_version,
            MdibAccessEvent::MetricStateModification { mdib_version, .. } => mdib_version,
            MdibAccessEvent::OperationStateModification { mdib_version, .. } => mdib_version,
            MdibAccessEvent::WaveformModification { mdib_version, .. } => mdib_version,
            MdibAccessEvent::DescriptionModification { mdib_version, .. } => mdib_version,
        }
    }
}
