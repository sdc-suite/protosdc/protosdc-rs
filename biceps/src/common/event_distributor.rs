use crate::common::access::mdib_access::MdibAccessEvent;
use crate::common::channel_publisher::ChannelPublisher;
use crate::common::mdib_entity::MdibEntity;
use crate::common::mdib_state_modifications::MdibStateModificationResults;
use crate::common::mdib_version::MdibVersion;

/// Distributes a description modification using a provided channel publisher.
///
/// # Arguments
///
/// * `channel_publisher`: to provide to
/// * `mdib_version`: mdib version after the change
/// * `inserted`: inserted entities
/// * `updated`: update entities
/// * `deleted`: deleted entities
pub async fn send_description_modification_event(
    channel_publisher: &ChannelPublisher<MdibAccessEvent>,
    mdib_version: MdibVersion,
    inserted: Vec<MdibEntity>,
    updated: Vec<MdibEntity>,
    deleted: Vec<MdibEntity>,
) {
    channel_publisher
        .send(MdibAccessEvent::DescriptionModification {
            mdib_version,
            inserted,
            updated,
            deleted,
        })
        .await
}

/// Distributes a state modification using a provided channel publisher.
///
/// # Arguments
///
/// * `channel_publisher`: to provide to
/// * `mdib_version`: mdib version after the change
/// * `states`: the state modification
pub async fn send_state_event(
    channel_publisher: &ChannelPublisher<MdibAccessEvent>,
    mdib_version: MdibVersion,
    states: MdibStateModificationResults,
) {
    match states {
        MdibStateModificationResults::Alert { alert_states } => {
            channel_publisher
                .send(MdibAccessEvent::AlertStateModification {
                    mdib_version,
                    states: alert_states,
                })
                .await
        }
        MdibStateModificationResults::Component { component_states } => {
            channel_publisher
                .send(MdibAccessEvent::ComponentStateModification {
                    mdib_version,
                    states: component_states,
                })
                .await
        }
        MdibStateModificationResults::Context {
            updated_context_states,
            removed_context_states,
        } => {
            channel_publisher
                .send(MdibAccessEvent::ContextStateModification {
                    mdib_version,
                    updated_context_states,
                    removed_context_states,
                })
                .await
        }
        MdibStateModificationResults::Metric { metric_states } => {
            channel_publisher
                .send(MdibAccessEvent::MetricStateModification {
                    mdib_version,
                    states: metric_states,
                })
                .await
        }
        MdibStateModificationResults::Operation { operation_states } => {
            channel_publisher
                .send(MdibAccessEvent::OperationStateModification {
                    mdib_version,
                    states: operation_states,
                })
                .await
        }
        MdibStateModificationResults::Waveform { waveform_states } => {
            channel_publisher
                .send(MdibAccessEvent::WaveformModification {
                    mdib_version,
                    states: waveform_states,
                })
                .await
        }
    }
}
