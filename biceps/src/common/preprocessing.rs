use std::fmt::Debug;
use std::marker::PhantomData;

use thiserror::Error;

use crate::common::mdib_description_modification::MdibDescriptionModification;
use crate::common::mdib_state_modifications::MdibStateModifications;
use crate::common::storage::mdib_storage::MdibStorage;

#[derive(Debug, PartialEq, Eq, Error)]
pub enum PreprocessingError {
    #[error("A cardinality error occurred, conflicts were {conflicts:?}")]
    CardinalityError { conflicts: Vec<(String, String)> },
    #[error("Duplicate context state handles detected: {handles:?}")]
    DuplicateContextStateHandle { handles: Vec<String> },
    #[error("Duplicate descriptor handles detected: {handles:?}")]
    DuplicateDescriptorHandle { handles: Vec<String> },
    #[error("Inconsistent parent child types for child handle: {handles:?}")]
    TypeConsistencyError { handles: Vec<String> },

    #[error("Handles have previously been used for other data types: {handles:?}")]
    TypeChangeError { handles: Vec<String> },

    // VersionHandler requires a bunch of specific errors
    #[error("Parent entity {parent} for child {child} was missing")]
    ParentEntityMissing { parent: String, child: String },
    #[error("Single state descriptor {parent} had {children} children, exactly 1 is allowed.")]
    SingleStateDescriptorCardinality { parent: String, children: usize },
    #[error("Preprocessing encountered a missing entity with handle {handle}")]
    EntityUnknown { handle: String },

    #[error("An unknown error occurred: {message}")]
    Unknown { message: String },
}

#[allow(unused_variables)] // it's a trait mate
/// Preprocessor which is applied during description modifications.
pub trait DescriptionPreprocessor<T: MdibStorage> {
    /// Function which is invoked before the first modification in the chain is applied.
    ///
    /// Danger: Do not modify the modifications in this segment!
    ///
    /// # Arguments
    ///
    /// * `mdib_storage`: storage on which the modifications will be applied
    /// * `modifications`: the unmodified modifications to be applied
    fn before_first_modification(
        &mut self,
        mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        Ok(modifications)
    }

    /// Function which is invoked after the all modification in the chain were applied.
    ///
    /// Danger: Do not modify the modifications in this segment!
    ///
    /// # Arguments
    ///
    /// * `mdib_storage`: storage on which the modifications will be applied
    /// * `modifications`: the possibly modified modifications to be applied
    fn after_last_modification(
        &mut self,
        mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        Ok(modifications)
    }

    /// Function which is invoked to process the modifications.
    ///
    /// Danger: Do not insert or remove modifications, changing modifications is fine.
    ///
    /// # Arguments
    ///
    /// * `mdib_storage`: storage on which the modifications will be applied
    /// * `modifications`: the modifications to be applied
    fn process(
        &mut self,
        mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        Ok(modifications)
    }
}

#[allow(unused_variables)] // it's a trait mate
/// Preprocessor which is applied during description modifications.
pub trait StatePreprocessor<T: MdibStorage> {
    /// Function which is invoked before the first modification in the chain is applied.
    ///
    /// Danger: Do not modify the modifications in this segment!
    ///
    /// # Arguments
    ///
    /// * `mdib_storage`: storage on which the modifications will be applied
    /// * `modifications`: the unmodified modifications to be applied
    fn before_first_modification(
        &mut self,
        mdib_storage: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError> {
        Ok(modifications)
    }

    /// Function which is invoked after the all modification in the chain were applied.
    ///
    /// Danger: Do not modify the modifications in this segment!
    ///
    /// # Arguments
    ///
    /// * `mdib_storage`: storage on which the modifications will be applied
    /// * `modifications`: the possibly modified modifications to be applied
    fn after_last_modification(
        &mut self,
        mdib_storage: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError> {
        Ok(modifications)
    }

    /// Function which is invoked to process the modifications.
    ///
    /// Danger: Do not insert or remove modifications, changing modifications is fine.
    ///
    /// # Arguments
    ///
    /// * `mdib_storage`: storage on which the modifications will be applied
    /// * `modifications`: the modifications to be applied
    fn process(
        &mut self,
        mdib_storage: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError> {
        Ok(modifications)
    }
}

/// Chains a variable amount of preprocessors into one [DescriptionPreprocessorSegment].
#[macro_export]
macro_rules! chain_descriptor_segment {
    // The pattern for a single `eval`
    (
        $first:expr, $second:expr, $($more:expr ),* $(,)?) => {
        {
            $crate::common::preprocessing::DescriptionPreprocessorSegment::create($first, chain_descriptor_segment!($second, $($more ),*))
        }
    };

    ($first:expr, $second:expr $(,)?) => {
        {
            $crate::common::preprocessing::DescriptionPreprocessorSegment::create($first, $second)
        }
    };
}

/// A `DescriptionPreprocessor` that calls two nested `DescriptionPreprocessor`s.
///
/// This struct represents a segment of the description preprocessor. It contains two processors,
/// `current` and `next`, which must implement the `DescriptionPreprocessor` trait. The `T` type parameter
/// represents the type of the `MdibStorage`. `DescriptionPreprocessorSegment` first calls the current segment,
/// then the next segment. Nested `DescriptionPreprocessorSegment` can be used to chain more processors.
///
/// # Type Parameters
/// - `CURR`: The processor type that will be called first.
/// - `NEXT`: The processor type that will be called second
/// - `T`: The type of the `MdibStorage`.
///
/// # Fields
/// - `current`: The processor that will be called first.
/// - `next`: The processor that will be called second.
/// - `phantom`: A PhantomData marker for the `MdibStorage` type parameter.
#[derive(Debug)]
pub struct DescriptionPreprocessorSegment<CURR, NEXT, T>
where
    CURR: DescriptionPreprocessor<T>,
    NEXT: DescriptionPreprocessor<T>,
    T: MdibStorage,
{
    current: CURR,
    next: NEXT,
    phantom: PhantomData<T>,
}

impl<CURR, NEXT, T> DescriptionPreprocessorSegment<CURR, NEXT, T>
where
    CURR: DescriptionPreprocessor<T>,
    NEXT: DescriptionPreprocessor<T>,
    T: MdibStorage,
{
    pub fn create(current: CURR, next: NEXT) -> Self {
        Self {
            current,
            next,
            phantom: PhantomData,
        }
    }
}

impl<CURR, NEXT, T> DescriptionPreprocessor<T> for DescriptionPreprocessorSegment<CURR, NEXT, T>
where
    CURR: DescriptionPreprocessor<T>,
    NEXT: DescriptionPreprocessor<T>,
    T: MdibStorage,
{
    fn before_first_modification(
        &mut self,
        mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        let modifications = self
            .current
            .before_first_modification(mdib_storage, modifications)?;
        self.next
            .before_first_modification(mdib_storage, modifications)
    }

    fn after_last_modification(
        &mut self,
        mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        let modifications = self
            .current
            .after_last_modification(mdib_storage, modifications)?;
        self.next
            .after_last_modification(mdib_storage, modifications)
    }

    fn process(
        &mut self,
        mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        let modifications = self.current.process(mdib_storage, modifications)?;
        self.next.process(mdib_storage, modifications)
    }
}

/// Chains a variable amount of preprocessors into one [StatePreprocessorSegment].
#[macro_export]
macro_rules! chain_state_segment {
    // The pattern for a single `eval`
    (
        $first:expr, $second:expr, $($more:expr ),* $(,)?) => {
        {
            $crate::common::preprocessing::StatePreprocessorSegment::create($first, chain_state_segment!($second, $($more ),*))
        }
    };

    ($first:expr, $second:expr $(,)?) => {
        {
            $crate::common::preprocessing::StatePreprocessorSegment::create($first, $second)
        }
    };
}

/// A `StatePreprocessor` that calls two nested `StatePreprocessor`s.
///
/// This struct represents a segment of the state preprocessor. It contains two processors,
/// `current` and `next`, which must implement the `StatePreprocessor` trait. The `T` type parameter
/// represents the type of the `MdibStorage`. `StatePreprocessorSegment` first calls the current segment,
/// then the next segment. Nested `StatePreprocessorSegment` can be used to chain more processors.
///
/// # Type Parameters
/// - `CURR`: The processor type that will be called first.
/// - `NEXT`: The processor type that will be called second
/// - `T`: The type of the `MdibStorage`.
///
/// # Fields
/// - `current`: The processor that will be called first.
/// - `next`: The processor that will be called second.
/// - `phantom`: A PhantomData marker for the `MdibStorage` type parameter.
#[derive(Debug)]
pub struct StatePreprocessorSegment<CURR, NEXT, T>
where
    CURR: StatePreprocessor<T>,
    NEXT: StatePreprocessor<T>,
    T: MdibStorage,
{
    current: CURR,
    next: NEXT,
    phantom: PhantomData<T>,
}

impl<CURR, NEXT, T> StatePreprocessorSegment<CURR, NEXT, T>
where
    CURR: StatePreprocessor<T>,
    NEXT: StatePreprocessor<T>,
    T: MdibStorage,
{
    pub fn create(current: CURR, next: NEXT) -> Self {
        Self {
            current,
            next,
            phantom: Default::default(),
        }
    }
}

impl<CURR, NEXT, T> StatePreprocessor<T> for StatePreprocessorSegment<CURR, NEXT, T>
where
    CURR: StatePreprocessor<T>,
    NEXT: StatePreprocessor<T>,
    T: MdibStorage,
{
    fn before_first_modification(
        &mut self,
        mdib_storage: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError> {
        let modifications = self
            .current
            .before_first_modification(mdib_storage, modifications)?;
        self.next
            .before_first_modification(mdib_storage, modifications)
    }

    fn after_last_modification(
        &mut self,
        mdib_storage: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError> {
        let modifications = self
            .current
            .after_last_modification(mdib_storage, modifications)?;
        self.next
            .after_last_modification(mdib_storage, modifications)
    }

    fn process(
        &mut self,
        mdib_storage: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError> {
        let modifications = self.current.process(mdib_storage, modifications)?;
        self.next.process(mdib_storage, modifications)
    }
}

/// Dummy processor that does nothing.
#[derive(Debug)]
pub struct DummyProcessor<T: MdibStorage> {
    phantom: PhantomData<T>,
}

impl<T: MdibStorage> StatePreprocessor<T> for DummyProcessor<T> {
    fn before_first_modification(
        &mut self,
        _: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError> {
        Ok(modifications)
    }

    fn after_last_modification(
        &mut self,
        _: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError> {
        Ok(modifications)
    }

    fn process(
        &mut self,
        _: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError> {
        Ok(modifications)
    }
}

impl<T: MdibStorage> DescriptionPreprocessor<T> for DummyProcessor<T> {
    fn before_first_modification(
        &mut self,
        _mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        Ok(modifications)
    }

    fn after_last_modification(
        &mut self,
        _mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        Ok(modifications)
    }

    fn process(
        &mut self,
        _mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        Ok(modifications)
    }
}

impl<T: MdibStorage> Default for DummyProcessor<T> {
    fn default() -> Self {
        DummyProcessor {
            phantom: PhantomData,
        }
    }
}
