use crate::common::context::context_identification_mapper::{
    from_instance_identifier, ContextIdentificationMapperError, ContextSource,
};
use crate::common::context::uri::encode_pchar;
use const_format::concatcp;
use lazy_static::lazy_static;
use protosdc_biceps::biceps::InstanceIdentifier;
use regex::Regex;
use std::ops::Add;
use thiserror::Error;

// The "&" character had to be excluded from the "pchar" definition as it already is used a delimiter.
// This is a bug in the GLUE standard. The "&" should not be allowed, except as a delimiter.
const QUERY_ITEM_SEGMENT: &str = "(?:(?:%[a-fA-F0-9]{2})+|(?:[a-zA-Z0-9\\-._~!$'()*+,;=:@])+)";

const FACILITY_FRAGMENT: &str = "fac";
const BUILDING_FRAGMENT: &str = "bldng";
const POINT_OF_CARE_FRAGMENT: &str = "poc";
const FLOOR_FRAGMENT: &str = "flr";
const ROOM_FRAGMENT: &str = "rm";
const BED_FRAGMENT: &str = "bed";

// fmt makes this absolutely unreadable
#[rustfmt::skip]
const QUERY_ITEM : &str= concatcp!("((", FACILITY_FRAGMENT, "=", QUERY_ITEM_SEGMENT, "+)|",
    "(", BUILDING_FRAGMENT, "=", QUERY_ITEM_SEGMENT, "+)|",
    "(", POINT_OF_CARE_FRAGMENT, "=", QUERY_ITEM_SEGMENT, "+)|",
    "(", FLOOR_FRAGMENT, "=", QUERY_ITEM_SEGMENT, "+)|",
    "(", ROOM_FRAGMENT, "=", QUERY_ITEM_SEGMENT, "+)|",
    "(", BED_FRAGMENT, "=", QUERY_ITEM_SEGMENT, "+))");
const LOC_CTXT_QUERY: &str = concatcp!("^(", QUERY_ITEM, "(&", QUERY_ITEM, ")*)?$");

lazy_static! {
    pub(crate) static ref LOC_CTXT_QUERY_PARSER: Regex = Regex::new(LOC_CTXT_QUERY).unwrap();
}

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    UrlError(#[from] url::ParseError),
    #[error("Query does not match expected regex")]
    InvalidQueryRegex,
    #[error("Query fragment {0} is not allowed in location")]
    InvalidQueryFragment(String),
    #[error(transparent)]
    ContextIdentificationMapperError(#[from] ContextIdentificationMapperError),
}

#[derive(Debug, Default, PartialEq)]
pub struct LocationDetail {
    facility: Option<String>,
    building: Option<String>,
    point_of_care: Option<String>,
    floor: Option<String>,
    room: Option<String>,
    bed: Option<String>,
}

fn create_query_item(key: &str, value: &str) -> String {
    format!("{}={}", key, encode_pchar(value, true))
}

impl From<protosdc_biceps::biceps::LocationDetail> for LocationDetail {
    fn from(value: protosdc_biceps::biceps::LocationDetail) -> Self {
        Self {
            facility: value.facility_attr,
            building: value.building_attr,
            point_of_care: value.po_c_attr,
            floor: value.floor_attr,
            room: value.room_attr,
            bed: value.bed_attr,
        }
    }
}

impl LocationDetail {
    fn create_query(&self) -> Option<String> {
        let mut output = String::new();

        if let Some(fac) = &self.facility {
            output = output.add(&create_query_item(FACILITY_FRAGMENT, fac));
        }
        if let Some(bld) = &self.building {
            if !output.is_empty() {
                output = output.add("&");
            }
            output = output.add(&create_query_item(BUILDING_FRAGMENT, bld));
        }
        if let Some(fac) = &self.point_of_care {
            if !output.is_empty() {
                output = output.add("&");
            }
            output = output.add(&create_query_item(POINT_OF_CARE_FRAGMENT, fac));
        }
        if let Some(flr) = &self.floor {
            if !output.is_empty() {
                output = output.add("&");
            }
            output = output.add(&create_query_item(FLOOR_FRAGMENT, flr));
        }
        if let Some(rm) = &self.room {
            if !output.is_empty() {
                output = output.add("&");
            }
            output = output.add(&create_query_item(ROOM_FRAGMENT, rm));
        }
        if let Some(bed) = &self.bed {
            if !output.is_empty() {
                output = output.add("&");
            }
            output = output.add(&create_query_item(BED_FRAGMENT, bed));
        }

        if output.is_empty() {
            None
        } else {
            Some(output)
        }
    }
}

pub fn create_with_location_detail_query(
    instance_identifier: &InstanceIdentifier,
    location_detail: &LocationDetail,
) -> Result<String, Error> {
    let uri = from_instance_identifier(instance_identifier, ContextSource::Location)?;
    let query = location_detail.create_query();
    let query_fragment = match query {
        None => "".to_string(),
        Some(q) => format!("?{}", q),
    };

    let resulting_uri = format!("{}{}", uri, query_fragment);

    // validate
    parse_location_detail(&resulting_uri)?;

    Ok(resulting_uri)
}

pub fn parse_location_detail(input: &str) -> Result<LocationDetail, Error> {
    let parsed_url = url::Url::parse(input)?;
    let query = match parsed_url.query() {
        Some(q) => q,
        None => return Ok(LocationDetail::default()),
    };

    if !LOC_CTXT_QUERY_PARSER.is_match(query) {
        return Err(Error::InvalidQueryRegex);
    };

    let mut detail = LocationDetail::default();

    for (key, value) in parsed_url.query_pairs() {
        match &*key {
            FACILITY_FRAGMENT => detail.facility = Some(value.to_string()),
            BUILDING_FRAGMENT => detail.building = Some(value.to_string()),
            POINT_OF_CARE_FRAGMENT => detail.point_of_care = Some(value.to_string()),
            FLOOR_FRAGMENT => detail.floor = Some(value.to_string()),
            ROOM_FRAGMENT => detail.room = Some(value.to_string()),
            BED_FRAGMENT => detail.bed = Some(value.to_string()),
            _ => return Err(Error::InvalidQueryFragment(key.to_string())),
        }
    }

    Ok(detail)
}

#[cfg(test)]
mod test {
    use crate::common::context::location_detail_query_mapper::{
        create_with_location_detail_query, parse_location_detail, LocationDetail,
    };
    use protosdc_biceps::biceps::instance_identifier_mod::{ExtensionAttr, RootAttr};
    use protosdc_biceps::biceps::InstanceIdentifier;
    use protosdc_biceps::types::ProtoUri;

    fn create_instance_identifier(
        root: Option<&str>,
        extension: Option<&str>,
    ) -> InstanceIdentifier {
        InstanceIdentifier {
            extension_element: None,
            r#type: None,
            identifier_name: vec![],
            root_attr: root.map(|it| RootAttr {
                any_u_r_i: ProtoUri {
                    uri: it.to_string(),
                },
            }),
            extension_attr: extension.map(|it| ExtensionAttr {
                string: it.to_string(),
            }),
        }
    }

    fn create_location_detail(
        facility: Option<&str>,
        building: Option<&str>,
        floor: Option<&str>,
        poc: Option<&str>,
        room: Option<&str>,
        bed: Option<&str>,
    ) -> LocationDetail {
        LocationDetail {
            facility: facility.map(|it| it.to_string()),
            building: building.map(|it| it.to_string()),
            point_of_care: poc.map(|it| it.to_string()),
            floor: floor.map(|it| it.to_string()),
            room: room.map(|it| it.to_string()),
            bed: bed.map(|it| it.to_string()),
        }
    }

    #[test]
    fn test_parse_location_detail() -> anyhow::Result<()> {
        {
            let data = "sdc.ctxt.loc:/http:%2F%2Froot/?fac=facility1%26&bldng=building1&poc=poc1&flr=floor1&rm=room1&bed=bed1";
            let parsed = parse_location_detail(data)?;

            assert_eq!(Some("facility1&".to_string()), parsed.facility);
            assert_eq!(Some("building1".to_string()), parsed.building);
            assert_eq!(Some("poc1".to_string()), parsed.point_of_care);
            assert_eq!(Some("floor1".to_string()), parsed.floor);
            assert_eq!(Some("room1".to_string()), parsed.room);
            assert_eq!(Some("bed1".to_string()), parsed.bed);
        }
        {
            let data = "sdc.ctxt.loc:/http:%2F%2Froot/?";
            let parsed = parse_location_detail(data)?;

            assert!(parsed.facility.is_none());
            assert!(parsed.building.is_none());
            assert!(parsed.point_of_care.is_none());
            assert!(parsed.floor.is_none());
            assert!(parsed.room.is_none());
            assert!(parsed.bed.is_none());
        }
        Ok(())
    }

    #[test]
    fn test_create_with_location_detail_query() -> anyhow::Result<()> {
        {
            let instance_identifier = create_instance_identifier(Some("http://root"), None);

            {
                let location_detail = create_location_detail(
                    Some("facility1&"),
                    Some("building1"),
                    Some("floor1"),
                    Some("poc1"),
                    Some("room1"),
                    Some("bed1"),
                );
                let actual_uri =
                    create_with_location_detail_query(&instance_identifier, &location_detail)?;
                assert_eq!(
                    "sdc.ctxt.loc:/http:%2F%2Froot/?fac=facility1%26&bldng=building1&poc=poc1&flr=floor1&rm=room1&bed=bed1",
                    &actual_uri
                )
            }
            {
                let location_detail = create_location_detail(
                    Some("facility1"),
                    None,
                    Some("floor1"),
                    Some("poc1"),
                    Some("room1"),
                    None,
                );
                let actual_uri =
                    create_with_location_detail_query(&instance_identifier, &location_detail)?;
                assert_eq!(
                    "sdc.ctxt.loc:/http:%2F%2Froot/?fac=facility1&poc=poc1&flr=floor1&rm=room1",
                    &actual_uri
                )
            }
            {
                let location_detail = create_location_detail(None, None, None, None, None, None);
                let actual_uri =
                    create_with_location_detail_query(&instance_identifier, &location_detail)?;
                assert_eq!("sdc.ctxt.loc:/http:%2F%2Froot/", &actual_uri)
            }
        }
        {
            let location_detail = create_location_detail(None, None, None, None, None, None);
            {
                let instance_identifier =
                    create_instance_identifier(Some("sdc.ctxt.loc.detail"), Some("//lol"));
                let actual_uri =
                    create_with_location_detail_query(&instance_identifier, &location_detail)?;
                assert_eq!("sdc.ctxt.loc:/sdc.ctxt.loc.detail/%2F%2Flol", &actual_uri)
            }

            {
                let instance_identifier =
                    create_instance_identifier(Some("sdc.ctxt.loc.detail"), Some("%2F%2Flol"));
                let actual_uri =
                    create_with_location_detail_query(&instance_identifier, &location_detail)?;
                assert_eq!(
                    "sdc.ctxt.loc:/sdc.ctxt.loc.detail/%252F%252Flol",
                    &actual_uri
                )
            }
        }

        Ok(())
    }
}
