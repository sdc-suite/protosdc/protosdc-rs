use crate::common::context::uri::encode_pchar;
use protosdc_biceps::biceps::instance_identifier_mod::{ExtensionAttr, RootAttr};
use protosdc_biceps::biceps::{InstanceIdentifier, LocationDetail};
use protosdc_biceps::types::ProtoUri;

fn get_and_encode_or_empty_string(field: &Option<String>) -> String {
    match field {
        None => "".to_string(),
        Some(it) => encode_pchar(it.as_str(), false),
    }
}

pub fn create_fallback_instance_identifier(
    location_detail: &LocationDetail,
) -> Option<InstanceIdentifier> {
    let extension = format!(
        "{}/{}/{}/{}/{}/{}",
        get_and_encode_or_empty_string(&location_detail.facility_attr),
        get_and_encode_or_empty_string(&location_detail.building_attr),
        get_and_encode_or_empty_string(&location_detail.floor_attr),
        get_and_encode_or_empty_string(&location_detail.po_c_attr),
        get_and_encode_or_empty_string(&location_detail.room_attr),
        get_and_encode_or_empty_string(&location_detail.bed_attr),
    );

    if "/////" != &extension {
        Some(InstanceIdentifier {
            extension_element: None,
            r#type: None,
            identifier_name: vec![],
            root_attr: Some(RootAttr {
                any_u_r_i: ProtoUri {
                    uri: "sdc.ctxt.loc.detail".to_string(),
                },
            }),
            extension_attr: Some(ExtensionAttr { string: extension }),
        })
    } else {
        None
    }
}

#[cfg(test)]
mod test {
    use crate::common::context::fallback_instance_identifier::create_fallback_instance_identifier;
    use protosdc_biceps::biceps::{InstanceIdentifier, LocationDetail};

    const EXPECTED_ROOT: &str = "sdc.ctxt.loc.detail";

    fn create(
        facility: Option<&str>,
        building: Option<&str>,
        floor: Option<&str>,
        poc: Option<&str>,
        room: Option<&str>,
        bed: Option<&str>,
    ) -> Option<InstanceIdentifier> {
        let detail = LocationDetail {
            extension_element: None,
            facility_attr: facility.map(|it| it.to_string()),
            building_attr: building.map(|it| it.to_string()),
            po_c_attr: poc.map(|it| it.to_string()),
            floor_attr: floor.map(|it| it.to_string()),
            room_attr: room.map(|it| it.to_string()),
            bed_attr: bed.map(|it| it.to_string()),
        };
        create_fallback_instance_identifier(&detail)
    }

    #[test]
    fn fully_populated() -> anyhow::Result<()> {
        let identifier = create(
            Some("Gebäude"),
            Some("?build ing"),
            Some("Flo%r+"),
            Some("p/o/c"),
            Some("room"),
            Some("Bed%"),
        )
        .expect("Identifier");
        let expected_extension = "Geb%C3%A4ude/%3Fbuild%20ing/Flo%25r+/p%2Fo%2Fc/room/Bed%25";

        assert_eq!(
            EXPECTED_ROOT,
            &identifier.root_attr.expect("root").any_u_r_i.uri
        );
        assert_eq!(
            expected_extension,
            &identifier.extension_attr.expect("extension").string
        );

        Ok(())
    }

    #[test]
    fn empty_segments_in_location_detail() -> anyhow::Result<()> {
        {
            let identifier = create(
                Some(""),
                Some("building"),
                Some(""),
                Some("poc"),
                Some("room"),
                Some(""),
            )
            .expect("Identifier");
            let expected_extension = "/building//poc/room/";
            assert_eq!(
                EXPECTED_ROOT,
                &identifier.root_attr.expect("root").any_u_r_i.uri
            );
            assert_eq!(
                expected_extension,
                &identifier.extension_attr.expect("extension").string
            );
        }
        {
            let identifier = create(
                None,
                Some("building"),
                Some(""),
                Some("poc"),
                Some("room"),
                None,
            )
            .expect("Identifier");
            let expected_extension = "/building//poc/room/";
            assert_eq!(
                EXPECTED_ROOT,
                &identifier.root_attr.expect("root").any_u_r_i.uri
            );
            assert_eq!(
                expected_extension,
                &identifier.extension_attr.expect("extension").string
            );
        }
        Ok(())
    }

    #[test]
    fn empty_location_detail() -> anyhow::Result<()> {
        {
            let identifier = create(Some(""), Some(""), Some(""), Some(""), Some(""), Some(""));
            assert!(identifier.is_none());
        }
        {
            let identifier = create(None, None, None, None, None, None);
            assert!(identifier.is_none());
        }
        Ok(())
    }

    #[test]
    fn special_characters() -> anyhow::Result<()> {
        {
            let identifier = create(
                Some("Gebäude"),
                Some("?build ing"),
                Some("Flo%r+"),
                Some("p/o/c"),
                Some("room"),
                Some("Bed%"),
            )
            .expect("Identifier");
            let expected_extension = "Geb%C3%A4ude/%3Fbuild%20ing/Flo%25r+/p%2Fo%2Fc/room/Bed%25";
            assert_eq!(
                EXPECTED_ROOT,
                &identifier.root_attr.expect("root").any_u_r_i.uri
            );
            assert_eq!(
                expected_extension,
                &identifier.extension_attr.expect("extension").string
            );
        }
        Ok(())
    }
}
