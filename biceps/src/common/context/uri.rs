use percent_encoding_rfc3986::{
    percent_decode_str, utf8_percent_encode, AsciiSet, NON_ALPHANUMERIC,
};
use std::str::Utf8Error;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum UriUtilError {
    #[error("Invalid URL")]
    Utf8Error(#[from] Utf8Error),
    #[error("Could not parse url: {reason}")]
    PercentDecodeError { reason: String },
}

const ASCII_SET_NO_PERCENT: &AsciiSet = &NON_ALPHANUMERIC
    .remove(b'-')
    .remove(b'.')
    .remove(b'_')
    .remove(b'~')
    .remove(b'!')
    .remove(b'$')
    .remove(b'&')
    .remove(b'\'')
    .remove(b'(')
    .remove(b')')
    .remove(b'*')
    .remove(b'+')
    .remove(b',')
    .remove(b';')
    .remove(b'=')
    .remove(b':')
    .remove(b'@');

const ASCII_SET_ENCODE_PERCENT: &AsciiSet = &ASCII_SET_NO_PERCENT.add(b'&');

pub fn encode_pchar(text: &str, escape_ampersand: bool) -> String {
    if text.is_empty() {
        return String::new();
    }

    match escape_ampersand {
        true => utf8_percent_encode(text, ASCII_SET_ENCODE_PERCENT),
        false => utf8_percent_encode(text, ASCII_SET_NO_PERCENT),
    }
    .to_string()
}

pub fn decode_pchar(text: &str) -> Result<String, UriUtilError> {
    Ok(percent_decode_str(text)
        .map_err(|err| UriUtilError::PercentDecodeError {
            reason: err.to_string(),
        })?
        .decode_utf8()?
        .to_string())
}

#[cfg(test)]
mod test {
    use crate::common::context::uri::{decode_pchar, encode_pchar};

    //noinspection SpellCheckingInspection
    #[test]
    fn test_valid() -> anyhow::Result<()> {
        {
            // unescaped characters
            let input_data = "abcdefg-._~!$&'()*+,;=:@";
            let encoded = encode_pchar(input_data, false);
            let decoded = decode_pchar(&encoded).expect("Invalid URL was not expected here");

            assert_eq!(input_data, &encoded);
            assert_eq!(input_data, &decoded);
        }
        {
            // don't escape ampersand
            let input_data = "&";
            let encoded = encode_pchar(input_data, false);
            let decoded = decode_pchar(&encoded).expect("Invalid URL was not expected here");

            assert_eq!(input_data, &encoded);
            assert_eq!(input_data, &decoded);
        }

        {
            // escape ampersand
            let input_data = "abcdefg-._~!$&'()*+,;=:@";
            let encoded = encode_pchar(input_data, true);
            let decoded = decode_pchar(&encoded).expect("Invalid URL was not expected here");

            assert_eq!("abcdefg-._~!$%26'()*+,;=:@", &encoded);
            assert_eq!(input_data, &decoded);
        }

        let to_be_escaped_inputs = vec![
            // naughty characters
            "Ṱ̺̺̕o͞ ̷i̲̬͇̪͙n̝̗͕v̟̜̘̦͟o̶̙̰̠kè͚̮̺̪̹̱̤ ̖t̝͕̳̣̻̪͞h̼͓̲̦̳̘̲e͇̣̰̦̬͎ ̢̼̻̱̘h͚͎͙̜̣̲ͅi̦̲̣̰̤v̻͍e̺̭̳̪̰-m̢iͅn̖̺̞̲̯̰d̵̼̟͙̩̼̘̳ ̞̥̱̳̭r̛̗̘e͙p͠r̼̞̻̭̗e̺̠̣͟s̘͇̳͍̝͉e͉̥̯̞̲͚̬͜ǹ̬͎͎̟̖͇̤t͍̬̤͓̼̭͘ͅi̪̱n͠g̴͉ ͏͉ͅc̬̟h͡a̫̻̯͘o̫̟̖͍̙̝͉s̗̦̲.̨̹͈̣\n\
            ̡͓̞ͅI̗̘̦͝n͇͇͙v̮̫ok̲̫̙͈i̖͙̭̹̠̞n̡̻̮̣̺g̲͈͙̭͙̬͎ ̰t͔̦h̞̲e̢̤ ͍̬̲͖f̴̘͕̣è͖ẹ̥̩l͖͔͚i͓͚̦͠n͖͍̗͓̳̮g͍ ̨o͚̪͡f̘̣̬ ̖̘͖̟͙̮c҉͔̫͖͓͇͖ͅh̵̤̣͚͔á̗̼͕ͅo̼̣̥s̱͈̺̖̦̻͢.̛̖̞̠̫̰\n\
            ̗̺͖̹̯͓Ṯ̤͍̥͇͈h̲́e͏͓̼̗̙̼̣͔ ͇̜̱̠͓͍ͅN͕͠e̗̱z̘̝̜̺͙p̤̺̹͍̯͚e̠̻̠͜r̨̤͍̺̖͔̖̖d̠̟̭̬̝͟i̦͖̩͓͔̤a̠̗̬͉̙n͚͜ ̻̞̰͚ͅh̵͉i̳̞v̢͇ḙ͎͟-҉̭̩̼͔m̤̭̫i͕͇̝̦n̗͙ḍ̟ ̯̲͕͞ǫ̟̯̰̲͙̻̝f ̪̰̰̗̖̭̘͘c̦͍̲̞͍̩̙ḥ͚a̮͎̟̙͜ơ̩̹͎s̤.̝̝ ҉Z̡̖̜͖̰̣͉̜a͖̰͙̬͡l̲̫̳͍̩g̡̟̼̱͚̞̬ͅo̗͜.̟\n\
            H̬̤̗̤͝e͜ ̜̥̝̻͍̟́w̕h̖̯͓o̝͙̖͎̱̮ ҉̺̙̞̟͈W̷̼̭a̺̪͍į͈͕̭͙̯̜t̶̼̮s̘͙͖̕ ̠̫̠B̻͍͙͉̳ͅe̵h̵̬͇̫͙i̹͓̳̳̮͎̫̕n͟d̴̪̜̖ ̰͉̩͇͙̲͞ͅT͖̼͓̪͢h͏͓̮̻e̬̝̟ͅ ̤̹̝W͙̞̝͔͇͝ͅa͏͓͔̹̼̣l̴͔̰̤̟͔ḽ̫.͕\n\
            Z̮̞̠͙͔ͅḀ̗̞͈̻̗Ḷ͙͎̯̹̞͓G̻O̭̗̮",
            "ü",
            // escaped characters
            "////",
        ];

        for input in to_be_escaped_inputs {
            let encoded = encode_pchar(input, false);
            let decoded = decode_pchar(&encoded).expect("Invalid URL was not expected here");

            assert_eq!(input, &decoded);
        }

        Ok(())
    }

    #[test]
    fn test_unnecessarily_encoded_characters() -> anyhow::Result<()> {
        let encoded = "%41%42%43";
        let decoded = decode_pchar(encoded).expect("Error not expected here");
        assert_eq!("ABC", decoded);
        Ok(())
    }
}
