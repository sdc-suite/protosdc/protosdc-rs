use log::{debug, info};
use std::time::{Duration, Instant};

use futures::StreamExt;
use protosdc_biceps::biceps::AbstractStateOneOf;

use biceps::common::access::mdib_access::MdibAccessEvent;
use biceps::common::biceps_util::MetricState;
use biceps::common::mdib_entity::Entity;
use biceps::common::mdib_state_modifications::MdibStateModifications;
use biceps::consumer::access::remote_mdib_access::RemoteMdibAccess;
use biceps::provider::access::local_mdib_access::LocalMdibAccess;
use biceps::test_util::handles;
use tokio_stream::wrappers::ReceiverStream;

pub async fn test_metric_change<REM, LOC>(remote_mdib: REM, local_mdib: LOC) -> anyhow::Result<()>
where
    REM: RemoteMdibAccess,
    LOC: LocalMdibAccess + Send + Sync + Clone + 'static,
{
    let num_messages = 10000;

    let remote_device_sub = remote_mdib.subscribe().await;
    let sub_collection_task = tokio::spawn(async move {
        let actual_messages = ReceiverStream::new(remote_device_sub)
            .take(num_messages)
            .collect::<Vec<MdibAccessEvent>>()
            .await;
        info!(
            "Received {} messages in sub_collection_task",
            actual_messages.len()
        );
        actual_messages
    });

    let start = Instant::now();

    // start updating before the connection is done
    let task_mdib = local_mdib.clone();
    let update_task = tokio::spawn(async move {
        let mut locked = task_mdib;
        // update METRIC_0
        for i in 0..num_messages {
            debug!("Updating METRIC_0 in provider to {}", i);
            let metric = locked
                .entity(handles::METRIC_0)
                .await
                .expect("METRIC_0 missing");

            let metric_state = metric.resolve_single_state().expect("state missing");
            let modifications = match metric_state {
                AbstractStateOneOf::NumericMetricState(mut state) => {
                    state.active_averaging_period_attr = Some(Duration::from_secs(i as u64).into());
                    MdibStateModifications::Metric {
                        metric_states: vec![state.into_abstract_metric_state_one_of()],
                    }
                }
                _ => panic!("Wrong state type"),
            };

            locked
                .write_states(modifications)
                .await
                .expect("Write failed");
        }
    });

    update_task.await.unwrap();
    let received_messages = sub_collection_task.await.unwrap();

    assert_eq!(num_messages, received_messages.len());

    // check that every message has a SourceMds
    for message in received_messages {
        match message {
            MdibAccessEvent::MetricStateModification { states, .. } => {
                assert_eq!(1, states.keys().len());
                assert_eq!(handles::MDS_0, states.keys().next().unwrap())
            }
            _ => panic!("Unexpected change"),
        }
    }

    let task_done = start.elapsed();
    info!(
        "Sending {} messages took {}ms end-to-end",
        num_messages,
        task_done.as_millis()
    );

    // assert metric is equal on both sides
    let device_entity = { local_mdib.entity(handles::METRIC_0).await.unwrap() };
    let remote_entity = remote_mdib.entity(handles::METRIC_0).await.unwrap();

    let device_state = match device_entity.entity {
        Entity::NumericMetric(met) => met.pair.state,
        _ => panic!("wrong type"),
    };

    let remote_state = match remote_entity.entity {
        Entity::NumericMetric(met) => met.pair.state,
        _ => panic!("wrong type"),
    };

    assert_eq!(
        device_state.active_averaging_period_attr,
        remote_state.active_averaging_period_attr
    );

    Ok(())
}
