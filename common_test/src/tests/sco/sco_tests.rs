use biceps::common::biceps_util::{ContextState, MetricState};
use protosdc_biceps::biceps::instance_identifier_mod::{ExtensionAttr, RootAttr};
use protosdc_biceps::biceps::{
    alert_activation_mod, AbstractAlertState, AbstractAlertStateOneOf, AbstractContextState,
    AbstractDeviceComponentState, AbstractDeviceComponentStateOneOf, AbstractMetricState,
    AbstractMultiState, AbstractSet, AbstractSetOneOf, AbstractState, Activate, AlertActivation,
    AlertConditionState, ClockState, Handle, HandleRef, InstanceIdentifierOneOf, InvocationState,
    MeansContextState, NumericMetricState, SetAlertState, SetComponentState, SetContextState,
    SetMetricState, SetString, SetValue,
};
use protosdc_biceps::types::{ProtoDecimal, ProtoUri};

use biceps::test_util::handles;
use protosdc::consumer::sco::sco_controller::{ScoController, SetServiceAccess, SetServiceHandler};
use protosdc::consumer::sco::sco_transaction::ScoTransaction;
use protosdc::provider::common::{ANONYMOUS_EXTENSION, ROOT_SDC};

use crate::crypto::GeneratedCerts;

pub fn get_activate() -> Activate {
    Activate {
        abstract_set: AbstractSet {
            extension_element: None,
            operation_handle_ref: HandleRef {
                string: handles::OPERATION_0.to_string(),
            },
        },
        argument: vec![],
    }
}

pub fn get_set_alert_state() -> SetAlertState {
    SetAlertState {
        abstract_set: AbstractSet {
            extension_element: None,
            operation_handle_ref: HandleRef {
                string: handles::OPERATION_1.to_string(),
            },
        },
        proposed_alert_state: AbstractAlertStateOneOf::AlertConditionState(AlertConditionState {
            abstract_alert_state: AbstractAlertState {
                abstract_state: AbstractState {
                    extension_element: None,
                    state_version_attr: None,
                    descriptor_handle_attr: HandleRef {
                        string: "whatever".to_string(),
                    },
                    descriptor_version_attr: None,
                },
                activation_state_attr: AlertActivation {
                    enum_type: alert_activation_mod::EnumType::Off,
                },
            },
            actual_condition_generation_delay_attr: None,
            actual_priority_attr: None,
            rank_attr: None,
            presence_attr: None,
            determination_time_attr: None,
        }),
    }
}

pub fn get_set_component_state() -> SetComponentState {
    SetComponentState {
        abstract_set: AbstractSet {
            extension_element: None,
            operation_handle_ref: HandleRef {
                string: handles::OPERATION_2.to_string(),
            },
        },
        proposed_component_state: vec![AbstractDeviceComponentStateOneOf::ClockState(ClockState {
            abstract_device_component_state: AbstractDeviceComponentState {
                abstract_state: AbstractState {
                    extension_element: None,
                    state_version_attr: None,
                    descriptor_handle_attr: HandleRef {
                        string: "cute_handle".to_string(),
                    },
                    descriptor_version_attr: None,
                },
                calibration_info: None,
                next_calibration: None,
                physical_connector: None,
                activation_state_attr: None,
                operating_hours_attr: None,
                operating_cycles_attr: None,
            },
            active_sync_protocol: None,
            reference_source: vec![],
            date_and_time_attr: None,
            remote_sync_attr: false,
            accuracy_attr: None,
            last_set_attr: None,
            time_zone_attr: None,
            critical_use_attr: None,
        })],
    }
}

pub fn get_set_context_state() -> SetContextState {
    SetContextState {
        abstract_set: AbstractSet {
            extension_element: None,
            operation_handle_ref: HandleRef {
                string: handles::OPERATION_3.to_string(),
            },
        },
        proposed_context_state: vec![MeansContextState {
            abstract_context_state: AbstractContextState {
                abstract_multi_state: AbstractMultiState {
                    abstract_state: AbstractState {
                        extension_element: None,
                        state_version_attr: None,
                        descriptor_handle_attr: HandleRef {
                            string: "blabla".to_string(),
                        },
                        descriptor_version_attr: None,
                    },
                    category: None,
                    handle_attr: Handle {
                        string: "cute".to_string(),
                    },
                },
                validator: vec![],
                identification: vec![],
                context_association_attr: None,
                binding_mdib_version_attr: None,
                unbinding_mdib_version_attr: None,
                binding_start_time_attr: None,
                binding_end_time_attr: None,
            },
        }
        .into_abstract_context_state_one_of()],
    }
}

pub fn get_set_metric_state() -> SetMetricState {
    SetMetricState {
        abstract_set: AbstractSet {
            extension_element: None,
            operation_handle_ref: HandleRef {
                string: handles::OPERATION_4.to_string(),
            },
        },
        proposed_metric_state: vec![NumericMetricState {
            abstract_metric_state: AbstractMetricState {
                abstract_state: AbstractState {
                    extension_element: None,
                    state_version_attr: None,
                    descriptor_handle_attr: HandleRef {
                        string: "my_metric_brings_all_the_fails_to_the_yard".to_string(),
                    },
                    descriptor_version_attr: None,
                },
                body_site: vec![],
                physical_connector: None,
                activation_state_attr: None,
                active_determination_period_attr: None,
                life_time_period_attr: None,
            },
            metric_value: None,
            physiological_range: vec![],
            active_averaging_period_attr: None,
        }
        .into_abstract_metric_state_one_of()],
    }
}

pub fn get_set_string() -> SetString {
    SetString {
        abstract_set: AbstractSet {
            extension_element: None,
            operation_handle_ref: HandleRef {
                string: handles::OPERATION_5.to_string(),
            },
        },
        requested_string_value: "whatever".to_string(),
    }
}

pub fn get_set_value() -> SetValue {
    SetValue {
        abstract_set: AbstractSet {
            extension_element: None,
            operation_handle_ref: HandleRef {
                string: handles::OPERATION_6.to_string(),
            },
        },
        requested_numeric_value: ProtoDecimal::from(123123),
    }
}

pub async fn test_with_synchronous_operation<SSA>(
    sco: &mut ScoController<SSA>,
    final_state: &InvocationState,
) -> anyhow::Result<()>
where
    SSA: SetServiceHandler + Send + Sync + Clone,
{
    {
        let activate = sco
            .invoke(AbstractSetOneOf::Activate(get_activate()))
            .await
            .expect("Could not invoke activate");
        let activate_result = activate.wait_for_final_report().await;
        assert_eq!(
            final_state,
            &activate_result
                .last()
                .expect("no last report")
                .report_part
                .invocation_info
                .invocation_state
        );

        // check for correctly set anonymous source in all report parts
        for x in &activate_result {
            let identifier = match &x.report_part.invocation_source {
                InstanceIdentifierOneOf::InstanceIdentifier(identifier) => identifier,
                _ => panic!("No invocation source"),
            };

            assert_eq!(
                Some(RootAttr {
                    any_u_r_i: ProtoUri {
                        uri: (*ROOT_SDC).to_string(),
                    }
                }),
                identifier.root_attr
            );
            assert_eq!(
                Some(ExtensionAttr {
                    string: ANONYMOUS_EXTENSION.to_string()
                }),
                identifier.extension_attr
            );

            // also check that source mds is set
            assert_eq!(
                Some(HandleRef {
                    string: handles::MDS_0.to_string()
                }),
                x.report_part.abstract_report_part.source_mds
            )
        }
    }
    {
        let set_alert_state = sco
            .invoke(AbstractSetOneOf::SetAlertState(get_set_alert_state()))
            .await
            .expect("Could not invoke set_alert_state");

        let set_alert_state_result = set_alert_state.wait_for_final_report().await;

        assert_eq!(
            final_state,
            &set_alert_state_result
                .last()
                .expect("no last report")
                .report_part
                .invocation_info
                .invocation_state
        );

        for x in &set_alert_state_result {
            // also check that source mds is set
            assert_eq!(
                Some(HandleRef {
                    string: handles::MDS_0.to_string()
                }),
                x.report_part.abstract_report_part.source_mds
            )
        }
    }
    {
        let set_component_state = sco
            .invoke(AbstractSetOneOf::SetComponentState(
                get_set_component_state(),
            ))
            .await
            .expect("Could not invoke set_component_state");

        let set_component_state_result = set_component_state.wait_for_final_report().await;

        assert_eq!(
            final_state,
            &set_component_state_result
                .last()
                .expect("no last report")
                .report_part
                .invocation_info
                .invocation_state
        );

        for x in &set_component_state_result {
            // also check that source mds is set
            assert_eq!(
                Some(HandleRef {
                    string: handles::MDS_0.to_string()
                }),
                x.report_part.abstract_report_part.source_mds
            )
        }
    }
    {
        let set_context_state = sco
            .invoke(AbstractSetOneOf::SetContextState(get_set_context_state()))
            .await
            .expect("Could not invoke set_context_state");

        let set_context_state_result = set_context_state.wait_for_final_report().await;

        assert_eq!(
            final_state,
            &set_context_state_result
                .last()
                .expect("no last report")
                .report_part
                .invocation_info
                .invocation_state
        );

        for x in &set_context_state_result {
            // also check that source mds is set
            assert_eq!(
                Some(HandleRef {
                    string: handles::MDS_0.to_string()
                }),
                x.report_part.abstract_report_part.source_mds
            )
        }
    }
    {
        let set_metric_state = sco
            .invoke(AbstractSetOneOf::SetMetricState(get_set_metric_state()))
            .await
            .expect("Could not invoke set_metric_state");

        let set_metric_state_result = set_metric_state.wait_for_final_report().await;

        assert_eq!(
            final_state,
            &set_metric_state_result
                .last()
                .expect("no last report")
                .report_part
                .invocation_info
                .invocation_state
        );

        for x in &set_metric_state_result {
            // also check that source mds is set
            assert_eq!(
                Some(HandleRef {
                    string: handles::MDS_0.to_string()
                }),
                x.report_part.abstract_report_part.source_mds
            )
        }
    }
    {
        let set_string = sco
            .invoke(AbstractSetOneOf::SetString(get_set_string()))
            .await
            .expect("Could not invoke set_string");

        let set_string_result = set_string.wait_for_final_report().await;

        assert_eq!(
            final_state,
            &set_string_result
                .last()
                .expect("no last report")
                .report_part
                .invocation_info
                .invocation_state
        );

        for x in &set_string_result {
            // also check that source mds is set
            assert_eq!(
                Some(HandleRef {
                    string: handles::MDS_0.to_string()
                }),
                x.report_part.abstract_report_part.source_mds
            )
        }
    }

    {
        let set_value = sco
            .invoke(AbstractSetOneOf::SetValue(get_set_value()))
            .await
            .expect("Could not invoke set_value");

        let set_value_result = set_value.wait_for_final_report().await;

        assert_eq!(
            final_state,
            &set_value_result
                .last()
                .expect("no last report")
                .report_part
                .invocation_info
                .invocation_state
        );

        for x in &set_value_result {
            // also check that source mds is set
            assert_eq!(
                Some(HandleRef {
                    string: handles::MDS_0.to_string()
                }),
                x.report_part.abstract_report_part.source_mds
            )
        }
    }

    Ok(())
}

pub async fn test_with_asynchronous_operation<SSA>(
    sco: &mut ScoController<SSA>,
    transitions: &[InvocationState],
) -> anyhow::Result<()>
where
    SSA: SetServiceHandler + Send + Sync + Clone,
{
    {
        let activate = sco
            .invoke(AbstractSetOneOf::Activate(get_activate()))
            .await
            .expect("Could not invoke activate");
        let activate_result = activate.wait_for_final_report().await;

        for (index, state) in transitions.iter().enumerate() {
            assert_eq!(
                state,
                &activate_result
                    .get(index)
                    .unwrap()
                    .report_part
                    .invocation_info
                    .invocation_state
            );
        }
    }
    {
        let set_alert_state = sco
            .invoke(AbstractSetOneOf::SetAlertState(get_set_alert_state()))
            .await
            .expect("Could not invoke set_alert_state");
        let set_alert_state_result = set_alert_state.wait_for_final_report().await;

        for (index, state) in transitions.iter().enumerate() {
            assert_eq!(
                state,
                &set_alert_state_result
                    .get(index)
                    .unwrap()
                    .report_part
                    .invocation_info
                    .invocation_state
            );
        }
    }
    {
        let set_component_state = sco
            .invoke(AbstractSetOneOf::SetComponentState(
                get_set_component_state(),
            ))
            .await
            .expect("Could not invoke set_component_state");
        let set_component_state_result = set_component_state.wait_for_final_report().await;

        for (index, state) in transitions.iter().enumerate() {
            assert_eq!(
                state,
                &set_component_state_result
                    .get(index)
                    .unwrap()
                    .report_part
                    .invocation_info
                    .invocation_state
            );
        }
    }
    {
        let set_context_state = sco
            .invoke(AbstractSetOneOf::SetContextState(get_set_context_state()))
            .await
            .expect("Could not invoke activate");
        let set_context_state_result = set_context_state.wait_for_final_report().await;

        for (index, state) in transitions.iter().enumerate() {
            assert_eq!(
                state,
                &set_context_state_result
                    .get(index)
                    .unwrap()
                    .report_part
                    .invocation_info
                    .invocation_state
            );
        }
    }
    {
        let set_metric_state = sco
            .invoke(AbstractSetOneOf::SetMetricState(get_set_metric_state()))
            .await
            .expect("Could not invoke activate");
        let set_metric_state_result = set_metric_state.wait_for_final_report().await;

        for (index, state) in transitions.iter().enumerate() {
            assert_eq!(
                state,
                &set_metric_state_result
                    .get(index)
                    .unwrap()
                    .report_part
                    .invocation_info
                    .invocation_state
            );
        }
    }
    {
        let set_string = sco
            .invoke(AbstractSetOneOf::SetString(get_set_string()))
            .await
            .expect("Could not invoke activate");
        let set_string_result = set_string.wait_for_final_report().await;

        for (index, state) in transitions.iter().enumerate() {
            assert_eq!(
                state,
                &set_string_result
                    .get(index)
                    .unwrap()
                    .report_part
                    .invocation_info
                    .invocation_state
            );
        }
    }
    {
        let set_value = sco
            .invoke(AbstractSetOneOf::SetValue(get_set_value()))
            .await
            .expect("Could not invoke activate");
        let set_value_result = set_value.wait_for_final_report().await;

        for (index, state) in transitions.iter().enumerate() {
            assert_eq!(
                state,
                &set_value_result
                    .get(index)
                    .unwrap()
                    .report_part
                    .invocation_info
                    .invocation_state
            );
        }
    }

    Ok(())
}

pub async fn test_tls_invocation_source<SSA>(
    sco: &mut ScoController<SSA>,
    final_state: &InvocationState,
    certificates: &GeneratedCerts,
    expected_root: &str,
) -> anyhow::Result<()>
where
    SSA: SetServiceHandler + Send + Sync + Clone,
{
    {
        let activate = sco
            .invoke(AbstractSetOneOf::Activate(get_activate()))
            .await
            .expect("Could not invoke activate");
        let activate_result = activate.wait_for_final_report().await;

        assert!(!activate_result.is_empty());

        for x in &activate_result {
            let identifier = match &x.report_part.invocation_source {
                InstanceIdentifierOneOf::InstanceIdentifier(identifier) => identifier,
                _ => panic!("No invocation source"),
            };

            assert_eq!(
                Some(RootAttr {
                    any_u_r_i: ProtoUri {
                        uri: expected_root.to_string()
                    }
                }),
                identifier.root_attr
            );
            assert_eq!(
                Some(ExtensionAttr {
                    string: String::from_utf8(certificates.consumer.public.clone())
                        .expect("This was a string, it should be again")
                }),
                identifier.extension_attr
            );
        }

        assert_eq!(
            final_state,
            &activate_result
                .last()
                .expect("no last report")
                .report_part
                .invocation_info
                .invocation_state
        );
    }

    Ok(())
}
