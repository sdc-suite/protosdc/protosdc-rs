use std::time::Duration;

use async_trait::async_trait;
use log::info;
use protosdc_biceps::biceps::{
    invocation_error_mod, Activate, SetAlertState, SetComponentState, SetContextState,
    SetMetricState, SetString, SetValue,
};
use tokio::task;

use biceps::common::mdib_entity::Entity;
use biceps::common::mdib_version::MdibVersion;
use biceps::provider::access::local_mdib_access::LocalMdibAccess;
use protosdc::provider::sco::{
    Context, InvocationCallError, InvocationResponse, OperationInvocationReceiver,
};
use protosdc::provider::sco_util::{InvocationStateTypes, ReportBuilder, ReportData};

#[derive(Clone)]
pub struct DirectOperationInvocationReceiver {
    pub response_state: InvocationStateTypes,
}

impl DirectOperationInvocationReceiver {
    async fn run_operation<MDIB>(
        &self,
        operation_handle: String,
        mut context: Context<MDIB>,
        caller: &str,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)>
    where
        MDIB: LocalMdibAccess + Send + Sync + Clone,
    {
        info!("Received {} for {}", caller, operation_handle);
        let mdib_version = { context.mdib_access.mdib_version().await };
        info!("Responding for mdib version {:?}", &mdib_version);
        let response_data = match &self.response_state {
            InvocationStateTypes::NotFailed(it) => ReportBuilder::create()
                .mdib_version(mdib_version)
                .success(it.clone())
                .build(),
            InvocationStateTypes::Failed(it) => ReportBuilder::create()
                .mdib_version(mdib_version)
                .failure(it.clone())
                .error(invocation_error_mod::EnumType::Oth)
                .build(),
        };
        Ok(context.create_response(response_data).await)
    }
}

#[async_trait]
impl<MDIB> OperationInvocationReceiver<MDIB> for DirectOperationInvocationReceiver
where
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
{
    async fn activate(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: Activate,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        // check types match
        let entity = context
            .mdib_access
            .entity(&operation_handle)
            .await
            .unwrap()
            .entity;
        assert!(matches!(entity, Entity::ActivateOperation(..)));
        self.run_operation(operation_handle, context, "activate")
            .await
    }

    async fn set_alert_state(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: SetAlertState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let entity = context
            .mdib_access
            .entity(&operation_handle)
            .await
            .unwrap()
            .entity;
        assert!(matches!(entity, Entity::SetAlertStateOperation(..)));
        self.run_operation(operation_handle, context, "set_alert_state")
            .await
    }

    async fn set_component_state(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: SetComponentState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let entity = context
            .mdib_access
            .entity(&operation_handle)
            .await
            .unwrap()
            .entity;
        assert!(matches!(entity, Entity::SetComponentStateOperation(..)));
        self.run_operation(operation_handle, context, "set_component_state")
            .await
    }

    async fn set_context_state(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: SetContextState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let entity = context
            .mdib_access
            .entity(&operation_handle)
            .await
            .unwrap()
            .entity;
        assert!(matches!(entity, Entity::SetContextStateOperation(..)));
        self.run_operation(operation_handle, context, "set_context_state")
            .await
    }

    async fn set_metric_state(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: SetMetricState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let entity = context
            .mdib_access
            .entity(&operation_handle)
            .await
            .unwrap()
            .entity;
        assert!(matches!(entity, Entity::SetMetricStateOperation(..)));
        self.run_operation(operation_handle, context, "set_metric_state")
            .await
    }

    async fn set_string(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: SetString,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let entity = context
            .mdib_access
            .entity(&operation_handle)
            .await
            .unwrap()
            .entity;
        assert!(matches!(entity, Entity::SetStringOperation(..)));
        self.run_operation(operation_handle, context, "set_string")
            .await
    }

    async fn set_value(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: SetValue,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let entity = context
            .mdib_access
            .entity(&operation_handle)
            .await
            .unwrap()
            .entity;
        assert!(matches!(entity, Entity::SetValueOperation(..)));
        self.run_operation(operation_handle, context, "set_value")
            .await
    }
}

#[derive(Clone)]
pub struct AsyncOperationInvocationReceiver {
    pub invocation_states: Vec<InvocationStateTypes>,
    pub time_between_steps: Duration,
}

fn create_report_data(
    invocation_state: InvocationStateTypes,
    mdib_version: MdibVersion,
) -> ReportData {
    match invocation_state {
        InvocationStateTypes::NotFailed(it) => ReportBuilder::create()
            .success(it)
            .mdib_version(mdib_version)
            .build(),
        InvocationStateTypes::Failed(it) => ReportBuilder::create()
            .failure(it)
            .mdib_version(mdib_version)
            .error(invocation_error_mod::EnumType::Oth)
            .build(),
    }
}

impl AsyncOperationInvocationReceiver {
    async fn run_operation<MDIB>(
        &self,
        operation_handle: String,
        mut context: Context<MDIB>,
        caller: &str,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)>
    where
        MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
    {
        info!(
            "AsyncOperationInvocationReceiver: Received {} for {}",
            caller, operation_handle
        );
        let mdib_version = { context.mdib_access.mdib_version().await };
        info!(
            "AsyncOperationInvocationReceiver: Responding for mdib version {:?} with steps: {:?}",
            &mdib_version, self.invocation_states
        );

        // don't allow "async" task with only one step
        assert!(self.invocation_states.len() > 1);

        let first = self.invocation_states.first().unwrap().clone();

        let report_data = create_report_data(first, mdib_version);

        let response = Ok(context.create_response(report_data).await);

        // spawn a task to process requests
        let task_invocation_states = self.invocation_states.clone();
        let task_time_between_steps = self.time_between_steps;
        task::spawn(async move {
            info!("run_operation async task started");
            for invocation_state in &task_invocation_states[1..] {
                tokio::time::sleep(task_time_between_steps).await;
                let mdib_version = { context.mdib_access.mdib_version().await };
                context
                    .send_report(create_report_data(invocation_state.clone(), mdib_version))
                    .await;
            }
            info!("run_operation async task finished");
        });

        response
    }
}

#[async_trait]
impl<MDIB> OperationInvocationReceiver<MDIB> for AsyncOperationInvocationReceiver
where
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
{
    async fn activate(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: Activate,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        self.run_operation(operation_handle, context, "activate")
            .await
    }

    async fn set_alert_state(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: SetAlertState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        self.run_operation(operation_handle, context, "set_alert_state")
            .await
    }

    async fn set_component_state(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: SetComponentState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        self.run_operation(operation_handle, context, "set_component_state")
            .await
    }

    async fn set_context_state(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: SetContextState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        self.run_operation(operation_handle, context, "set_context_state")
            .await
    }

    async fn set_metric_state(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: SetMetricState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        self.run_operation(operation_handle, context, "set_metric_state")
            .await
    }

    async fn set_string(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: SetString,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        self.run_operation(operation_handle, context, "set_string")
            .await
    }

    async fn set_value(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        _request: SetValue,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        self.run_operation(operation_handle, context, "set_value")
            .await
    }
}
