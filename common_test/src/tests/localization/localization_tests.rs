use protosdc_biceps::biceps::LocalizedText;

pub async fn test_localization_service(
    supported_languages: &[String],
    texts: &[LocalizedText],
) -> anyhow::Result<()> {
    assert_eq!(2, supported_languages.len());
    assert!(supported_languages.contains(&"en".to_string()));
    assert!(supported_languages.contains(&"fr".to_string()));

    assert_eq!(9, texts.len());

    Ok(())
}
