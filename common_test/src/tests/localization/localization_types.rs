use protosdc::provider::localization_storage::{InputText, LocalizationStorageImpl, TextWidth};

pub fn create_text_lang_only(lang: impl Into<String>) -> InputText {
    create_text_lang_ref(lang, uuid::Uuid::new_v4().to_string())
}

pub fn create_text_lang_ref(lang: impl Into<String>, text_ref: impl Into<String>) -> InputText {
    create_text(lang, text_ref, "abc", TextWidth::XS)
}

pub fn create_text(
    lang: impl Into<String>,
    text_ref: impl Into<String>,
    text: impl Into<String>,
    width: TextWidth,
) -> InputText {
    InputText {
        text_ref: text_ref.into(),
        lang: lang.into(),
        text: text.into(),
        width,
        lines: 0,
    }
}

pub fn localization_data() -> LocalizationStorageImpl {
    let texts = [
        ("en", "ref1"),
        ("en", "ref2"),
        ("en", "ref3"),
        ("en", "ref4"),
        ("en", "ref4"),
        ("fr", "ref1"),
        ("fr", "ref2"),
        ("fr", "ref3"),
        ("fr", "ref4"),
    ]
    .into_iter()
    .map(|(lang, text_ref)| create_text_lang_ref(lang, text_ref))
    .collect();

    LocalizationStorageImpl::new(texts)
}
