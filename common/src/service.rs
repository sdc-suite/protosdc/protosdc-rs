use async_trait::async_trait;
use log::error;
use std::fmt::Debug;
use thiserror::Error;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ServiceState {
    NEW,
    STARTING,
    RUNNING,
    STOPPING,
    STOPPED,
    FAIL,
}

#[async_trait]
pub trait Service {
    /// Starts a `ServiceState::NEW` service, going through `ServiceState::STARTING` and
    /// finishing in `ServiceState::RUNNING`/
    async fn start(&mut self) -> Result<(), ServiceError>;

    /// Starts a `ServiceState::RUNNING` service, going through `ServiceState::STOPPING` and
    /// finishing in `ServiceState::STOPPED`/
    async fn stop(&mut self) -> Result<(), ServiceError>;

    /// Returns true if the service is currently `ServiceState::RUNNING`
    fn is_running(&self) -> bool;

    /// Returns the current `ServiceState`
    fn state(&self) -> &ServiceState;
}

#[derive(Debug, Error)]
pub enum ServiceError {
    #[error("Service could not be started: {source:?}")]
    StartFailed { source: anyhow::Error },
    #[error("Service could not be stopped: {source:?}")]
    StopFailed { source: anyhow::Error },
    #[error("Service is already running")]
    AlreadyRunning,
    #[error("Service was already running and cannot be run again")]
    AlreadyStopped,
    #[error("Service is not running and could not be stopped")]
    ServiceNotRunning,
    #[error(transparent)]
    Other(#[from] anyhow::Error),
}

#[derive(Debug)]
pub struct ServiceDelegate {
    state: ServiceState,
}

impl ServiceDelegate {
    pub async fn start(&mut self) -> Result<(), ServiceError> {
        if self.state == ServiceState::STOPPED || self.state == ServiceState::FAIL {
            return Err(ServiceError::AlreadyStopped);
        }
        if self.state != ServiceState::NEW {
            return Err(ServiceError::AlreadyRunning);
        }

        self.state = ServiceState::STARTING;
        Ok(())
    }

    pub async fn started(&mut self) -> Result<(), ServiceError> {
        self.state = ServiceState::RUNNING;
        Ok(())
    }

    pub async fn stop(&mut self) -> Result<(), ServiceError> {
        if self.state != ServiceState::RUNNING {
            return Err(ServiceError::ServiceNotRunning);
        }
        self.state = ServiceState::STOPPING;
        Ok(())
    }

    pub async fn stopped(&mut self) -> Result<(), ServiceError> {
        self.state = ServiceState::STOPPED;
        Ok(())
    }

    pub async fn failed(&mut self) -> Result<(), ServiceError> {
        self.state = ServiceState::FAIL;
        Ok(())
    }

    pub fn state(&self) -> &ServiceState {
        &self.state
    }

    pub fn is_running(&self) -> bool {
        self.state == ServiceState::RUNNING
    }
}

impl Default for ServiceDelegate {
    fn default() -> Self {
        Self {
            state: ServiceState::NEW,
        }
    }
}
