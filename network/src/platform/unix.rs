use crate::common::{compare_address, ip_address_to_socket_address};
use crate::udp_binding::UdpBindingError;
use log::debug;
use socket2::Socket;
use std::net::{IpAddr, SocketAddr};

#[cfg(not(any(target_os = "macos", target_os = "freebsd")))]
pub const MULTICAST_BITS: u32 = interfaces::InterfaceFlags::IFF_MULTICAST.bits();

// probably the other bsd's too
#[cfg(any(target_os = "macos", target_os = "freebsd"))]
pub const MULTICAST_BITS: u32 = 0x8000;

fn is_multicast(flags: u32) -> bool {
    flags & MULTICAST_BITS == MULTICAST_BITS
}

/// Unixes bind to the multicast address.
pub fn bind_multicast(socket: &Socket, addr: &SocketAddr) -> Result<(), UdpBindingError> {
    debug!("(Unix) binding {:?} to multicast {:?}", socket, addr);
    socket
        .bind(&socket2::SockAddr::from(*addr))
        .map_err(|source| UdpBindingError::MulticastBindFailed {
            source,
            address: addr.to_string(),
        })?;
    Ok(())
}

pub fn get_socket_addr_for_ip(adapter_address: IpAddr) -> Option<SocketAddr> {
    let interfaces = interfaces::Interface::get_all().expect("Could not retrieve interfaces");
    let adapter_socket_addr: SocketAddr = ip_address_to_socket_address(adapter_address, None);

    // retrieve the SocketAddr from the adapter since we need the correct scope_id
    let mut selected_adapter_socket_addr_opt = None;
    for interface in interfaces {
        debug!("Checking interface");
        debug!("  name: {:?}", interface.name);
        debug!("  addresses: {:?}", interface.addresses);
        debug!(
            "  flags: ({}) {:?}",
            interface.flags.bits(),
            interface.flags
        );
        debug!("  multicast: {}", is_multicast(interface.flags.bits()));

        for address in &interface.addresses {
            if compare_address(address.addr, &adapter_socket_addr) {
                debug!(
                    "It's a match! Interface {:?} chosen, SocketAddr {:?}",
                    &interface.name, address.addr
                );
                selected_adapter_socket_addr_opt = address.addr;
                break;
            }
        }
    }

    selected_adapter_socket_addr_opt
}

#[cfg(any(feature = "test-utilities", test))]
pub mod test_util {
    use interfaces::Kind;
    use std::net::{IpAddr, SocketAddr};

    use crate::platform::unix::is_multicast;
    use crate::udp_binding::PROTOSDC_MULTICAST_PORT;
    use log::debug;

    use crate::udp_binding::test_util::test_adapter_address_working_multicast;

    /// Returns the address of an adapter which can be used to send to the provided multicast
    /// address without OS errors.
    ///
    /// Note: Since this is test code, instead of returning an error, this will just panic if no
    ///       adapters are available.
    ///
    /// # Arguments
    ///
    /// * `multicast_address`: to get appropriate adapter for
    ///
    /// returns: IpAddr address of the adapter which can be used for multicast to the address.
    ///
    /// # Examples
    ///
    /// ```
    ///
    /// ```
    pub async fn determine_adapter_address_for_multicast(multicast_address: IpAddr) -> IpAddr {
        let mut ifs = interfaces::Interface::get_all().expect("could not get interfaces");

        // filter out interfaces without connection
        ifs.retain(|interface| {
            interface.is_running()
                    && interface.is_up()
                    && !interface.is_loopback()
                    && is_multicast(interface.flags.bits())
                    // only adapters with v4 and v6 are interesting, others are odd
                    && interface.addresses.iter().any(|it| it.kind == Kind::Ipv4)
                    && interface.addresses.iter().any(|it| it.kind == Kind::Ipv6)
        });

        for interfaces in ifs {
            for address in &interfaces.addresses {
                match (address.addr, multicast_address) {
                    (Some(SocketAddr::V6(addr)), IpAddr::V6(_)) => {
                        if test_adapter_address_working_multicast(
                            IpAddr::V6(*addr.ip()),
                            multicast_address,
                            addr.scope_id(),
                            PROTOSDC_MULTICAST_PORT,
                        )
                        .await
                        {
                            return IpAddr::V6(*addr.ip());
                        }
                    }
                    (Some(SocketAddr::V4(addr)), IpAddr::V4(_)) => {
                        if test_adapter_address_working_multicast(
                            IpAddr::V4(*addr.ip()),
                            multicast_address,
                            0,
                            PROTOSDC_MULTICAST_PORT,
                        )
                        .await
                        {
                            return IpAddr::V4(*addr.ip());
                        }
                    }
                    // nothing to do there
                    (_, _) => {
                        debug!("Discarding address {:?}", address)
                    }
                }
            }
        }

        panic!(
            "Could not find matching network interface for multicast address {:?}",
            &multicast_address
        );
    }
}
