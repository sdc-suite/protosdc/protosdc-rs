use std::net::{IpAddr, SocketAddr, SocketAddrV4, SocketAddrV6};

pub(crate) fn ip_address_to_socket_address(ip_addr: IpAddr, scope: Option<u32>) -> SocketAddr {
    match ip_addr {
        IpAddr::V4(addr) => SocketAddr::V4(SocketAddrV4::new(addr, 0)),
        IpAddr::V6(addr) => SocketAddr::V6(SocketAddrV6::new(addr, 0, 0, scope.unwrap_or(0))),
    }
}

pub(crate) fn compare_address(a: Option<SocketAddr>, b: &SocketAddr) -> bool {
    match (a, b) {
        (Some(SocketAddr::V4(a_addr)), SocketAddr::V4(b_addr)) => &a_addr == b_addr,
        (Some(SocketAddr::V6(a_addr)), SocketAddr::V6(b_addr)) => {
            // compare without scope here, we don't care
            a_addr.ip() == b_addr.ip()
        }
        _ => false,
    }
}
