use std::fmt::{Display, Formatter};
use std::net::{IpAddr, Ipv4Addr};
use std::str::FromStr;
use std::sync::{Arc, Mutex};

use async_trait::async_trait;
use log::info;
use tokio::sync::broadcast::{
    channel as broadcast_channel, Receiver as BroadcastReceiver, Sender as BroadcastSender,
};
use tokio::sync::mpsc::Sender;
use tokio::sync::mpsc::{channel, Receiver};
use tokio::task;
use tokio_util::sync::{CancellationToken, DropGuard};

use crate::udp_binding::{
    InboundUdpMessage, OutboundUdpMessage, UdpBinding, UdpBindingError, UdpHeader, UdpMessage,
};

#[derive(Debug)]
struct MockUdpBindingInner {
    _drop_guard: DropGuard,
    _receiver_task: task::JoinHandle<()>,
    _message_channel_task: task::JoinHandle<()>,
}

#[derive(Clone, Debug)]
pub struct MockUdpBinding {
    sender: BroadcastSender<InboundUdpMessage>,
    pub mock_sender: BroadcastSender<InboundUdpMessage>,
    message_channel_sender: Sender<UdpMessage>,

    _inner: Arc<Mutex<MockUdpBindingInner>>,
}

impl Default for MockUdpBinding {
    fn default() -> Self {
        let (mock_sender, _mock_receiver) = broadcast_channel(1);
        Self::new_with_channel(mock_sender)
    }
}

async fn process_message(
    mut message_channel_receiver: Receiver<UdpMessage>,
    sender: BroadcastSender<InboundUdpMessage>,
    cancellation_token: CancellationToken,
) {
    loop {
        tokio::select! {
            _ = cancellation_token.cancelled() => {
                info!("MockUdpBinding message channel task cancelled");
                break
            }
            msg = message_channel_receiver.recv() => {
                match msg {
                    // don't care if this fails, we're a mock
                    #[allow(unused_must_use)]
                    Some(data) => {
                        match data {
                            // don't care if this fails, we're a mock
                            #[allow(unused_must_use)]
                            UdpMessage::OutboundMulticast { message, response } => {
                                sender.send(to_inbound(message));
                                match response {
                                    None => {}
                                    Some(sender) => {
                                        info!("Sending mock multicast message");
                                        sender.send(Ok(()));
                                    }
                                };
                            }
                            // don't care if this fails, we're a mock
                            #[allow(unused_must_use)]
                            UdpMessage::OutboundUnicast { message, response } => {
                                sender.send(to_inbound(message));
                                match response {
                                    None => {}
                                    // don't care if this fails, we're a mock
                                    #[allow(unused_must_use)]
                                    Some(sender) => {
                                        info!("Sending mock unicast message");
                                        sender.send(Ok(()));
                                    }
                                };
                            }
                        }
                    }
                    None => {
                        info!("MockUdpBinding message channel task error");
                        break
                    }
                }
            }
        }
    }
}

impl MockUdpBinding {
    pub fn new_with_channel(mock_sender: BroadcastSender<InboundUdpMessage>) -> Self {
        let (broadcast_sender, _) = broadcast_channel(10);
        let (message_channel_sender, message_channel_receiver) = channel(10);
        let cancellation_token = CancellationToken::new();

        let mut receiver = mock_sender.subscribe();
        let sender_clone = broadcast_sender.clone();
        let task_token = cancellation_token.clone();
        let receiver_task = tokio::spawn(async move {
            loop {
                tokio::select! {
                    _ = task_token.cancelled() => {
                        info!("MockUdpBinding receiver task cancelled");
                        break
                    }
                    msg = receiver.recv() => {
                        match msg {
                            // don't care if this fails, we're a mock
                            #[allow(unused_must_use)]
                            Ok(data) => {
                                info!("Forwarding inbound message");
                                sender_clone.send(data);
                            }
                            Err(err) => {
                                info!("MockUdpBinding receiver task error: {}", err);
                                break
                            }
                        }
                    }
                }
            }
        });

        let cancellation_token = CancellationToken::new();
        let task_token = cancellation_token.clone();
        let sender = mock_sender.clone();
        let message_channel_task = tokio::spawn(async move {
            process_message(message_channel_receiver, sender, task_token).await;
        });

        MockUdpBinding {
            sender: broadcast_sender,
            mock_sender,
            message_channel_sender,
            _inner: Arc::new(Mutex::new(MockUdpBindingInner {
                _drop_guard: cancellation_token.drop_guard(),
                _receiver_task: receiver_task,
                _message_channel_task: message_channel_task,
            })),
        }
    }
}

fn to_inbound(outbound: OutboundUdpMessage) -> InboundUdpMessage {
    InboundUdpMessage {
        data: outbound.data,
        udp_header: UdpHeader {
            source_port: 0,
            destination_port: outbound.address.port(),
            source_address: IpAddr::V4(Ipv4Addr::from_str("127.0.0.1").expect("")),
            destination_address: outbound.address.ip(),
        },
    }
}

#[async_trait]
impl UdpBinding for MockUdpBinding {
    fn subscribe(&self) -> BroadcastReceiver<InboundUdpMessage> {
        self.sender.subscribe()
    }

    fn get_message_channel_sender(&self) -> Sender<UdpMessage> {
        self.message_channel_sender.clone()
    }

    // don't care if this fails, we're a mock
    #[allow(unused_must_use)]
    async fn send_message_multicast(
        &mut self,
        udp_message: OutboundUdpMessage,
    ) -> Result<(), UdpBindingError> {
        self.mock_sender.send(to_inbound(udp_message));
        Ok(())
    }

    // don't care if this fails, we're a mock
    #[allow(unused_must_use)]
    async fn send_message_unicast(
        &mut self,
        udp_message: OutboundUdpMessage,
    ) -> Result<(), UdpBindingError> {
        self.mock_sender.send(to_inbound(udp_message));
        Ok(())
    }
}

impl Display for MockUdpBinding {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "MockUdpBinding")
    }
}
