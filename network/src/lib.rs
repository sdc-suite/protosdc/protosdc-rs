#![forbid(unsafe_code)]
#![cfg_attr(not(debug_assertions), deny(warnings))] // Forbid warnings in release builds
#![warn(clippy::all, rust_2018_idioms)]

pub(crate) mod common;
pub mod platform;
pub mod udp_binding;

#[cfg(any(feature = "test-utilities", test))]
pub mod mock_udp_binding;
